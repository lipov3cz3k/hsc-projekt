@echo off
SETLOCAL ENABLEDELAYEDEXPANSION
:: Main project testing script
::
:: Author: Lukas Kekely <ikekely@fit.vutbr.cz>
:: Date: 1.11.2014

set TARGET=projekt.zip
set LOGIN=xlipov02



set WORK_DIR=work
set LOG_DIR=log
set EXE_DIR=bin
set KIT_DIR=fitkit
set WIS_DIR=%WORK_DIR%\wis
set SYNTH_DIR=%WORK_DIR%\synth
set SIM_DIR=%WORK_DIR%\sim
set RUNKIT_DIR=%WORK_DIR%\runkit
set EVAL_DIR=%WORK_DIR%\eval
set CATAPULT_BIN="C:\Program Files\Calypto Design Systems\Catapult Synthesis 8.2b-244325 Production Release\Mgc_home\bin\catapult.exe"



if [%1] == [] (
    call scripts\unpack.bat
    call scripts\synth.bat
    call scripts\simulate.bat
    call scripts\runkit.bat
    call scripts\eval.bat
)

if [%1] == [unpack] (
    call scripts\unpack.bat
)

if [%1] == [synth] (
    if exist work\wis (
        call scripts\synth.bat
    ) else (
        echo Out-of-order step execution!
    )
)

if [%1] == [sim] (
    if exist work\synth (
        call scripts\simulate.bat
    ) else (
        echo Out-of-order step execution!
    )
)

if [%1] == [runkit] (
    if exist work\sim (
        call scripts\runkit.bat
    ) else (
        echo Out-of-order step execution!
    )
)

if [%1] == [eval] (
    if exist work\runkit (
        call scripts\eval.bat
    ) else (
        echo Out-of-order step execution!
    )
)

if [%1] == [clean] (
    rd /s /q work >NUL 2>NUL
    rd /s /q log >NUL 2>NUL
)
