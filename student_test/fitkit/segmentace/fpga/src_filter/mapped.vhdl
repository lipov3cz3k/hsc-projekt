
-- 
-- Definition of  filter
-- 
--      11/04/15 11:34:30
--      
--      Precision RTL Synthesis, 2012a.10
-- 

library IEEE;
use IEEE.STD_LOGIC_1164.all;
-- Library use clause for technology cells
library unisim ;
use unisim.vcomponents.all;

entity filter is 
   port (
      clk : IN std_logic ;
      rst : IN std_logic ;
      in_data_rsc_z : IN std_logic_vector (2 DOWNTO 0) ;
      in_data_rsc_lz : OUT std_logic ;
      in_data_vld_rsc_z : IN std_logic ;
      out_data_rsc_z : OUT std_logic_vector (2 DOWNTO 0) ;
      out_data_rsc_lz : OUT std_logic ;
      mcu_data_rsc_data_in : OUT std_logic_vector (31 DOWNTO 0) ;
      mcu_data_rsc_addr : OUT std_logic_vector (8 DOWNTO 0) ;
      mcu_data_rsc_re : OUT std_logic ;
      mcu_data_rsc_we : OUT std_logic ;
      mcu_data_rsc_data_out : IN std_logic_vector (31 DOWNTO 0)) ;
end filter ;

architecture v8 of filter is 
   signal filter_core_inst_asn_itm: std_logic ;
   
   signal filter_core_inst_filter_core_core_fsm_inst_state_var: 
   std_logic_vector (1 DOWNTO 0) ;
   
   signal mcu_data_rsc_addr_0_EXMPLR50, mcu_data_rsc_addr_1_EXMPLR51, 
      nx60225z1, mcu_data_rsc_re_EXMPLR53, nx26346z1, nx21354z1, nx53520z1, 
      nx53520z2, nx53520z3, nx53520z4, nx53520z5, nx53520z6, nx53520z10, 
      nx53520z11, nx53520z8, nx53520z9, nx53520z7: std_logic ;

begin
   mcu_data_rsc_data_in(31) <= mcu_data_rsc_addr_1_EXMPLR51 ;
   mcu_data_rsc_data_in(30) <= mcu_data_rsc_addr_1_EXMPLR51 ;
   mcu_data_rsc_data_in(29) <= mcu_data_rsc_addr_1_EXMPLR51 ;
   mcu_data_rsc_data_in(28) <= mcu_data_rsc_addr_1_EXMPLR51 ;
   mcu_data_rsc_data_in(27) <= mcu_data_rsc_addr_1_EXMPLR51 ;
   mcu_data_rsc_data_in(26) <= mcu_data_rsc_addr_1_EXMPLR51 ;
   mcu_data_rsc_data_in(25) <= mcu_data_rsc_addr_1_EXMPLR51 ;
   mcu_data_rsc_data_in(24) <= mcu_data_rsc_addr_1_EXMPLR51 ;
   mcu_data_rsc_data_in(23) <= mcu_data_rsc_addr_1_EXMPLR51 ;
   mcu_data_rsc_data_in(22) <= mcu_data_rsc_addr_1_EXMPLR51 ;
   mcu_data_rsc_data_in(21) <= mcu_data_rsc_addr_1_EXMPLR51 ;
   mcu_data_rsc_data_in(20) <= mcu_data_rsc_addr_1_EXMPLR51 ;
   mcu_data_rsc_data_in(19) <= mcu_data_rsc_addr_1_EXMPLR51 ;
   mcu_data_rsc_data_in(18) <= mcu_data_rsc_addr_1_EXMPLR51 ;
   mcu_data_rsc_data_in(17) <= mcu_data_rsc_addr_1_EXMPLR51 ;
   mcu_data_rsc_data_in(16) <= mcu_data_rsc_addr_1_EXMPLR51 ;
   mcu_data_rsc_data_in(15) <= mcu_data_rsc_addr_1_EXMPLR51 ;
   mcu_data_rsc_data_in(14) <= mcu_data_rsc_addr_1_EXMPLR51 ;
   mcu_data_rsc_data_in(13) <= mcu_data_rsc_addr_1_EXMPLR51 ;
   mcu_data_rsc_data_in(12) <= mcu_data_rsc_addr_1_EXMPLR51 ;
   mcu_data_rsc_data_in(11) <= mcu_data_rsc_addr_1_EXMPLR51 ;
   mcu_data_rsc_data_in(10) <= mcu_data_rsc_addr_1_EXMPLR51 ;
   mcu_data_rsc_data_in(9) <= mcu_data_rsc_addr_1_EXMPLR51 ;
   mcu_data_rsc_data_in(8) <= mcu_data_rsc_addr_1_EXMPLR51 ;
   mcu_data_rsc_data_in(7) <= mcu_data_rsc_addr_1_EXMPLR51 ;
   mcu_data_rsc_data_in(6) <= mcu_data_rsc_addr_1_EXMPLR51 ;
   mcu_data_rsc_data_in(5) <= mcu_data_rsc_addr_1_EXMPLR51 ;
   mcu_data_rsc_data_in(4) <= mcu_data_rsc_addr_1_EXMPLR51 ;
   mcu_data_rsc_data_in(3) <= mcu_data_rsc_addr_1_EXMPLR51 ;
   mcu_data_rsc_data_in(2) <= mcu_data_rsc_addr_1_EXMPLR51 ;
   mcu_data_rsc_data_in(1) <= mcu_data_rsc_addr_0_EXMPLR50 ;
   mcu_data_rsc_data_in(0) <= mcu_data_rsc_addr_1_EXMPLR51 ;
   mcu_data_rsc_addr(8) <= mcu_data_rsc_addr_1_EXMPLR51 ;
   mcu_data_rsc_addr(7) <= mcu_data_rsc_addr_1_EXMPLR51 ;
   mcu_data_rsc_addr(6) <= mcu_data_rsc_addr_1_EXMPLR51 ;
   mcu_data_rsc_addr(5) <= mcu_data_rsc_addr_1_EXMPLR51 ;
   mcu_data_rsc_addr(4) <= mcu_data_rsc_addr_1_EXMPLR51 ;
   mcu_data_rsc_addr(3) <= mcu_data_rsc_addr_0_EXMPLR50 ;
   mcu_data_rsc_addr(2) <= mcu_data_rsc_addr_1_EXMPLR51 ;
   mcu_data_rsc_addr(1) <= mcu_data_rsc_addr_1_EXMPLR51 ;
   mcu_data_rsc_addr(0) <= mcu_data_rsc_addr_0_EXMPLR50 ;
   mcu_data_rsc_re <= mcu_data_rsc_re_EXMPLR53 ;
   filter_core_inst_reg_out_data_rsci_d_2 : FDRE port map ( Q=>
      out_data_rsc_z(2), C=>clk, CE=>nx26346z1, D=>in_data_rsc_z(2), R=>rst
   );
   filter_core_inst_reg_out_data_rsci_d_1 : FDRE port map ( Q=>
      out_data_rsc_z(1), C=>clk, CE=>nx26346z1, D=>in_data_rsc_z(1), R=>rst
   );
   filter_core_inst_reg_out_data_rsci_d_0 : FDRE port map ( Q=>
      out_data_rsc_z(0), C=>clk, CE=>nx26346z1, D=>in_data_rsc_z(0), R=>rst
   );
   filter_core_inst_reg_asn_itm : FDR port map ( Q=>filter_core_inst_asn_itm, 
      C=>clk, D=>in_data_vld_rsc_z, R=>rst);
   filter_core_inst_reg_in_data_rsci_ld : FDR port map ( Q=>in_data_rsc_lz, 
      C=>clk, D=>nx21354z1, R=>rst);
   filter_core_inst_reg_out_data_rsci_ld : FDR port map ( Q=>out_data_rsc_lz, 
      C=>clk, D=>nx26346z1, R=>rst);
   filter_core_inst_filter_core_core_fsm_inst_reg_state_var_1 : FDR
       port map ( Q=>filter_core_inst_filter_core_core_fsm_inst_state_var(1), 
      C=>clk, D=>nx60225z1, R=>rst);
   filter_core_inst_filter_core_core_fsm_inst_reg_state_var_0 : FDR
       port map ( Q=>filter_core_inst_filter_core_core_fsm_inst_state_var(0), 
      C=>clk, D=>mcu_data_rsc_re_EXMPLR53, R=>rst);
   mcu_data_rsc_data_in_1_EXMPLR57 : VCC port map ( P=>
      mcu_data_rsc_addr_0_EXMPLR50);
   mcu_data_rsc_data_in_2_EXMPLR58 : GND port map ( G=>
      mcu_data_rsc_addr_1_EXMPLR51);
   ix53520z1312 : LUT4
      generic map (INIT => X"FFFE") 
       port map ( O=>mcu_data_rsc_we, I0=>nx53520z1, I1=>nx53520z2, I2=>
      nx53520z7, I3=>nx53520z9);
   ix60225z1328 : LUT2
      generic map (INIT => X"E") 
       port map ( O=>nx60225z1, I0=>
      filter_core_inst_filter_core_core_fsm_inst_state_var(1), I1=>
      filter_core_inst_filter_core_core_fsm_inst_state_var(0));
   ix59228z1315 : LUT1
      generic map (INIT => X"1") 
       port map ( O=>mcu_data_rsc_re_EXMPLR53, I0=>
      filter_core_inst_filter_core_core_fsm_inst_state_var(0));
   ix26346z1442 : LUT3
      generic map (INIT => X"80") 
       port map ( O=>nx26346z1, I0=>
      filter_core_inst_filter_core_core_fsm_inst_state_var(0), I1=>
      filter_core_inst_asn_itm, I2=>
      filter_core_inst_filter_core_core_fsm_inst_state_var(1));
   ix21354z1322 : LUT3
      generic map (INIT => X"08") 
       port map ( O=>nx21354z1, I0=>in_data_vld_rsc_z, I1=>
      filter_core_inst_filter_core_core_fsm_inst_state_var(1), I2=>
      filter_core_inst_filter_core_core_fsm_inst_state_var(0));
   ix53520z1313 : LUT4
      generic map (INIT => X"FFFE") 
       port map ( O=>nx53520z1, I0=>mcu_data_rsc_data_out(30), I1=>
      mcu_data_rsc_data_out(29), I2=>mcu_data_rsc_data_out(28), I3=>
      mcu_data_rsc_data_out(27));
   ix53520z1314 : LUT4
      generic map (INIT => X"FFFE") 
       port map ( O=>nx53520z2, I0=>nx53520z3, I1=>nx53520z4, I2=>nx53520z5, 
      I3=>nx53520z6);
   ix53520z1315 : LUT4
      generic map (INIT => X"FFFE") 
       port map ( O=>nx53520z3, I0=>mcu_data_rsc_data_out(26), I1=>
      mcu_data_rsc_data_out(25), I2=>mcu_data_rsc_data_out(24), I3=>
      mcu_data_rsc_data_out(23));
   ix53520z1316 : LUT4
      generic map (INIT => X"FFFE") 
       port map ( O=>nx53520z4, I0=>mcu_data_rsc_data_out(22), I1=>
      mcu_data_rsc_data_out(21), I2=>mcu_data_rsc_data_out(20), I3=>
      mcu_data_rsc_data_out(19));
   ix53520z1317 : LUT4
      generic map (INIT => X"FFFE") 
       port map ( O=>nx53520z5, I0=>mcu_data_rsc_data_out(18), I1=>
      mcu_data_rsc_data_out(17), I2=>mcu_data_rsc_data_out(16), I3=>
      mcu_data_rsc_data_out(15));
   ix53520z1318 : LUT4
      generic map (INIT => X"FFFE") 
       port map ( O=>nx53520z6, I0=>mcu_data_rsc_data_out(14), I1=>
      mcu_data_rsc_data_out(13), I2=>mcu_data_rsc_data_out(12), I3=>
      mcu_data_rsc_data_out(11));
   ix53520z1322 : LUT4
      generic map (INIT => X"FFFE") 
       port map ( O=>nx53520z10, I0=>mcu_data_rsc_data_out(6), I1=>
      mcu_data_rsc_data_out(5), I2=>mcu_data_rsc_data_out(4), I3=>
      mcu_data_rsc_data_out(3));
   ix53520z1572 : LUT3
      generic map (INIT => X"F7") 
       port map ( O=>nx53520z11, I0=>mcu_data_rsc_data_out(0), I1=>
      filter_core_inst_filter_core_core_fsm_inst_state_var(1), I2=>
      filter_core_inst_filter_core_core_fsm_inst_state_var(0));
   ix53520z1336 : LUT2
      generic map (INIT => X"E") 
       port map ( O=>nx53520z8, I0=>mcu_data_rsc_data_out(10), I1=>
      mcu_data_rsc_data_out(9));
   ix53520z1321 : LUT4
      generic map (INIT => X"FFFE") 
       port map ( O=>nx53520z9, I0=>mcu_data_rsc_data_out(2), I1=>
      mcu_data_rsc_data_out(1), I2=>nx53520z10, I3=>nx53520z11);
   ix53520z1319 : LUT4
      generic map (INIT => X"FFFE") 
       port map ( O=>nx53520z7, I0=>mcu_data_rsc_data_out(31), I1=>
      mcu_data_rsc_data_out(8), I2=>mcu_data_rsc_data_out(7), I3=>nx53520z8
   );
end v8 ;

