@echo off
::                             =================
::                             Project unpacking
::                             =================
::
::
:: Author:  Lukas Kekely <ikekely@fit.vutbr.cz>
:: Date:  1.11.2015

set ZIP_CONTENT=( ^
    mcu\main_sw.c, ^
    mcu\main_swhw.c, ^
    fpga\src_filter\filter.cpp, ^
    fpga\src_filter\filter.h, ^
    fpga\src_filter\directives.tcl, ^
    build\med_filtr.bin, ^
    report.pdf ^
)

echo|set /p ="Unpack archive                          "

taskkill /im fkterm.exe /f >NUL 2>&1
taskkill /im catapult.exe /f >NUL 2>&1
taskkill /im scverify_top.exe /f >NUL 2>&1
if exist %WORK_DIR% rd /s /q %WORK_DIR% >NUL 2>&1
if exist %LOG_DIR% rd /s /q %LOG_DIR% >NUL 2>&1
md %WORK_DIR% >NUL 2>&1
md %WIS_DIR%  >NUL 2>&1
md %LOG_DIR%  >NUL 2>&1
copy /y %TARGET% %WIS_DIR% >NUL

unzip %WIS_DIR%\projekt.zip -d %WIS_DIR% >%LOG_DIR%\unpack.log 2>&1
if %errorlevel% neq 0 echo [Failed] & exit /b 1
del %WIS_DIR%\projekt.zip

echo Missing files: >>%LOG_DIR%\unpack.log
set ZIP_OK=1
for %%f in %ZIP_CONTENT% do if not exist %WIS_DIR%\%%f echo   %%f >>%LOG_DIR%\unpack.log & set ZIP_OK=0
if %ZIP_OK% neq 1 echo [Failed] & exit /b 1
echo [Done]