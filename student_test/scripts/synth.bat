@echo off
SETLOCAL ENABLEEXTENSIONS ENABLEDELAYEDEXPANSION
for /f %%a in ('copy /z "%~dpf0" nul') do set "CR=%%a"
::                    ====================================
::                    Synthesis and compilation of project
::                    ====================================
::
::
:: Author:  Lukas Kekely <ikekely@fit.vutbr.cz>
:: Date:  1.11.2014

echo Project synthesis:

if exist %SYNTH_DIR% rd /s /q %SYNTH_DIR% >NUL 2>&1
del %LOG_DIR%\synth*.log >NUL 2>&1
md %SYNTH_DIR% >NUL 2>&1
xcopy /s /e %WIS_DIR% %SYNTH_DIR% >NUL 2>&1

if exist %SYNTH_DIR%\mcu\main_sw.c (
    move %SYNTH_DIR%\mcu\main_sw.c %SYNTH_DIR%\mcu\main_sw.c.bkup >NUL 2>&1
    echo. >>%SYNTH_DIR%\mcu\main_sw.c.bkup
    type %SYNTH_DIR%\mcu\main_sw.c.bkup | findstr /v /c:"#define PROFILE" >%SYNTH_DIR%\mcu\main_sw.c
)
if exist %SYNTH_DIR%\mcu\main_swhw.c (
    move %SYNTH_DIR%\mcu\main_swhw.c %SYNTH_DIR%\mcu\main_swhw.c.bkup >NUL 2>&1
    echo. >>%SYNTH_DIR%\mcu\main_swhw.c.bkup
    type %SYNTH_DIR%\mcu\main_swhw.c.bkup | findstr /v /c:"#define PROFILE" >%SYNTH_DIR%\mcu\main_swhw.c
)



echo|set /p ="--> software solution                   "
md %SYNTH_DIR%\test_sw
md %SYNTH_DIR%\segmentace
xcopy /s %KIT_DIR%\segmentace %SYNTH_DIR%\segmentace >NUL

del %SYNTH_DIR%\segmentace\project.xml
copy /y %KIT_DIR%\project_sw.xml %SYNTH_DIR%\segmentace\project.xml >NUL

rd /s /q %SYNTH_DIR%\segmentace\mcu
md %SYNTH_DIR%\segmentace\mcu
xcopy /s %SYNTH_DIR%\mcu %SYNTH_DIR%\segmentace\mcu >NUL 2>&1

move %SYNTH_DIR%\segmentace\cpu\common.h %SYNTH_DIR%\segmentace\cpu\common_tmp.h >NUL 2>&1
sed s/xnovak00/%LOGIN%/ %SYNTH_DIR%\segmentace\cpu\common_tmp.h > %SYNTH_DIR%\segmentace\cpu\common.h

fcmake -s %KIT_DIR%\trunk %SYNTH_DIR%\segmentace\mcu >%LOG_DIR%\synth_sw.log 2>&1
gmake -C %SYNTH_DIR%\segmentace >>%LOG_DIR%\synth_sw.log 2>&1

set COMPILED=Failed
if exist %SYNTH_DIR%\segmentace\build\med_filtr_f1xx.hex if exist %SYNTH_DIR%\segmentace\build\med_filtr.bin set COMPILED=Done

md %SYNTH_DIR%\test_sw\build
copy /y %SYNTH_DIR%\segmentace\build\*.hex %SYNTH_DIR%\test_sw\build >NUL 2>&1
copy /y %SYNTH_DIR%\segmentace\build\*.bin %SYNTH_DIR%\test_sw\build >NUL 2>&1

echo [%COMPILED%]
timeout /T 2 >NUL
del /f /q %SYNTH_DIR%\segmentace\*.* >NUL 2>&1
for /d %%p in ("%SYNTH_DIR%\segmentace\*.*") do rd /s /q "%%p" >NUL 2>&1



echo|set /p ="--> accelerated solution (submitted)    "
md %SYNTH_DIR%\test_hw_acc
xcopy /s %KIT_DIR%\segmentace %SYNTH_DIR%\segmentace >NUL

del %SYNTH_DIR%\segmentace\project.xml
copy /y %KIT_DIR%\project_swhw.xml %SYNTH_DIR%\segmentace\project.xml >NUL

rd /s /q %SYNTH_DIR%\segmentace\mcu
md %SYNTH_DIR%\segmentace\mcu
xcopy /s %SYNTH_DIR%\mcu %SYNTH_DIR%\segmentace\mcu >NUL 2>&1

xcopy /s /y %SYNTH_DIR%\fpga\src_filter %SYNTH_DIR%\segmentace\fpga\src_filter >NUL 2>&1
copy /y %KIT_DIR%\segmentace\fpga\src_filter\mapped.vhdl %SYNTH_DIR%\segmentace\fpga\src_filter\mapped.vhdl >NUL

fcmake -s %KIT_DIR%\trunk %SYNTH_DIR%\segmentace\mcu >%LOG_DIR%\synth_hw_acc.log 2>&1
gmake -C %SYNTH_DIR%\segmentace >>%LOG_DIR%\synth_hw_acc.log 2>&1

set COMPILED=Done
if not exist %SYNTH_DIR%\segmentace\build\med_filtr_f1xx.hex set COMPILED=Failed
if not exist %SYNTH_DIR%\build\med_filtr.bin set COMPILED=Failed

md %SYNTH_DIR%\test_hw_acc\build
copy /y %SYNTH_DIR%\segmentace\build\*.hex %SYNTH_DIR%\test_hw_acc\build >NUL 2>&1
copy /y %SYNTH_DIR%\build\*.bin %SYNTH_DIR%\test_hw_acc\build >NUL 2>&1

echo [%COMPILED%]
timeout /T 2 >NUL
del /f /q %SYNTH_DIR%\segmentace\*.* >NUL 2>&1
for /d %%p in ("%SYNTH_DIR%\segmentace\*.*") do rd /s /q "%%p" >NUL 2>&1



set "LINE_BASE=--> accelerated solution (generated)    "
call :PROGRESS_BAR "%LINE_BASE%" 0
md %SYNTH_DIR%\test_catapult
md %SYNTH_DIR%\test_catapult\vhdl
xcopy /s %KIT_DIR%\segmentace %SYNTH_DIR%\segmentace >NUL

del %SYNTH_DIR%\segmentace\project.xml
copy /y %KIT_DIR%\project_swhw.xml %SYNTH_DIR%\segmentace\project.xml >NUL

rd /s /q %SYNTH_DIR%\segmentace\mcu
md %SYNTH_DIR%\segmentace\mcu
xcopy /s %SYNTH_DIR%\mcu %SYNTH_DIR%\segmentace\mcu >NUL 2>&1

rd /s /q %SYNTH_DIR%\segmentace\fpga\src_filter
md %SYNTH_DIR%\segmentace\fpga\src_filter
xcopy /s %SYNTH_DIR%\fpga\src_filter %SYNTH_DIR%\segmentace\fpga\src_filter >NUL 2>&1
xcopy /s %KIT_DIR%\segmentace\fpga\src_filter\addr_space.h %SYNTH_DIR%\segmentace\fpga\src_filter >NUL 2>&1
del /f /q %SYNTH_DIR%\segmentace\fpga\src_filter\mapped.vhdl >NUL 2>&1

call :PROGRESS_BAR "%LINE_BASE%" 5
cd %SYNTH_DIR%\segmentace\fpga
%CATAPULT_BIN% -shell -f src_filter\directives.tcl >..\..\..\..\%LOG_DIR%\synth_catapult_filter.log 2>&1
cd ..\..\..\..
call :PROGRESS_BAR "%LINE_BASE%" 35

copy /y %SYNTH_DIR%\segmentace\fpga\Catapult\filter.v1\mapped.vhdl   %SYNTH_DIR%\test_catapult\vhdl\filter_mapped.vhdl >NUL 2>&1
copy /y %SYNTH_DIR%\test_catapult\vhdl\filter_mapped.vhdl %SYNTH_DIR%\segmentace\fpga\src_filter\mapped.vhdl >NUL 2>&1

sed s/,simprim.vcomponents.all// %SYNTH_DIR%\segmentace\fpga\src_filter\mapped.vhdl > %SYNTH_DIR%\segmentace\fpga\src_filter\mapped_tmp.vhdl 2>NUL
sed s/,simprim// %SYNTH_DIR%\segmentace\fpga\src_filter\mapped_tmp.vhdl > %SYNTH_DIR%\segmentace\fpga\src_filter\mapped.vhdl 2>NUL
del %SYNTH_DIR%\segmentace\fpga\src_filter\mapped_tmp.vhdl >NUL 2>&1

timeout /T 2 >NUL
rd /s /q %SYNTH_DIR%\segmentace\fpga\Catapult >NUL 2>&1
del %SYNTH_DIR%\segmentace\fpga\src_genpix\mapped.vhdl

move %SYNTH_DIR%\segmentace\cpu\common.h %SYNTH_DIR%\segmentace\cpu\common_tmp.h >NUL 2>&1
sed s/xnovak00/%LOGIN%/ %SYNTH_DIR%\segmentace\cpu\common_tmp.h > %SYNTH_DIR%\segmentace\cpu\common.h

call :PROGRESS_BAR "%LINE_BASE%" 40
cd %SYNTH_DIR%\segmentace\fpga
%CATAPULT_BIN% -shell -f src_genpix\directives.tcl >..\..\..\..\%LOG_DIR%\synth_catapult_genpix.log 2>&1
cd ..\..\..\..
call :PROGRESS_BAR "%LINE_BASE%" 70

copy /y %SYNTH_DIR%\segmentace\fpga\Catapult\genpix.v1\mapped.vhdl   %SYNTH_DIR%\test_catapult\vhdl\genpix_mapped.vhdl >NUL 2>&1
copy /y %SYNTH_DIR%\test_catapult\vhdl\genpix_mapped.vhdl %SYNTH_DIR%\segmentace\fpga\src_genpix\mapped.vhdl >NUL 2>&1

sed s/,simprim.vcomponents.all// %SYNTH_DIR%\segmentace\fpga\src_genpix\mapped.vhdl > %SYNTH_DIR%\segmentace\fpga\src_genpix\mapped_tmp.vhdl 2>NUL
sed s/,simprim// %SYNTH_DIR%\segmentace\fpga\src_genpix\mapped_tmp.vhdl > %SYNTH_DIR%\segmentace\fpga\src_genpix\mapped.vhdl 2>NUL
del %SYNTH_DIR%\segmentace\fpga\src_genpix\mapped_tmp.vhdl >NUL 2>&1

fcmake -s %KIT_DIR%\trunk %SYNTH_DIR%\segmentace\mcu >%LOG_DIR%\synth_catapult.log 2>&1
call :PROGRESS_BAR "%LINE_BASE%" 75
gmake -C %SYNTH_DIR%\segmentace >>%LOG_DIR%\synth_catapult.log 2>&1
call :PROGRESS_BAR "%LINE_BASE%" 100

set COMPILED=Done
if not exist %SYNTH_DIR%\segmentace\build\med_filtr_f1xx.hex set COMPILED=Failed
if not exist %SYNTH_DIR%\segmentace\build\med_filtr.bin set COMPILED=Failed
if not exist %SYNTH_DIR%\test_catapult\vhdl\filter_mapped.vhdl set COMPILED=Failed
if not exist %SYNTH_DIR%\test_catapult\vhdl\genpix_mapped.vhdl set COMPILED=Failed

md %SYNTH_DIR%\test_catapult\build
copy /y %SYNTH_DIR%\segmentace\build\*.hex %SYNTH_DIR%\test_catapult\build >NUL 2>&1
copy /y %SYNTH_DIR%\segmentace\build\*.bin %SYNTH_DIR%\test_catapult\build >NUL 2>&1

<NUL set /p .="%LINE_BASE%[%COMPILED%]                         " & echo.
timeout /T 2 >NUL
del /f /q %SYNTH_DIR%\segmentace\*.* >NUL 2>&1
for /d %%p in ("%SYNTH_DIR%\segmentace\*.*") do rd /s /q "%%p" >NUL 2>&1
goto :EOF


:PROGRESS_BAR
    set "line_base=%~1"
    set progress=%~2
    set "bar_display=["
    set j=-2
    for /l %%i in (4,4,100) do (
        set /a j = %%i - 2
        if %progress% geq %%i (
            set "bar_display=!bar_display!="
        ) else if %progress% geq !j! (
            set "bar_display=!bar_display!-"
        ) else (
            set "bar_display=!bar_display! "
        )
    )
    set "bar_display=!bar_display!]"
    <nul set /p .="!line_base!!bar_display!!CR!"
exit /b