@echo off
::                             ==================
::                             Project evaluation
::                             ==================
::
::
:: Author:  Lukas Kekely <ikekely@fit.vutbr.cz>
:: Date:  1.11.2014

echo|set /p ="Results evaluation                      "

if exist %EVAL_DIR% rd /s /q %EVAL_DIR% >NUL 2>&1
del %LOG_DIR%\eval*.log >NUL 2>&1
md %EVAL_DIR% >NUL 2>&1
xcopy /s /e %RUNKIT_DIR% %EVAL_DIR% >NUL 2>&1

set PIPELINE_STAGES=0
if exist %EVAL_DIR%\fpga\src_filter\directives.tcl for /f "tokens=5" %%i in ('type %EVAL_DIR%\fpga\src_filter\directives.tcl ^| findstr /c:"directive set /filter/core/main -PIPELINE_INIT_INTERVAL"') do set PIPELINE_STAGES=%%i
if [%PIPELINE_STAGES%] == [] set PIPELINE_STAGES=0

md %EVAL_DIR%\test_output_log
copy /y %EVAL_DIR%\test_sw\sw_histogram.log          %EVAL_DIR%\test_output_log\hist_sw_test.txt >NUL 2>&1
copy /y %EVAL_DIR%\test_hw_acc\hwacc_histogram.log   %EVAL_DIR%\test_output_log\hist_hw_test.txt >NUL 2>&1
copy /y %EVAL_DIR%\test_catapult\hwacc_histogram.log %EVAL_DIR%\test_output_log\hist_catapult_test.txt >NUL 2>&1

%EXE_DIR%\program.exe %LOGIN% >%EVAL_DIR%\test_output_log\hist_ref.txt 2>%LOG_DIR%\eval.log
if %errorlevel% neq 0 echo [Failed] & exit /b 1

set EVALUATE=Done
%EXE_DIR%\compare.exe %LOGIN% %PIPELINE_STAGES% ^
        %EVAL_DIR%\test_output_log\hist_ref.txt ^
        %EVAL_DIR%\test_output_log\hist_sw_test.txt       ^
        %EVAL_DIR%\test_output_log\hist_hw_test.txt       ^
        %EVAL_DIR%\test_output_log\hist_catapult_test.txt ^
        %EVAL_DIR%\test_output_log\body.txt               ^
        %EVAL_DIR%\test_output_log\student.log            ^
        >>%LOG_DIR%\eval.log 2>&1
if %errorlevel% neq 0 echo [Failed] & exit /b 1

type %EVAL_DIR%\test_output_log\student.log >>%LOG_DIR%\eval.log 2>NUL
if exist %EVAL_DIR%\test_sim\Done (
    echo simulation                          [100%% Ok] >>%EVAL_DIR%\test_output_log\body.txt
) else (
    echo simulation                          [  0%% Failed] >>%EVAL_DIR%\test_output_log\body.txt
)

echo [Done]

echo.
echo ###############################################################################
echo Evaluation results:
for /f "tokens=*" %%l in ('type %EVAL_DIR%\test_output_log\body.txt') do echo --^> %%l
echo ###############################################################################
