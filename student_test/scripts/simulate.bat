@echo off
SETLOCAL ENABLEEXTENSIONS ENABLEDELAYEDEXPANSION
for /f %%a in ('copy /z "%~dpf0" nul') do set "CR=%%a"
::                     ===============================
::                     Project simulation in CatapultC
::                     ===============================
::
::
:: Autor:  Lukas Kekely <ikekely@fit.vutbr.cz>
:: Datum:  1.11.2014

set "LINE_END=                         "
set "LINE_BASE=Simulating project in CatapultC         "
call :PROGRESS_BAR "%LINE_BASE%" 0

taskkill /im catapult.exe /f >NUL 2>&1
taskkill /im scverify_top.exe /f >NUL 2>&1
if exist %SIM_DIR% rd /s /q %SIM_DIR% >NUL 2>&1
del %LOG_DIR%\sim*.log >NUL 2>&1
md %SIM_DIR% >NUL 2>&1
xcopy /s /e %SYNTH_DIR% %SIM_DIR% >NUL 2>&1
set SIM_STATUS=Done

md %SIM_DIR%\test_sim
xcopy /s %KIT_DIR%\segmentace %SIM_DIR%\segmentace >NUL

rd /s /q %SIM_DIR%\segmentace\fpga\src_filter
md %SIM_DIR%\segmentace\fpga\src_filter
xcopy /s %SIM_DIR%\fpga\src_filter %SIM_DIR%\segmentace\fpga\src_filter >NUL 2>&1
xcopy /s %KIT_DIR%\segmentace\fpga\src_filter\directives_sim.tcl %SIM_DIR%\segmentace\fpga\src_filter >NUL 2>&1
xcopy /s %KIT_DIR%\segmentace\fpga\src_filter\addr_space.h %SIM_DIR%\segmentace\fpga\src_filter >NUL 2>&1
xcopy /s %KIT_DIR%\segmentace\fpga\src_filter\tb_filter.cpp %SIM_DIR%\segmentace\fpga\src_filter >NUL 2>&1
del /f /q %SIM_DIR%\segmentace\fpga\src_filter\mapped.vhdl >NUL 2>&1

sed "s/^#define FRAMES[ ]*[0-9]*$/#define FRAMES 26/" %SIM_DIR%\segmentace\cpu\common.h >%SIM_DIR%\segmentace\cpu\common_tmp.h
sed s/xnovak00/%LOGIN%/ %SIM_DIR%\segmentace\cpu\common_tmp.h > %SIM_DIR%\segmentace\cpu\common.h

cd %SIM_DIR%\segmentace\fpga
start /b "" %CATAPULT_BIN% -shell -f src_filter\directives_sim.tcl >..\..\..\..\%LOG_DIR%\sim.log 2>&1
for /l %%i in (1,1,100) do (
    timeout /T 4 >NUL
    call :PROGRESS_BAR "%LINE_BASE%" %%i
    set RUNNING_CAT=False
    for /f "tokens=*" %%i in ('tasklist 2^>NUL ^| find /i "catapult.exe"') do set RUNNING_CAT=True
    if !RUNNING_CAT! == False goto :ENDWAIT
)
set SIM_STATUS=Failed
taskkill /im catapult.exe /f >NUL 2>&1
taskkill /im scverify_top.exe /f >NUL 2>&1
echo Simulation takes too long - aborted! >>..\..\..\..\%LOG_DIR%\sim.log
:ENDWAIT
cd ..\..\..\..

if not exist %SIM_DIR%\segmentace\fpga\catapult.log set SIM_STATUS=Failed & echo. >%SIM_DIR%\segmentace\fpga\catapult.log
set SIM_RESULT=Failed
for /f "tokens=*" %%i in ('type %SIM_DIR%\segmentace\fpga\catapult.log ^| findstr /c:"Test referencniho kodu oproti upravenemu probehl v poradku."') do set SIM_RESULT=Done
if %SIM_RESULT% == Failed set SIM_STATUS=Failed

<NUL set /p .="%LINE_BASE%[%SIM_STATUS%]%LINE_END%" & echo.
echo. >%SIM_DIR%\test_sim\%SIM_STATUS%
timeout /T 2 >NUL
del /f /q %SIM_DIR%\segmentace\*.* >NUL 2>&1
for /d %%p in ("%SIM_DIR%\segmentace\*.*") do rd /s /q "%%p" >NUL 2>&1
goto :EOF



:PROGRESS_BAR
    set "line_base=%~1"
    set progress=%~2
    set "bar_display=["
    set j=-2
    for /l %%i in (4,4,100) do (
        set /a j = %%i - 2
        if %progress% geq %%i (
            set "bar_display=!bar_display!="
        ) else if %progress% geq !j! (
            set "bar_display=!bar_display!-"
        ) else (
            set "bar_display=!bar_display! "
        )
    )
    set "bar_display=!bar_display!]"
    <nul set /p .="!line_base!!bar_display!!CR!"
exit /b