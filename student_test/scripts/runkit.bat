@echo off
SETLOCAL ENABLEEXTENSIONS ENABLEDELAYEDEXPANSION
for /f %%a in ('copy /z "%~dpf0" nul') do set "CR=%%a"
::                       ==============================
::                       Execution of project on FITkit
::                       ==============================
::
::
:: Author:  Lukas Kekely <ikekely@fit.vutbr.cz>
:: Date:  1.11.2014

echo Executing project on FITkit:

taskkill /im fkterm.exe /f >NUL 2>&1
if exist %RUNKIT_DIR% rd /s /q %RUNKIT_DIR% >NUL 2>&1
del %LOG_DIR%\runkit*.log >NUL 2>&1
md %RUNKIT_DIR% >NUL 2>&1
xcopy /s /e %SIM_DIR% %RUNKIT_DIR% >NUL 2>&1



set "LINE_END=                         "
set "LINE_BASE=--> software solution                   "
<NUL set /p .="%LINE_BASE%!CR!"
xcopy /s %KIT_DIR%\segmentace %RUNKIT_DIR%\segmentace >NUL

del %RUNKIT_DIR%\segmentace\project.xml
copy /y %KIT_DIR%\project_sw.xml %RUNKIT_DIR%\segmentace\project.xml >NUL
 
md %RUNKIT_DIR%\segmentace\build
xcopy /s %RUNKIT_DIR%\test_sw\build %RUNKIT_DIR%\segmentace\build >NUL

if not exist %RUNKIT_DIR%\segmentace\build\med_filtr_f1xx.hex (
    echo Not executed on FITkit: missing files from synthesis! >%LOG_DIR%\runkit_sw.log
    echo. >%RUNKIT_DIR%\test_sw\sw_histogram.log
    <NUL set /p .="%LINE_BASE%[Failed]%LINE_END%" & echo.
) else (
    echo. >%LOG_DIR%\runkit_sw.log
    set FKFLASH_RUN=0
:BOOTAGAIN_SW
    set /a FKFLASH_RUN+=1
    echo Boot attempt #!FKFLASH_RUN! >>%LOG_DIR%\runkit_sw.log
    call :PROGRESS_BAR "%LINE_BASE%" 0
    fkflash --force -f %RUNKIT_DIR%\segmentace\build\med_filtr_f1xx.hex -g %RUNKIT_DIR%\segmentace\build\med_filtr_f2xx.hex -b %RUNKIT_DIR%\segmentace\build\med_filtr.bin >>%LOG_DIR%\runkit_sw.log 2>&1
    if not !FKFLASH_RUN! geq 4 if !errorlevel! neq 0 timeout /T 1 >NUL & goto :BOOTAGAIN_SW
    if !errorlevel! neq 0 (
        echo Not executed on FITkit: unable to boot FITkit! >>%LOG_DIR%\runkit_sw.log
        echo. >%RUNKIT_DIR%\test_sw\sw_histogram.log
        <NUL set /p .="%LINE_BASE%[Failed]%LINE_END%" & echo.
    ) else (
        set FKTERM_RUN=0
:RUNAGAIN_SW
        taskkill /im fkterm.exe /f >NUL 2>&1
        set /a FKTERM_RUN+=1
        if exist %RUNKIT_DIR%\test_sw\sw_histogram.log type %RUNKIT_DIR%\test_sw\sw_histogram.log >>%LOG_DIR%\runkit_sw.log
        echo. >>%LOG_DIR%\runkit_sw.log
        echo Run attempt #!FKFLASH_RUN!-!FKTERM_RUN! >>%LOG_DIR%\runkit_sw.log
        call :PROGRESS_BAR "%LINE_BASE%" 20
        start /b fkterm -c "reset mcu" > %RUNKIT_DIR%\test_sw\sw_histogram.log
        for /l %%i in (21,1,100) do (
            timeout /T 4 >NUL
            call :PROGRESS_BAR "%LINE_BASE%" %%i
            set RESET_LINES=0
            set FRAME_LINES=0
            set RUN_OK=True
            if %%i geq 25 set RUN_OK=False
            for /f "tokens=*" %%i in ('type %RUNKIT_DIR%\test_sw\sw_histogram.log ^| findstr /c:"reset mcu"') do set /a RESET_LINES+=1
            for /f "tokens=*" %%i in ('type %RUNKIT_DIR%\test_sw\sw_histogram.log ^| findstr /c:"Threshold"') do set /a FRAME_LINES+=1
            for /f "tokens=*" %%i in ('type %RUNKIT_DIR%\test_sw\sw_histogram.log ^| findstr /c:"Pripojuji se k FITkitu pomoci zarizeni"') do ( for /f "tokens=*" %%j in ('tasklist 2^>NUL ^| find /i "fkterm.exe"') do set RUN_OK=True )
            if !RUN_OK! == False (if !FKTERM_RUN! geq 4 (goto :ENDWAIT_SW) else timeout /T 1 >NUL & goto :RUNAGAIN_SW)
            if !RESET_LINES! geq 2 if !FRAME_LINES! geq 5  goto :ENDWAIT_SW
        )
:ENDWAIT_SW
        taskkill /im fkterm.exe /f >NUL 2>&1
        if !errorlevel! neq 0 set RUN_OK=False
        if !RUN_OK! == False (
            type %RUNKIT_DIR%\test_sw\sw_histogram.log >>%LOG_DIR%\runkit_sw.log
            echo Not executed on FITkit: unable to execute! >>%LOG_DIR%\runkit_sw.log
            <NUL set /p .="%LINE_BASE%[Failed]%LINE_END%" & echo.
        ) else (
            type %RUNKIT_DIR%\test_sw\sw_histogram.log >>%LOG_DIR%\runkit_sw.log
            <NUL set /p .="%LINE_BASE%[Done]%LINE_END%" & echo.
        )
    )
)

timeout /T 1 >NUL



set "LINE_BASE=--> accelerated solution (submitted)    "
<NUL set /p .="%LINE_BASE%!CR!"
rd /s /q %RUNKIT_DIR%\segmentace
md %RUNKIT_DIR%\segmentace
xcopy /s %KIT_DIR%\segmentace %RUNKIT_DIR%\segmentace >NUL

copy /y %KIT_DIR%\project_swhw.xml %RUNKIT_DIR%\segmentace\project.xml >NUL

md %RUNKIT_DIR%\segmentace\build 
xcopy /s %RUNKIT_DIR%\test_hw_acc\build %RUNKIT_DIR%\segmentace\build >NUL

set SYNTH_FILES=True
if not exist %RUNKIT_DIR%\segmentace\build\med_filtr_f1xx.hex set SYNTH_FILES=False
if not exist %RUNKIT_DIR%\segmentace\build\med_filtr.bin set SYNTH_FILES=False
if %SYNTH_FILES% == False (
    echo Not executed on FITkit: missing files from synthesis! >%LOG_DIR%\runkit_hw_acc.log
    echo. >%RUNKIT_DIR%\test_hw_acc\hwacc_histogram.log
    <NUL set /p .="%LINE_BASE%[Failed]%LINE_END%" & echo.
) else (
    echo. >%LOG_DIR%\runkit_hw_acc.log
    set FKFLASH_RUN=0
:BOOTAGAIN_HW
    set /a FKFLASH_RUN+=1
    echo Boot attempt #!FKFLASH_RUN! >>%LOG_DIR%\runkit_hw_acc.log
    call :PROGRESS_BAR "%LINE_BASE%" 0
    fkflash --force -f %RUNKIT_DIR%\segmentace\build\med_filtr_f1xx.hex -g %RUNKIT_DIR%\segmentace\build\med_filtr_f2xx.hex -b %RUNKIT_DIR%\segmentace\build\med_filtr.bin >>%LOG_DIR%\runkit_hw_acc.log 2>&1
    if not !FKFLASH_RUN! geq 4 if !errorlevel! neq 0 timeout /T 1 >NUL & goto :BOOTAGAIN_HW
    if !errorlevel! neq 0 (
        echo Not executed on FITkit: unable to boot FITkit! >>%LOG_DIR%\runkit_hw_acc.log
        echo. >%RUNKIT_DIR%\test_hw_acc\hwacc_histogram.log
        <NUL set /p .="%LINE_BASE%[Failed]%LINE_END%" & echo.
    ) else (
        set FKTERM_RUN=0
:RUNAGAIN_HW
        taskkill /im fkterm.exe /f >NUL 2>&1
        set /a FKTERM_RUN+=1
        if exist %RUNKIT_DIR%\test_hw_acc\hwacc_histogram.log type %RUNKIT_DIR%\test_hw_acc\hwacc_histogram.log >>%LOG_DIR%\runkit_hw_acc.log
        echo. >>%LOG_DIR%\runkit_hw_acc.log
        echo Run attempt #!FKFLASH_RUN!-!FKTERM_RUN! >>%LOG_DIR%\runkit_hw_acc.log
        call :PROGRESS_BAR "%LINE_BASE%" 40
        start /b fkterm -c "reset mcu" >%RUNKIT_DIR%\test_hw_acc\hwacc_histogram.log
        for /l %%i in (41,1,100) do (
            timeout /T 1 >NUL
            call :PROGRESS_BAR "%LINE_BASE%" %%i
            set RESET_LINES=0
            set FRAME_LINES=0
            set RUN_OK=True
            if %%i geq 60 set RUN_OK=False
            for /f "tokens=*" %%i in ('type %RUNKIT_DIR%\test_hw_acc\hwacc_histogram.log ^| findstr /c:"reset mcu"') do set /a RESET_LINES+=1
            for /f "tokens=*" %%i in ('type %RUNKIT_DIR%\test_hw_acc\hwacc_histogram.log ^| findstr /c:"Threshold"') do set /a FRAME_LINES+=1
            for /f "tokens=*" %%i in ('type %RUNKIT_DIR%\test_hw_acc\hwacc_histogram.log ^| findstr /c:"Pripojuji se k FITkitu pomoci zarizeni"') do ( for /f "tokens=*" %%j in ('tasklist 2^>NUL ^| find /i "fkterm.exe"') do set RUN_OK=True )
            if !RUN_OK! == False (if !FKTERM_RUN! geq 4 (goto :ENDWAIT_HW) else timeout /T 1 >NUL >NUL & goto :RUNAGAIN_HW)
            if !RESET_LINES! geq 2 if !FRAME_LINES! geq 5 goto :ENDWAIT_HW
        )
:ENDWAIT_HW
        taskkill /im fkterm.exe /f >NUL 2>&1
        if !errorlevel! neq 0 set RUN_OK=False
        if !RUN_OK! == False (
            type %RUNKIT_DIR%\test_hw_acc\hwacc_histogram.log >>%LOG_DIR%\runkit_hw_acc.log
            echo Not executed on FITkit: unable to execute! >>%LOG_DIR%\runkit_hw_acc.log
            <NUL set /p .="%LINE_BASE%[Failed]%LINE_END%" & echo.
        ) else (
            type %RUNKIT_DIR%\test_hw_acc\hwacc_histogram.log >>%LOG_DIR%\runkit_hw_acc.log
            <NUL set /p .="%LINE_BASE%[Done]%LINE_END%" & echo.
        )
    )
)

timeout /T 1 >NUL



set "LINE_BASE=--> accelerated solution (generated)    "
<NUL set /p .="%LINE_BASE%!CR!"
rd /s /q %RUNKIT_DIR%\segmentace
md %RUNKIT_DIR%\segmentace
xcopy /s %KIT_DIR%\segmentace %RUNKIT_DIR%\segmentace >NUL

copy /y %KIT_DIR%\project_swhw.xml %RUNKIT_DIR%\segmentace\project.xml >NUL

md %RUNKIT_DIR%\segmentace\build 
xcopy /s %RUNKIT_DIR%\test_catapult\build %RUNKIT_DIR%\segmentace\build >NUL

set SYNTH_FILES=True
if not exist %RUNKIT_DIR%\segmentace\build\med_filtr_f1xx.hex set SYNTH_FILES=False
if not exist %RUNKIT_DIR%\segmentace\build\med_filtr.bin set SYNTH_FILES=False
if %SYNTH_FILES% == False (
    echo Not executed on FITkit: missing files from synthesis! >%LOG_DIR%\runkit_catapult.log
    echo. >%RUNKIT_DIR%\test_catapult\hwacc_histogram.log
    <NUL set /p .="%LINE_BASE%[Failed]%LINE_END%" & echo.
) else (
    set FKFLASH_RUN=0
    echo. >%LOG_DIR%\runkit_catapult.log
:BOOTAGAIN_CATA
    set /a FKFLASH_RUN+=1
    echo Boot attempt #!FKFLASH_RUN! >>%LOG_DIR%\runkit_catapult.log
    call :PROGRESS_BAR "%LINE_BASE%" 0
    fkflash --force -f %RUNKIT_DIR%\segmentace\build\med_filtr_f1xx.hex -g %RUNKIT_DIR%\segmentace\build\med_filtr_f2xx.hex -b %RUNKIT_DIR%\segmentace\build\med_filtr.bin >>%LOG_DIR%\runkit_catapult.log 2>&1
    if not !FKFLASH_RUN! geq 4 if !errorlevel! neq 0 timeout /T 1 >NUL & goto :BOOTAGAIN_CATA
    if !errorlevel! neq 0 (
        echo Not executed on FITkit: unable to boot FITkit! >>%LOG_DIR%\runkit_catapult.log
        echo. >%RUNKIT_DIR%\test_catapult\hwacc_histogram.log
        <NUL set /p .="%LINE_BASE%[Failed]%LINE_END%" & echo.
    ) else (
        set FKTERM_RUN=0
:RUNAGAIN_CATA
        taskkill /im fkterm.exe /f >NUL 2>&1
        set /a FKTERM_RUN+=1
        if exist %RUNKIT_DIR%\test_catapult\hwacc_histogram.log type %RUNKIT_DIR%\test_catapult\hwacc_histogram.log >>%LOG_DIR%\runkit_catapult.log
        echo. >>%LOG_DIR%\runkit_catapult.log
        echo Run attempt #!FKFLASH_RUN!-!FKTERM_RUN! >>%LOG_DIR%\runkit_catapult.log
        call :PROGRESS_BAR "%LINE_BASE%" 40
        start /b fkterm -c "reset mcu" >%RUNKIT_DIR%\test_catapult\hwacc_histogram.log
        for /l %%i in (41,1,100) do (
            timeout /T 1 >NUL
            call :PROGRESS_BAR "%LINE_BASE%" %%i
            set RESET_LINES=0
            set FRAME_LINES=0
            set RUN_OK=True
            if %%i geq 60 set RUN_OK=False
            for /f "tokens=*" %%i in ('type %RUNKIT_DIR%\test_catapult\hwacc_histogram.log ^| findstr /c:"reset mcu"') do set /a RESET_LINES+=1
            for /f "tokens=*" %%i in ('type %RUNKIT_DIR%\test_catapult\hwacc_histogram.log ^| findstr /c:"Threshold"') do set /a FRAME_LINES+=1
            for /f "tokens=*" %%i in ('type %RUNKIT_DIR%\test_catapult\hwacc_histogram.log ^| findstr /c:"Pripojuji se k FITkitu pomoci zarizeni"') do ( for /f "tokens=*" %%j in ('tasklist 2^>NUL ^| find /i "fkterm.exe"') do set RUN_OK=True )
            if !RUN_OK! == False (if !FKTERM_RUN! geq 4 (goto :ENDWAIT_CATA) else timeout /T 1 >NUL & goto :RUNAGAIN_CATA)
            if !RESET_LINES! geq 2 if !FRAME_LINES! geq 5 goto :ENDWAIT_CATA
        )
:ENDWAIT_CATA
        taskkill /im fkterm.exe /f >NUL 2>&1
        if !errorlevel! neq 0 set RUN_OK=False
        if !RUN_OK! == False (
            type %RUNKIT_DIR%\test_catapult\hwacc_histogram.log >>%LOG_DIR%\runkit_catapult.log
            echo Not executed on FITkit: unable to execute! >>%LOG_DIR%\runkit_catapult.log
            <NUL set /p .="%LINE_BASE%[Failed]%LINE_END%" & echo.
        ) else (
            type %RUNKIT_DIR%\test_catapult\hwacc_histogram.log >>%LOG_DIR%\runkit_catapult.log
            <NUL set /p .="%LINE_BASE%[Done]%LINE_END%" & echo.
        )
    )
)

timeout /T 1 >NUL



rd /s /q %RUNKIT_DIR%\segmentace
goto :EOF



:PROGRESS_BAR
    set "line_base=%~1"
    set progress=%~2
    set "bar_display=["
    set j=-2
    for /l %%i in (4,4,100) do (
        set /a j = %%i - 2
        if %progress% geq %%i (
            set "bar_display=!bar_display!="
        ) else if %progress% geq !j! (
            set "bar_display=!bar_display!-"
        ) else (
            set "bar_display=!bar_display! "
        )
    )
    set "bar_display=!bar_display!]"
    <nul set /p .="!line_base!!bar_display!!CR!"
exit /b
