===============================================================================
N�VOD NA POU�IT�:
===============================================================================

1) Zkop�rujte soubor projekt.zip p�ipraven� na odevzd�n� do WIS do t�to slo�ky.
   POZN: Alternativn� m��ete cestu k tomuto souboru zm�nit nastaven�m prom�nn�
   TARGET ve skriptu test.bat.

2) Hodnotu sv�ho loginu vlo�te do prom�nn� LOGIN ve skriptu test.bat.

3) Ujist�te se, �e je FITkit spr�vn� p�ipojen k virtu�ln�mu stroji.

4) Spus�te kompletn� test projektu vol�n�m skriptu test.bat (doba b�hu bude 
   10-15 minut).
   POZN: Alternativn� je mo�n� realizovat test krok po kroku jako sekvenci
   p��kaz�:
   test.bat unpack
   test.bat synth
   test.bat sim
   test.bat runkit
   test.bat eval

===============================================================================
V�STUPY:
===============================================================================

work/ - Slo�ka obsahuj�c� pracovn� soubory vytvo�en� v pr�b�hu testu rozd�len� 
        do podslo�ek podle kroku testu.

log/ - Slo�ka obsahuj�c� podrobn� informace o pr�b�hu jednotliv�ch krok� testu.

stdout - Stru�n� p�ehled pr�b�hu a v�sledku testu.

POZN1: Test nekontroluje obsah dokumentace, ale pouze jej� existenci.
POZN2: V�sledky testu jsou pouze orienta�n� a mohou se li�it od kone�n�ho
       hodnocen� projektu. V z�sad� ale, pokud test objev� n�kter� nedostatek, 
       je vysoce pravd�podobn�, �e se tento nedostatek objev� i p�i hodnocen�.


===============================================================================
CO TEST.BAT D�L�:
===============================================================================

1) Nejprve si rozbal� vstupn� soubor projekt.zip a zkontroluje, zda obsahuje 
   v�echny po�adovan� soubory ve spr�vn�ch slo�k�ch.

2) Provede kompilaci/synt�zu zdrojov�ch soubor�. 
   Skript:
      a) zkompiluje �ist� softwarov� �e�en� na MCU (software solution), 
      b) d�le zkompiluje odevzdan� �e�en�, p�esn�ji pouze program pro MCU, 
         do FPGA �ipu se nahraje odevzdan� med_filtr.bin soubor 
         (accelerated solution - submitted)
      c) a na z�v�r zkompiluje jak program pro MCU, tak i zdrojov� soubory pro
         FPGA (accelerated solution - generated).

3) Spust� simulaci projektu v prost�ed� Catapult C.

4) Spust� aplikaci na p��pravku FITkit.
   Skript:
      a) spust� �ist� softwarov� �e�en�,
      b) hardwarov� akcelerovan� �e�en� odevzdan�
      c) hardwarov� akcelerovan� �e�en� p�elo�eno z odevzdan�ch zdrojov�ch k�d�
   V�stupem tohoto kroku jsou log soubory obsahuj�c� p�ev�n� vypo�ten�
   histogramy z jednotliv�ch f�z� (a), (b) a (c).

5) Provede ohodnocen� dosa�en�ch v�sledk�, tj. porovn� histogramy v log
   souborech z�skan�ch v kroku (4) se vzorov�mi histogramy. V�sledky tohoto
   porovn�n� jsou ulo�eny do souboru log/eval.log. Zkr�cen� v�pis je pak
   vyps�n na stdout.
                                                                                
