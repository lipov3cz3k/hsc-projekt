
-- 
-- Definition of  filter
-- 
--      01/10/16 21:51:47
--      
--      Precision RTL Synthesis, 2012a.10
-- 

library IEEE;
use IEEE.STD_LOGIC_1164.all;
-- Library use clause for technology cells
library unisim ;
use unisim.vcomponents.all;

entity inc_9_0 is 
   port (
      cin : IN std_logic ;
      a : IN std_logic_vector (8 DOWNTO 0) ;
      d : OUT std_logic_vector (8 DOWNTO 0) ;
      cout : OUT std_logic) ;
end inc_9_0 ;

architecture IMPLEMENTATION of inc_9_0 is 
   signal nx8473z2, nx8473z1, nx8473z3, nx8474z1, nx8474z2, nx8475z1, 
      nx8475z2, nx8476z1, nx8476z2, nx8477z1, nx8477z2, nx8478z1, nx8478z2, 
      nx8479z1, nx8479z2, nx8480z1, nx8480z2, nx8481z1, nx8481z2: std_logic
    ;

begin
   ps_gnd : GND port map ( G=>nx8473z2);
   ps_vcc : VCC port map ( P=>nx8473z1);
   ix8473z1318 : LUT1_L
      generic map (INIT => X"2") 
       port map ( LO=>nx8473z3, I0=>a(0));
   muxcy_0 : MUXCY_L port map ( LO=>nx8474z1, CI=>nx8473z1, DI=>nx8473z2, S
      =>nx8473z3);
   ix8474z1316 : LUT1_L
      generic map (INIT => X"2") 
       port map ( LO=>nx8474z2, I0=>a(1));
   muxcy_1 : MUXCY_L port map ( LO=>nx8475z1, CI=>nx8474z1, DI=>nx8473z2, S
      =>nx8474z2);
   ix8475z1316 : LUT1_L
      generic map (INIT => X"2") 
       port map ( LO=>nx8475z2, I0=>a(2));
   muxcy_2 : MUXCY_L port map ( LO=>nx8476z1, CI=>nx8475z1, DI=>nx8473z2, S
      =>nx8475z2);
   ix8476z1316 : LUT1_L
      generic map (INIT => X"2") 
       port map ( LO=>nx8476z2, I0=>a(3));
   muxcy_3 : MUXCY_L port map ( LO=>nx8477z1, CI=>nx8476z1, DI=>nx8473z2, S
      =>nx8476z2);
   ix8477z1316 : LUT1_L
      generic map (INIT => X"2") 
       port map ( LO=>nx8477z2, I0=>a(4));
   muxcy_4 : MUXCY_L port map ( LO=>nx8478z1, CI=>nx8477z1, DI=>nx8473z2, S
      =>nx8477z2);
   ix8478z1316 : LUT1_L
      generic map (INIT => X"2") 
       port map ( LO=>nx8478z2, I0=>a(5));
   muxcy_5 : MUXCY_L port map ( LO=>nx8479z1, CI=>nx8478z1, DI=>nx8473z2, S
      =>nx8478z2);
   ix8479z1316 : LUT1_L
      generic map (INIT => X"2") 
       port map ( LO=>nx8479z2, I0=>a(6));
   muxcy_6 : MUXCY_L port map ( LO=>nx8480z1, CI=>nx8479z1, DI=>nx8473z2, S
      =>nx8479z2);
   ix8480z1316 : LUT1_L
      generic map (INIT => X"2") 
       port map ( LO=>nx8480z2, I0=>a(7));
   muxcy_7 : MUXCY_L port map ( LO=>nx8481z1, CI=>nx8480z1, DI=>nx8473z2, S
      =>nx8480z2);
   ix8481z1316 : LUT1_L
      generic map (INIT => X"2") 
       port map ( LO=>nx8481z2, I0=>a(8));
   muxcy_8 : MUXCY port map ( O=>cout, CI=>nx8481z1, DI=>nx8473z2, S=>
      nx8481z2);
end IMPLEMENTATION ;

library IEEE;
use IEEE.STD_LOGIC_1164.all;
-- Library use clause for technology cells
library unisim ;
use unisim.vcomponents.all;

entity incdec_10_0 is 
   port (
      mode : IN std_logic ;
      cin : IN std_logic ;
      a : IN std_logic_vector (9 DOWNTO 0) ;
      d : OUT std_logic_vector (9 DOWNTO 0) ;
      cout : OUT std_logic ;
      p_fsm_output_1 : IN std_logic ;
      p_frame_sva_9 : IN std_logic ;
      p_buffer_buf_vinit_ndx_sva_9 : IN std_logic ;
      p_frame_sva_8 : IN std_logic ;
      p_buffer_buf_vinit_ndx_sva_8 : IN std_logic ;
      p_frame_sva_7 : IN std_logic ;
      p_buffer_buf_vinit_ndx_sva_7 : IN std_logic ;
      p_frame_sva_6 : IN std_logic ;
      p_buffer_buf_vinit_ndx_sva_6 : IN std_logic ;
      p_frame_sva_5 : IN std_logic ;
      p_buffer_buf_vinit_ndx_sva_5 : IN std_logic ;
      p_buffer_buf_vinit_ndx_sva_4 : IN std_logic ;
      p_frame_sva_4 : IN std_logic ;
      p_frame_sva_3 : IN std_logic ;
      p_buffer_buf_vinit_ndx_sva_3 : IN std_logic ;
      p_frame_sva_2 : IN std_logic ;
      p_buffer_buf_vinit_ndx_sva_2 : IN std_logic ;
      p_frame_sva_1 : IN std_logic ;
      p_buffer_buf_vinit_ndx_sva_1 : IN std_logic ;
      p_frame_sva_0 : IN std_logic ;
      p_buffer_buf_vinit_ndx_sva_0 : IN std_logic) ;
end incdec_10_0 ;

architecture IMPLEMENTATION of incdec_10_0 is 
   signal nx8473z2, nx8474z1, nx8474z2, nx8475z1, nx8475z2, nx8476z1, 
      nx8476z2, nx8477z1, nx8477z2, nx8478z1, nx8478z2, nx8479z1, nx8479z2, 
      nx8480z1, nx8480z2, nx8481z1, nx8481z2, nx51679z1, nx51679z2: 
   std_logic ;

begin
   xorcy_0 : XORCY port map ( O=>d(0), CI=>mode, LI=>nx8473z2);
   muxcy_0 : MUXCY_L port map ( LO=>nx8474z1, CI=>mode, DI=>a(0), S=>
      nx8473z2);
   xorcy_1 : XORCY port map ( O=>d(1), CI=>nx8474z1, LI=>nx8474z2);
   muxcy_1 : MUXCY_L port map ( LO=>nx8475z1, CI=>nx8474z1, DI=>a(1), S=>
      nx8474z2);
   xorcy_2 : XORCY port map ( O=>d(2), CI=>nx8475z1, LI=>nx8475z2);
   muxcy_2 : MUXCY_L port map ( LO=>nx8476z1, CI=>nx8475z1, DI=>a(2), S=>
      nx8475z2);
   xorcy_3 : XORCY port map ( O=>d(3), CI=>nx8476z1, LI=>nx8476z2);
   muxcy_3 : MUXCY_L port map ( LO=>nx8477z1, CI=>nx8476z1, DI=>a(3), S=>
      nx8476z2);
   xorcy_4 : XORCY port map ( O=>d(4), CI=>nx8477z1, LI=>nx8477z2);
   muxcy_4 : MUXCY_L port map ( LO=>nx8478z1, CI=>nx8477z1, DI=>a(4), S=>
      nx8477z2);
   xorcy_5 : XORCY port map ( O=>d(5), CI=>nx8478z1, LI=>nx8478z2);
   muxcy_5 : MUXCY_L port map ( LO=>nx8479z1, CI=>nx8478z1, DI=>a(5), S=>
      nx8478z2);
   xorcy_6 : XORCY port map ( O=>d(6), CI=>nx8479z1, LI=>nx8479z2);
   muxcy_6 : MUXCY_L port map ( LO=>nx8480z1, CI=>nx8479z1, DI=>a(6), S=>
      nx8479z2);
   xorcy_7 : XORCY port map ( O=>d(7), CI=>nx8480z1, LI=>nx8480z2);
   muxcy_7 : MUXCY_L port map ( LO=>nx8481z1, CI=>nx8480z1, DI=>a(7), S=>
      nx8480z2);
   xorcy_8 : XORCY port map ( O=>d(8), CI=>nx8481z1, LI=>nx8481z2);
   muxcy_8 : MUXCY_L port map ( LO=>nx51679z1, CI=>nx8481z1, DI=>a(8), S=>
      nx8481z2);
   xorcy_9 : XORCY port map ( O=>d(9), CI=>nx51679z1, LI=>nx51679z2);
   ix51679z40647 : LUT4_L
      generic map (INIT => X"99A5") 
       port map ( LO=>nx51679z2, I0=>mode, I1=>p_buffer_buf_vinit_ndx_sva_9, 
      I2=>p_frame_sva_9, I3=>p_fsm_output_1);
   ix8481z40647 : LUT4_L
      generic map (INIT => X"99A5") 
       port map ( LO=>nx8481z2, I0=>mode, I1=>p_buffer_buf_vinit_ndx_sva_8, 
      I2=>p_frame_sva_8, I3=>p_fsm_output_1);
   ix8480z40647 : LUT4_L
      generic map (INIT => X"99A5") 
       port map ( LO=>nx8480z2, I0=>mode, I1=>p_buffer_buf_vinit_ndx_sva_7, 
      I2=>p_frame_sva_7, I3=>p_fsm_output_1);
   ix8479z40647 : LUT4_L
      generic map (INIT => X"99A5") 
       port map ( LO=>nx8479z2, I0=>mode, I1=>p_buffer_buf_vinit_ndx_sva_6, 
      I2=>p_frame_sva_6, I3=>p_fsm_output_1);
   ix8478z40647 : LUT4_L
      generic map (INIT => X"99A5") 
       port map ( LO=>nx8478z2, I0=>mode, I1=>p_buffer_buf_vinit_ndx_sva_5, 
      I2=>p_frame_sva_5, I3=>p_fsm_output_1);
   ix8477z43707 : LUT4_L
      generic map (INIT => X"A599") 
       port map ( LO=>nx8477z2, I0=>mode, I1=>p_frame_sva_4, I2=>
      p_buffer_buf_vinit_ndx_sva_4, I3=>p_fsm_output_1);
   ix8476z40647 : LUT4_L
      generic map (INIT => X"99A5") 
       port map ( LO=>nx8476z2, I0=>mode, I1=>p_buffer_buf_vinit_ndx_sva_3, 
      I2=>p_frame_sva_3, I3=>p_fsm_output_1);
   ix8475z40647 : LUT4_L
      generic map (INIT => X"99A5") 
       port map ( LO=>nx8475z2, I0=>mode, I1=>p_buffer_buf_vinit_ndx_sva_2, 
      I2=>p_frame_sva_2, I3=>p_fsm_output_1);
   ix8474z40647 : LUT4_L
      generic map (INIT => X"99A5") 
       port map ( LO=>nx8474z2, I0=>mode, I1=>p_buffer_buf_vinit_ndx_sva_1, 
      I2=>p_frame_sva_1, I3=>p_fsm_output_1);
   ix8473z40647 : LUT4_L
      generic map (INIT => X"99A5") 
       port map ( LO=>nx8473z2, I0=>mode, I1=>p_buffer_buf_vinit_ndx_sva_0, 
      I2=>p_frame_sva_0, I3=>p_fsm_output_1);
end IMPLEMENTATION ;

library IEEE;
use IEEE.STD_LOGIC_1164.all;
-- Library use clause for technology cells
library unisim ;
use unisim.vcomponents.all;

entity inc_17_0 is 
   port (
      cin : IN std_logic ;
      a : IN std_logic_vector (16 DOWNTO 0) ;
      d : OUT std_logic_vector (16 DOWNTO 0) ;
      cout : OUT std_logic ;
      p_histogram_rsci_data_out_d_16 : IN std_logic ;
      p_histogram_rsci_data_out_d_15 : IN std_logic ;
      p_histogram_rsci_data_out_d_14 : IN std_logic ;
      p_histogram_rsci_data_out_d_13 : IN std_logic ;
      p_histogram_rsci_data_out_d_12 : IN std_logic ;
      p_histogram_rsci_data_out_d_11 : IN std_logic ;
      p_histogram_rsci_data_out_d_10 : IN std_logic ;
      p_histogram_rsci_data_out_d_9 : IN std_logic ;
      p_histogram_rsci_data_out_d_8 : IN std_logic ;
      p_histogram_rsci_data_out_d_7 : IN std_logic ;
      p_histogram_rsci_data_out_d_6 : IN std_logic ;
      p_histogram_rsci_data_out_d_5 : IN std_logic ;
      p_histogram_rsci_data_out_d_4 : IN std_logic ;
      p_histogram_rsci_data_out_d_3 : IN std_logic ;
      p_histogram_rsci_data_out_d_2 : IN std_logic ;
      p_histogram_rsci_data_out_d_1 : IN std_logic ;
      p_histogram_rsci_data_out_d_0 : IN std_logic) ;
end inc_17_0 ;

architecture IMPLEMENTATION of inc_17_0 is 
   signal nx8473z2, nx8473z3, nx8474z1, nx8474z2, nx8475z1, nx8475z2, 
      nx8476z1, nx8476z2, nx8477z1, nx8477z2, nx8478z1, nx8478z2, nx8479z1, 
      nx8479z2, nx8480z1, nx8480z2, nx8481z1, nx8481z2, nx8482z1, nx8482z2, 
      nx60018z1, nx60018z2, nx60019z1, nx60019z2, nx60020z1, nx60020z2, 
      nx60021z1, nx60021z2, nx60022z1, nx60022z2, nx60023z1, nx60023z2, 
      nx20589z1, nx20589z2: std_logic ;

begin
   ps_gnd : GND port map ( G=>nx8473z2);
   xorcy_0 : XORCY port map ( O=>d(0), CI=>cin, LI=>nx8473z3);
   muxcy_0 : MUXCY_L port map ( LO=>nx8474z1, CI=>cin, DI=>nx8473z2, S=>
      nx8473z3);
   xorcy_1 : XORCY port map ( O=>d(1), CI=>nx8474z1, LI=>nx8474z2);
   muxcy_1 : MUXCY_L port map ( LO=>nx8475z1, CI=>nx8474z1, DI=>nx8473z2, S
      =>nx8474z2);
   xorcy_2 : XORCY port map ( O=>d(2), CI=>nx8475z1, LI=>nx8475z2);
   muxcy_2 : MUXCY_L port map ( LO=>nx8476z1, CI=>nx8475z1, DI=>nx8473z2, S
      =>nx8475z2);
   xorcy_3 : XORCY port map ( O=>d(3), CI=>nx8476z1, LI=>nx8476z2);
   muxcy_3 : MUXCY_L port map ( LO=>nx8477z1, CI=>nx8476z1, DI=>nx8473z2, S
      =>nx8476z2);
   xorcy_4 : XORCY port map ( O=>d(4), CI=>nx8477z1, LI=>nx8477z2);
   muxcy_4 : MUXCY_L port map ( LO=>nx8478z1, CI=>nx8477z1, DI=>nx8473z2, S
      =>nx8477z2);
   xorcy_5 : XORCY port map ( O=>d(5), CI=>nx8478z1, LI=>nx8478z2);
   muxcy_5 : MUXCY_L port map ( LO=>nx8479z1, CI=>nx8478z1, DI=>nx8473z2, S
      =>nx8478z2);
   xorcy_6 : XORCY port map ( O=>d(6), CI=>nx8479z1, LI=>nx8479z2);
   muxcy_6 : MUXCY_L port map ( LO=>nx8480z1, CI=>nx8479z1, DI=>nx8473z2, S
      =>nx8479z2);
   xorcy_7 : XORCY port map ( O=>d(7), CI=>nx8480z1, LI=>nx8480z2);
   muxcy_7 : MUXCY_L port map ( LO=>nx8481z1, CI=>nx8480z1, DI=>nx8473z2, S
      =>nx8480z2);
   xorcy_8 : XORCY port map ( O=>d(8), CI=>nx8481z1, LI=>nx8481z2);
   muxcy_8 : MUXCY_L port map ( LO=>nx8482z1, CI=>nx8481z1, DI=>nx8473z2, S
      =>nx8481z2);
   xorcy_9 : XORCY port map ( O=>d(9), CI=>nx8482z1, LI=>nx8482z2);
   muxcy_9 : MUXCY_L port map ( LO=>nx60018z1, CI=>nx8482z1, DI=>nx8473z2, S
      =>nx8482z2);
   xorcy_10 : XORCY port map ( O=>d(10), CI=>nx60018z1, LI=>nx60018z2);
   muxcy_10 : MUXCY_L port map ( LO=>nx60019z1, CI=>nx60018z1, DI=>nx8473z2, 
      S=>nx60018z2);
   xorcy_11 : XORCY port map ( O=>d(11), CI=>nx60019z1, LI=>nx60019z2);
   muxcy_11 : MUXCY_L port map ( LO=>nx60020z1, CI=>nx60019z1, DI=>nx8473z2, 
      S=>nx60019z2);
   xorcy_12 : XORCY port map ( O=>d(12), CI=>nx60020z1, LI=>nx60020z2);
   muxcy_12 : MUXCY_L port map ( LO=>nx60021z1, CI=>nx60020z1, DI=>nx8473z2, 
      S=>nx60020z2);
   xorcy_13 : XORCY port map ( O=>d(13), CI=>nx60021z1, LI=>nx60021z2);
   muxcy_13 : MUXCY_L port map ( LO=>nx60022z1, CI=>nx60021z1, DI=>nx8473z2, 
      S=>nx60021z2);
   xorcy_14 : XORCY port map ( O=>d(14), CI=>nx60022z1, LI=>nx60022z2);
   muxcy_14 : MUXCY_L port map ( LO=>nx60023z1, CI=>nx60022z1, DI=>nx8473z2, 
      S=>nx60022z2);
   xorcy_15 : XORCY port map ( O=>d(15), CI=>nx60023z1, LI=>nx60023z2);
   muxcy_15 : MUXCY_L port map ( LO=>nx20589z1, CI=>nx60023z1, DI=>nx8473z2, 
      S=>nx60023z2);
   xorcy_16 : XORCY port map ( O=>d(16), CI=>nx20589z1, LI=>nx20589z2);
   ix20589z1322 : LUT2_L
      generic map (INIT => X"8") 
       port map ( LO=>nx20589z2, I0=>p_histogram_rsci_data_out_d_16, I1=>cin
   );
   ix60023z1322 : LUT2_L
      generic map (INIT => X"8") 
       port map ( LO=>nx60023z2, I0=>p_histogram_rsci_data_out_d_15, I1=>cin
   );
   ix60022z1322 : LUT2_L
      generic map (INIT => X"8") 
       port map ( LO=>nx60022z2, I0=>p_histogram_rsci_data_out_d_14, I1=>cin
   );
   ix60021z1322 : LUT2_L
      generic map (INIT => X"8") 
       port map ( LO=>nx60021z2, I0=>p_histogram_rsci_data_out_d_13, I1=>cin
   );
   ix60020z1322 : LUT2_L
      generic map (INIT => X"8") 
       port map ( LO=>nx60020z2, I0=>p_histogram_rsci_data_out_d_12, I1=>cin
   );
   ix60019z1322 : LUT2_L
      generic map (INIT => X"8") 
       port map ( LO=>nx60019z2, I0=>p_histogram_rsci_data_out_d_11, I1=>cin
   );
   ix60018z1322 : LUT2_L
      generic map (INIT => X"8") 
       port map ( LO=>nx60018z2, I0=>p_histogram_rsci_data_out_d_10, I1=>cin
   );
   ix8482z1322 : LUT2_L
      generic map (INIT => X"8") 
       port map ( LO=>nx8482z2, I0=>p_histogram_rsci_data_out_d_9, I1=>cin);
   ix8481z1322 : LUT2_L
      generic map (INIT => X"8") 
       port map ( LO=>nx8481z2, I0=>p_histogram_rsci_data_out_d_8, I1=>cin);
   ix8480z1322 : LUT2_L
      generic map (INIT => X"8") 
       port map ( LO=>nx8480z2, I0=>p_histogram_rsci_data_out_d_7, I1=>cin);
   ix8479z1322 : LUT2_L
      generic map (INIT => X"8") 
       port map ( LO=>nx8479z2, I0=>p_histogram_rsci_data_out_d_6, I1=>cin);
   ix8478z1322 : LUT2_L
      generic map (INIT => X"8") 
       port map ( LO=>nx8478z2, I0=>p_histogram_rsci_data_out_d_5, I1=>cin);
   ix8477z1322 : LUT2_L
      generic map (INIT => X"8") 
       port map ( LO=>nx8477z2, I0=>p_histogram_rsci_data_out_d_4, I1=>cin);
   ix8476z1322 : LUT2_L
      generic map (INIT => X"8") 
       port map ( LO=>nx8476z2, I0=>p_histogram_rsci_data_out_d_3, I1=>cin);
   ix8475z1322 : LUT2_L
      generic map (INIT => X"8") 
       port map ( LO=>nx8475z2, I0=>p_histogram_rsci_data_out_d_2, I1=>cin);
   ix8474z1322 : LUT2_L
      generic map (INIT => X"8") 
       port map ( LO=>nx8474z2, I0=>p_histogram_rsci_data_out_d_1, I1=>cin);
   ix8473z1323 : LUT2_L
      generic map (INIT => X"8") 
       port map ( LO=>nx8473z3, I0=>p_histogram_rsci_data_out_d_0, I1=>cin);

end IMPLEMENTATION ;

library IEEE;
use IEEE.STD_LOGIC_1164.all;
-- Library use clause for technology cells
library unisim ;
use unisim.vcomponents.all;

entity filter_core is 
   port (
      clk : IN std_logic ;
      rst : IN std_logic ;
      in_data_rsc_z : IN std_logic_vector (2 DOWNTO 0) ;
      in_data_rsc_lz : OUT std_logic ;
      in_data_vld_rsc_z : IN std_logic ;
      out_data_rsc_z : OUT std_logic_vector (2 DOWNTO 0) ;
      out_data_rsc_lz : OUT std_logic ;
      mcu_data_rsci_data_in_d : OUT std_logic_vector (31 DOWNTO 0) ;
      mcu_data_rsci_addr_d : OUT std_logic_vector (8 DOWNTO 0) ;
      mcu_data_rsci_re_d : OUT std_logic ;
      mcu_data_rsci_we_d : OUT std_logic ;
      mcu_data_rsci_data_out_d : IN std_logic_vector (31 DOWNTO 0) ;
      histogram_rsci_data_in_d : OUT std_logic_vector (16 DOWNTO 0) ;
      histogram_rsci_addr_d : OUT std_logic_vector (2 DOWNTO 0) ;
      histogram_rsci_re_d : OUT std_logic ;
      histogram_rsci_we_d : OUT std_logic ;
      histogram_rsci_data_out_d : IN std_logic_vector (16 DOWNTO 0) ;
      buffer_buf_rsci_data_in_d : OUT std_logic_vector (2 DOWNTO 0) ;
      buffer_buf_rsci_addr_d : OUT std_logic_vector (9 DOWNTO 0) ;
      buffer_buf_rsci_re_d : OUT std_logic ;
      buffer_buf_rsci_we_d : OUT std_logic ;
      buffer_buf_rsci_data_out_d : IN std_logic_vector (2 DOWNTO 0) ;
      px2584 : OUT std_logic ;
      px2583 : OUT std_logic ;
      p_fsm_output_4 : OUT std_logic ;
      p_exit_if_if_if_1_if_for_lpi_1_dfm_3 : OUT std_logic) ;
end filter_core ;

architecture v35_unfold_1901 of filter_core is 
   component inc_9_0
      port (
         cin : IN std_logic ;
         a : IN std_logic_vector (8 DOWNTO 0) ;
         d : OUT std_logic_vector (8 DOWNTO 0) ;
         cout : OUT std_logic) ;
   end component ;
   component incdec_10_0
      port (
         mode : IN std_logic ;
         cin : IN std_logic ;
         a : IN std_logic_vector (9 DOWNTO 0) ;
         d : OUT std_logic_vector (9 DOWNTO 0) ;
         cout : OUT std_logic ;
         p_fsm_output_1 : IN std_logic ;
         p_frame_sva_9 : IN std_logic ;
         p_buffer_buf_vinit_ndx_sva_9 : IN std_logic ;
         p_frame_sva_8 : IN std_logic ;
         p_buffer_buf_vinit_ndx_sva_8 : IN std_logic ;
         p_frame_sva_7 : IN std_logic ;
         p_buffer_buf_vinit_ndx_sva_7 : IN std_logic ;
         p_frame_sva_6 : IN std_logic ;
         p_buffer_buf_vinit_ndx_sva_6 : IN std_logic ;
         p_frame_sva_5 : IN std_logic ;
         p_buffer_buf_vinit_ndx_sva_5 : IN std_logic ;
         p_buffer_buf_vinit_ndx_sva_4 : IN std_logic ;
         p_frame_sva_4 : IN std_logic ;
         p_frame_sva_3 : IN std_logic ;
         p_buffer_buf_vinit_ndx_sva_3 : IN std_logic ;
         p_frame_sva_2 : IN std_logic ;
         p_buffer_buf_vinit_ndx_sva_2 : IN std_logic ;
         p_frame_sva_1 : IN std_logic ;
         p_buffer_buf_vinit_ndx_sva_1 : IN std_logic ;
         p_frame_sva_0 : IN std_logic ;
         p_buffer_buf_vinit_ndx_sva_0 : IN std_logic) ;
   end component ;
   component inc_17_0
      port (
         cin : IN std_logic ;
         a : IN std_logic_vector (16 DOWNTO 0) ;
         d : OUT std_logic_vector (16 DOWNTO 0) ;
         cout : OUT std_logic ;
         p_histogram_rsci_data_out_d_16 : IN std_logic ;
         p_histogram_rsci_data_out_d_15 : IN std_logic ;
         p_histogram_rsci_data_out_d_14 : IN std_logic ;
         p_histogram_rsci_data_out_d_13 : IN std_logic ;
         p_histogram_rsci_data_out_d_12 : IN std_logic ;
         p_histogram_rsci_data_out_d_11 : IN std_logic ;
         p_histogram_rsci_data_out_d_10 : IN std_logic ;
         p_histogram_rsci_data_out_d_9 : IN std_logic ;
         p_histogram_rsci_data_out_d_8 : IN std_logic ;
         p_histogram_rsci_data_out_d_7 : IN std_logic ;
         p_histogram_rsci_data_out_d_6 : IN std_logic ;
         p_histogram_rsci_data_out_d_5 : IN std_logic ;
         p_histogram_rsci_data_out_d_4 : IN std_logic ;
         p_histogram_rsci_data_out_d_3 : IN std_logic ;
         p_histogram_rsci_data_out_d_2 : IN std_logic ;
         p_histogram_rsci_data_out_d_1 : IN std_logic ;
         p_histogram_rsci_data_out_d_0 : IN std_logic) ;
   end component ;
   signal fsm_output: std_logic_vector (6 DOWNTO 0) ;
   
   signal clip_window_qr_lpi_1: std_logic_vector (2 DOWNTO 0) ;
   
   signal clip_window_qr_1_lpi_1: std_logic_vector (2 DOWNTO 0) ;
   
   signal clip_window_qr_2_lpi_1: std_logic_vector (2 DOWNTO 0) ;
   
   signal clip_window_qr_3_lpi_1: std_logic_vector (2 DOWNTO 0) ;
   
   signal window_0_lpi_1: std_logic_vector (2 DOWNTO 0) ;
   
   signal window_2_lpi_1: std_logic_vector (2 DOWNTO 0) ;
   
   signal system_input_land_1_lpi_1, asn_sft_3_lpi_1: std_logic ;
   
   signal median_max_4_1_lpi_1: std_logic_vector (2 DOWNTO 0) ;
   
   signal median_max_5_1_lpi_1: std_logic_vector (2 DOWNTO 0) ;
   
   signal median_max_3_1_lpi_1: std_logic_vector (2 DOWNTO 0) ;
   
   signal median_max_6_1_lpi_1: std_logic_vector (2 DOWNTO 0) ;
   
   signal median_max_2_1_lpi_1: std_logic_vector (2 DOWNTO 0) ;
   
   signal median_max_7_1_lpi_1: std_logic_vector (2 DOWNTO 0) ;
   
   signal median_max_1_1_lpi_1: std_logic_vector (2 DOWNTO 0) ;
   
   signal median_max_0_lpi_1: std_logic_vector (2 DOWNTO 0) ;
   
   signal median_i_1_lpi_2: std_logic_vector (3 DOWNTO 0) ;
   
   signal exit_if_if_if_1_if_for_lpi_1, exit_Linit_lpi_1, asn_sft_lpi_1: 
   std_logic ;
   
   signal histogram_init_idx_1_lpi_1: std_logic_vector (2 DOWNTO 0) ;
   
   signal sfi_system_input_land_1_lpi_1, asn_sft_2_lpi_1, 
      sfi_if_if_read_mem_mcu_data_rsc_lpi_1, asn_sft_1_lpi_1: std_logic ;
   
   signal if_if_if_acc_5_psp_lpi_1: std_logic_vector (2 DOWNTO 0) ;
   
   signal if_if_equal_cse_lpi_1: std_logic ;
   
   signal if_if_read_mem_mcu_data_rsc_sft_lpi_1: std_logic_vector
    (31 DOWNTO 0) ;
   
   signal histogram_init_idx_sva: std_logic_vector (2 DOWNTO 0) ;
   
   signal exit_histogram_init_sva: std_logic ;
   
   signal threshold_sva: std_logic_vector (2 DOWNTO 0) ;
   
   signal frame_sva: std_logic_vector (9 DOWNTO 0) ;
   
   signal system_input_c_sva: std_logic_vector (8 DOWNTO 0) ;
   
   signal system_input_r_sva: std_logic_vector (7 DOWNTO 0) ;
   
   signal system_input_c_filter_sva: std_logic_vector (8 DOWNTO 0) ;
   
   signal system_input_r_filter_sva: std_logic_vector (7 DOWNTO 0) ;
   
   signal system_input_output_vld_sva: std_logic ;
   
   signal system_input_window_4_sva: std_logic_vector (2 DOWNTO 0) ;
   
   signal system_input_window_3_sva: std_logic_vector (2 DOWNTO 0) ;
   
   signal system_input_window_5_sva: std_logic_vector (2 DOWNTO 0) ;
   
   signal system_input_window_6_sva: std_logic_vector (2 DOWNTO 0) ;
   
   signal system_input_window_7_sva: std_logic_vector (2 DOWNTO 0) ;
   
   signal system_input_window_8_sva: std_logic_vector (2 DOWNTO 0) ;
   
   signal buffer_buf_vinit_ndx_sva: std_logic_vector (9 DOWNTO 0) ;
   
   signal exit_histogram_init_sva_dfm_1, buffer_sel_1_sva, 
      exit_histogram_init_1_sva, io_read_in_data_vld_rsc_sft_lpi_1_dfm, 
      sfi_io_read_in_data_vld_rsc_lpi_1_dfm: std_logic ;
   
   signal buffer_din_sva_1: std_logic_vector (2 DOWNTO 0) ;
   
   signal buffer_sel_1_sva_dfm: std_logic ;
   
   signal buffer_t0_sva_1: std_logic_vector (2 DOWNTO 0) ;
   
   signal window_0_lpi_1_dfm_2: std_logic_vector (2 DOWNTO 0) ;
   
   signal window_2_lpi_1_dfm_2: std_logic_vector (2 DOWNTO 0) ;
   
   signal window_6_lpi_1_dfm_2: std_logic_vector (2 DOWNTO 0) ;
   
   signal window_8_lpi_1_dfm_2: std_logic_vector (2 DOWNTO 0) ;
   
   signal clip_window_qr_lpi_1_dfm_2: std_logic_vector (2 DOWNTO 0) ;
   
   signal clip_window_qr_1_lpi_1_dfm_2: std_logic_vector (2 DOWNTO 0) ;
   
   signal clip_window_qr_3_lpi_1_dfm_2: std_logic_vector (2 DOWNTO 0) ;
   
   signal asn_sft_3_lpi_1_dfm: std_logic ;
   
   signal median_i_1_lpi_1_dfm_1: std_logic_vector (3 DOWNTO 0) ;
   
   signal exit_if_if_if_1_if_for_lpi_1_dfm_3, exit_Linit_lpi_1_dfm_1, 
      asn_sft_lpi_1_dfm_1: std_logic ;
   
   signal histogram_init_idx_1_lpi_1_dfm_2: std_logic_vector (2 DOWNTO 0) ;
   
   signal asn_sft_2_lpi_1_dfm, exit_Linit_sva_2: std_logic ;
   
   signal median_max2_1_lpi_1_dfm_1: std_logic_vector (2 DOWNTO 0) ;
   
   signal median_max2_2_lpi_1_dfm_1: std_logic_vector (2 DOWNTO 0) ;
   
   signal median_max2_3_lpi_1_dfm_1: std_logic_vector (2 DOWNTO 0) ;
   
   signal median_max2_9_lpi_1_dfm_2: std_logic_vector (2 DOWNTO 0) ;
   
   signal median_max_5_2_lpi_1_dfm_1: std_logic_vector (2 DOWNTO 0) ;
   
   signal median_max_6_2_lpi_1_dfm_1: std_logic_vector (2 DOWNTO 0) ;
   
   signal median_max_8_lpi_1_dfm_1: std_logic_vector (2 DOWNTO 0) ;
   
   signal median_max_7_3_lpi_1_dfm_1: std_logic_vector (2 DOWNTO 0) ;
   
   signal if_if_equal_cse_sva_1: std_logic ;
   
   signal shift_window_slc_system_input_window_23_21_ncse_lpi_1_dfm_1: 
   std_logic_vector (2 DOWNTO 0) ;
   
   signal and_20_psp_1, and_18_psp, and_16_psp, and_14_psp, and_2_psp, 
      and_13_psp, and_15_psp, and_17_psp, buffer_buf_nor_itm_1: std_logic ;
   
   signal histogram_mux_itm_1: std_logic_vector (2 DOWNTO 0) ;
   
   signal buffer_buf_acc_itm_2: std_logic_vector (9 DOWNTO 0) ;
   
   signal io_read_in_data_vld_rsc_sft_lpi_1_dfm_2: std_logic ;
   
   signal median_max_5_lpi_1_dfm_6: std_logic_vector (2 DOWNTO 0) ;
   
   signal buffer_acc_1_itm_2: std_logic_vector (3 DOWNTO 0) ;
   
   signal buffer_acc_3_itm_1: std_logic_vector (3 DOWNTO 0) ;
   
   signal buffer_slc_buffer_c_5_0_1_itm_1: std_logic_vector (5 DOWNTO 0) ;
   
   signal and_32_itm_1, and_33_itm_1, and_34_itm_1, and_35_itm_1: std_logic
    ;
   
   signal asn_39_itm: std_logic_vector (2 DOWNTO 0) ;
   
   signal and_36_itm_1, and_37_itm_1, and_38_itm_1, and_39_itm_1, 
      and_45_itm_1, and_46_itm_1, asn_sft_1_lpi_1_st_1: std_logic ;
   
   signal if_if_read_mem_mcu_data_rsc_sft_lpi_1_st: std_logic_vector
    (31 DOWNTO 0) ;
   
   signal mux_36_itm: std_logic_vector (2 DOWNTO 0) ;
   
   signal thresholding_asn_itm: std_logic_vector (2 DOWNTO 0) ;
   
   signal and_76_itm_1, asn_104_itm, and_64_itm_1, and_66_itm, and_67_itm, 
      and_68_itm, and_69_itm, and_70_itm, and_71_itm, and_72_itm, 
      asn_sft_3_lpi_1_dfm_st_2, exit_Linit_lpi_1_dfm_st_2, 
      exit_Linit_sva_1_st_2, if_if_equal_cse_sva_st_2, main_stage_0_2: 
   std_logic ;
   
   signal p_1_lpi_3: std_logic_vector (2 DOWNTO 0) ;
   
   signal p_1_lpi_1_dfm_8: std_logic_vector (2 DOWNTO 0) ;
   
   signal and_51_cse, and_65_cse_1: std_logic ;
   
   signal z_out_11: std_logic_vector (9 DOWNTO 0) ;
   
   signal nx53617z3, inc_d_0, nx8474z1, inc_d_1, nx8475z1, inc_d_2, nx8476z1, 
      inc_d_3, nx8477z1, inc_d_4, nx8478z1, inc_d_5, nx51682z1, inc_d_6, 
      nx51681z1, inc_d_7, inc_d_0_dup_261, nx18047z1, inc_d_1_dup_263, 
      nx61390z1, inc_d_2_dup_265, nx26339z1, inc_d_3_dup_267, nx17004z1, 
      inc_d_4_dup_269, nx60347z1, inc_d_5_dup_271, nx26395z1, 
      inc_d_6_dup_273, nx8480z1, inc_d_7_dup_274, nx51680z1, inc_d_8, 
      inc_d_0_dup_306, nx20044z1, inc_d_1_dup_309, nx63387z1, 
      inc_d_2_dup_312, nx24342z1, inc_d_3_dup_315, nx19988z1, 
      inc_d_4_dup_318, nx63331z1, inc_d_5_dup_321, nx24398z1, 
      inc_d_6_dup_324, nx3661z1, inc_d_7_dup_327, b_1, 
      mcu_data_rsci_addr_d_3_EXMPLR122: std_logic ;
   
   signal if_if_if_acc_7_nl_0n0s2: std_logic_vector (1 DOWNTO 0) ;
   
   signal if_if_if_acc_6_nl_0n0s2: std_logic_vector (1 DOWNTO 0) ;
   
   signal nx49961z1, nx49961z2, nx49961z3, nx49961z4, nx49961z5, 
      not_fsm_output_4, not_system_input_c_sva_6, NOT_fsm_output_1, 
      nx35918z1, nx36915z1, nx38909z1, nx49340z1, nx50337z1, nx51334z1, 
      nx51358z1, nx50361z1, nx49364z1, nx42672z1, nx41675z1, nx40678z1, 
      nx43534z1, nx42537z2, nx28217z1, nx23219z1, nx34118z1, nx57209z1, 
      nx6197z2, nx6197z1, nx39764z1, nx40761z1, nx41758z1, nx56146z1, 
      nx55149z1, nx54152z1, nx20706z1, nx21703z1, nx22700z1, nx53518z1, 
      nx54515z1, nx55512z1, nx42392z1, nx41395z1, nx40398z1, nx12035z1, 
      nx58700z2, nx58700z1, nx41243z1, nx26076z2, nx51398z9, nx19476z1, 
      nx42113z1, nx60878z1, nx9441z1, nx10438z1, nx11435z1, nx37821z1, 
      nx60110z1, nx59113z1, nx2400z1, nx49337z1, nx1167z1, nx2164z1, 
      nx1007z1, nx48581z3, nx23299z1, nx19409z1, nx18922z1, nx59247z1, 
      nx9489z1, nx63226z9, nx63226z8, nx63226z7, nx63226z6, nx63226z5, 
      nx63226z4, nx63226z3, nx63226z2, nx63226z1, nx50264z1, nx51261z1, 
      nx52258z1, nx53255z1, nx54252z1, nx55249z1, nx56246z2, nx56246z1, 
      nx57243z2, nx57243z1, nx58240z2, nx58240z1, nx59237z1, nx26870z3, 
      nx26870z2, nx37888z2, nx37888z1, nx38885z2, nx38885z1, nx39882z2, 
      nx39882z1, nx7280z13, nx7280z12, nx7280z11, nx7280z10, nx7280z9, 
      nx7280z8, nx7280z7, nx7280z6, nx7280z5, nx7280z4, nx7280z3, nx7280z2, 
      nx7280z1, nx49375z4, nx49375z3, nx49375z2, nx49375z1, nx9271z1, 
      nx8274z1, nx12565z1, nx14557z1, nx46758z1, nx55200z1, nx42537z1, 
      nx59421z1, nx27220z1, nx1894z1, nx9027z1, nx10024z1, nx11021z1, 
      nx13168z1, nx20586z1, nx6587z1, nx12424z1, nx19521z1, nx10870z1, 
      nx11325z1, nx19066z1, nx49457z1, nx51224z1, nx1817z1, nx28574z1, 
      nx58965z1, nx41716z1, nx64568z1, nx29z1, nx28225z1, nx35115z1, 
      nx33121z1, nx32124z1, nx1352z2, nx1352z1, nx17700z1, nx59049z1, 
      nx53914z2, nx58106z1, nx53914z1, nx48779z2, nx48779z1, nx43865z2, 
      nx43865z1, nx49000z2, nx49000z1, nx49000z3, nx54135z2, nx54135z1, 
      nx1352z3, nx13421z1, nx60014z2, nx60014z1, nx23219z2, nx6197z3, 
      nx39764z6, nx39764z2, nx40761z4, nx40761z2, nx39764z7, nx41758z4, 
      nx41758z2, nx56146z4, nx56146z2, nx55149z3, nx55149z2, nx54152z3, 
      nx56146z3, nx54152z2, nx20706z2, nx40761z3, nx20706z5, nx20706z4, 
      nx20706z3, nx41758z3, nx53518z2, nx54515z2, nx55512z2, nx42392z2, 
      nx41395z2, nx39764z5, nx39764z4, nx39764z3, nx40398z2, nx11038z1, 
      nx10041z1, nx9044z4, nx9044z2, nx9044z1, nx59203z1, nx60067z1, 
      nx58700z3, nx35363z1, nx29676z1, nx25079z1, nx24082z1, nx29676z3, 
      nx29676z2, nx26076z1, nx715z1, nx56505z15, nx56505z3, nx56505z2, 
      nx10935z5, nx10935z1, nx56331z1, nx55334z1, nx56505z14, nx56505z12, 
      nx56505z11, nx56505z10, nx56505z5, nx56505z9, nx56505z8, nx56505z7, 
      nx56505z6, nx56505z13, nx56505z4, nx54337z1, nx56505z1, nx46688z2, 
      nx46688z1, nx50292z2, nx50292z1, nx19476z2, nx715z2, nx42113z15, 
      nx42113z14, nx42113z13, nx42113z12, nx42113z11, nx42113z10, nx42113z9, 
      nx42113z8, nx42113z7, nx42113z6, nx42113z5, nx42113z4, nx42113z3, 
      nx42113z2, nx51398z8, nx51398z1, nx7623z2, nx7623z1, nx30850z1, 
      nx60878z2, nx29518z3, nx29518z2, nx29518z1, nx50292z4, nx50292z5, 
      nx60878z3, nx51289z1, nx28896z1, nx27899z1, nx26902z3, nx26902z2, 
      nx26902z1, nx9441z2, nx10438z2, nx11435z2, nx49742z1, nx60878z4, 
      nx60878z5, nx61107z1, nx9044z3, nx10935z4, nx34709z1, nx10935z3, 
      nx10935z2, nx7856z1, nx53617z4, nx53617z2, nx58700z4, nx50292z3, 
      nx53617z1, nx5327z1, nx6324z1, nx7321z1, nx5327z2, nx406z1, nx6324z18, 
      nx6324z15, nx6324z4, nx6324z3, nx6324z2, nx1403z1, nx6324z25, 
      nx51331z1, nx50334z1, nx51331z10, nx24872z1, nx50334z2, nx51331z3, 
      nx6324z14, nx6324z13, nx6324z7, nx6324z6, nx6324z5, nx51331z2, 
      nx23875z4, nx23875z3, nx23875z2, nx23875z1, nx6324z24, nx7321z4, 
      nx23875z47, nx22878z2, nx22878z1, nx6324z8, nx6324z16, nx170z2, 
      nx170z1, nx6324z23, nx6324z17, nx6324z21, nx6324z20, nx6324z19, 
      nx7321z2, nx170z50, nx170z47, nx170z46, nx170z43, nx170z42, nx170z39, 
      nx170z38, nx170z22, nx170z6, nx170z5, nx170z4, nx170z3, nx7321z3, 
      nx6324z22, nx49337z2, nx31361z1, nx30364z1, nx29367z1, nx16594z1, 
      nx17591z1, nx18588z1, nx64549z36, nx64549z1, nx10z2, nx64549z35, 
      nx64549z4, nx64549z3, nx64549z2, nx10z1, nx29367z22, nx29367z20, 
      nx29367z18, nx29367z17, nx29367z16, nx29367z14, nx29367z19, nx10z3, 
      nx29367z21, nx64549z10, nx64549z9, nx64549z8, nx64549z7, nx64549z6, 
      nx64549z5, nx29367z13, nx29367z10, nx29367z9, nx1007z2, nx29367z8, 
      nx29367z7, nx29367z6, nx29367z15, nx29367z4, nx29367z3, nx29367z2, 
      nx48581z24, nx48581z23, nx48581z22, nx48581z21, nx23875z42, nx23875z41, 
      nx23875z40, nx23875z35, nx23875z33, nx23875z7, nx23875z6, nx23875z5, 
      nx48581z20, nx51331z12, nx51331z11, nx29367z12, nx29367z11, nx48581z19, 
      nx51331z38, nx51331z37, nx51331z36, nx51331z35, nx51331z16, nx51331z15, 
      nx51331z14, nx51331z13, nx23875z32, nx23875z29, nx23875z28, nx23875z26, 
      nx23875z25, nx23875z24, nx51331z32, nx51331z29, nx51331z28, nx51331z27, 
      nx51331z26, nx51331z25, nx51331z24, nx51331z23, nx51331z22, nx51331z19, 
      nx51331z18, nx51331z17, nx23875z23, nx23875z22, nx23875z31, nx23875z30, 
      nx23875z27, nx64549z13, nx64549z12, nx64549z11, nx23875z21, nx23875z10, 
      nx23875z9, nx23875z8, nx29367z5, nx7321z37, nx7321z36, nx51331z5, 
      nx51331z4, nx7321z8, nx7321z7, nx7321z6, nx7321z5, nx23875z46, 
      nx23875z43, nx23875z39, nx23875z36, nx51331z42, nx51331z39, nx51331z9, 
      nx51331z8, nx51331z7, nx51331z6, nx51331z21, nx51331z20, nx7321z14, 
      nx7321z11, nx7321z10, nx7321z9, nx23875z19, nx23875z18, nx23875z17, 
      nx23875z16, nx23875z15, nx23875z14, nx23875z20, nx64549z34, nx64549z33, 
      nx64549z32, nx51331z31, nx51331z30, nx64549z31, nx64549z30, nx64549z29, 
      nx64549z28, nx64549z27, nx64549z26, nx64549z25, nx64549z24, nx64549z23, 
      nx64549z22, nx64549z21, nx64549z20, nx64549z19, nx64549z18, nx64549z17, 
      nx64549z16, nx64549z15, nx64549z14, nx23875z13, nx23875z12, nx23875z11, 
      nx23875z34, nx7321z39, nx7321z38, nx6324z12, nx6324z11, nx6324z10, 
      nx170z49, nx170z48, nx6324z9, nx7321z13, nx7321z12, nx170z72, nx170z71, 
      nx170z70, nx51331z41, nx51331z40, nx170z69, nx170z68, nx170z67, 
      nx170z66, nx170z65, nx170z64, nx170z63, nx170z62, nx170z61, nx170z60, 
      nx170z59, nx170z58, nx170z57, nx170z56, nx170z55, nx170z54, nx170z53, 
      nx170z52, nx170z51, nx170z41, nx170z40, nx49337z5, nx49337z4, 
      nx49337z3, nx48581z18, nx48581z17, nx23875z38, nx23875z37, nx48581z16, 
      nx48581z15, nx7321z35, nx7321z34, nx7321z33, nx51331z34, nx51331z33, 
      nx7321z32, nx7321z31, nx7321z30, nx7321z29, nx7321z28, nx7321z27, 
      nx7321z26, nx7321z25, nx7321z24, nx7321z23, nx7321z22, nx7321z21, 
      nx7321z20, nx7321z19, nx7321z18, nx7321z17, nx7321z16, nx7321z15, 
      nx23875z45, nx49337z50, nx49337z49, nx49337z48, nx170z45, nx170z44, 
      nx49337z47, nx49337z46, nx49337z45, nx170z37, nx11174z1, nx170z36, 
      nx170z35, nx170z34, nx170z33, nx170z32, nx170z31, nx170z30, nx51334z2, 
      nx170z29, nx170z28, nx170z27, nx170z26, nx170z25, nx170z24, nx170z23, 
      nx49337z44, nx49337z43, nx49337z42, nx49337z41, nx49337z40, nx49337z39, 
      nx170z21, nx170z20, nx170z19, nx170z18, nx170z17, nx12171z1, nx170z16, 
      nx50361z2, nx170z15, nx170z14, nx170z13, nx170z12, nx170z11, nx170z10, 
      nx170z9, nx170z8, nx170z7, nx49337z38, nx49337z37, nx49337z36, 
      nx49337z35, nx49337z34, nx49337z33, nx49337z32, nx49337z31, nx49337z30, 
      nx49337z29, nx49337z28, nx49337z27, nx13168z2, nx49337z26, nx49337z25, 
      nx49337z24, nx49337z23, nx49337z22, nx49337z21, nx49337z20, nx49337z19, 
      nx49337z18, nx49337z17, nx49337z16, nx49337z15, nx49337z14, nx49337z13, 
      nx49337z12, nx49337z11, nx49337z10, nx49337z9, nx49337z8, nx49337z7, 
      nx49337z6, nx23875z44, nx48581z14, nx48581z13, nx48581z12, nx48581z11, 
      nx48581z10, nx48581z9, nx48581z8, nx48581z7, nx48581z6, nx48581z5, 
      nx48581z4, nx48581z2, nx48581z1, nx59203z7, nx59203z6, nx59203z5, 
      nx59203z4, nx59203z3, nx59203z2, nx19409z2, nx51398z7, nx51398z6, 
      nx51398z5, nx51398z4, nx51398z3, nx51398z2, nx51271z4, nx51271z3, 
      nx51271z1, nx11984z1, nx9489z4, nx9489z3, nx9489z2, nx51271z2: 
   std_logic ;
   
   signal DANGLING : std_logic_vector (30 downto 0 );

begin
   mcu_data_rsci_addr_d(3) <= mcu_data_rsci_addr_d_3_EXMPLR122 ;
   p_fsm_output_4 <= fsm_output(4) ;
   p_exit_if_if_if_1_if_for_lpi_1_dfm_3 <= 
   exit_if_if_if_1_if_for_lpi_1_dfm_3 ;
   if_if_if_2_else_if_acc_nl_inc9_32 : inc_9_0 port map ( cin=>DANGLING(0), 
      a(8)=>nx49961z1, a(7)=>if_if_if_acc_6_nl_0n0s2(1), a(6)=>
      if_if_if_acc_6_nl_0n0s2(0), a(5)=>nx49961z2, a(4)=>nx49961z3, a(3)=>
      if_if_if_acc_7_nl_0n0s2(1), a(2)=>if_if_if_acc_7_nl_0n0s2(0), a(1)=>
      nx49961z4, a(0)=>nx49961z5, d(8)=>DANGLING(1), d(7)=>DANGLING(2), d(6)
      =>DANGLING(3), d(5)=>DANGLING(4), d(4)=>DANGLING(5), d(3)=>DANGLING(6), 
      d(2)=>DANGLING(7), d(1)=>DANGLING(8), d(0)=>DANGLING(9), cout=>
      nx53617z3);
   z_out_11_add10_4 : incdec_10_0 port map ( mode=>NOT_fsm_output_1, cin=>
      DANGLING(10), a(9)=>DANGLING(11), a(8)=>nx63226z1, a(7)=>nx63226z2, 
      a(6)=>nx63226z3, a(5)=>nx63226z4, a(4)=>nx63226z5, a(3)=>nx63226z6, 
      a(2)=>nx63226z7, a(1)=>nx63226z8, a(0)=>nx63226z9, d(9)=>z_out_11(9), 
      d(8)=>z_out_11(8), d(7)=>z_out_11(7), d(6)=>z_out_11(6), d(5)=>
      z_out_11(5), d(4)=>z_out_11(4), d(3)=>z_out_11(3), d(2)=>z_out_11(2), 
      d(1)=>z_out_11(1), d(0)=>z_out_11(0), cout=>DANGLING(12), 
      p_fsm_output_1=>fsm_output(1), p_frame_sva_9=>frame_sva(9), 
      p_buffer_buf_vinit_ndx_sva_9=>buffer_buf_vinit_ndx_sva(9), 
      p_frame_sva_8=>frame_sva(8), p_buffer_buf_vinit_ndx_sva_8=>
      buffer_buf_vinit_ndx_sva(8), p_frame_sva_7=>frame_sva(7), 
      p_buffer_buf_vinit_ndx_sva_7=>buffer_buf_vinit_ndx_sva(7), 
      p_frame_sva_6=>frame_sva(6), p_buffer_buf_vinit_ndx_sva_6=>
      buffer_buf_vinit_ndx_sva(6), p_frame_sva_5=>frame_sva(5), 
      p_buffer_buf_vinit_ndx_sva_5=>buffer_buf_vinit_ndx_sva(5), 
      p_buffer_buf_vinit_ndx_sva_4=>buffer_buf_vinit_ndx_sva(4), 
      p_frame_sva_4=>frame_sva(4), p_frame_sva_3=>frame_sva(3), 
      p_buffer_buf_vinit_ndx_sva_3=>buffer_buf_vinit_ndx_sva(3), 
      p_frame_sva_2=>frame_sva(2), p_buffer_buf_vinit_ndx_sva_2=>
      buffer_buf_vinit_ndx_sva(2), p_frame_sva_1=>frame_sva(1), 
      p_buffer_buf_vinit_ndx_sva_1=>buffer_buf_vinit_ndx_sva(1), 
      p_frame_sva_0=>frame_sva(0), p_buffer_buf_vinit_ndx_sva_0=>
      buffer_buf_vinit_ndx_sva(0));
   reg_q_7 : FDRE port map ( Q=>system_input_r_sva(7), C=>clk, CE=>nx9489z1, 
      D=>inc_d_7, R=>nx51271z1);
   reg_q_6 : FDRE port map ( Q=>system_input_r_sva(6), C=>clk, CE=>nx9489z1, 
      D=>inc_d_6, R=>nx51271z1);
   reg_q_5 : FDRE port map ( Q=>system_input_r_sva(5), C=>clk, CE=>nx9489z1, 
      D=>inc_d_5, R=>nx51271z1);
   reg_q_4 : FDRE port map ( Q=>system_input_r_sva(4), C=>clk, CE=>nx9489z1, 
      D=>inc_d_4, R=>nx51271z1);
   reg_q_3 : FDRE port map ( Q=>system_input_r_sva(3), C=>clk, CE=>nx9489z1, 
      D=>inc_d_3, R=>nx51271z1);
   reg_q_2 : FDRE port map ( Q=>system_input_r_sva(2), C=>clk, CE=>nx9489z1, 
      D=>inc_d_2, R=>nx51271z1);
   reg_q_1 : FDRE port map ( Q=>system_input_r_sva(1), C=>clk, CE=>nx9489z1, 
      D=>inc_d_1, R=>nx51271z1);
   reg_q_0 : FDRE port map ( Q=>system_input_r_sva(0), C=>clk, CE=>nx9489z1, 
      D=>inc_d_0, R=>nx51271z1);
   xorcy_0 : XORCY port map ( O=>inc_d_0, CI=>nx23299z1, LI=>
      system_input_r_sva(0));
   muxcy_0 : MUXCY_L port map ( LO=>nx8474z1, CI=>nx23299z1, DI=>nx35918z1, 
      S=>system_input_r_sva(0));
   xorcy_1 : XORCY port map ( O=>inc_d_1, CI=>nx8474z1, LI=>
      system_input_r_sva(1));
   muxcy_1 : MUXCY_L port map ( LO=>nx8475z1, CI=>nx8474z1, DI=>nx35918z1, S
      =>system_input_r_sva(1));
   xorcy_2 : XORCY port map ( O=>inc_d_2, CI=>nx8475z1, LI=>
      system_input_r_sva(2));
   muxcy_2 : MUXCY_L port map ( LO=>nx8476z1, CI=>nx8475z1, DI=>nx35918z1, S
      =>system_input_r_sva(2));
   xorcy_3 : XORCY port map ( O=>inc_d_3, CI=>nx8476z1, LI=>
      system_input_r_sva(3));
   muxcy_3 : MUXCY_L port map ( LO=>nx8477z1, CI=>nx8476z1, DI=>nx35918z1, S
      =>system_input_r_sva(3));
   xorcy_4 : XORCY port map ( O=>inc_d_4, CI=>nx8477z1, LI=>
      system_input_r_sva(4));
   muxcy_4 : MUXCY_L port map ( LO=>nx8478z1, CI=>nx8477z1, DI=>nx35918z1, S
      =>system_input_r_sva(4));
   xorcy_5 : XORCY port map ( O=>inc_d_5, CI=>nx8478z1, LI=>
      system_input_r_sva(5));
   muxcy_5 : MUXCY_L port map ( LO=>nx51682z1, CI=>nx8478z1, DI=>nx35918z1, 
      S=>system_input_r_sva(5));
   xorcy_6 : XORCY port map ( O=>inc_d_6, CI=>nx51682z1, LI=>
      system_input_r_sva(6));
   muxcy_6 : MUXCY_L port map ( LO=>nx51681z1, CI=>nx51682z1, DI=>nx35918z1, 
      S=>system_input_r_sva(6));
   xorcy_7 : XORCY port map ( O=>inc_d_7, CI=>nx51681z1, LI=>
      system_input_r_sva(7));
   reg_q_8 : FDRE port map ( Q=>system_input_c_sva(8), C=>clk, CE=>nx59247z1, 
      D=>inc_d_8, R=>nx9489z1);
   reg_q_7_dup_0 : FDRE port map ( Q=>system_input_c_sva(7), C=>clk, CE=>
      nx59247z1, D=>inc_d_7_dup_274, R=>nx9489z1);
   reg_q_6_dup_1 : FDRE port map ( Q=>system_input_c_sva(6), C=>clk, CE=>
      nx59247z1, D=>inc_d_6_dup_273, R=>nx9489z1);
   reg_q_5_dup_2 : FDRE port map ( Q=>system_input_c_sva(5), C=>clk, CE=>
      nx59247z1, D=>inc_d_5_dup_271, R=>nx9489z1);
   reg_q_4_dup_3 : FDRE port map ( Q=>system_input_c_sva(4), C=>clk, CE=>
      nx59247z1, D=>inc_d_4_dup_269, R=>nx9489z1);
   reg_q_3_dup_4 : FDRE port map ( Q=>system_input_c_sva(3), C=>clk, CE=>
      nx59247z1, D=>inc_d_3_dup_267, R=>nx9489z1);
   reg_q_2_dup_5 : FDRE port map ( Q=>system_input_c_sva(2), C=>clk, CE=>
      nx59247z1, D=>inc_d_2_dup_265, R=>nx9489z1);
   reg_q_1_dup_6 : FDRE port map ( Q=>system_input_c_sva(1), C=>clk, CE=>
      nx59247z1, D=>inc_d_1_dup_263, R=>nx9489z1);
   reg_q_0_dup_7 : FDRE port map ( Q=>system_input_c_sva(0), C=>clk, CE=>
      nx59247z1, D=>inc_d_0_dup_261, R=>nx9489z1);
   xorcy_0_dup_8 : XORCY port map ( O=>inc_d_0_dup_261, CI=>nx23299z1, LI=>
      system_input_c_sva(0));
   muxcy_0_dup_9 : MUXCY_L port map ( LO=>nx18047z1, CI=>nx23299z1, DI=>
      nx35918z1, S=>system_input_c_sva(0));
   xorcy_1_dup_10 : XORCY port map ( O=>inc_d_1_dup_263, CI=>nx18047z1, LI=>
      system_input_c_sva(1));
   muxcy_1_dup_11 : MUXCY_L port map ( LO=>nx61390z1, CI=>nx18047z1, DI=>
      nx35918z1, S=>system_input_c_sva(1));
   xorcy_2_dup_12 : XORCY port map ( O=>inc_d_2_dup_265, CI=>nx61390z1, LI=>
      system_input_c_sva(2));
   muxcy_2_dup_13 : MUXCY_L port map ( LO=>nx26339z1, CI=>nx61390z1, DI=>
      nx35918z1, S=>system_input_c_sva(2));
   xorcy_3_dup_14 : XORCY port map ( O=>inc_d_3_dup_267, CI=>nx26339z1, LI=>
      system_input_c_sva(3));
   muxcy_3_dup_15 : MUXCY_L port map ( LO=>nx17004z1, CI=>nx26339z1, DI=>
      nx35918z1, S=>system_input_c_sva(3));
   xorcy_4_dup_16 : XORCY port map ( O=>inc_d_4_dup_269, CI=>nx17004z1, LI=>
      system_input_c_sva(4));
   muxcy_4_dup_17 : MUXCY_L port map ( LO=>nx60347z1, CI=>nx17004z1, DI=>
      nx35918z1, S=>system_input_c_sva(4));
   xorcy_5_dup_18 : XORCY port map ( O=>inc_d_5_dup_271, CI=>nx60347z1, LI=>
      system_input_c_sva(5));
   muxcy_5_dup_19 : MUXCY_L port map ( LO=>nx26395z1, CI=>nx60347z1, DI=>
      nx35918z1, S=>system_input_c_sva(5));
   xorcy_6_dup_20 : XORCY port map ( O=>inc_d_6_dup_273, CI=>nx26395z1, LI=>
      system_input_c_sva(6));
   muxcy_6_dup_21 : MUXCY_L port map ( LO=>nx8480z1, CI=>nx26395z1, DI=>
      nx35918z1, S=>system_input_c_sva(6));
   xorcy_7_dup_22 : XORCY port map ( O=>inc_d_7_dup_274, CI=>nx8480z1, LI=>
      system_input_c_sva(7));
   muxcy_7 : MUXCY_L port map ( LO=>nx51680z1, CI=>nx8480z1, DI=>nx35918z1, 
      S=>system_input_c_sva(7));
   xorcy_8 : XORCY port map ( O=>inc_d_8, CI=>nx51680z1, LI=>
      system_input_c_sva(8));
   reg_q_7_dup_23 : FDRE port map ( Q=>system_input_r_filter_sva(7), C=>clk, 
      CE=>nx18922z1, D=>inc_d_7_dup_327, R=>nx19409z1);
   reg_q_6_dup_24 : FDRE port map ( Q=>system_input_r_filter_sva(6), C=>clk, 
      CE=>nx18922z1, D=>inc_d_6_dup_324, R=>nx19409z1);
   reg_q_5_dup_25 : FDRE port map ( Q=>system_input_r_filter_sva(5), C=>clk, 
      CE=>nx18922z1, D=>inc_d_5_dup_321, R=>nx19409z1);
   reg_q_4_dup_26 : FDRE port map ( Q=>system_input_r_filter_sva(4), C=>clk, 
      CE=>nx18922z1, D=>inc_d_4_dup_318, R=>nx19409z1);
   reg_q_3_dup_27 : FDRE port map ( Q=>system_input_r_filter_sva(3), C=>clk, 
      CE=>nx18922z1, D=>inc_d_3_dup_315, R=>nx19409z1);
   reg_q_2_dup_28 : FDRE port map ( Q=>system_input_r_filter_sva(2), C=>clk, 
      CE=>nx18922z1, D=>inc_d_2_dup_312, R=>nx19409z1);
   reg_q_1_dup_29 : FDRE port map ( Q=>system_input_r_filter_sva(1), C=>clk, 
      CE=>nx18922z1, D=>inc_d_1_dup_309, R=>nx19409z1);
   reg_q_0_dup_30 : FDRE port map ( Q=>system_input_r_filter_sva(0), C=>clk, 
      CE=>nx18922z1, D=>inc_d_0_dup_306, R=>nx19409z1);
   xorcy_0_dup_31 : XORCY port map ( O=>inc_d_0_dup_306, CI=>nx23299z1, LI=>
      system_input_r_filter_sva(0));
   muxcy_0_dup_32 : MUXCY_L port map ( LO=>nx20044z1, CI=>nx23299z1, DI=>
      nx35918z1, S=>system_input_r_filter_sva(0));
   xorcy_1_dup_33 : XORCY port map ( O=>inc_d_1_dup_309, CI=>nx20044z1, LI=>
      system_input_r_filter_sva(1));
   muxcy_1_dup_34 : MUXCY_L port map ( LO=>nx63387z1, CI=>nx20044z1, DI=>
      nx35918z1, S=>system_input_r_filter_sva(1));
   xorcy_2_dup_35 : XORCY port map ( O=>inc_d_2_dup_312, CI=>nx63387z1, LI=>
      system_input_r_filter_sva(2));
   muxcy_2_dup_36 : MUXCY_L port map ( LO=>nx24342z1, CI=>nx63387z1, DI=>
      nx35918z1, S=>system_input_r_filter_sva(2));
   xorcy_3_dup_37 : XORCY port map ( O=>inc_d_3_dup_315, CI=>nx24342z1, LI=>
      system_input_r_filter_sva(3));
   muxcy_3_dup_38 : MUXCY_L port map ( LO=>nx19988z1, CI=>nx24342z1, DI=>
      nx35918z1, S=>system_input_r_filter_sva(3));
   xorcy_4_dup_39 : XORCY port map ( O=>inc_d_4_dup_318, CI=>nx19988z1, LI=>
      system_input_r_filter_sva(4));
   muxcy_4_dup_40 : MUXCY_L port map ( LO=>nx63331z1, CI=>nx19988z1, DI=>
      nx35918z1, S=>system_input_r_filter_sva(4));
   xorcy_5_dup_41 : XORCY port map ( O=>inc_d_5_dup_321, CI=>nx63331z1, LI=>
      system_input_r_filter_sva(5));
   muxcy_5_dup_42 : MUXCY_L port map ( LO=>nx24398z1, CI=>nx63331z1, DI=>
      nx35918z1, S=>system_input_r_filter_sva(5));
   xorcy_6_dup_43 : XORCY port map ( O=>inc_d_6_dup_324, CI=>nx24398z1, LI=>
      system_input_r_filter_sva(6));
   muxcy_6_dup_44 : MUXCY_L port map ( LO=>nx3661z1, CI=>nx24398z1, DI=>
      nx35918z1, S=>system_input_r_filter_sva(6));
   xorcy_7_dup_45 : XORCY port map ( O=>inc_d_7_dup_327, CI=>nx3661z1, LI=>
      system_input_r_filter_sva(7));
   ix56505z4670 : GND port map ( G=>b_1);
   modgen_inc_39 : inc_17_0 port map ( cin=>fsm_output(3), a(16)=>DANGLING(
      13), a(15)=>DANGLING(14), a(14)=>DANGLING(15), a(13)=>DANGLING(16), 
      a(12)=>DANGLING(17), a(11)=>DANGLING(18), a(10)=>DANGLING(19), a(9)=>
      DANGLING(20), a(8)=>DANGLING(21), a(7)=>DANGLING(22), a(6)=>DANGLING(
      23), a(5)=>DANGLING(24), a(4)=>DANGLING(25), a(3)=>DANGLING(26), a(2)
      =>DANGLING(27), a(1)=>DANGLING(28), a(0)=>DANGLING(29), d(16)=>
      histogram_rsci_data_in_d(16), d(15)=>histogram_rsci_data_in_d(15), 
      d(14)=>histogram_rsci_data_in_d(14), d(13)=>
      histogram_rsci_data_in_d(13), d(12)=>histogram_rsci_data_in_d(12), 
      d(11)=>histogram_rsci_data_in_d(11), d(10)=>
      histogram_rsci_data_in_d(10), d(9)=>histogram_rsci_data_in_d(9), d(8)
      =>histogram_rsci_data_in_d(8), d(7)=>histogram_rsci_data_in_d(7), d(6)
      =>histogram_rsci_data_in_d(6), d(5)=>histogram_rsci_data_in_d(5), d(4)
      =>histogram_rsci_data_in_d(4), d(3)=>histogram_rsci_data_in_d(3), d(2)
      =>histogram_rsci_data_in_d(2), d(1)=>histogram_rsci_data_in_d(1), d(0)
      =>histogram_rsci_data_in_d(0), cout=>DANGLING(30), 
      p_histogram_rsci_data_out_d_16=>histogram_rsci_data_out_d(16), 
      p_histogram_rsci_data_out_d_15=>histogram_rsci_data_out_d(15), 
      p_histogram_rsci_data_out_d_14=>histogram_rsci_data_out_d(14), 
      p_histogram_rsci_data_out_d_13=>histogram_rsci_data_out_d(13), 
      p_histogram_rsci_data_out_d_12=>histogram_rsci_data_out_d(12), 
      p_histogram_rsci_data_out_d_11=>histogram_rsci_data_out_d(11), 
      p_histogram_rsci_data_out_d_10=>histogram_rsci_data_out_d(10), 
      p_histogram_rsci_data_out_d_9=>histogram_rsci_data_out_d(9), 
      p_histogram_rsci_data_out_d_8=>histogram_rsci_data_out_d(8), 
      p_histogram_rsci_data_out_d_7=>histogram_rsci_data_out_d(7), 
      p_histogram_rsci_data_out_d_6=>histogram_rsci_data_out_d(6), 
      p_histogram_rsci_data_out_d_5=>histogram_rsci_data_out_d(5), 
      p_histogram_rsci_data_out_d_4=>histogram_rsci_data_out_d(4), 
      p_histogram_rsci_data_out_d_3=>histogram_rsci_data_out_d(3), 
      p_histogram_rsci_data_out_d_2=>histogram_rsci_data_out_d(2), 
      p_histogram_rsci_data_out_d_1=>histogram_rsci_data_out_d(1), 
      p_histogram_rsci_data_out_d_0=>histogram_rsci_data_out_d(0));
   reg_out_data_rsci_d_1 : FDRE port map ( Q=>out_data_rsc_z(2), C=>clk, CE
      =>nx48581z1, D=>nx48581z3, R=>rst);
   reg_median_max2_1_lpi_1_dfm_1_2 : FDR port map ( Q=>
      median_max2_1_lpi_1_dfm_1(2), C=>clk, D=>nx1007z1, R=>rst);
   reg_median_max2_1_lpi_1_dfm_1_1 : FDR port map ( Q=>
      median_max2_1_lpi_1_dfm_1(1), C=>clk, D=>nx10z1, R=>rst);
   reg_median_max2_1_lpi_1_dfm_1_0 : FDR port map ( Q=>
      median_max2_1_lpi_1_dfm_1(0), C=>clk, D=>nx64549z1, R=>rst);
   reg_median_max2_2_lpi_1_dfm_1_2 : FDR port map ( Q=>
      median_max2_2_lpi_1_dfm_1(2), C=>clk, D=>nx18588z1, R=>rst);
   reg_median_max2_2_lpi_1_dfm_1_1 : FDR port map ( Q=>
      median_max2_2_lpi_1_dfm_1(1), C=>clk, D=>nx17591z1, R=>rst);
   reg_median_max2_2_lpi_1_dfm_1_0 : FDR port map ( Q=>
      median_max2_2_lpi_1_dfm_1(0), C=>clk, D=>nx16594z1, R=>rst);
   reg_median_max2_3_lpi_1_dfm_1_2 : FDR port map ( Q=>
      median_max2_3_lpi_1_dfm_1(2), C=>clk, D=>nx29367z1, R=>rst);
   reg_median_max2_3_lpi_1_dfm_1_1 : FDR port map ( Q=>
      median_max2_3_lpi_1_dfm_1(1), C=>clk, D=>nx30364z1, R=>rst);
   reg_median_max2_3_lpi_1_dfm_1_0 : FDR port map ( Q=>
      median_max2_3_lpi_1_dfm_1(0), C=>clk, D=>nx31361z1, R=>rst);
   reg_median_max2_9_lpi_1_dfm_2_2 : FDR port map ( Q=>
      median_max2_9_lpi_1_dfm_2(2), C=>clk, D=>nx2164z1, R=>rst);
   reg_median_max2_9_lpi_1_dfm_2_1 : FDR port map ( Q=>
      median_max2_9_lpi_1_dfm_2(1), C=>clk, D=>nx1167z1, R=>rst);
   reg_median_max2_9_lpi_1_dfm_2_0 : FDR port map ( Q=>
      median_max2_9_lpi_1_dfm_2(0), C=>clk, D=>nx170z1, R=>rst);
   reg_median_max_5_2_lpi_1_dfm_1_2 : FDR port map ( Q=>
      median_max_5_2_lpi_1_dfm_1(2), C=>clk, D=>nx22878z1, R=>rst);
   reg_median_max_5_2_lpi_1_dfm_1_1 : FDR port map ( Q=>
      median_max_5_2_lpi_1_dfm_1(1), C=>clk, D=>nx23875z1, R=>rst);
   reg_median_max_5_2_lpi_1_dfm_1_0 : FDR port map ( Q=>
      median_max_5_2_lpi_1_dfm_1(0), C=>clk, D=>nx24872z1, R=>rst);
   reg_median_max_6_2_lpi_1_dfm_1_2 : FDR port map ( Q=>
      median_max_6_2_lpi_1_dfm_1(2), C=>clk, D=>nx49337z1, R=>rst);
   reg_median_max_6_2_lpi_1_dfm_1_1 : FDR port map ( Q=>
      median_max_6_2_lpi_1_dfm_1(1), C=>clk, D=>nx50334z1, R=>rst);
   reg_median_max_6_2_lpi_1_dfm_1_0 : FDR port map ( Q=>
      median_max_6_2_lpi_1_dfm_1(0), C=>clk, D=>nx51331z1, R=>rst);
   reg_median_max_8_lpi_1_dfm_1_2 : FDR port map ( Q=>
      median_max_8_lpi_1_dfm_1(2), C=>clk, D=>nx2400z1, R=>rst);
   reg_median_max_8_lpi_1_dfm_1_1 : FDR port map ( Q=>
      median_max_8_lpi_1_dfm_1(1), C=>clk, D=>nx1403z1, R=>rst);
   reg_median_max_8_lpi_1_dfm_1_0 : FDR port map ( Q=>
      median_max_8_lpi_1_dfm_1(0), C=>clk, D=>nx406z1, R=>rst);
   reg_median_max_7_3_lpi_1_dfm_1_2 : FDR port map ( Q=>
      median_max_7_3_lpi_1_dfm_1(2), C=>clk, D=>nx7321z1, R=>rst);
   reg_median_max_7_3_lpi_1_dfm_1_1 : FDR port map ( Q=>
      median_max_7_3_lpi_1_dfm_1(1), C=>clk, D=>nx6324z1, R=>rst);
   reg_median_max_7_3_lpi_1_dfm_1_0 : FDR port map ( Q=>
      median_max_7_3_lpi_1_dfm_1(0), C=>clk, D=>nx5327z1, R=>rst);
   reg_asn_39_itm_2 : FDR port map ( Q=>asn_39_itm(2), C=>clk, D=>
      system_input_window_8_sva(2), R=>rst);
   reg_asn_39_itm_1 : FDR port map ( Q=>asn_39_itm(1), C=>clk, D=>
      system_input_window_8_sva(1), R=>rst);
   reg_asn_39_itm_0 : FDR port map ( Q=>asn_39_itm(0), C=>clk, D=>
      system_input_window_8_sva(0), R=>rst);
   reg_buffer_din_sva_1_2 : FDR port map ( Q=>buffer_din_sva_1(2), C=>clk, D
      =>in_data_rsc_z(2), R=>rst);
   reg_buffer_din_sva_1_1 : FDR port map ( Q=>buffer_din_sva_1(1), C=>clk, D
      =>in_data_rsc_z(1), R=>rst);
   reg_buffer_din_sva_1_0 : FDR port map ( Q=>buffer_din_sva_1(0), C=>clk, D
      =>in_data_rsc_z(0), R=>rst);
   reg_buffer_t0_sva_1_2 : FDR port map ( Q=>buffer_t0_sva_1(2), C=>clk, D=>
      buffer_buf_rsci_data_out_d(2), R=>rst);
   reg_buffer_t0_sva_1_1 : FDR port map ( Q=>buffer_t0_sva_1(1), C=>clk, D=>
      buffer_buf_rsci_data_out_d(1), R=>rst);
   reg_buffer_t0_sva_1_0 : FDR port map ( Q=>buffer_t0_sva_1(0), C=>clk, D=>
      buffer_buf_rsci_data_out_d(0), R=>rst);
   reg_and_76_itm_1 : FDR port map ( Q=>and_76_itm_1, C=>clk, D=>nx53617z1, 
      R=>rst);
   reg_asn_104_itm : FDR port map ( Q=>asn_104_itm, C=>clk, D=>
      exit_histogram_init_1_sva, R=>rst);
   reg_and_65_cse_1 : FDR port map ( Q=>and_65_cse_1, C=>clk, D=>nx7856z1, R
      =>rst);
   reg_buffer_acc_1_itm_2_3 : FDR port map ( Q=>buffer_acc_1_itm_2(3), C=>
      clk, D=>nx59113z1, R=>rst);
   reg_buffer_acc_1_itm_2_2 : FDR port map ( Q=>buffer_acc_1_itm_2(2), C=>
      clk, D=>nx60110z1, R=>rst);
   reg_buffer_acc_1_itm_2_1 : FDR port map ( Q=>buffer_acc_1_itm_2(1), C=>
      clk, D=>nx61107z1, R=>rst);
   reg_buffer_acc_1_itm_2_0 : FDR port map ( Q=>buffer_acc_1_itm_2(0), C=>
      clk, D=>not_system_input_c_sva_6, R=>rst);
   reg_asn_sft_1_lpi_1_st_1 : FDR port map ( Q=>asn_sft_1_lpi_1_st_1, C=>clk, 
      D=>asn_sft_1_lpi_1, R=>rst);
   reg_asn_sft_lpi_1_dfm_1 : FDR port map ( Q=>asn_sft_lpi_1_dfm_1, C=>clk, 
      D=>nx37821z1, R=>rst);
   reg_histogram_init_idx_1_lpi_1_dfm_2_2 : FDR port map ( Q=>
      histogram_init_idx_1_lpi_1_dfm_2(2), C=>clk, D=>nx60878z5, R=>rst);
   reg_histogram_init_idx_1_lpi_1_dfm_2_1 : FDR port map ( Q=>
      histogram_init_idx_1_lpi_1_dfm_2(1), C=>clk, D=>nx60878z4, R=>rst);
   reg_histogram_init_idx_1_lpi_1_dfm_2_0 : FDR port map ( Q=>
      histogram_init_idx_1_lpi_1_dfm_2(0), C=>clk, D=>nx49742z1, R=>rst);
   reg_mux_36_itm_2 : FDR port map ( Q=>mux_36_itm(2), C=>clk, D=>nx11435z1, 
      R=>rst);
   reg_mux_36_itm_1 : FDR port map ( Q=>mux_36_itm(1), C=>clk, D=>nx10438z1, 
      R=>rst);
   reg_mux_36_itm_0 : FDR port map ( Q=>mux_36_itm(0), C=>clk, D=>nx9441z1, 
      R=>rst);
   reg_main_stage_0_2 : FDR port map ( Q=>main_stage_0_2, C=>clk, D=>
      fsm_output(6), R=>rst);
   reg_io_read_in_data_vld_rsc_sft_lpi_1_dfm_2 : FDR port map ( Q=>
      io_read_in_data_vld_rsc_sft_lpi_1_dfm_2, C=>clk, D=>
      io_read_in_data_vld_rsc_sft_lpi_1_dfm, R=>rst);
   reg_asn_sft_3_lpi_1_dfm_st_2 : FDR port map ( Q=>asn_sft_3_lpi_1_dfm_st_2, 
      C=>clk, D=>asn_sft_3_lpi_1_dfm, R=>rst);
   reg_exit_Linit_lpi_1_dfm_st_2 : FDR port map ( Q=>
      exit_Linit_lpi_1_dfm_st_2, C=>clk, D=>exit_Linit_lpi_1_dfm_1, R=>rst);
   reg_exit_Linit_sva_1_st_2 : FDR port map ( Q=>exit_Linit_sva_1_st_2, C=>
      clk, D=>exit_Linit_sva_2, R=>rst);
   reg_if_if_equal_cse_sva_st_2 : FDR port map ( Q=>if_if_equal_cse_sva_st_2, 
      C=>clk, D=>if_if_equal_cse_sva_1, R=>rst);
   reg_median_max_5_lpi_1_dfm_6_2 : FDR port map ( Q=>
      median_max_5_lpi_1_dfm_6(2), C=>clk, D=>nx26902z1, R=>rst);
   reg_median_max_5_lpi_1_dfm_6_1 : FDR port map ( Q=>
      median_max_5_lpi_1_dfm_6(1), C=>clk, D=>nx27899z1, R=>rst);
   reg_median_max_5_lpi_1_dfm_6_0 : FDR port map ( Q=>
      median_max_5_lpi_1_dfm_6(0), C=>clk, D=>nx28896z1, R=>rst);
   reg_histogram_mux_itm_1_2 : FDR port map ( Q=>histogram_mux_itm_1(2), C=>
      clk, D=>nx51289z1, R=>rst);
   reg_histogram_mux_itm_1_1 : FDR port map ( Q=>histogram_mux_itm_1(1), C=>
      clk, D=>nx50292z4, R=>rst);
   reg_histogram_mux_itm_1_0 : FDR port map ( Q=>histogram_mux_itm_1(0), C=>
      clk, D=>nx50292z5, R=>rst);
   reg_buffer_buf_acc_itm_2_9 : FDR port map ( Q=>buffer_buf_acc_itm_2(9), C
      =>clk, D=>z_out_11(9), R=>rst);
   reg_buffer_buf_acc_itm_2_8 : FDR port map ( Q=>buffer_buf_acc_itm_2(8), C
      =>clk, D=>z_out_11(8), R=>rst);
   reg_buffer_buf_acc_itm_2_7 : FDR port map ( Q=>buffer_buf_acc_itm_2(7), C
      =>clk, D=>z_out_11(7), R=>rst);
   reg_buffer_buf_acc_itm_2_6 : FDR port map ( Q=>buffer_buf_acc_itm_2(6), C
      =>clk, D=>z_out_11(6), R=>rst);
   reg_buffer_buf_acc_itm_2_5 : FDR port map ( Q=>buffer_buf_acc_itm_2(5), C
      =>clk, D=>z_out_11(5), R=>rst);
   reg_buffer_buf_acc_itm_2_4 : FDR port map ( Q=>buffer_buf_acc_itm_2(4), C
      =>clk, D=>z_out_11(4), R=>rst);
   reg_buffer_buf_acc_itm_2_3 : FDR port map ( Q=>buffer_buf_acc_itm_2(3), C
      =>clk, D=>z_out_11(3), R=>rst);
   reg_buffer_buf_acc_itm_2_2 : FDR port map ( Q=>buffer_buf_acc_itm_2(2), C
      =>clk, D=>z_out_11(2), R=>rst);
   reg_buffer_buf_acc_itm_2_1 : FDR port map ( Q=>buffer_buf_acc_itm_2(1), C
      =>clk, D=>z_out_11(1), R=>rst);
   reg_buffer_buf_acc_itm_2_0 : FDR port map ( Q=>buffer_buf_acc_itm_2(0), C
      =>clk, D=>z_out_11(0), R=>rst);
   reg_buffer_buf_nor_itm_1 : FDR port map ( Q=>buffer_buf_nor_itm_1, C=>clk, 
      D=>nx29518z1, R=>rst);
   reg_exit_histogram_init_sva_dfm_1 : FDR port map ( Q=>
      exit_histogram_init_sva_dfm_1, C=>clk, D=>nx60878z1, R=>rst);
   reg_in_data_rsci_ld : FDR port map ( Q=>in_data_rsc_lz, C=>clk, D=>
      nx11984z1, R=>rst);
   reg_out_data_rsci_ld : FDR port map ( Q=>out_data_rsc_lz, C=>clk, D=>
      nx48581z1, R=>rst);
   reg_histogram_init_idx_sva_2 : FDRE port map ( Q=>
      histogram_init_idx_sva(2), C=>clk, CE=>fsm_output(2), D=>
      histogram_mux_itm_1(2), R=>rst);
   reg_histogram_init_idx_sva_1 : FDRE port map ( Q=>
      histogram_init_idx_sva(1), C=>clk, CE=>fsm_output(2), D=>
      histogram_mux_itm_1(1), R=>rst);
   reg_histogram_init_idx_sva_0 : FDRE port map ( Q=>
      histogram_init_idx_sva(0), C=>clk, CE=>fsm_output(2), D=>
      histogram_mux_itm_1(0), R=>rst);
   reg_buffer_buf_vinit_ndx_sva_9 : FDSE port map ( Q=>
      buffer_buf_vinit_ndx_sva(9), C=>clk, CE=>fsm_output(2), D=>
      buffer_buf_acc_itm_2(9), S=>rst);
   reg_buffer_buf_vinit_ndx_sva_8 : FDRE port map ( Q=>
      buffer_buf_vinit_ndx_sva(8), C=>clk, CE=>fsm_output(2), D=>
      buffer_buf_acc_itm_2(8), R=>rst);
   reg_buffer_buf_vinit_ndx_sva_7 : FDRE port map ( Q=>
      buffer_buf_vinit_ndx_sva(7), C=>clk, CE=>fsm_output(2), D=>
      buffer_buf_acc_itm_2(7), R=>rst);
   reg_buffer_buf_vinit_ndx_sva_6 : FDSE port map ( Q=>
      buffer_buf_vinit_ndx_sva(6), C=>clk, CE=>fsm_output(2), D=>
      buffer_buf_acc_itm_2(6), S=>rst);
   reg_buffer_buf_vinit_ndx_sva_5 : FDSE port map ( Q=>
      buffer_buf_vinit_ndx_sva(5), C=>clk, CE=>fsm_output(2), D=>
      buffer_buf_acc_itm_2(5), S=>rst);
   reg_buffer_buf_vinit_ndx_sva_4 : FDSE port map ( Q=>
      buffer_buf_vinit_ndx_sva(4), C=>clk, CE=>fsm_output(2), D=>
      buffer_buf_acc_itm_2(4), S=>rst);
   reg_buffer_buf_vinit_ndx_sva_3 : FDSE port map ( Q=>
      buffer_buf_vinit_ndx_sva(3), C=>clk, CE=>fsm_output(2), D=>
      buffer_buf_acc_itm_2(3), S=>rst);
   reg_buffer_buf_vinit_ndx_sva_2 : FDSE port map ( Q=>
      buffer_buf_vinit_ndx_sva(2), C=>clk, CE=>fsm_output(2), D=>
      buffer_buf_acc_itm_2(2), S=>rst);
   reg_buffer_buf_vinit_ndx_sva_1 : FDSE port map ( Q=>
      buffer_buf_vinit_ndx_sva(1), C=>clk, CE=>fsm_output(2), D=>
      buffer_buf_acc_itm_2(1), S=>rst);
   reg_buffer_buf_vinit_ndx_sva_0 : FDSE port map ( Q=>
      buffer_buf_vinit_ndx_sva(0), C=>clk, CE=>fsm_output(2), D=>
      buffer_buf_acc_itm_2(0), S=>rst);
   reg_exit_histogram_init_sva : FDRE port map ( Q=>exit_histogram_init_sva, 
      C=>clk, CE=>fsm_output(2), D=>exit_histogram_init_sva_dfm_1, R=>rst);
   reg_threshold_sva_2 : FDSE port map ( Q=>threshold_sva(2), C=>clk, CE=>
      nx30850z1, D=>mcu_data_rsci_data_out_d(2), S=>rst);
   reg_threshold_sva_1 : FDRE port map ( Q=>threshold_sva(1), C=>clk, CE=>
      nx30850z1, D=>mcu_data_rsci_data_out_d(1), R=>rst);
   reg_threshold_sva_0 : FDRE port map ( Q=>threshold_sva(0), C=>clk, CE=>
      nx30850z1, D=>mcu_data_rsci_data_out_d(0), R=>rst);
   reg_frame_sva_9 : FDRE port map ( Q=>frame_sva(9), C=>clk, CE=>nx7623z1, 
      D=>z_out_11(9), R=>rst);
   reg_frame_sva_8 : FDRE port map ( Q=>frame_sva(8), C=>clk, CE=>nx7623z1, 
      D=>z_out_11(8), R=>rst);
   reg_frame_sva_7 : FDRE port map ( Q=>frame_sva(7), C=>clk, CE=>nx7623z1, 
      D=>z_out_11(7), R=>rst);
   reg_frame_sva_6 : FDRE port map ( Q=>frame_sva(6), C=>clk, CE=>nx7623z1, 
      D=>z_out_11(6), R=>rst);
   reg_frame_sva_5 : FDRE port map ( Q=>frame_sva(5), C=>clk, CE=>nx7623z1, 
      D=>z_out_11(5), R=>rst);
   reg_frame_sva_4 : FDRE port map ( Q=>frame_sva(4), C=>clk, CE=>nx7623z1, 
      D=>z_out_11(4), R=>rst);
   reg_frame_sva_3 : FDRE port map ( Q=>frame_sva(3), C=>clk, CE=>nx7623z1, 
      D=>z_out_11(3), R=>rst);
   reg_frame_sva_2 : FDRE port map ( Q=>frame_sva(2), C=>clk, CE=>nx7623z1, 
      D=>z_out_11(2), R=>rst);
   reg_frame_sva_1 : FDRE port map ( Q=>frame_sva(1), C=>clk, CE=>nx7623z1, 
      D=>z_out_11(1), R=>rst);
   reg_frame_sva_0 : FDSE port map ( Q=>frame_sva(0), C=>clk, CE=>nx7623z1, 
      D=>z_out_11(0), S=>rst);
   reg_exit_if_if_if_1_if_for_lpi_1 : FDRE port map ( Q=>
      exit_if_if_if_1_if_for_lpi_1, C=>clk, CE=>nx51398z1, D=>nx42113z1, R=>
      rst);
   reg_sfi_if_if_read_mem_mcu_data_rsc_lpi_1 : FDRE port map ( Q=>
      sfi_if_if_read_mem_mcu_data_rsc_lpi_1, C=>clk, CE=>nx51398z1, D=>
      nx19476z1, R=>rst);
   reg_exit_Linit_lpi_1 : FDRE port map ( Q=>exit_Linit_lpi_1, C=>clk, CE=>
      nx51398z1, D=>nx51398z9, R=>rst);
   reg_histogram_init_idx_1_lpi_1_2 : FDRE port map ( Q=>
      histogram_init_idx_1_lpi_1(2), C=>clk, CE=>nx50292z1, D=>nx51289z1, R
      =>rst);
   reg_histogram_init_idx_1_lpi_1_1 : FDRE port map ( Q=>
      histogram_init_idx_1_lpi_1(1), C=>clk, CE=>nx50292z1, D=>nx50292z4, R
      =>rst);
   reg_histogram_init_idx_1_lpi_1_0 : FDRE port map ( Q=>
      histogram_init_idx_1_lpi_1(0), C=>clk, CE=>nx50292z1, D=>nx50292z5, R
      =>rst);
   reg_sfi_system_input_land_1_lpi_1 : FDRE port map ( Q=>
      sfi_system_input_land_1_lpi_1, C=>clk, CE=>nx46688z1, D=>nx46688z2, R
      =>rst);
   reg_if_if_if_acc_5_psp_lpi_1_2 : FDRE port map ( Q=>
      if_if_if_acc_5_psp_lpi_1(2), C=>clk, CE=>nx56505z1, D=>nx54337z1, R=>
      rst);
   reg_if_if_if_acc_5_psp_lpi_1_1 : FDRE port map ( Q=>
      if_if_if_acc_5_psp_lpi_1(1), C=>clk, CE=>nx56505z1, D=>nx55334z1, R=>
      rst);
   reg_if_if_if_acc_5_psp_lpi_1_0 : FDRE port map ( Q=>
      if_if_if_acc_5_psp_lpi_1(0), C=>clk, CE=>nx56505z1, D=>nx56331z1, R=>
      rst);
   reg_asn_sft_lpi_1 : FDRE port map ( Q=>asn_sft_lpi_1, C=>clk, CE=>
      nx10935z1, D=>if_if_equal_cse_lpi_1, R=>rst);
   reg_if_if_equal_cse_lpi_1 : FDRE port map ( Q=>if_if_equal_cse_lpi_1, C=>
      clk, CE=>nx56505z1, D=>nx56505z2, R=>rst);
   reg_asn_sft_1_lpi_1 : FDRE port map ( Q=>asn_sft_1_lpi_1, C=>clk, CE=>
      nx715z1, D=>system_input_land_1_lpi_1, R=>rst);
   reg_p_1_lpi_3_2 : FDRE port map ( Q=>p_1_lpi_3(2), C=>clk, CE=>nx26076z1, 
      D=>nx24082z1, R=>rst);
   reg_p_1_lpi_3_1 : FDRE port map ( Q=>p_1_lpi_3(1), C=>clk, CE=>nx26076z1, 
      D=>nx25079z1, R=>rst);
   reg_p_1_lpi_3_0 : FDRE port map ( Q=>p_1_lpi_3(0), C=>clk, CE=>nx26076z1, 
      D=>nx26076z2, R=>rst);
   reg_asn_sft_2_lpi_1 : FDRE port map ( Q=>asn_sft_2_lpi_1, C=>clk, CE=>
      nx29676z1, D=>if_if_equal_cse_lpi_1, R=>rst);
   reg_system_input_window_8_sva_2 : FDRE port map ( Q=>
      system_input_window_8_sva(2), C=>clk, CE=>nx35363z1, D=>
      in_data_rsc_z(2), R=>rst);
   reg_system_input_window_8_sva_1 : FDRE port map ( Q=>
      system_input_window_8_sva(1), C=>clk, CE=>nx35363z1, D=>
      in_data_rsc_z(1), R=>rst);
   reg_system_input_window_8_sva_0 : FDRE port map ( Q=>
      system_input_window_8_sva(0), C=>clk, CE=>nx35363z1, D=>
      in_data_rsc_z(0), R=>rst);
   reg_system_input_window_4_sva_2 : FDRE port map ( Q=>
      system_input_window_4_sva(2), C=>clk, CE=>nx11984z1, D=>
      system_input_window_7_sva(2), R=>rst);
   reg_system_input_window_4_sva_1 : FDRE port map ( Q=>
      system_input_window_4_sva(1), C=>clk, CE=>nx11984z1, D=>
      system_input_window_7_sva(1), R=>rst);
   reg_system_input_window_4_sva_0 : FDRE port map ( Q=>
      system_input_window_4_sva(0), C=>clk, CE=>nx11984z1, D=>
      system_input_window_7_sva(0), R=>rst);
   reg_system_input_window_5_sva_2 : FDRE port map ( Q=>
      system_input_window_5_sva(2), C=>clk, CE=>nx11984z1, D=>
      system_input_window_8_sva(2), R=>rst);
   reg_system_input_window_5_sva_1 : FDRE port map ( Q=>
      system_input_window_5_sva(1), C=>clk, CE=>nx11984z1, D=>
      system_input_window_8_sva(1), R=>rst);
   reg_system_input_window_5_sva_0 : FDRE port map ( Q=>
      system_input_window_5_sva(0), C=>clk, CE=>nx11984z1, D=>
      system_input_window_8_sva(0), R=>rst);
   reg_system_input_window_3_sva_2 : FDRE port map ( Q=>
      system_input_window_3_sva(2), C=>clk, CE=>nx11984z1, D=>
      system_input_window_6_sva(2), R=>rst);
   reg_system_input_window_3_sva_1 : FDRE port map ( Q=>
      system_input_window_3_sva(1), C=>clk, CE=>nx11984z1, D=>
      system_input_window_6_sva(1), R=>rst);
   reg_system_input_window_3_sva_0 : FDRE port map ( Q=>
      system_input_window_3_sva(0), C=>clk, CE=>nx11984z1, D=>
      system_input_window_6_sva(0), R=>rst);
   reg_system_input_output_vld_sva : FDRE port map ( Q=>
      system_input_output_vld_sva, C=>clk, CE=>nx11984z1, D=>nx41243z1, R=>
      rst);
   reg_system_input_c_filter_sva_8 : FDRE port map ( Q=>
      system_input_c_filter_sva(8), C=>clk, CE=>nx11984z1, D=>
      system_input_c_sva(8), R=>rst);
   reg_system_input_c_filter_sva_7 : FDRE port map ( Q=>
      system_input_c_filter_sva(7), C=>clk, CE=>nx11984z1, D=>
      system_input_c_sva(7), R=>rst);
   reg_system_input_c_filter_sva_6 : FDRE port map ( Q=>
      system_input_c_filter_sva(6), C=>clk, CE=>nx11984z1, D=>
      system_input_c_sva(6), R=>rst);
   reg_system_input_c_filter_sva_5 : FDRE port map ( Q=>
      system_input_c_filter_sva(5), C=>clk, CE=>nx11984z1, D=>
      system_input_c_sva(5), R=>rst);
   reg_system_input_c_filter_sva_4 : FDRE port map ( Q=>
      system_input_c_filter_sva(4), C=>clk, CE=>nx11984z1, D=>
      system_input_c_sva(4), R=>rst);
   reg_system_input_c_filter_sva_3 : FDRE port map ( Q=>
      system_input_c_filter_sva(3), C=>clk, CE=>nx11984z1, D=>
      system_input_c_sva(3), R=>rst);
   reg_system_input_c_filter_sva_2 : FDRE port map ( Q=>
      system_input_c_filter_sva(2), C=>clk, CE=>nx11984z1, D=>
      system_input_c_sva(2), R=>rst);
   reg_system_input_c_filter_sva_1 : FDRE port map ( Q=>
      system_input_c_filter_sva(1), C=>clk, CE=>nx11984z1, D=>
      system_input_c_sva(1), R=>rst);
   reg_system_input_c_filter_sva_0 : FDRE port map ( Q=>
      system_input_c_filter_sva(0), C=>clk, CE=>nx11984z1, D=>
      system_input_c_sva(0), R=>rst);
   reg_exit_histogram_init_1_sva : FDRE port map ( Q=>
      exit_histogram_init_1_sva, C=>clk, CE=>nx58700z1, D=>nx58700z2, R=>rst
   );
   reg_system_input_land_1_lpi_1 : FDRE port map ( Q=>
      system_input_land_1_lpi_1, C=>clk, CE=>nx60067z1, D=>nx59203z1, R=>rst
   );
   reg_asn_sft_3_lpi_1 : FDRE port map ( Q=>asn_sft_3_lpi_1, C=>clk, CE=>
      nx60067z1, D=>nx41243z1, R=>rst);
   reg_shift_window_slc_system_input_window_23_21_ncse_lpi_1_dfm_1_2 : FDRE
       port map ( Q=>
      shift_window_slc_system_input_window_23_21_ncse_lpi_1_dfm_1(2), C=>clk, 
      CE=>nx60067z1, D=>system_input_window_7_sva(2), R=>rst);
   reg_shift_window_slc_system_input_window_23_21_ncse_lpi_1_dfm_1_1 : FDRE
       port map ( Q=>
      shift_window_slc_system_input_window_23_21_ncse_lpi_1_dfm_1(1), C=>clk, 
      CE=>nx60067z1, D=>system_input_window_7_sva(1), R=>rst);
   reg_shift_window_slc_system_input_window_23_21_ncse_lpi_1_dfm_1_0 : FDRE
       port map ( Q=>
      shift_window_slc_system_input_window_23_21_ncse_lpi_1_dfm_1(0), C=>clk, 
      CE=>nx60067z1, D=>system_input_window_7_sva(0), R=>rst);
   reg_median_i_1_lpi_2_3 : FDRE port map ( Q=>median_i_1_lpi_2(3), C=>clk, 
      CE=>nx9044z1, D=>nx9044z2, R=>rst);
   reg_median_i_1_lpi_2_2 : FDRE port map ( Q=>median_i_1_lpi_2(2), C=>clk, 
      CE=>nx9044z1, D=>nx10041z1, R=>rst);
   reg_median_i_1_lpi_2_1 : FDRE port map ( Q=>median_i_1_lpi_2(1), C=>clk, 
      CE=>nx9044z1, D=>nx11038z1, R=>rst);
   reg_median_i_1_lpi_2_0 : FDRE port map ( Q=>median_i_1_lpi_2(0), C=>clk, 
      CE=>nx9044z1, D=>nx12035z1, R=>rst);
   reg_clip_window_qr_3_lpi_1_2 : FDRE port map ( Q=>
      clip_window_qr_3_lpi_1(2), C=>clk, CE=>nx9044z1, D=>nx40398z1, R=>rst
   );
   reg_clip_window_qr_3_lpi_1_1 : FDRE port map ( Q=>
      clip_window_qr_3_lpi_1(1), C=>clk, CE=>nx9044z1, D=>nx41395z1, R=>rst
   );
   reg_clip_window_qr_3_lpi_1_0 : FDRE port map ( Q=>
      clip_window_qr_3_lpi_1(0), C=>clk, CE=>nx9044z1, D=>nx42392z1, R=>rst
   );
   reg_clip_window_qr_1_lpi_1_2 : FDRE port map ( Q=>
      clip_window_qr_1_lpi_1(2), C=>clk, CE=>nx9044z1, D=>nx55512z1, R=>rst
   );
   reg_clip_window_qr_1_lpi_1_1 : FDRE port map ( Q=>
      clip_window_qr_1_lpi_1(1), C=>clk, CE=>nx9044z1, D=>nx54515z1, R=>rst
   );
   reg_clip_window_qr_1_lpi_1_0 : FDRE port map ( Q=>
      clip_window_qr_1_lpi_1(0), C=>clk, CE=>nx9044z1, D=>nx53518z1, R=>rst
   );
   reg_clip_window_qr_lpi_1_2 : FDRE port map ( Q=>clip_window_qr_lpi_1(2), 
      C=>clk, CE=>nx9044z1, D=>nx22700z1, R=>rst);
   reg_clip_window_qr_lpi_1_1 : FDRE port map ( Q=>clip_window_qr_lpi_1(1), 
      C=>clk, CE=>nx9044z1, D=>nx21703z1, R=>rst);
   reg_clip_window_qr_lpi_1_0 : FDRE port map ( Q=>clip_window_qr_lpi_1(0), 
      C=>clk, CE=>nx9044z1, D=>nx20706z1, R=>rst);
   reg_window_2_lpi_1_2 : FDRE port map ( Q=>window_2_lpi_1(2), C=>clk, CE=>
      nx9044z1, D=>nx54152z1, R=>rst);
   reg_window_2_lpi_1_1 : FDRE port map ( Q=>window_2_lpi_1(1), C=>clk, CE=>
      nx9044z1, D=>nx55149z1, R=>rst);
   reg_window_2_lpi_1_0 : FDRE port map ( Q=>window_2_lpi_1(0), C=>clk, CE=>
      nx9044z1, D=>nx56146z1, R=>rst);
   reg_window_0_lpi_1_2 : FDRE port map ( Q=>window_0_lpi_1(2), C=>clk, CE=>
      nx9044z1, D=>nx41758z1, R=>rst);
   reg_window_0_lpi_1_1 : FDRE port map ( Q=>window_0_lpi_1(1), C=>clk, CE=>
      nx9044z1, D=>nx40761z1, R=>rst);
   reg_window_0_lpi_1_0 : FDRE port map ( Q=>window_0_lpi_1(0), C=>clk, CE=>
      nx9044z1, D=>nx39764z1, R=>rst);
   reg_buffer_sel_1_sva : FDRE port map ( Q=>buffer_sel_1_sva, C=>clk, CE=>
      nx6197z1, D=>nx6197z2, R=>rst);
   reg_if_if_read_mem_mcu_data_rsc_sft_lpi_1_st_31 : FDRE port map ( Q=>
      if_if_read_mem_mcu_data_rsc_sft_lpi_1_st(31), C=>clk, CE=>
      not_fsm_output_4, D=>if_if_read_mem_mcu_data_rsc_sft_lpi_1(31), R=>rst
   );
   reg_if_if_read_mem_mcu_data_rsc_sft_lpi_1_st_30 : FDRE port map ( Q=>
      if_if_read_mem_mcu_data_rsc_sft_lpi_1_st(30), C=>clk, CE=>
      not_fsm_output_4, D=>if_if_read_mem_mcu_data_rsc_sft_lpi_1(30), R=>rst
   );
   reg_if_if_read_mem_mcu_data_rsc_sft_lpi_1_st_29 : FDRE port map ( Q=>
      if_if_read_mem_mcu_data_rsc_sft_lpi_1_st(29), C=>clk, CE=>
      not_fsm_output_4, D=>if_if_read_mem_mcu_data_rsc_sft_lpi_1(29), R=>rst
   );
   reg_if_if_read_mem_mcu_data_rsc_sft_lpi_1_st_28 : FDRE port map ( Q=>
      if_if_read_mem_mcu_data_rsc_sft_lpi_1_st(28), C=>clk, CE=>
      not_fsm_output_4, D=>if_if_read_mem_mcu_data_rsc_sft_lpi_1(28), R=>rst
   );
   reg_if_if_read_mem_mcu_data_rsc_sft_lpi_1_st_27 : FDRE port map ( Q=>
      if_if_read_mem_mcu_data_rsc_sft_lpi_1_st(27), C=>clk, CE=>
      not_fsm_output_4, D=>if_if_read_mem_mcu_data_rsc_sft_lpi_1(27), R=>rst
   );
   reg_if_if_read_mem_mcu_data_rsc_sft_lpi_1_st_26 : FDRE port map ( Q=>
      if_if_read_mem_mcu_data_rsc_sft_lpi_1_st(26), C=>clk, CE=>
      not_fsm_output_4, D=>if_if_read_mem_mcu_data_rsc_sft_lpi_1(26), R=>rst
   );
   reg_if_if_read_mem_mcu_data_rsc_sft_lpi_1_st_25 : FDRE port map ( Q=>
      if_if_read_mem_mcu_data_rsc_sft_lpi_1_st(25), C=>clk, CE=>
      not_fsm_output_4, D=>if_if_read_mem_mcu_data_rsc_sft_lpi_1(25), R=>rst
   );
   reg_if_if_read_mem_mcu_data_rsc_sft_lpi_1_st_24 : FDRE port map ( Q=>
      if_if_read_mem_mcu_data_rsc_sft_lpi_1_st(24), C=>clk, CE=>
      not_fsm_output_4, D=>if_if_read_mem_mcu_data_rsc_sft_lpi_1(24), R=>rst
   );
   reg_if_if_read_mem_mcu_data_rsc_sft_lpi_1_st_23 : FDRE port map ( Q=>
      if_if_read_mem_mcu_data_rsc_sft_lpi_1_st(23), C=>clk, CE=>
      not_fsm_output_4, D=>if_if_read_mem_mcu_data_rsc_sft_lpi_1(23), R=>rst
   );
   reg_if_if_read_mem_mcu_data_rsc_sft_lpi_1_st_22 : FDRE port map ( Q=>
      if_if_read_mem_mcu_data_rsc_sft_lpi_1_st(22), C=>clk, CE=>
      not_fsm_output_4, D=>if_if_read_mem_mcu_data_rsc_sft_lpi_1(22), R=>rst
   );
   reg_if_if_read_mem_mcu_data_rsc_sft_lpi_1_st_21 : FDRE port map ( Q=>
      if_if_read_mem_mcu_data_rsc_sft_lpi_1_st(21), C=>clk, CE=>
      not_fsm_output_4, D=>if_if_read_mem_mcu_data_rsc_sft_lpi_1(21), R=>rst
   );
   reg_if_if_read_mem_mcu_data_rsc_sft_lpi_1_st_20 : FDRE port map ( Q=>
      if_if_read_mem_mcu_data_rsc_sft_lpi_1_st(20), C=>clk, CE=>
      not_fsm_output_4, D=>if_if_read_mem_mcu_data_rsc_sft_lpi_1(20), R=>rst
   );
   reg_if_if_read_mem_mcu_data_rsc_sft_lpi_1_st_19 : FDRE port map ( Q=>
      if_if_read_mem_mcu_data_rsc_sft_lpi_1_st(19), C=>clk, CE=>
      not_fsm_output_4, D=>if_if_read_mem_mcu_data_rsc_sft_lpi_1(19), R=>rst
   );
   reg_if_if_read_mem_mcu_data_rsc_sft_lpi_1_st_18 : FDRE port map ( Q=>
      if_if_read_mem_mcu_data_rsc_sft_lpi_1_st(18), C=>clk, CE=>
      not_fsm_output_4, D=>if_if_read_mem_mcu_data_rsc_sft_lpi_1(18), R=>rst
   );
   reg_if_if_read_mem_mcu_data_rsc_sft_lpi_1_st_17 : FDRE port map ( Q=>
      if_if_read_mem_mcu_data_rsc_sft_lpi_1_st(17), C=>clk, CE=>
      not_fsm_output_4, D=>if_if_read_mem_mcu_data_rsc_sft_lpi_1(17), R=>rst
   );
   reg_if_if_read_mem_mcu_data_rsc_sft_lpi_1_st_16 : FDRE port map ( Q=>
      if_if_read_mem_mcu_data_rsc_sft_lpi_1_st(16), C=>clk, CE=>
      not_fsm_output_4, D=>if_if_read_mem_mcu_data_rsc_sft_lpi_1(16), R=>rst
   );
   reg_if_if_read_mem_mcu_data_rsc_sft_lpi_1_st_15 : FDRE port map ( Q=>
      if_if_read_mem_mcu_data_rsc_sft_lpi_1_st(15), C=>clk, CE=>
      not_fsm_output_4, D=>if_if_read_mem_mcu_data_rsc_sft_lpi_1(15), R=>rst
   );
   reg_if_if_read_mem_mcu_data_rsc_sft_lpi_1_st_14 : FDRE port map ( Q=>
      if_if_read_mem_mcu_data_rsc_sft_lpi_1_st(14), C=>clk, CE=>
      not_fsm_output_4, D=>if_if_read_mem_mcu_data_rsc_sft_lpi_1(14), R=>rst
   );
   reg_if_if_read_mem_mcu_data_rsc_sft_lpi_1_st_13 : FDRE port map ( Q=>
      if_if_read_mem_mcu_data_rsc_sft_lpi_1_st(13), C=>clk, CE=>
      not_fsm_output_4, D=>if_if_read_mem_mcu_data_rsc_sft_lpi_1(13), R=>rst
   );
   reg_if_if_read_mem_mcu_data_rsc_sft_lpi_1_st_12 : FDRE port map ( Q=>
      if_if_read_mem_mcu_data_rsc_sft_lpi_1_st(12), C=>clk, CE=>
      not_fsm_output_4, D=>if_if_read_mem_mcu_data_rsc_sft_lpi_1(12), R=>rst
   );
   reg_if_if_read_mem_mcu_data_rsc_sft_lpi_1_st_11 : FDRE port map ( Q=>
      if_if_read_mem_mcu_data_rsc_sft_lpi_1_st(11), C=>clk, CE=>
      not_fsm_output_4, D=>if_if_read_mem_mcu_data_rsc_sft_lpi_1(11), R=>rst
   );
   reg_if_if_read_mem_mcu_data_rsc_sft_lpi_1_st_10 : FDRE port map ( Q=>
      if_if_read_mem_mcu_data_rsc_sft_lpi_1_st(10), C=>clk, CE=>
      not_fsm_output_4, D=>if_if_read_mem_mcu_data_rsc_sft_lpi_1(10), R=>rst
   );
   reg_if_if_read_mem_mcu_data_rsc_sft_lpi_1_st_9 : FDRE port map ( Q=>
      if_if_read_mem_mcu_data_rsc_sft_lpi_1_st(9), C=>clk, CE=>
      not_fsm_output_4, D=>if_if_read_mem_mcu_data_rsc_sft_lpi_1(9), R=>rst
   );
   reg_if_if_read_mem_mcu_data_rsc_sft_lpi_1_st_8 : FDRE port map ( Q=>
      if_if_read_mem_mcu_data_rsc_sft_lpi_1_st(8), C=>clk, CE=>
      not_fsm_output_4, D=>if_if_read_mem_mcu_data_rsc_sft_lpi_1(8), R=>rst
   );
   reg_if_if_read_mem_mcu_data_rsc_sft_lpi_1_st_7 : FDRE port map ( Q=>
      if_if_read_mem_mcu_data_rsc_sft_lpi_1_st(7), C=>clk, CE=>
      not_fsm_output_4, D=>if_if_read_mem_mcu_data_rsc_sft_lpi_1(7), R=>rst
   );
   reg_if_if_read_mem_mcu_data_rsc_sft_lpi_1_st_6 : FDRE port map ( Q=>
      if_if_read_mem_mcu_data_rsc_sft_lpi_1_st(6), C=>clk, CE=>
      not_fsm_output_4, D=>if_if_read_mem_mcu_data_rsc_sft_lpi_1(6), R=>rst
   );
   reg_if_if_read_mem_mcu_data_rsc_sft_lpi_1_st_5 : FDRE port map ( Q=>
      if_if_read_mem_mcu_data_rsc_sft_lpi_1_st(5), C=>clk, CE=>
      not_fsm_output_4, D=>if_if_read_mem_mcu_data_rsc_sft_lpi_1(5), R=>rst
   );
   reg_if_if_read_mem_mcu_data_rsc_sft_lpi_1_st_4 : FDRE port map ( Q=>
      if_if_read_mem_mcu_data_rsc_sft_lpi_1_st(4), C=>clk, CE=>
      not_fsm_output_4, D=>if_if_read_mem_mcu_data_rsc_sft_lpi_1(4), R=>rst
   );
   reg_if_if_read_mem_mcu_data_rsc_sft_lpi_1_st_3 : FDRE port map ( Q=>
      if_if_read_mem_mcu_data_rsc_sft_lpi_1_st(3), C=>clk, CE=>
      not_fsm_output_4, D=>if_if_read_mem_mcu_data_rsc_sft_lpi_1(3), R=>rst
   );
   reg_if_if_read_mem_mcu_data_rsc_sft_lpi_1_st_2 : FDRE port map ( Q=>
      if_if_read_mem_mcu_data_rsc_sft_lpi_1_st(2), C=>clk, CE=>
      not_fsm_output_4, D=>if_if_read_mem_mcu_data_rsc_sft_lpi_1(2), R=>rst
   );
   reg_if_if_read_mem_mcu_data_rsc_sft_lpi_1_st_1 : FDRE port map ( Q=>
      if_if_read_mem_mcu_data_rsc_sft_lpi_1_st(1), C=>clk, CE=>
      not_fsm_output_4, D=>if_if_read_mem_mcu_data_rsc_sft_lpi_1(1), R=>rst
   );
   reg_if_if_read_mem_mcu_data_rsc_sft_lpi_1_st_0 : FDRE port map ( Q=>
      if_if_read_mem_mcu_data_rsc_sft_lpi_1_st(0), C=>clk, CE=>
      not_fsm_output_4, D=>if_if_read_mem_mcu_data_rsc_sft_lpi_1(0), R=>rst
   );
   reg_io_read_in_data_vld_rsc_sft_lpi_1_dfm : FDRE port map ( Q=>
      io_read_in_data_vld_rsc_sft_lpi_1_dfm, C=>clk, CE=>fsm_output(3), D=>
      nx57209z1, R=>rst);
   reg_and_51_cse : FDRE port map ( Q=>and_51_cse, C=>clk, CE=>fsm_output(3), 
      D=>nx34709z1, R=>rst);
   reg_and_64_itm_1 : FDRE port map ( Q=>and_64_itm_1, C=>clk, CE=>
      not_fsm_output_4, D=>nx60014z1, R=>rst);
   reg_and_72_itm : FDRE port map ( Q=>and_72_itm, C=>clk, CE=>fsm_output(3), 
      D=>nx54135z1, R=>rst);
   reg_and_71_itm : FDRE port map ( Q=>and_71_itm, C=>clk, CE=>fsm_output(3), 
      D=>nx49000z1, R=>rst);
   reg_and_70_itm : FDRE port map ( Q=>and_70_itm, C=>clk, CE=>fsm_output(3), 
      D=>nx43865z1, R=>rst);
   reg_and_69_itm : FDRE port map ( Q=>and_69_itm, C=>clk, CE=>fsm_output(3), 
      D=>nx48779z1, R=>rst);
   reg_and_68_itm : FDRE port map ( Q=>and_68_itm, C=>clk, CE=>fsm_output(3), 
      D=>nx53914z1, R=>rst);
   reg_and_67_itm : FDRE port map ( Q=>and_67_itm, C=>clk, CE=>fsm_output(3), 
      D=>nx59049z1, R=>rst);
   reg_and_66_itm : FDRE port map ( Q=>and_66_itm, C=>clk, CE=>fsm_output(3), 
      D=>nx1352z1, R=>rst);
   reg_thresholding_asn_itm_2 : FDRE port map ( Q=>thresholding_asn_itm(2), 
      C=>clk, CE=>mcu_data_rsci_addr_d_3_EXMPLR122, D=>threshold_sva(2), R=>
      rst);
   reg_thresholding_asn_itm_1 : FDRE port map ( Q=>thresholding_asn_itm(1), 
      C=>clk, CE=>mcu_data_rsci_addr_d_3_EXMPLR122, D=>threshold_sva(1), R=>
      rst);
   reg_thresholding_asn_itm_0 : FDRE port map ( Q=>thresholding_asn_itm(0), 
      C=>clk, CE=>mcu_data_rsci_addr_d_3_EXMPLR122, D=>threshold_sva(0), R=>
      rst);
   reg_buffer_acc_3_itm_1_3 : FDRE port map ( Q=>buffer_acc_3_itm_1(3), C=>
      clk, CE=>not_fsm_output_4, D=>nx32124z1, R=>rst);
   reg_buffer_acc_3_itm_1_2 : FDRE port map ( Q=>buffer_acc_3_itm_1(2), C=>
      clk, CE=>not_fsm_output_4, D=>nx33121z1, R=>rst);
   reg_buffer_acc_3_itm_1_1 : FDRE port map ( Q=>buffer_acc_3_itm_1(1), C=>
      clk, CE=>not_fsm_output_4, D=>nx34118z1, R=>rst);
   reg_buffer_acc_3_itm_1_0 : FDRE port map ( Q=>buffer_acc_3_itm_1(0), C=>
      clk, CE=>not_fsm_output_4, D=>nx35115z1, R=>rst);
   reg_buffer_slc_buffer_c_5_0_1_itm_1_5 : FDRE port map ( Q=>
      buffer_slc_buffer_c_5_0_1_itm_1(5), C=>clk, CE=>not_fsm_output_4, D=>
      system_input_c_sva(5), R=>rst);
   reg_buffer_slc_buffer_c_5_0_1_itm_1_4 : FDRE port map ( Q=>
      buffer_slc_buffer_c_5_0_1_itm_1(4), C=>clk, CE=>not_fsm_output_4, D=>
      system_input_c_sva(4), R=>rst);
   reg_buffer_slc_buffer_c_5_0_1_itm_1_3 : FDRE port map ( Q=>
      buffer_slc_buffer_c_5_0_1_itm_1(3), C=>clk, CE=>not_fsm_output_4, D=>
      system_input_c_sva(3), R=>rst);
   reg_buffer_slc_buffer_c_5_0_1_itm_1_2 : FDRE port map ( Q=>
      buffer_slc_buffer_c_5_0_1_itm_1(2), C=>clk, CE=>not_fsm_output_4, D=>
      system_input_c_sva(2), R=>rst);
   reg_buffer_slc_buffer_c_5_0_1_itm_1_1 : FDRE port map ( Q=>
      buffer_slc_buffer_c_5_0_1_itm_1(1), C=>clk, CE=>not_fsm_output_4, D=>
      system_input_c_sva(1), R=>rst);
   reg_buffer_slc_buffer_c_5_0_1_itm_1_0 : FDRE port map ( Q=>
      buffer_slc_buffer_c_5_0_1_itm_1(0), C=>clk, CE=>not_fsm_output_4, D=>
      system_input_c_sva(0), R=>rst);
   reg_buffer_sel_1_sva_dfm : FDRE port map ( Q=>buffer_sel_1_sva_dfm, C=>
      clk, CE=>not_fsm_output_4, D=>nx23219z1, R=>rst);
   reg_sfi_io_read_in_data_vld_rsc_lpi_1_dfm : FDRE port map ( Q=>
      sfi_io_read_in_data_vld_rsc_lpi_1_dfm, C=>clk, CE=>not_fsm_output_4, D
      =>exit_histogram_init_1_sva, R=>rst);
   reg_asn_sft_3_lpi_1_dfm : FDRE port map ( Q=>asn_sft_3_lpi_1_dfm, C=>clk, 
      CE=>nx28225z1, D=>nx10935z3, R=>rst);
   reg_exit_if_if_if_1_if_for_lpi_1_dfm_3 : FDRE port map ( Q=>
      exit_if_if_if_1_if_for_lpi_1_dfm_3, C=>clk, CE=>not_fsm_output_4, D=>
      nx715z2, R=>rst);
   reg_asn_sft_2_lpi_1_dfm : FDRE port map ( Q=>asn_sft_2_lpi_1_dfm, C=>clk, 
      CE=>not_fsm_output_4, D=>nx42113z14, R=>rst);
   reg_p_1_lpi_1_dfm_8_2 : FDRE port map ( Q=>p_1_lpi_1_dfm_8(2), C=>clk, CE
      =>not_fsm_output_4, D=>nx29z1, R=>rst);
   reg_p_1_lpi_1_dfm_8_1 : FDRE port map ( Q=>p_1_lpi_1_dfm_8(1), C=>clk, CE
      =>not_fsm_output_4, D=>nx64568z1, R=>rst);
   reg_p_1_lpi_1_dfm_8_0 : FDRE port map ( Q=>p_1_lpi_1_dfm_8(0), C=>clk, CE
      =>not_fsm_output_4, D=>nx42113z15, R=>rst);
   reg_exit_Linit_lpi_1_dfm_1 : FDRE port map ( Q=>exit_Linit_lpi_1_dfm_1, C
      =>clk, CE=>nx28225z1, D=>nx10935z4, R=>rst);
   reg_and_35_itm_1 : FDRE port map ( Q=>and_35_itm_1, C=>clk, CE=>
      not_fsm_output_4, D=>nx41716z1, R=>rst);
   reg_and_34_itm_1 : FDRE port map ( Q=>and_34_itm_1, C=>clk, CE=>
      not_fsm_output_4, D=>nx58965z1, R=>rst);
   reg_and_33_itm_1 : FDRE port map ( Q=>and_33_itm_1, C=>clk, CE=>
      not_fsm_output_4, D=>nx28574z1, R=>rst);
   reg_and_32_itm_1 : FDRE port map ( Q=>and_32_itm_1, C=>clk, CE=>
      not_fsm_output_4, D=>nx1817z1, R=>rst);
   reg_and_39_itm_1 : FDRE port map ( Q=>and_39_itm_1, C=>clk, CE=>
      not_fsm_output_4, D=>nx51224z1, R=>rst);
   reg_and_38_itm_1 : FDRE port map ( Q=>and_38_itm_1, C=>clk, CE=>
      not_fsm_output_4, D=>nx49457z1, R=>rst);
   reg_and_37_itm_1 : FDRE port map ( Q=>and_37_itm_1, C=>clk, CE=>
      not_fsm_output_4, D=>nx19066z1, R=>rst);
   reg_and_36_itm_1 : FDRE port map ( Q=>and_36_itm_1, C=>clk, CE=>
      not_fsm_output_4, D=>nx11325z1, R=>rst);
   reg_and_46_itm_1 : FDRE port map ( Q=>and_46_itm_1, C=>clk, CE=>
      not_fsm_output_4, D=>nx10870z1, R=>rst);
   reg_and_45_itm_1 : FDRE port map ( Q=>and_45_itm_1, C=>clk, CE=>
      not_fsm_output_4, D=>nx19521z1, R=>rst);
   reg_median_i_1_lpi_1_dfm_1_3 : FDRE port map ( Q=>
      median_i_1_lpi_1_dfm_1(3), C=>clk, CE=>not_fsm_output_4, D=>nx9044z3, 
      R=>rst);
   reg_median_i_1_lpi_1_dfm_1_2 : FDRE port map ( Q=>
      median_i_1_lpi_1_dfm_1(2), C=>clk, CE=>not_fsm_output_4, D=>nx12424z1, 
      R=>rst);
   reg_median_i_1_lpi_1_dfm_1_1 : FDRE port map ( Q=>
      median_i_1_lpi_1_dfm_1(1), C=>clk, CE=>not_fsm_output_4, D=>nx13421z1, 
      R=>rst);
   reg_median_i_1_lpi_1_dfm_1_0 : FDRE port map ( Q=>
      median_i_1_lpi_1_dfm_1(0), C=>clk, CE=>not_fsm_output_4, D=>nx9044z4, 
      R=>rst);
   reg_window_0_lpi_1_dfm_2_2 : FDRE port map ( Q=>window_0_lpi_1_dfm_2(2), 
      C=>clk, CE=>not_fsm_output_4, D=>nx41758z1, R=>rst);
   reg_window_0_lpi_1_dfm_2_1 : FDRE port map ( Q=>window_0_lpi_1_dfm_2(1), 
      C=>clk, CE=>not_fsm_output_4, D=>nx40761z1, R=>rst);
   reg_window_0_lpi_1_dfm_2_0 : FDRE port map ( Q=>window_0_lpi_1_dfm_2(0), 
      C=>clk, CE=>not_fsm_output_4, D=>nx39764z1, R=>rst);
   reg_clip_window_qr_lpi_1_dfm_2_2 : FDRE port map ( Q=>
      clip_window_qr_lpi_1_dfm_2(2), C=>clk, CE=>not_fsm_output_4, D=>
      nx22700z1, R=>rst);
   reg_clip_window_qr_lpi_1_dfm_2_1 : FDRE port map ( Q=>
      clip_window_qr_lpi_1_dfm_2(1), C=>clk, CE=>not_fsm_output_4, D=>
      nx21703z1, R=>rst);
   reg_clip_window_qr_lpi_1_dfm_2_0 : FDRE port map ( Q=>
      clip_window_qr_lpi_1_dfm_2(0), C=>clk, CE=>not_fsm_output_4, D=>
      nx20706z1, R=>rst);
   reg_window_2_lpi_1_dfm_2_2 : FDRE port map ( Q=>window_2_lpi_1_dfm_2(2), 
      C=>clk, CE=>not_fsm_output_4, D=>nx54152z1, R=>rst);
   reg_window_2_lpi_1_dfm_2_1 : FDRE port map ( Q=>window_2_lpi_1_dfm_2(1), 
      C=>clk, CE=>not_fsm_output_4, D=>nx55149z1, R=>rst);
   reg_window_2_lpi_1_dfm_2_0 : FDRE port map ( Q=>window_2_lpi_1_dfm_2(0), 
      C=>clk, CE=>not_fsm_output_4, D=>nx56146z1, R=>rst);
   reg_clip_window_qr_3_lpi_1_dfm_2_2 : FDRE port map ( Q=>
      clip_window_qr_3_lpi_1_dfm_2(2), C=>clk, CE=>not_fsm_output_4, D=>
      nx40398z1, R=>rst);
   reg_clip_window_qr_3_lpi_1_dfm_2_1 : FDRE port map ( Q=>
      clip_window_qr_3_lpi_1_dfm_2(1), C=>clk, CE=>not_fsm_output_4, D=>
      nx41395z1, R=>rst);
   reg_clip_window_qr_3_lpi_1_dfm_2_0 : FDRE port map ( Q=>
      clip_window_qr_3_lpi_1_dfm_2(0), C=>clk, CE=>not_fsm_output_4, D=>
      nx42392z1, R=>rst);
   reg_clip_window_qr_1_lpi_1_dfm_2_2 : FDRE port map ( Q=>
      clip_window_qr_1_lpi_1_dfm_2(2), C=>clk, CE=>not_fsm_output_4, D=>
      nx55512z1, R=>rst);
   reg_clip_window_qr_1_lpi_1_dfm_2_1 : FDRE port map ( Q=>
      clip_window_qr_1_lpi_1_dfm_2(1), C=>clk, CE=>not_fsm_output_4, D=>
      nx54515z1, R=>rst);
   reg_clip_window_qr_1_lpi_1_dfm_2_0 : FDRE port map ( Q=>
      clip_window_qr_1_lpi_1_dfm_2(0), C=>clk, CE=>not_fsm_output_4, D=>
      nx53518z1, R=>rst);
   reg_and_20_psp_1 : FDRE port map ( Q=>and_20_psp_1, C=>clk, CE=>
      not_fsm_output_4, D=>nx60014z2, R=>rst);
   reg_exit_Linit_sva_2 : FDRE port map ( Q=>exit_Linit_sva_2, C=>clk, CE=>
      nx28225z1, D=>nx9044z3, R=>rst);
   reg_if_if_equal_cse_sva_1 : FDRE port map ( Q=>if_if_equal_cse_sva_1, C=>
      clk, CE=>nx28225z1, D=>nx56505z2, R=>rst);
   reg_median_max_0_lpi_1_2 : FDRE port map ( Q=>median_max_0_lpi_1(2), C=>
      clk, CE=>nx6587z1, D=>nx28217z1, R=>rst);
   reg_median_max_0_lpi_1_1 : FDRE port map ( Q=>median_max_0_lpi_1(1), C=>
      clk, CE=>nx6587z1, D=>nx42537z2, R=>rst);
   reg_median_max_0_lpi_1_0 : FDRE port map ( Q=>median_max_0_lpi_1(0), C=>
      clk, CE=>nx6587z1, D=>nx43534z1, R=>rst);
   reg_if_if_read_mem_mcu_data_rsc_sft_lpi_1_31 : FDRE port map ( Q=>
      if_if_read_mem_mcu_data_rsc_sft_lpi_1(31), C=>clk, CE=>nx20586z1, D=>
      mcu_data_rsci_data_out_d(31), R=>rst);
   reg_if_if_read_mem_mcu_data_rsc_sft_lpi_1_30 : FDRE port map ( Q=>
      if_if_read_mem_mcu_data_rsc_sft_lpi_1(30), C=>clk, CE=>nx20586z1, D=>
      mcu_data_rsci_data_out_d(30), R=>rst);
   reg_if_if_read_mem_mcu_data_rsc_sft_lpi_1_29 : FDRE port map ( Q=>
      if_if_read_mem_mcu_data_rsc_sft_lpi_1(29), C=>clk, CE=>nx20586z1, D=>
      mcu_data_rsci_data_out_d(29), R=>rst);
   reg_if_if_read_mem_mcu_data_rsc_sft_lpi_1_28 : FDRE port map ( Q=>
      if_if_read_mem_mcu_data_rsc_sft_lpi_1(28), C=>clk, CE=>nx20586z1, D=>
      mcu_data_rsci_data_out_d(28), R=>rst);
   reg_if_if_read_mem_mcu_data_rsc_sft_lpi_1_27 : FDRE port map ( Q=>
      if_if_read_mem_mcu_data_rsc_sft_lpi_1(27), C=>clk, CE=>nx20586z1, D=>
      mcu_data_rsci_data_out_d(27), R=>rst);
   reg_if_if_read_mem_mcu_data_rsc_sft_lpi_1_26 : FDRE port map ( Q=>
      if_if_read_mem_mcu_data_rsc_sft_lpi_1(26), C=>clk, CE=>nx20586z1, D=>
      mcu_data_rsci_data_out_d(26), R=>rst);
   reg_if_if_read_mem_mcu_data_rsc_sft_lpi_1_25 : FDRE port map ( Q=>
      if_if_read_mem_mcu_data_rsc_sft_lpi_1(25), C=>clk, CE=>nx20586z1, D=>
      mcu_data_rsci_data_out_d(25), R=>rst);
   reg_if_if_read_mem_mcu_data_rsc_sft_lpi_1_24 : FDRE port map ( Q=>
      if_if_read_mem_mcu_data_rsc_sft_lpi_1(24), C=>clk, CE=>nx20586z1, D=>
      mcu_data_rsci_data_out_d(24), R=>rst);
   reg_if_if_read_mem_mcu_data_rsc_sft_lpi_1_23 : FDRE port map ( Q=>
      if_if_read_mem_mcu_data_rsc_sft_lpi_1(23), C=>clk, CE=>nx20586z1, D=>
      mcu_data_rsci_data_out_d(23), R=>rst);
   reg_if_if_read_mem_mcu_data_rsc_sft_lpi_1_22 : FDRE port map ( Q=>
      if_if_read_mem_mcu_data_rsc_sft_lpi_1(22), C=>clk, CE=>nx20586z1, D=>
      mcu_data_rsci_data_out_d(22), R=>rst);
   reg_if_if_read_mem_mcu_data_rsc_sft_lpi_1_21 : FDRE port map ( Q=>
      if_if_read_mem_mcu_data_rsc_sft_lpi_1(21), C=>clk, CE=>nx20586z1, D=>
      mcu_data_rsci_data_out_d(21), R=>rst);
   reg_if_if_read_mem_mcu_data_rsc_sft_lpi_1_20 : FDRE port map ( Q=>
      if_if_read_mem_mcu_data_rsc_sft_lpi_1(20), C=>clk, CE=>nx20586z1, D=>
      mcu_data_rsci_data_out_d(20), R=>rst);
   reg_if_if_read_mem_mcu_data_rsc_sft_lpi_1_19 : FDRE port map ( Q=>
      if_if_read_mem_mcu_data_rsc_sft_lpi_1(19), C=>clk, CE=>nx20586z1, D=>
      mcu_data_rsci_data_out_d(19), R=>rst);
   reg_if_if_read_mem_mcu_data_rsc_sft_lpi_1_18 : FDRE port map ( Q=>
      if_if_read_mem_mcu_data_rsc_sft_lpi_1(18), C=>clk, CE=>nx20586z1, D=>
      mcu_data_rsci_data_out_d(18), R=>rst);
   reg_if_if_read_mem_mcu_data_rsc_sft_lpi_1_17 : FDRE port map ( Q=>
      if_if_read_mem_mcu_data_rsc_sft_lpi_1(17), C=>clk, CE=>nx20586z1, D=>
      mcu_data_rsci_data_out_d(17), R=>rst);
   reg_if_if_read_mem_mcu_data_rsc_sft_lpi_1_16 : FDRE port map ( Q=>
      if_if_read_mem_mcu_data_rsc_sft_lpi_1(16), C=>clk, CE=>nx20586z1, D=>
      mcu_data_rsci_data_out_d(16), R=>rst);
   reg_if_if_read_mem_mcu_data_rsc_sft_lpi_1_15 : FDRE port map ( Q=>
      if_if_read_mem_mcu_data_rsc_sft_lpi_1(15), C=>clk, CE=>nx20586z1, D=>
      mcu_data_rsci_data_out_d(15), R=>rst);
   reg_if_if_read_mem_mcu_data_rsc_sft_lpi_1_14 : FDRE port map ( Q=>
      if_if_read_mem_mcu_data_rsc_sft_lpi_1(14), C=>clk, CE=>nx20586z1, D=>
      mcu_data_rsci_data_out_d(14), R=>rst);
   reg_if_if_read_mem_mcu_data_rsc_sft_lpi_1_13 : FDRE port map ( Q=>
      if_if_read_mem_mcu_data_rsc_sft_lpi_1(13), C=>clk, CE=>nx20586z1, D=>
      mcu_data_rsci_data_out_d(13), R=>rst);
   reg_if_if_read_mem_mcu_data_rsc_sft_lpi_1_12 : FDRE port map ( Q=>
      if_if_read_mem_mcu_data_rsc_sft_lpi_1(12), C=>clk, CE=>nx20586z1, D=>
      mcu_data_rsci_data_out_d(12), R=>rst);
   reg_if_if_read_mem_mcu_data_rsc_sft_lpi_1_11 : FDRE port map ( Q=>
      if_if_read_mem_mcu_data_rsc_sft_lpi_1(11), C=>clk, CE=>nx20586z1, D=>
      mcu_data_rsci_data_out_d(11), R=>rst);
   reg_if_if_read_mem_mcu_data_rsc_sft_lpi_1_10 : FDRE port map ( Q=>
      if_if_read_mem_mcu_data_rsc_sft_lpi_1(10), C=>clk, CE=>nx20586z1, D=>
      mcu_data_rsci_data_out_d(10), R=>rst);
   reg_if_if_read_mem_mcu_data_rsc_sft_lpi_1_9 : FDRE port map ( Q=>
      if_if_read_mem_mcu_data_rsc_sft_lpi_1(9), C=>clk, CE=>nx20586z1, D=>
      mcu_data_rsci_data_out_d(9), R=>rst);
   reg_if_if_read_mem_mcu_data_rsc_sft_lpi_1_8 : FDRE port map ( Q=>
      if_if_read_mem_mcu_data_rsc_sft_lpi_1(8), C=>clk, CE=>nx20586z1, D=>
      mcu_data_rsci_data_out_d(8), R=>rst);
   reg_if_if_read_mem_mcu_data_rsc_sft_lpi_1_7 : FDRE port map ( Q=>
      if_if_read_mem_mcu_data_rsc_sft_lpi_1(7), C=>clk, CE=>nx20586z1, D=>
      mcu_data_rsci_data_out_d(7), R=>rst);
   reg_if_if_read_mem_mcu_data_rsc_sft_lpi_1_6 : FDRE port map ( Q=>
      if_if_read_mem_mcu_data_rsc_sft_lpi_1(6), C=>clk, CE=>nx20586z1, D=>
      mcu_data_rsci_data_out_d(6), R=>rst);
   reg_if_if_read_mem_mcu_data_rsc_sft_lpi_1_5 : FDRE port map ( Q=>
      if_if_read_mem_mcu_data_rsc_sft_lpi_1(5), C=>clk, CE=>nx20586z1, D=>
      mcu_data_rsci_data_out_d(5), R=>rst);
   reg_if_if_read_mem_mcu_data_rsc_sft_lpi_1_4 : FDRE port map ( Q=>
      if_if_read_mem_mcu_data_rsc_sft_lpi_1(4), C=>clk, CE=>nx20586z1, D=>
      mcu_data_rsci_data_out_d(4), R=>rst);
   reg_if_if_read_mem_mcu_data_rsc_sft_lpi_1_3 : FDRE port map ( Q=>
      if_if_read_mem_mcu_data_rsc_sft_lpi_1(3), C=>clk, CE=>nx20586z1, D=>
      mcu_data_rsci_data_out_d(3), R=>rst);
   reg_if_if_read_mem_mcu_data_rsc_sft_lpi_1_2 : FDRE port map ( Q=>
      if_if_read_mem_mcu_data_rsc_sft_lpi_1(2), C=>clk, CE=>nx20586z1, D=>
      mcu_data_rsci_data_out_d(2), R=>rst);
   reg_if_if_read_mem_mcu_data_rsc_sft_lpi_1_1 : FDRE port map ( Q=>
      if_if_read_mem_mcu_data_rsc_sft_lpi_1(1), C=>clk, CE=>nx20586z1, D=>
      mcu_data_rsci_data_out_d(1), R=>rst);
   reg_if_if_read_mem_mcu_data_rsc_sft_lpi_1_0 : FDRE port map ( Q=>
      if_if_read_mem_mcu_data_rsc_sft_lpi_1(0), C=>clk, CE=>nx20586z1, D=>
      mcu_data_rsci_data_out_d(0), R=>rst);
   reg_clip_window_qr_2_lpi_1_2 : FDRE port map ( Q=>
      clip_window_qr_2_lpi_1(2), C=>clk, CE=>fsm_output(5), D=>nx40678z1, R
      =>rst);
   reg_clip_window_qr_2_lpi_1_1 : FDRE port map ( Q=>
      clip_window_qr_2_lpi_1(1), C=>clk, CE=>fsm_output(5), D=>nx41675z1, R
      =>rst);
   reg_clip_window_qr_2_lpi_1_0 : FDRE port map ( Q=>
      clip_window_qr_2_lpi_1(0), C=>clk, CE=>fsm_output(5), D=>nx42672z1, R
      =>rst);
   reg_window_6_lpi_1_dfm_2_2 : FDRE port map ( Q=>window_6_lpi_1_dfm_2(2), 
      C=>clk, CE=>fsm_output(5), D=>nx49364z1, R=>rst);
   reg_window_6_lpi_1_dfm_2_1 : FDRE port map ( Q=>window_6_lpi_1_dfm_2(1), 
      C=>clk, CE=>fsm_output(5), D=>nx50361z1, R=>rst);
   reg_window_6_lpi_1_dfm_2_0 : FDRE port map ( Q=>window_6_lpi_1_dfm_2(0), 
      C=>clk, CE=>fsm_output(5), D=>nx51358z1, R=>rst);
   reg_window_8_lpi_1_dfm_2_2 : FDRE port map ( Q=>window_8_lpi_1_dfm_2(2), 
      C=>clk, CE=>fsm_output(5), D=>nx51334z1, R=>rst);
   reg_window_8_lpi_1_dfm_2_1 : FDRE port map ( Q=>window_8_lpi_1_dfm_2(1), 
      C=>clk, CE=>fsm_output(5), D=>nx50337z1, R=>rst);
   reg_window_8_lpi_1_dfm_2_0 : FDRE port map ( Q=>window_8_lpi_1_dfm_2(0), 
      C=>clk, CE=>fsm_output(5), D=>nx49340z1, R=>rst);
   reg_system_input_window_6_sva_2 : FDRE port map ( Q=>
      system_input_window_6_sva(2), C=>clk, CE=>nx13168z1, D=>nx11021z1, R=>
      rst);
   reg_system_input_window_6_sva_1 : FDRE port map ( Q=>
      system_input_window_6_sva(1), C=>clk, CE=>nx13168z1, D=>nx10024z1, R=>
      rst);
   reg_system_input_window_6_sva_0 : FDRE port map ( Q=>
      system_input_window_6_sva(0), C=>clk, CE=>nx13168z1, D=>nx9027z1, R=>
      rst);
   reg_system_input_window_7_sva_2 : FDRE port map ( Q=>
      system_input_window_7_sva(2), C=>clk, CE=>nx13168z1, D=>nx11174z1, R=>
      rst);
   reg_system_input_window_7_sva_1 : FDRE port map ( Q=>
      system_input_window_7_sva(1), C=>clk, CE=>nx13168z1, D=>nx12171z1, R=>
      rst);
   reg_system_input_window_7_sva_0 : FDRE port map ( Q=>
      system_input_window_7_sva(0), C=>clk, CE=>nx13168z1, D=>nx13168z2, R=>
      rst);
   reg_median_max_1_1_lpi_1_2 : FDRE port map ( Q=>median_max_1_1_lpi_1(2), 
      C=>clk, CE=>nx1894z1, D=>nx28217z1, R=>rst);
   reg_median_max_1_1_lpi_1_1 : FDRE port map ( Q=>median_max_1_1_lpi_1(1), 
      C=>clk, CE=>nx1894z1, D=>nx42537z2, R=>rst);
   reg_median_max_1_1_lpi_1_0 : FDRE port map ( Q=>median_max_1_1_lpi_1(0), 
      C=>clk, CE=>nx1894z1, D=>nx43534z1, R=>rst);
   reg_median_max_7_1_lpi_1_2 : FDRE port map ( Q=>median_max_7_1_lpi_1(2), 
      C=>clk, CE=>nx27220z1, D=>nx28217z1, R=>rst);
   reg_median_max_7_1_lpi_1_1 : FDRE port map ( Q=>median_max_7_1_lpi_1(1), 
      C=>clk, CE=>nx27220z1, D=>nx42537z2, R=>rst);
   reg_median_max_7_1_lpi_1_0 : FDRE port map ( Q=>median_max_7_1_lpi_1(0), 
      C=>clk, CE=>nx27220z1, D=>nx43534z1, R=>rst);
   reg_median_max_2_1_lpi_1_2 : FDRE port map ( Q=>median_max_2_1_lpi_1(2), 
      C=>clk, CE=>nx59421z1, D=>nx28217z1, R=>rst);
   reg_median_max_2_1_lpi_1_1 : FDRE port map ( Q=>median_max_2_1_lpi_1(1), 
      C=>clk, CE=>nx59421z1, D=>nx42537z2, R=>rst);
   reg_median_max_2_1_lpi_1_0 : FDRE port map ( Q=>median_max_2_1_lpi_1(0), 
      C=>clk, CE=>nx59421z1, D=>nx43534z1, R=>rst);
   reg_median_max_6_1_lpi_1_2 : FDRE port map ( Q=>median_max_6_1_lpi_1(2), 
      C=>clk, CE=>nx42537z1, D=>nx28217z1, R=>rst);
   reg_median_max_6_1_lpi_1_1 : FDRE port map ( Q=>median_max_6_1_lpi_1(1), 
      C=>clk, CE=>nx42537z1, D=>nx42537z2, R=>rst);
   reg_median_max_6_1_lpi_1_0 : FDRE port map ( Q=>median_max_6_1_lpi_1(0), 
      C=>clk, CE=>nx42537z1, D=>nx43534z1, R=>rst);
   reg_median_max_3_1_lpi_1_2 : FDRE port map ( Q=>median_max_3_1_lpi_1(2), 
      C=>clk, CE=>nx55200z1, D=>nx28217z1, R=>rst);
   reg_median_max_3_1_lpi_1_1 : FDRE port map ( Q=>median_max_3_1_lpi_1(1), 
      C=>clk, CE=>nx55200z1, D=>nx42537z2, R=>rst);
   reg_median_max_3_1_lpi_1_0 : FDRE port map ( Q=>median_max_3_1_lpi_1(0), 
      C=>clk, CE=>nx55200z1, D=>nx43534z1, R=>rst);
   reg_median_max_5_1_lpi_1_2 : FDRE port map ( Q=>median_max_5_1_lpi_1(2), 
      C=>clk, CE=>nx46758z1, D=>nx28217z1, R=>rst);
   reg_median_max_5_1_lpi_1_1 : FDRE port map ( Q=>median_max_5_1_lpi_1(1), 
      C=>clk, CE=>nx46758z1, D=>nx42537z2, R=>rst);
   reg_median_max_5_1_lpi_1_0 : FDRE port map ( Q=>median_max_5_1_lpi_1(0), 
      C=>clk, CE=>nx46758z1, D=>nx43534z1, R=>rst);
   reg_median_max_4_1_lpi_1_2 : FDRE port map ( Q=>median_max_4_1_lpi_1(2), 
      C=>clk, CE=>nx14557z1, D=>nx28217z1, R=>rst);
   reg_median_max_4_1_lpi_1_1 : FDRE port map ( Q=>median_max_4_1_lpi_1(1), 
      C=>clk, CE=>nx14557z1, D=>nx42537z2, R=>rst);
   reg_median_max_4_1_lpi_1_0 : FDRE port map ( Q=>median_max_4_1_lpi_1(0), 
      C=>clk, CE=>nx14557z1, D=>nx43534z1, R=>rst);
   reg_and_2_psp : FDRE port map ( Q=>and_2_psp, C=>clk, CE=>nx12565z1, D=>
      nx48779z2, R=>rst);
   reg_and_13_psp : FDRE port map ( Q=>and_13_psp, C=>clk, CE=>nx12565z1, D
      =>nx43865z2, R=>rst);
   reg_and_14_psp : FDRE port map ( Q=>and_14_psp, C=>clk, CE=>nx12565z1, D
      =>nx58106z1, R=>rst);
   reg_and_15_psp : FDRE port map ( Q=>and_15_psp, C=>clk, CE=>nx12565z1, D
      =>nx49000z2, R=>rst);
   reg_and_16_psp : FDRE port map ( Q=>and_16_psp, C=>clk, CE=>nx12565z1, D
      =>nx17700z1, R=>rst);
   reg_and_17_psp : FDRE port map ( Q=>and_17_psp, C=>clk, CE=>nx12565z1, D
      =>nx54135z2, R=>rst);
   reg_and_18_psp : FDRE port map ( Q=>and_18_psp, C=>clk, CE=>nx12565z1, D
      =>nx1352z2, R=>rst);
   filter_core_core_fsm_inst_reg_state_var_6 : FDR port map ( Q=>
      fsm_output(6), C=>clk, D=>fsm_output(5), R=>rst);
   filter_core_core_fsm_inst_reg_state_var_5 : FDR port map ( Q=>
      fsm_output(5), C=>clk, D=>fsm_output(4), R=>rst);
   filter_core_core_fsm_inst_reg_state_var_4 : FDR port map ( Q=>
      fsm_output(4), C=>clk, D=>fsm_output(3), R=>rst);
   filter_core_core_fsm_inst_reg_state_var_3 : FDR port map ( Q=>
      fsm_output(3), C=>clk, D=>nx38909z1, R=>rst);
   filter_core_core_fsm_inst_reg_state_var_2 : FDR port map ( Q=>
      fsm_output(2), C=>clk, D=>fsm_output(1), R=>rst);
   filter_core_core_fsm_inst_reg_state_var_1 : FDR port map ( Q=>
      fsm_output(1), C=>clk, D=>nx36915z1, R=>rst);
   filter_core_core_fsm_inst_reg_state_var_0 : FDS port map ( Q=>
      fsm_output(0), C=>clk, D=>nx35918z1, S=>rst);
   mcu_data_rsci_addr_d_3_EXMPLR170 : INV port map ( O=>
      mcu_data_rsci_addr_d_3_EXMPLR122, I=>fsm_output(5));
   if_if_if_acc_7_nl_0n0s2_1_EXMPLR171 : INV port map ( O=>
      if_if_if_acc_7_nl_0n0s2(1), I=>frame_sva(4));
   if_if_if_acc_7_nl_0n0s2_0_EXMPLR172 : INV port map ( O=>
      if_if_if_acc_7_nl_0n0s2(0), I=>frame_sva(3));
   if_if_if_acc_6_nl_0n0s2_1_EXMPLR173 : INV port map ( O=>
      if_if_if_acc_6_nl_0n0s2(1), I=>frame_sva(8));
   if_if_if_acc_6_nl_0n0s2_0_EXMPLR174 : INV port map ( O=>
      if_if_if_acc_6_nl_0n0s2(0), I=>frame_sva(7));
   ix49961z26622 : INV port map ( O=>nx49961z1, I=>frame_sva(9));
   ix49961z26623 : INV port map ( O=>nx49961z2, I=>frame_sva(6));
   ix49961z26624 : INV port map ( O=>nx49961z3, I=>frame_sva(5));
   ix49961z26625 : INV port map ( O=>nx49961z4, I=>frame_sva(2));
   ix49961z26626 : INV port map ( O=>nx49961z5, I=>frame_sva(1));
   not_fsm_output_4_EXMPLR175 : INV port map ( O=>not_fsm_output_4, I=>
      fsm_output(4));
   not_system_input_c_sva_6_EXMPLR176 : INV port map ( O=>
      not_system_input_c_sva_6, I=>system_input_c_sva(6));
   NOT_fsm_output_1_EXMPLR177 : INV port map ( O=>NOT_fsm_output_1, I=>
      fsm_output(1));
   ix50264z58913 : LUT4
      generic map (INIT => X"E0FF") 
       port map ( O=>buffer_buf_rsci_addr_d(0), I0=>fsm_output(5), I1=>
      fsm_output(4), I2=>buffer_slc_buffer_c_5_0_1_itm_1(0), I3=>nx50264z1);
   ix51261z58913 : LUT4
      generic map (INIT => X"E0FF") 
       port map ( O=>buffer_buf_rsci_addr_d(1), I0=>fsm_output(5), I1=>
      fsm_output(4), I2=>buffer_slc_buffer_c_5_0_1_itm_1(1), I3=>nx51261z1);
   ix52258z58913 : LUT4
      generic map (INIT => X"E0FF") 
       port map ( O=>buffer_buf_rsci_addr_d(2), I0=>fsm_output(5), I1=>
      fsm_output(4), I2=>buffer_slc_buffer_c_5_0_1_itm_1(2), I3=>nx52258z1);
   ix53255z58913 : LUT4
      generic map (INIT => X"E0FF") 
       port map ( O=>buffer_buf_rsci_addr_d(3), I0=>fsm_output(5), I1=>
      fsm_output(4), I2=>buffer_slc_buffer_c_5_0_1_itm_1(3), I3=>nx53255z1);
   ix54252z58913 : LUT4
      generic map (INIT => X"E0FF") 
       port map ( O=>buffer_buf_rsci_addr_d(4), I0=>fsm_output(5), I1=>
      fsm_output(4), I2=>buffer_slc_buffer_c_5_0_1_itm_1(4), I3=>nx54252z1);
   ix55249z58913 : LUT4
      generic map (INIT => X"E0FF") 
       port map ( O=>buffer_buf_rsci_addr_d(5), I0=>fsm_output(5), I1=>
      fsm_output(4), I2=>buffer_slc_buffer_c_5_0_1_itm_1(5), I3=>nx55249z1);
   ix56246z767 : LUT4
      generic map (INIT => X"FDDD") 
       port map ( O=>buffer_buf_rsci_addr_d(6), I0=>nx56246z1, I1=>nx56246z2, 
      I2=>fsm_output(4), I3=>buffer_acc_1_itm_2(0));
   ix57243z767 : LUT4
      generic map (INIT => X"FDDD") 
       port map ( O=>buffer_buf_rsci_addr_d(7), I0=>nx57243z1, I1=>nx57243z2, 
      I2=>fsm_output(4), I3=>buffer_acc_1_itm_2(1));
   ix58240z767 : LUT4
      generic map (INIT => X"FDDD") 
       port map ( O=>buffer_buf_rsci_addr_d(8), I0=>nx58240z1, I1=>nx58240z2, 
      I2=>fsm_output(4), I3=>buffer_acc_1_itm_2(2));
   ix59237z1527 : LUT3
      generic map (INIT => X"D5") 
       port map ( O=>buffer_buf_rsci_addr_d(9), I0=>nx59237z1, I1=>
      fsm_output(5), I2=>buffer_acc_3_itm_1(3));
   ix37888z64665 : LUT4
      generic map (INIT => X"F777") 
       port map ( O=>histogram_rsci_addr_d(0), I0=>nx37888z1, I1=>nx37888z2, 
      I2=>fsm_output(1), I3=>histogram_init_idx_sva(0));
   ix38885z64665 : LUT4
      generic map (INIT => X"F777") 
       port map ( O=>histogram_rsci_addr_d(1), I0=>nx38885z1, I1=>nx38885z2, 
      I2=>fsm_output(1), I3=>histogram_init_idx_sva(1));
   ix39882z64665 : LUT4
      generic map (INIT => X"F777") 
       port map ( O=>histogram_rsci_addr_d(2), I0=>nx39882z1, I1=>nx39882z2, 
      I2=>fsm_output(1), I3=>histogram_init_idx_sva(2));
   ix49375z57889 : LUT4
      generic map (INIT => X"DCFF") 
       port map ( O=>mcu_data_rsci_re_d, I0=>nx49375z1, I1=>nx49375z2, I2=>
      nx10935z4, I3=>nx49375z4);
   ix10268z64938 : LUT4
      generic map (INIT => X"F888") 
       port map ( O=>mcu_data_rsci_addr_d(0), I0=>nx58700z4, I1=>nx9271z1, 
      I2=>nx8274z1, I3=>p_1_lpi_1_dfm_8(0));
   ix9271z63846 : LUT4
      generic map (INIT => X"F444") 
       port map ( O=>mcu_data_rsci_addr_d(1), I0=>nx58700z4, I1=>nx9271z1, 
      I2=>nx8274z1, I3=>p_1_lpi_1_dfm_8(1));
   ps_gnd : GND port map ( G=>nx35918z1);
   ix36915z54288 : LUT4
      generic map (INIT => X"CEEE") 
       port map ( O=>nx36915z1, I0=>fsm_output(2), I1=>fsm_output(0), I2=>
      exit_histogram_init_sva_dfm_1, I3=>buffer_buf_nor_itm_1);
   ix38909z61388 : LUT4
      generic map (INIT => X"EAAA") 
       port map ( O=>nx38909z1, I0=>fsm_output(6), I1=>fsm_output(2), I2=>
      exit_histogram_init_sva_dfm_1, I3=>buffer_buf_nor_itm_1);
   ix49340z1505 : LUT3
      generic map (INIT => X"BF") 
       port map ( O=>nx49340z1, I0=>nx49337z29, I1=>nx49337z30, I2=>
      nx49337z31);
   ix50337z1505 : LUT3
      generic map (INIT => X"BF") 
       port map ( O=>nx50337z1, I0=>nx170z19, I1=>nx170z20, I2=>nx170z21);
   ix51334z1505 : LUT3
      generic map (INIT => X"BF") 
       port map ( O=>nx51334z1, I0=>nx170z28, I1=>nx170z29, I2=>nx51334z2);
   ix51358z1057 : LUT4
      generic map (INIT => X"FEFF") 
       port map ( O=>nx51358z1, I0=>nx49337z21, I1=>nx49337z22, I2=>
      nx49337z23, I3=>nx49337z24);
   ix50361z1057 : LUT4
      generic map (INIT => X"FEFF") 
       port map ( O=>nx50361z1, I0=>nx170z13, I1=>nx170z14, I2=>nx170z15, I3
      =>nx50361z2);
   ix49364z1057 : LUT4
      generic map (INIT => X"FEFF") 
       port map ( O=>nx49364z1, I0=>nx170z32, I1=>nx170z33, I2=>nx170z34, I3
      =>nx170z35);
   ix42672z1493 : LUT3
      generic map (INIT => X"B3") 
       port map ( O=>nx42672z1, I0=>nx13168z2, I1=>nx49337z27, I2=>
      and_45_itm_1);
   ix41675z1493 : LUT3
      generic map (INIT => X"B3") 
       port map ( O=>nx41675z1, I0=>nx12171z1, I1=>nx170z17, I2=>
      and_45_itm_1);
   ix40678z1493 : LUT3
      generic map (INIT => X"B3") 
       port map ( O=>nx40678z1, I0=>nx11174z1, I1=>nx170z37, I2=>
      and_45_itm_1);
   ix43534z34081 : LUT4
      generic map (INIT => X"7FFF") 
       port map ( O=>nx43534z1, I0=>nx49337z10, I1=>nx49337z13, I2=>
      nx49337z16, I3=>nx49337z19);
   ix42537z34082 : LUT4
      generic map (INIT => X"7FFF") 
       port map ( O=>nx42537z2, I0=>nx170z8, I1=>nx170z9, I2=>nx170z10, I3=>
      nx170z11);
   ix28217z1117 : LUT4
      generic map (INIT => X"FF3B") 
       port map ( O=>nx28217z1, I0=>nx49337z32, I1=>nx170z23, I2=>nx170z31, 
      I3=>nx170z36);
   ix23219z64554 : LUT4
      generic map (INIT => X"F708") 
       port map ( O=>nx23219z1, I0=>nx51398z3, I1=>nx23219z2, I2=>
      system_input_c_sva(0), I3=>buffer_sel_1_sva);
   ix34118z1470 : LUT3
      generic map (INIT => X"9C") 
       port map ( O=>nx34118z1, I0=>nx6197z3, I1=>system_input_c_sva(7), I2
      =>system_input_c_sva(6));
   ix57209z46020 : LUT4
      generic map (INIT => X"AEA2") 
       port map ( O=>nx57209z1, I0=>in_data_vld_rsc_z, I1=>fsm_output(3), I2
      =>exit_histogram_init_1_sva, I3=>
      io_read_in_data_vld_rsc_sft_lpi_1_dfm_2);
   ix6197z1328 : LUT2
      generic map (INIT => X"D") 
       port map ( O=>nx6197z2, I0=>nx6197z3, I1=>nx51398z8);
   ix6197z65042 : LUT4
      generic map (INIT => X"F8F0") 
       port map ( O=>nx6197z1, I0=>in_data_vld_rsc_z, I1=>fsm_output(3), I2
      =>fsm_output(2), I3=>exit_histogram_init_1_sva);
   ix39764z49437 : LUT4
      generic map (INIT => X"BBFB") 
       port map ( O=>nx39764z1, I0=>nx39764z2, I1=>nx39764z6, I2=>
      window_0_lpi_1(0), I3=>exit_histogram_init_1_sva);
   ix40761z49437 : LUT4
      generic map (INIT => X"BBFB") 
       port map ( O=>nx40761z1, I0=>nx40761z2, I1=>nx40761z4, I2=>
      window_0_lpi_1(1), I3=>exit_histogram_init_1_sva);
   ix41758z49437 : LUT4
      generic map (INIT => X"BBFB") 
       port map ( O=>nx41758z1, I0=>nx41758z2, I1=>nx41758z4, I2=>
      window_0_lpi_1(2), I3=>exit_histogram_init_1_sva);
   ix56146z64791 : LUT4
      generic map (INIT => X"F7F5") 
       port map ( O=>nx56146z1, I0=>nx56146z2, I1=>nx20706z2, I2=>nx56146z4, 
      I3=>nx59203z5);
   ix55149z64791 : LUT4
      generic map (INIT => X"F7F5") 
       port map ( O=>nx55149z1, I0=>nx55149z2, I1=>nx40761z3, I2=>nx55149z3, 
      I3=>nx59203z5);
   ix54152z64791 : LUT4
      generic map (INIT => X"F7F5") 
       port map ( O=>nx54152z1, I0=>nx54152z2, I1=>nx41758z3, I2=>nx54152z3, 
      I3=>nx59203z5);
   ix20706z1407 : LUT3
      generic map (INIT => X"5D") 
       port map ( O=>nx20706z1, I0=>nx20706z2, I1=>clip_window_qr_lpi_1(0), 
      I2=>exit_histogram_init_1_sva);
   ix21703z1407 : LUT3
      generic map (INIT => X"5D") 
       port map ( O=>nx21703z1, I0=>nx40761z3, I1=>clip_window_qr_lpi_1(1), 
      I2=>exit_histogram_init_1_sva);
   ix22700z1407 : LUT3
      generic map (INIT => X"5D") 
       port map ( O=>nx22700z1, I0=>nx41758z3, I1=>clip_window_qr_lpi_1(2), 
      I2=>exit_histogram_init_1_sva);
   ix53518z55927 : LUT4
      generic map (INIT => X"D555") 
       port map ( O=>nx53518z1, I0=>nx53518z2, I1=>nx59203z5, I2=>
      system_input_window_7_sva(0), I3=>exit_histogram_init_1_sva);
   ix54515z55927 : LUT4
      generic map (INIT => X"D555") 
       port map ( O=>nx54515z1, I0=>nx54515z2, I1=>nx59203z5, I2=>
      system_input_window_7_sva(1), I3=>exit_histogram_init_1_sva);
   ix55512z55927 : LUT4
      generic map (INIT => X"D555") 
       port map ( O=>nx55512z1, I0=>nx55512z2, I1=>nx59203z5, I2=>
      system_input_window_7_sva(2), I3=>exit_histogram_init_1_sva);
   ix42392z1407 : LUT3
      generic map (INIT => X"5D") 
       port map ( O=>nx42392z1, I0=>nx42392z2, I1=>clip_window_qr_3_lpi_1(0), 
      I2=>exit_histogram_init_1_sva);
   ix41395z1407 : LUT3
      generic map (INIT => X"5D") 
       port map ( O=>nx41395z1, I0=>nx41395z2, I1=>clip_window_qr_3_lpi_1(1), 
      I2=>exit_histogram_init_1_sva);
   ix40398z1407 : LUT3
      generic map (INIT => X"5D") 
       port map ( O=>nx40398z1, I0=>nx40398z2, I1=>clip_window_qr_3_lpi_1(2), 
      I2=>exit_histogram_init_1_sva);
   ix12035z1327 : LUT2
      generic map (INIT => X"D") 
       port map ( O=>nx12035z1, I0=>median_i_1_lpi_2(0), I1=>
      exit_histogram_init_1_sva);
   ix58700z13362 : LUT4
      generic map (INIT => X"2F0F") 
       port map ( O=>nx58700z2, I0=>exit_Linit_lpi_1, I1=>
      exit_histogram_init_1_sva, I2=>nx51398z1, I3=>nx58700z3);
   ix58700z1328 : LUT2
      generic map (INIT => X"E") 
       port map ( O=>nx58700z1, I0=>fsm_output(3), I1=>fsm_output(2));
   ix41243z1186 : LUT4
      generic map (INIT => X"FF80") 
       port map ( O=>nx41243z1, I0=>nx51398z3, I1=>nx51398z5, I2=>nx9489z4, 
      I3=>system_input_output_vld_sva);
   ix26076z1326 : LUT2
      generic map (INIT => X"B") 
       port map ( O=>nx26076z2, I0=>sfi_if_if_read_mem_mcu_data_rsc_lpi_1, 
      I1=>p_1_lpi_3(0));
   ix51398z1337 : LUT3
      generic map (INIT => X"0E") 
       port map ( O=>nx51398z9, I0=>median_i_1_lpi_2(3), I1=>
      exit_Linit_lpi_1, I2=>exit_histogram_init_1_sva);
   ix19476z63556 : LUT4
      generic map (INIT => X"F322") 
       port map ( O=>nx19476z1, I0=>nx9044z3, I1=>nx19476z2, I2=>
      exit_if_if_if_1_if_for_lpi_1, I3=>
      sfi_if_if_read_mem_mcu_data_rsc_lpi_1);
   ix42113z13380 : LUT4
      generic map (INIT => X"2F22") 
       port map ( O=>nx42113z1, I0=>exit_if_if_if_1_if_for_lpi_1, I1=>
      exit_histogram_init_1_sva, I2=>nx42113z2, I3=>nx19476z2);
   ix60878z1328 : LUT2
      generic map (INIT => X"E") 
       port map ( O=>nx60878z1, I0=>nx60878z2, I1=>exit_histogram_init_sva);
   ix9441z1527 : LUT3
      generic map (INIT => X"D5") 
       port map ( O=>nx9441z1, I0=>nx9441z2, I1=>fsm_output(4), I2=>
      mux_36_itm(0));
   ix10438z1527 : LUT3
      generic map (INIT => X"D5") 
       port map ( O=>nx10438z1, I0=>nx10438z2, I1=>fsm_output(4), I2=>
      mux_36_itm(1));
   ix11435z1527 : LUT3
      generic map (INIT => X"D5") 
       port map ( O=>nx11435z1, I0=>nx11435z2, I1=>fsm_output(4), I2=>
      mux_36_itm(2));
   ix37821z1540 : LUT3
      generic map (INIT => X"E2") 
       port map ( O=>nx37821z1, I0=>asn_sft_lpi_1, I1=>
      sfi_system_input_land_1_lpi_1, I2=>if_if_equal_cse_lpi_1);
   ix60110z1463 : LUT3
      generic map (INIT => X"95") 
       port map ( O=>nx60110z1, I0=>system_input_c_sva(8), I1=>
      system_input_c_sva(7), I2=>system_input_c_sva(6));
   ix59113z1548 : LUT3
      generic map (INIT => X"EA") 
       port map ( O=>nx59113z1, I0=>system_input_c_sva(8), I1=>
      system_input_c_sva(7), I2=>system_input_c_sva(6));
   ix2400z1236 : LUT4
      generic map (INIT => X"FFB2") 
       port map ( O=>nx2400z1, I0=>nx49337z2, I1=>nx7321z3, I2=>nx170z43, I3
      =>nx7321z4);
   ix49337z65042 : LUT4
      generic map (INIT => X"F8F0") 
       port map ( O=>nx49337z1, I0=>nx49337z2, I1=>nx7321z2, I2=>nx22878z2, 
      I3=>nx7321z4);
   ix1167z21913 : LUT4
      generic map (INIT => X"5077") 
       port map ( O=>nx1167z1, I0=>nx170z3, I1=>nx6324z19, I2=>nx6324z23, I3
      =>nx6324z21);
   ix2164z1325 : LUT2
      generic map (INIT => X"B") 
       port map ( O=>nx2164z1, I0=>nx49337z2, I1=>nx6324z22);
   ix1007z1546 : LUT3
      generic map (INIT => X"E8") 
       port map ( O=>nx1007z1, I0=>nx29367z6, I1=>nx29367z8, I2=>nx1007z2);
   ix48581z62418 : LUT4
      generic map (INIT => X"EEAE") 
       port map ( O=>nx48581z3, I0=>nx48581z4, I1=>nx29367z3, I2=>nx29367z9, 
      I3=>nx29367z16);
   ps_vcc : VCC port map ( P=>nx23299z1);
   ix19409z1122 : LUT4
      generic map (INIT => X"FF40") 
       port map ( O=>nx19409z1, I0=>nx51398z2, I1=>nx19409z2, I2=>nx59203z5, 
      I3=>rst);
   ix18922z46028 : LUT4
      generic map (INIT => X"AEAA") 
       port map ( O=>nx18922z1, I0=>rst, I1=>nx11984z1, I2=>nx51398z2, I3=>
      nx59203z2);
   ix59247z61388 : LUT4
      generic map (INIT => X"EAAA") 
       port map ( O=>nx59247z1, I0=>rst, I1=>in_data_vld_rsc_z, I2=>
      fsm_output(3), I3=>exit_histogram_init_1_sva);
   ix9489z1562 : LUT3
      generic map (INIT => X"F8") 
       port map ( O=>nx9489z1, I0=>nx9489z2, I1=>nx11984z1, I2=>rst);
   ix63226z1550 : LUT3
      generic map (INIT => X"E4") 
       port map ( O=>nx63226z9, I0=>fsm_output(1), I1=>frame_sva(0), I2=>
      buffer_buf_vinit_ndx_sva(0));
   ix63226z1549 : LUT3
      generic map (INIT => X"E4") 
       port map ( O=>nx63226z8, I0=>fsm_output(1), I1=>frame_sva(1), I2=>
      buffer_buf_vinit_ndx_sva(1));
   ix63226z1548 : LUT3
      generic map (INIT => X"E4") 
       port map ( O=>nx63226z7, I0=>fsm_output(1), I1=>frame_sva(2), I2=>
      buffer_buf_vinit_ndx_sva(2));
   ix63226z1547 : LUT3
      generic map (INIT => X"E4") 
       port map ( O=>nx63226z6, I0=>fsm_output(1), I1=>frame_sva(3), I2=>
      buffer_buf_vinit_ndx_sva(3));
   ix63226z1534 : LUT3
      generic map (INIT => X"D8") 
       port map ( O=>nx63226z5, I0=>fsm_output(1), I1=>
      buffer_buf_vinit_ndx_sva(4), I2=>frame_sva(4));
   ix63226z1545 : LUT3
      generic map (INIT => X"E4") 
       port map ( O=>nx63226z4, I0=>fsm_output(1), I1=>frame_sva(5), I2=>
      buffer_buf_vinit_ndx_sva(5));
   ix63226z1544 : LUT3
      generic map (INIT => X"E4") 
       port map ( O=>nx63226z3, I0=>fsm_output(1), I1=>frame_sva(6), I2=>
      buffer_buf_vinit_ndx_sva(6));
   ix63226z1543 : LUT3
      generic map (INIT => X"E4") 
       port map ( O=>nx63226z2, I0=>fsm_output(1), I1=>frame_sva(7), I2=>
      buffer_buf_vinit_ndx_sva(7));
   ix63226z1542 : LUT3
      generic map (INIT => X"E4") 
       port map ( O=>nx63226z1, I0=>fsm_output(1), I1=>frame_sva(8), I2=>
      buffer_buf_vinit_ndx_sva(8));
   ix24044z6229 : LUT4
      generic map (INIT => X"1333") 
       port map ( O=>buffer_buf_rsci_we_d, I0=>fsm_output(5), I1=>
      fsm_output(1), I2=>io_read_in_data_vld_rsc_sft_lpi_1_dfm, I3=>
      sfi_io_read_in_data_vld_rsc_lpi_1_dfm);
   ix50264z6274 : LUT4
      generic map (INIT => X"135F") 
       port map ( O=>nx50264z1, I0=>fsm_output(3), I1=>fsm_output(1), I2=>
      system_input_c_sva(0), I3=>buffer_buf_vinit_ndx_sva(0));
   ix51261z6274 : LUT4
      generic map (INIT => X"135F") 
       port map ( O=>nx51261z1, I0=>fsm_output(3), I1=>fsm_output(1), I2=>
      system_input_c_sva(1), I3=>buffer_buf_vinit_ndx_sva(1));
   ix52258z6274 : LUT4
      generic map (INIT => X"135F") 
       port map ( O=>nx52258z1, I0=>fsm_output(3), I1=>fsm_output(1), I2=>
      system_input_c_sva(2), I3=>buffer_buf_vinit_ndx_sva(2));
   ix53255z6274 : LUT4
      generic map (INIT => X"135F") 
       port map ( O=>nx53255z1, I0=>fsm_output(3), I1=>fsm_output(1), I2=>
      system_input_c_sva(3), I3=>buffer_buf_vinit_ndx_sva(3));
   ix54252z6274 : LUT4
      generic map (INIT => X"135F") 
       port map ( O=>nx54252z1, I0=>fsm_output(3), I1=>fsm_output(1), I2=>
      system_input_c_sva(4), I3=>buffer_buf_vinit_ndx_sva(4));
   ix55249z6274 : LUT4
      generic map (INIT => X"135F") 
       port map ( O=>nx55249z1, I0=>fsm_output(3), I1=>fsm_output(1), I2=>
      system_input_c_sva(5), I3=>buffer_buf_vinit_ndx_sva(5));
   ix56246z1324 : LUT2
      generic map (INIT => X"8") 
       port map ( O=>nx56246z2, I0=>fsm_output(3), I1=>system_input_c_sva(6)
   );
   ix56246z6754 : LUT4
      generic map (INIT => X"153F") 
       port map ( O=>nx56246z1, I0=>fsm_output(5), I1=>fsm_output(1), I2=>
      buffer_buf_vinit_ndx_sva(6), I3=>buffer_acc_3_itm_1(0));
   ix57243z1324 : LUT2
      generic map (INIT => X"8") 
       port map ( O=>nx57243z2, I0=>fsm_output(3), I1=>system_input_c_sva(7)
   );
   ix57243z6754 : LUT4
      generic map (INIT => X"153F") 
       port map ( O=>nx57243z1, I0=>fsm_output(5), I1=>fsm_output(1), I2=>
      buffer_buf_vinit_ndx_sva(7), I3=>buffer_acc_3_itm_1(1));
   ix58240z1324 : LUT2
      generic map (INIT => X"8") 
       port map ( O=>nx58240z2, I0=>fsm_output(3), I1=>system_input_c_sva(8)
   );
   ix58240z6754 : LUT4
      generic map (INIT => X"153F") 
       port map ( O=>nx58240z1, I0=>fsm_output(5), I1=>fsm_output(1), I2=>
      buffer_buf_vinit_ndx_sva(8), I3=>buffer_acc_3_itm_1(2));
   ix59237z6754 : LUT4
      generic map (INIT => X"153F") 
       port map ( O=>nx59237z1, I0=>fsm_output(4), I1=>fsm_output(1), I2=>
      buffer_buf_vinit_ndx_sva(9), I3=>buffer_acc_1_itm_2(3));
   ix50755z1322 : LUT2
      generic map (INIT => X"8") 
       port map ( O=>buffer_buf_rsci_data_in_d(0), I0=>fsm_output(5), I1=>
      buffer_din_sva_1(0));
   ix49758z1322 : LUT2
      generic map (INIT => X"8") 
       port map ( O=>buffer_buf_rsci_data_in_d(1), I0=>fsm_output(5), I1=>
      buffer_din_sva_1(1));
   ix48761z1322 : LUT2
      generic map (INIT => X"8") 
       port map ( O=>buffer_buf_rsci_data_in_d(2), I0=>fsm_output(5), I1=>
      buffer_din_sva_1(2));
   ix26870z34084 : LUT4
      generic map (INIT => X"8000") 
       port map ( O=>nx26870z3, I0=>io_read_in_data_vld_rsc_sft_lpi_1_dfm_2, 
      I1=>exit_Linit_sva_1_st_2, I2=>if_if_equal_cse_sva_st_2, I3=>
      main_stage_0_2);
   ix26870z1323 : LUT3
      generic map (INIT => X"08") 
       port map ( O=>nx26870z2, I0=>fsm_output(3), I1=>
      asn_sft_3_lpi_1_dfm_st_2, I2=>exit_Linit_lpi_1_dfm_st_2);
   ix26870z31785 : LUT4
      generic map (INIT => X"7707") 
       port map ( O=>px2583, I0=>nx26870z2, I1=>nx26870z3, I2=>fsm_output(1), 
      I3=>exit_histogram_init_sva);
   ix26869z1442 : LUT3
      generic map (INIT => X"80") 
       port map ( O=>px2584, I0=>nx7280z5, I1=>asn_sft_lpi_1_dfm_1, I2=>
      asn_sft_1_lpi_1_st_1);
   ix37888z25251 : LUT4
      generic map (INIT => X"5D7F") 
       port map ( O=>nx37888z2, I0=>fsm_output(4), I1=>
      exit_if_if_if_1_if_for_lpi_1_dfm_3, I2=>
      histogram_init_idx_1_lpi_1_dfm_2(0), I3=>p_1_lpi_1_dfm_8(0));
   ix37888z3226 : LUT4
      generic map (INIT => X"0777") 
       port map ( O=>nx37888z1, I0=>nx48581z24, I1=>fsm_output(6), I2=>
      fsm_output(3), I3=>median_max_5_lpi_1_dfm_6(0));
   ix38885z25251 : LUT4
      generic map (INIT => X"5D7F") 
       port map ( O=>nx38885z2, I0=>fsm_output(4), I1=>
      exit_if_if_if_1_if_for_lpi_1_dfm_3, I2=>
      histogram_init_idx_1_lpi_1_dfm_2(1), I3=>p_1_lpi_1_dfm_8(1));
   ix38885z3226 : LUT4
      generic map (INIT => X"0777") 
       port map ( O=>nx38885z1, I0=>nx48581z22, I1=>fsm_output(6), I2=>
      fsm_output(3), I3=>median_max_5_lpi_1_dfm_6(1));
   ix39882z25251 : LUT4
      generic map (INIT => X"5D7F") 
       port map ( O=>nx39882z2, I0=>fsm_output(4), I1=>
      exit_if_if_if_1_if_for_lpi_1_dfm_3, I2=>
      histogram_init_idx_1_lpi_1_dfm_2(2), I3=>p_1_lpi_1_dfm_8(2));
   ix39882z3226 : LUT4
      generic map (INIT => X"0777") 
       port map ( O=>nx39882z1, I0=>nx48581z6, I1=>fsm_output(6), I2=>
      fsm_output(3), I3=>median_max_5_lpi_1_dfm_6(2));
   ix7280z1584 : LUT4
      generic map (INIT => X"0100") 
       port map ( O=>nx7280z13, I0=>
      if_if_read_mem_mcu_data_rsc_sft_lpi_1_st(22), I1=>
      if_if_read_mem_mcu_data_rsc_sft_lpi_1_st(21), I2=>
      if_if_read_mem_mcu_data_rsc_sft_lpi_1_st(20), I3=>
      if_if_read_mem_mcu_data_rsc_sft_lpi_1_st(0));
   ix7280z1328 : LUT4
      generic map (INIT => X"0001") 
       port map ( O=>nx7280z12, I0=>
      if_if_read_mem_mcu_data_rsc_sft_lpi_1_st(27), I1=>
      if_if_read_mem_mcu_data_rsc_sft_lpi_1_st(26), I2=>
      if_if_read_mem_mcu_data_rsc_sft_lpi_1_st(25), I3=>
      if_if_read_mem_mcu_data_rsc_sft_lpi_1_st(12));
   ix7280z1327 : LUT4
      generic map (INIT => X"0001") 
       port map ( O=>nx7280z11, I0=>
      if_if_read_mem_mcu_data_rsc_sft_lpi_1_st(16), I1=>
      if_if_read_mem_mcu_data_rsc_sft_lpi_1_st(14), I2=>
      if_if_read_mem_mcu_data_rsc_sft_lpi_1_st(13), I3=>
      if_if_read_mem_mcu_data_rsc_sft_lpi_1_st(9));
   ix7280z1338 : LUT4
      generic map (INIT => X"0001") 
       port map ( O=>nx7280z10, I0=>
      if_if_read_mem_mcu_data_rsc_sft_lpi_1_st(31), I1=>
      if_if_read_mem_mcu_data_rsc_sft_lpi_1_st(18), I2=>
      if_if_read_mem_mcu_data_rsc_sft_lpi_1_st(11), I3=>
      if_if_read_mem_mcu_data_rsc_sft_lpi_1_st(5));
   ix7280z1324 : LUT4
      generic map (INIT => X"0001") 
       port map ( O=>nx7280z9, I0=>exit_if_if_if_1_if_for_lpi_1_dfm_3, I1=>
      if_if_read_mem_mcu_data_rsc_sft_lpi_1_st(28), I2=>
      if_if_read_mem_mcu_data_rsc_sft_lpi_1_st(4), I3=>
      if_if_read_mem_mcu_data_rsc_sft_lpi_1_st(3));
   ix7280z1323 : LUT4
      generic map (INIT => X"0001") 
       port map ( O=>nx7280z8, I0=>
      if_if_read_mem_mcu_data_rsc_sft_lpi_1_st(15), I1=>
      if_if_read_mem_mcu_data_rsc_sft_lpi_1_st(10), I2=>
      if_if_read_mem_mcu_data_rsc_sft_lpi_1_st(7), I3=>
      if_if_read_mem_mcu_data_rsc_sft_lpi_1_st(2));
   ix7280z34089 : LUT4
      generic map (INIT => X"8000") 
       port map ( O=>nx7280z7, I0=>nx7280z8, I1=>nx7280z9, I2=>nx7280z10, I3
      =>nx7280z11);
   ix7280z1321 : LUT3
      generic map (INIT => X"01") 
       port map ( O=>nx7280z6, I0=>
      if_if_read_mem_mcu_data_rsc_sft_lpi_1_st(29), I1=>
      if_if_read_mem_mcu_data_rsc_sft_lpi_1_st(24), I2=>
      if_if_read_mem_mcu_data_rsc_sft_lpi_1_st(6));
   ix7280z1447 : LUT3
      generic map (INIT => X"80") 
       port map ( O=>nx7280z5, I0=>io_read_in_data_vld_rsc_sft_lpi_1_dfm, I1
      =>asn_sft_3_lpi_1_dfm, I2=>exit_Linit_lpi_1_dfm_1);
   ix7280z1319 : LUT4
      generic map (INIT => X"0001") 
       port map ( O=>nx7280z4, I0=>
      if_if_read_mem_mcu_data_rsc_sft_lpi_1_st(30), I1=>
      if_if_read_mem_mcu_data_rsc_sft_lpi_1_st(19), I2=>
      if_if_read_mem_mcu_data_rsc_sft_lpi_1_st(17), I3=>
      if_if_read_mem_mcu_data_rsc_sft_lpi_1_st(8));
   ix7280z1325 : LUT4
      generic map (INIT => X"0008") 
       port map ( O=>nx7280z3, I0=>fsm_output(5), I1=>asn_sft_2_lpi_1_dfm, 
      I2=>if_if_read_mem_mcu_data_rsc_sft_lpi_1_st(23), I3=>
      if_if_read_mem_mcu_data_rsc_sft_lpi_1_st(1));
   ix7280z34084 : LUT4
      generic map (INIT => X"8000") 
       port map ( O=>nx7280z2, I0=>nx7280z3, I1=>nx7280z4, I2=>nx7280z5, I3
      =>nx7280z6);
   ix7280z34083 : LUT4
      generic map (INIT => X"8000") 
       port map ( O=>nx7280z1, I0=>nx7280z2, I1=>nx7280z7, I2=>nx7280z12, I3
      =>nx7280z13);
   ix7280z6775 : LUT4
      generic map (INIT => X"1555") 
       port map ( O=>mcu_data_rsci_we_d, I0=>nx7280z1, I1=>nx7623z2, I2=>
      sfi_system_input_land_1_lpi_1, I3=>if_if_equal_cse_lpi_1);
   ix49375z3880 : LUT4
      generic map (INIT => X"0A02") 
       port map ( O=>nx49375z4, I0=>fsm_output(3), I1=>exit_Linit_lpi_1, I2
      =>exit_histogram_init_1_sva, I3=>
      io_read_in_data_vld_rsc_sft_lpi_1_dfm_2);
   ix49375z1325 : LUT2
      generic map (INIT => X"8") 
       port map ( O=>nx49375z3, I0=>median_i_1_lpi_2(3), I1=>
      io_read_in_data_vld_rsc_sft_lpi_1_dfm_2);
   ix49375z1371 : LUT4
      generic map (INIT => X"0037") 
       port map ( O=>nx49375z2, I0=>nx49375z3, I1=>asn_sft_3_lpi_1, I2=>
      exit_Linit_lpi_1, I3=>exit_histogram_init_1_sva);
   ix49375z1443 : LUT3
      generic map (INIT => X"80") 
       port map ( O=>nx49375z1, I0=>nx58700z4, I1=>nx53617z2, I2=>nx53617z4
   );
   ix9271z1323 : LUT3
      generic map (INIT => X"08") 
       port map ( O=>nx9271z1, I0=>fsm_output(3), I1=>exit_Linit_lpi_1, I2=>
      exit_histogram_init_1_sva);
   ix8274z10181 : LUT4
      generic map (INIT => X"22A2") 
       port map ( O=>nx8274z1, I0=>fsm_output(5), I1=>fsm_output(3), I2=>
      exit_Linit_lpi_1, I3=>exit_histogram_init_1_sva);
   ix8274z1322 : LUT2
      generic map (INIT => X"8") 
       port map ( O=>mcu_data_rsci_addr_d(2), I0=>nx8274z1, I1=>
      p_1_lpi_1_dfm_8(2));
   ix25753z1498 : LUT3
      generic map (INIT => X"B8") 
       port map ( O=>mcu_data_rsci_data_in_d(0), I0=>
      histogram_rsci_data_out_d(0), I1=>fsm_output(5), I2=>frame_sva(0));
   ix26750z1498 : LUT3
      generic map (INIT => X"B8") 
       port map ( O=>mcu_data_rsci_data_in_d(1), I0=>
      histogram_rsci_data_out_d(1), I1=>fsm_output(5), I2=>frame_sva(1));
   ix27747z1498 : LUT3
      generic map (INIT => X"B8") 
       port map ( O=>mcu_data_rsci_data_in_d(2), I0=>
      histogram_rsci_data_out_d(2), I1=>fsm_output(5), I2=>frame_sva(2));
   ix28744z1498 : LUT3
      generic map (INIT => X"B8") 
       port map ( O=>mcu_data_rsci_data_in_d(3), I0=>
      histogram_rsci_data_out_d(3), I1=>fsm_output(5), I2=>frame_sva(3));
   ix29741z1498 : LUT3
      generic map (INIT => X"B8") 
       port map ( O=>mcu_data_rsci_data_in_d(4), I0=>
      histogram_rsci_data_out_d(4), I1=>fsm_output(5), I2=>frame_sva(4));
   ix30738z1498 : LUT3
      generic map (INIT => X"B8") 
       port map ( O=>mcu_data_rsci_data_in_d(5), I0=>
      histogram_rsci_data_out_d(5), I1=>fsm_output(5), I2=>frame_sva(5));
   ix31735z1498 : LUT3
      generic map (INIT => X"B8") 
       port map ( O=>mcu_data_rsci_data_in_d(6), I0=>
      histogram_rsci_data_out_d(6), I1=>fsm_output(5), I2=>frame_sva(6));
   ix32732z1498 : LUT3
      generic map (INIT => X"B8") 
       port map ( O=>mcu_data_rsci_data_in_d(7), I0=>
      histogram_rsci_data_out_d(7), I1=>fsm_output(5), I2=>frame_sva(7));
   ix33729z1498 : LUT3
      generic map (INIT => X"B8") 
       port map ( O=>mcu_data_rsci_data_in_d(8), I0=>
      histogram_rsci_data_out_d(8), I1=>fsm_output(5), I2=>frame_sva(8));
   ix34726z1498 : LUT3
      generic map (INIT => X"B8") 
       port map ( O=>mcu_data_rsci_data_in_d(9), I0=>
      histogram_rsci_data_out_d(9), I1=>fsm_output(5), I2=>frame_sva(9));
   ix61918z1322 : LUT2
      generic map (INIT => X"8") 
       port map ( O=>mcu_data_rsci_data_in_d(10), I0=>
      histogram_rsci_data_out_d(10), I1=>fsm_output(5));
   ix60921z1322 : LUT2
      generic map (INIT => X"8") 
       port map ( O=>mcu_data_rsci_data_in_d(11), I0=>
      histogram_rsci_data_out_d(11), I1=>fsm_output(5));
   ix59924z1322 : LUT2
      generic map (INIT => X"8") 
       port map ( O=>mcu_data_rsci_data_in_d(12), I0=>
      histogram_rsci_data_out_d(12), I1=>fsm_output(5));
   ix58927z1322 : LUT2
      generic map (INIT => X"8") 
       port map ( O=>mcu_data_rsci_data_in_d(13), I0=>
      histogram_rsci_data_out_d(13), I1=>fsm_output(5));
   ix57930z1322 : LUT2
      generic map (INIT => X"8") 
       port map ( O=>mcu_data_rsci_data_in_d(14), I0=>
      histogram_rsci_data_out_d(14), I1=>fsm_output(5));
   ix56933z1322 : LUT2
      generic map (INIT => X"8") 
       port map ( O=>mcu_data_rsci_data_in_d(15), I0=>
      histogram_rsci_data_out_d(15), I1=>fsm_output(5));
   ix55936z1322 : LUT2
      generic map (INIT => X"8") 
       port map ( O=>mcu_data_rsci_data_in_d(16), I0=>
      histogram_rsci_data_out_d(16), I1=>fsm_output(5));
   ix12565z42786 : LUT4
      generic map (INIT => X"A200") 
       port map ( O=>nx12565z1, I0=>fsm_output(3), I1=>exit_Linit_lpi_1, I2
      =>exit_histogram_init_1_sva, I3=>nx10935z2);
   ix14557z1322 : LUT2
      generic map (INIT => X"8") 
       port map ( O=>nx14557z1, I0=>fsm_output(5), I1=>and_69_itm);
   ix46758z1322 : LUT2
      generic map (INIT => X"8") 
       port map ( O=>nx46758z1, I0=>fsm_output(5), I1=>and_70_itm);
   ix55200z1322 : LUT2
      generic map (INIT => X"8") 
       port map ( O=>nx55200z1, I0=>fsm_output(5), I1=>and_68_itm);
   ix42537z1322 : LUT2
      generic map (INIT => X"8") 
       port map ( O=>nx42537z1, I0=>fsm_output(5), I1=>and_71_itm);
   ix59421z1322 : LUT2
      generic map (INIT => X"8") 
       port map ( O=>nx59421z1, I0=>fsm_output(5), I1=>and_67_itm);
   ix27220z1322 : LUT2
      generic map (INIT => X"8") 
       port map ( O=>nx27220z1, I0=>fsm_output(5), I1=>and_72_itm);
   ix1894z1322 : LUT2
      generic map (INIT => X"8") 
       port map ( O=>nx1894z1, I0=>fsm_output(5), I1=>and_66_itm);
   ix9027z1498 : LUT3
      generic map (INIT => X"B8") 
       port map ( O=>nx9027z1, I0=>buffer_buf_rsci_data_out_d(0), I1=>
      buffer_sel_1_sva_dfm, I2=>buffer_t0_sva_1(0));
   ix10024z1498 : LUT3
      generic map (INIT => X"B8") 
       port map ( O=>nx10024z1, I0=>buffer_buf_rsci_data_out_d(1), I1=>
      buffer_sel_1_sva_dfm, I2=>buffer_t0_sva_1(1));
   ix11021z1498 : LUT3
      generic map (INIT => X"B8") 
       port map ( O=>nx11021z1, I0=>buffer_buf_rsci_data_out_d(2), I1=>
      buffer_sel_1_sva_dfm, I2=>buffer_t0_sva_1(2));
   ix13168z1322 : LUT2
      generic map (INIT => X"8") 
       port map ( O=>nx13168z1, I0=>fsm_output(5), I1=>and_51_cse);
   ix20586z1322 : LUT2
      generic map (INIT => X"8") 
       port map ( O=>nx20586z1, I0=>fsm_output(4), I1=>and_65_cse_1);
   ix6587z1322 : LUT2
      generic map (INIT => X"8") 
       port map ( O=>nx6587z1, I0=>fsm_output(5), I1=>and_64_itm_1);
   ix12424z1316 : LUT2
      generic map (INIT => X"2") 
       port map ( O=>nx12424z1, I0=>median_i_1_lpi_2(2), I1=>
      exit_histogram_init_1_sva);
   ix19521z1318 : LUT2
      generic map (INIT => X"4") 
       port map ( O=>nx19521z1, I0=>nx59203z2, I1=>exit_histogram_init_1_sva
   );
   ix10870z1322 : LUT2
      generic map (INIT => X"8") 
       port map ( O=>nx10870z1, I0=>nx59203z2, I1=>exit_histogram_init_1_sva
   );
   ix11325z1442 : LUT3
      generic map (INIT => X"80") 
       port map ( O=>nx11325z1, I0=>nx59203z2, I1=>nx59203z5, I2=>
      exit_histogram_init_1_sva);
   ix19066z1378 : LUT3
      generic map (INIT => X"40") 
       port map ( O=>nx19066z1, I0=>nx59203z2, I1=>nx59203z5, I2=>
      exit_histogram_init_1_sva);
   ix49457z1346 : LUT3
      generic map (INIT => X"20") 
       port map ( O=>nx49457z1, I0=>nx59203z2, I1=>nx59203z5, I2=>
      exit_histogram_init_1_sva);
   ix51224z1330 : LUT3
      generic map (INIT => X"10") 
       port map ( O=>nx51224z1, I0=>nx59203z2, I1=>nx59203z5, I2=>
      exit_histogram_init_1_sva);
   ix1817z1442 : LUT3
      generic map (INIT => X"80") 
       port map ( O=>nx1817z1, I0=>nx59203z2, I1=>nx39764z3, I2=>
      exit_histogram_init_1_sva);
   ix28574z1378 : LUT3
      generic map (INIT => X"40") 
       port map ( O=>nx28574z1, I0=>nx59203z2, I1=>nx39764z3, I2=>
      exit_histogram_init_1_sva);
   ix58965z1346 : LUT3
      generic map (INIT => X"20") 
       port map ( O=>nx58965z1, I0=>nx59203z2, I1=>nx39764z3, I2=>
      exit_histogram_init_1_sva);
   ix41716z1330 : LUT3
      generic map (INIT => X"10") 
       port map ( O=>nx41716z1, I0=>nx59203z2, I1=>nx39764z3, I2=>
      exit_histogram_init_1_sva);
   ix64568z1318 : LUT2
      generic map (INIT => X"4") 
       port map ( O=>nx64568z1, I0=>sfi_if_if_read_mem_mcu_data_rsc_lpi_1, 
      I1=>p_1_lpi_3(1));
   ix29z1318 : LUT2
      generic map (INIT => X"4") 
       port map ( O=>nx29z1, I0=>sfi_if_if_read_mem_mcu_data_rsc_lpi_1, I1=>
      p_1_lpi_3(2));
   ix28225z1315 : LUT2
      generic map (INIT => X"1") 
       port map ( O=>nx28225z1, I0=>fsm_output(5), I1=>fsm_output(4));
   ix35115z1323 : LUT2
      generic map (INIT => X"9") 
       port map ( O=>nx35115z1, I0=>nx6197z3, I1=>system_input_c_sva(6));
   ix32124z1548 : LUT4
      generic map (INIT => X"00EA") 
       port map ( O=>nx32124z1, I0=>system_input_c_sva(8), I1=>
      system_input_c_sva(7), I2=>system_input_c_sva(6), I3=>nx6197z3);
   ix1352z2339 : LUT4
      generic map (INIT => X"0400") 
       port map ( O=>nx1352z2, I0=>median_i_1_lpi_2(1), I1=>
      median_i_1_lpi_2(0), I2=>exit_histogram_init_1_sva, I3=>nx1352z3);
   ix1352z54562 : LUT4
      generic map (INIT => X"D000") 
       port map ( O=>nx1352z1, I0=>exit_Linit_lpi_1, I1=>
      exit_histogram_init_1_sva, I2=>nx10935z2, I3=>nx1352z2);
   ix17700z1522 : LUT3
      generic map (INIT => X"D0") 
       port map ( O=>nx17700z1, I0=>median_i_1_lpi_2(0), I1=>
      exit_histogram_init_1_sva, I2=>nx53914z2);
   ix59049z1826 : LUT4
      generic map (INIT => X"0200") 
       port map ( O=>nx59049z1, I0=>nx10935z2, I1=>nx10935z4, I2=>nx9044z4, 
      I3=>nx53914z2);
   ix53914z1331 : LUT4
      generic map (INIT => X"0010") 
       port map ( O=>nx53914z2, I0=>median_i_1_lpi_2(3), I1=>
      median_i_1_lpi_2(2), I2=>median_i_1_lpi_2(1), I3=>
      exit_histogram_init_1_sva);
   ix58106z1346 : LUT3
      generic map (INIT => X"20") 
       port map ( O=>nx58106z1, I0=>median_i_1_lpi_2(0), I1=>
      exit_histogram_init_1_sva, I2=>nx53914z2);
   ix53914z9506 : LUT4
      generic map (INIT => X"2000") 
       port map ( O=>nx53914z1, I0=>nx10935z2, I1=>nx10935z4, I2=>nx9044z4, 
      I3=>nx53914z2);
   ix48779z22051 : LUT4
      generic map (INIT => X"5100") 
       port map ( O=>nx48779z2, I0=>median_i_1_lpi_2(1), I1=>
      median_i_1_lpi_2(0), I2=>exit_histogram_init_1_sva, I3=>nx49000z3);
   ix48779z54562 : LUT4
      generic map (INIT => X"D000") 
       port map ( O=>nx48779z1, I0=>exit_Linit_lpi_1, I1=>
      exit_histogram_init_1_sva, I2=>nx10935z2, I3=>nx48779z2);
   ix43865z2339 : LUT4
      generic map (INIT => X"0400") 
       port map ( O=>nx43865z2, I0=>median_i_1_lpi_2(1), I1=>
      median_i_1_lpi_2(0), I2=>exit_histogram_init_1_sva, I3=>nx49000z3);
   ix43865z54562 : LUT4
      generic map (INIT => X"D000") 
       port map ( O=>nx43865z1, I0=>exit_Linit_lpi_1, I1=>
      exit_histogram_init_1_sva, I2=>nx10935z2, I3=>nx43865z2);
   ix49000z42787 : LUT4
      generic map (INIT => X"A200") 
       port map ( O=>nx49000z2, I0=>median_i_1_lpi_2(1), I1=>
      median_i_1_lpi_2(0), I2=>exit_histogram_init_1_sva, I3=>nx49000z3);
   ix49000z54562 : LUT4
      generic map (INIT => X"D000") 
       port map ( O=>nx49000z1, I0=>exit_Linit_lpi_1, I1=>
      exit_histogram_init_1_sva, I2=>nx10935z2, I3=>nx49000z2);
   ix49000z1320 : LUT3
      generic map (INIT => X"04") 
       port map ( O=>nx49000z3, I0=>median_i_1_lpi_2(3), I1=>
      median_i_1_lpi_2(2), I2=>exit_histogram_init_1_sva);
   ix54135z3363 : LUT4
      generic map (INIT => X"0800") 
       port map ( O=>nx54135z2, I0=>median_i_1_lpi_2(1), I1=>
      median_i_1_lpi_2(0), I2=>exit_histogram_init_1_sva, I3=>nx49000z3);
   ix54135z54562 : LUT4
      generic map (INIT => X"D000") 
       port map ( O=>nx54135z1, I0=>exit_Linit_lpi_1, I1=>
      exit_histogram_init_1_sva, I2=>nx10935z2, I3=>nx54135z2);
   ix1352z1557 : LUT3
      generic map (INIT => X"F1") 
       port map ( O=>nx1352z3, I0=>median_i_1_lpi_2(3), I1=>
      median_i_1_lpi_2(2), I2=>exit_histogram_init_1_sva);
   ix13421z1316 : LUT2
      generic map (INIT => X"2") 
       port map ( O=>nx13421z1, I0=>median_i_1_lpi_2(1), I1=>
      exit_histogram_init_1_sva);
   ix60014z54562 : LUT4
      generic map (INIT => X"D000") 
       port map ( O=>nx60014z1, I0=>exit_Linit_lpi_1, I1=>
      exit_histogram_init_1_sva, I2=>nx10935z2, I3=>nx60014z2);
   ix23219z1316 : LUT2
      generic map (INIT => X"1") 
       port map ( O=>nx23219z2, I0=>system_input_c_sva(7), I1=>
      system_input_c_sva(6));
   ix6197z3611 : LUT4
      generic map (INIT => X"08F7") 
       port map ( O=>nx6197z3, I0=>nx51398z3, I1=>nx23219z2, I2=>
      system_input_c_sva(0), I3=>buffer_sel_1_sva);
   ix39764z23815 : LUT4
      generic map (INIT => X"57DF") 
       port map ( O=>nx39764z6, I0=>nx39764z7, I1=>nx20706z3, I2=>
      system_input_window_3_sva(0), I3=>system_input_window_6_sva(0));
   ix39764z1319 : LUT2
      generic map (INIT => X"4") 
       port map ( O=>nx39764z2, I0=>nx20706z2, I1=>nx39764z3);
   ix40761z23812 : LUT4
      generic map (INIT => X"57DF") 
       port map ( O=>nx40761z4, I0=>nx39764z7, I1=>nx20706z3, I2=>
      system_input_window_3_sva(1), I3=>system_input_window_6_sva(1));
   ix40761z1319 : LUT2
      generic map (INIT => X"4") 
       port map ( O=>nx40761z2, I0=>nx40761z3, I1=>nx39764z3);
   ix39764z1325 : LUT2
      generic map (INIT => X"4") 
       port map ( O=>nx39764z7, I0=>nx39764z3, I1=>exit_histogram_init_1_sva
   );
   ix41758z23812 : LUT4
      generic map (INIT => X"57DF") 
       port map ( O=>nx41758z4, I0=>nx39764z7, I1=>nx20706z3, I2=>
      system_input_window_3_sva(2), I3=>system_input_window_6_sva(2));
   ix41758z1319 : LUT2
      generic map (INIT => X"4") 
       port map ( O=>nx41758z2, I0=>nx41758z3, I1=>nx39764z3);
   ix56146z17701 : LUT4
      generic map (INIT => X"4000") 
       port map ( O=>nx56146z4, I0=>nx59203z5, I1=>nx20706z3, I2=>
      system_input_window_8_sva(0), I3=>exit_histogram_init_1_sva);
   ix56146z25654 : LUT4
      generic map (INIT => X"5F13") 
       port map ( O=>nx56146z2, I0=>nx56146z3, I1=>window_2_lpi_1(0), I2=>
      system_input_window_5_sva(0), I3=>exit_histogram_init_1_sva);
   ix55149z17700 : LUT4
      generic map (INIT => X"4000") 
       port map ( O=>nx55149z3, I0=>nx59203z5, I1=>nx20706z3, I2=>
      system_input_window_8_sva(1), I3=>exit_histogram_init_1_sva);
   ix55149z25654 : LUT4
      generic map (INIT => X"5F13") 
       port map ( O=>nx55149z2, I0=>nx56146z3, I1=>window_2_lpi_1(1), I2=>
      system_input_window_5_sva(1), I3=>exit_histogram_init_1_sva);
   ix54152z17700 : LUT4
      generic map (INIT => X"4000") 
       port map ( O=>nx54152z3, I0=>nx59203z5, I1=>nx20706z3, I2=>
      system_input_window_8_sva(2), I3=>exit_histogram_init_1_sva);
   ix56146z1332 : LUT3
      generic map (INIT => X"10") 
       port map ( O=>nx56146z3, I0=>nx59203z5, I1=>nx20706z3, I2=>
      exit_histogram_init_1_sva);
   ix54152z25654 : LUT4
      generic map (INIT => X"5F13") 
       port map ( O=>nx54152z2, I0=>nx56146z3, I1=>window_2_lpi_1(2), I2=>
      system_input_window_5_sva(2), I3=>exit_histogram_init_1_sva);
   ix20706z8482 : LUT4
      generic map (INIT => X"1BFF") 
       port map ( O=>nx20706z2, I0=>nx20706z3, I1=>
      system_input_window_4_sva(0), I2=>system_input_window_7_sva(0), I3=>
      exit_histogram_init_1_sva);
   ix40761z8483 : LUT4
      generic map (INIT => X"1BFF") 
       port map ( O=>nx40761z3, I0=>nx20706z3, I1=>
      system_input_window_4_sva(1), I2=>system_input_window_7_sva(1), I3=>
      exit_histogram_init_1_sva);
   ix20706z1319 : LUT4
      generic map (INIT => X"0001") 
       port map ( O=>nx20706z5, I0=>system_input_c_filter_sva(6), I1=>
      system_input_c_filter_sva(4), I2=>system_input_c_filter_sva(3), I3=>
      system_input_c_filter_sva(2));
   ix20706z1318 : LUT3
      generic map (INIT => X"01") 
       port map ( O=>nx20706z4, I0=>system_input_c_filter_sva(8), I1=>
      system_input_c_filter_sva(1), I2=>system_input_c_filter_sva(0));
   ix20706z1324 : LUT4
      generic map (INIT => X"0008") 
       port map ( O=>nx20706z3, I0=>nx20706z4, I1=>nx20706z5, I2=>
      system_input_c_filter_sva(7), I3=>system_input_c_filter_sva(5));
   ix41758z8483 : LUT4
      generic map (INIT => X"1BFF") 
       port map ( O=>nx41758z3, I0=>nx20706z3, I1=>
      system_input_window_4_sva(2), I2=>system_input_window_7_sva(2), I3=>
      exit_histogram_init_1_sva);
   ix53518z46166 : LUT4
      generic map (INIT => X"AF33") 
       port map ( O=>nx53518z2, I0=>nx59203z5, I1=>clip_window_qr_1_lpi_1(0), 
      I2=>system_input_window_8_sva(0), I3=>exit_histogram_init_1_sva);
   ix54515z46166 : LUT4
      generic map (INIT => X"AF33") 
       port map ( O=>nx54515z2, I0=>nx59203z5, I1=>clip_window_qr_1_lpi_1(1), 
      I2=>system_input_window_8_sva(1), I3=>exit_histogram_init_1_sva);
   ix55512z46166 : LUT4
      generic map (INIT => X"AF33") 
       port map ( O=>nx55512z2, I0=>nx59203z5, I1=>clip_window_qr_1_lpi_1(2), 
      I2=>system_input_window_8_sva(2), I3=>exit_histogram_init_1_sva);
   ix42392z8482 : LUT4
      generic map (INIT => X"1BFF") 
       port map ( O=>nx42392z2, I0=>nx39764z3, I1=>
      system_input_window_6_sva(0), I2=>system_input_window_7_sva(0), I3=>
      exit_histogram_init_1_sva);
   ix41395z8482 : LUT4
      generic map (INIT => X"1BFF") 
       port map ( O=>nx41395z2, I0=>nx39764z3, I1=>
      system_input_window_6_sva(1), I2=>system_input_window_7_sva(1), I3=>
      exit_histogram_init_1_sva);
   ix39764z1326 : LUT2
      generic map (INIT => X"1") 
       port map ( O=>nx39764z5, I0=>system_input_r_filter_sva(5), I1=>
      system_input_r_filter_sva(2));
   ix39764z1318 : LUT4
      generic map (INIT => X"0001") 
       port map ( O=>nx39764z4, I0=>system_input_r_filter_sva(7), I1=>
      system_input_r_filter_sva(6), I2=>system_input_r_filter_sva(3), I3=>
      system_input_r_filter_sva(0));
   ix39764z1324 : LUT4
      generic map (INIT => X"0008") 
       port map ( O=>nx39764z3, I0=>nx39764z4, I1=>nx39764z5, I2=>
      system_input_r_filter_sva(4), I3=>system_input_r_filter_sva(1));
   ix40398z8482 : LUT4
      generic map (INIT => X"1BFF") 
       port map ( O=>nx40398z2, I0=>nx39764z3, I1=>
      system_input_window_6_sva(2), I2=>system_input_window_7_sva(2), I3=>
      exit_histogram_init_1_sva);
   ix11038z1320 : LUT3
      generic map (INIT => X"06") 
       port map ( O=>nx11038z1, I0=>median_i_1_lpi_2(1), I1=>
      median_i_1_lpi_2(0), I2=>exit_histogram_init_1_sva);
   ix10041z1420 : LUT4
      generic map (INIT => X"006A") 
       port map ( O=>nx10041z1, I0=>median_i_1_lpi_2(2), I1=>
      median_i_1_lpi_2(1), I2=>median_i_1_lpi_2(0), I3=>
      exit_histogram_init_1_sva);
   ix9044z1319 : LUT2
      generic map (INIT => X"2") 
       port map ( O=>nx9044z4, I0=>median_i_1_lpi_2(0), I1=>
      exit_histogram_init_1_sva);
   ix9044z28621 : LUT4
      generic map (INIT => X"6AAA") 
       port map ( O=>nx9044z2, I0=>nx9044z3, I1=>nx9044z4, I2=>
      median_i_1_lpi_2(2), I3=>median_i_1_lpi_2(1));
   ix9044z1315 : LUT3
      generic map (INIT => X"01") 
       port map ( O=>nx9044z1, I0=>fsm_output(6), I1=>fsm_output(5), I2=>
      fsm_output(4));
   ix59203z1322 : LUT2
      generic map (INIT => X"8") 
       port map ( O=>nx59203z1, I0=>nx59203z2, I1=>nx59203z5);
   ix60067z1570 : LUT4
      generic map (INIT => X"0100") 
       port map ( O=>nx60067z1, I0=>fsm_output(6), I1=>fsm_output(5), I2=>
      fsm_output(4), I3=>exit_histogram_init_1_sva);
   ix58700z58900 : LUT4
      generic map (INIT => X"E0F0") 
       port map ( O=>nx58700z3, I0=>nx60878z2, I1=>nx58700z4, I2=>
      exit_if_if_if_1_if_for_lpi_1, I3=>asn_sft_1_lpi_1);
   ix35363z1442 : LUT3
      generic map (INIT => X"80") 
       port map ( O=>nx35363z1, I0=>fsm_output(4), I1=>
      io_read_in_data_vld_rsc_sft_lpi_1_dfm, I2=>asn_104_itm);
   ix29676z34082 : LUT4
      generic map (INIT => X"8000") 
       port map ( O=>nx29676z1, I0=>nx42113z4, I1=>nx42113z9, I2=>nx29676z2, 
      I3=>sfi_if_if_read_mem_mcu_data_rsc_lpi_1);
   ix25079z1334 : LUT3
      generic map (INIT => X"14") 
       port map ( O=>nx25079z1, I0=>sfi_if_if_read_mem_mcu_data_rsc_lpi_1, 
      I1=>p_1_lpi_3(1), I2=>p_1_lpi_3(0));
   ix24082z6502 : LUT4
      generic map (INIT => X"1444") 
       port map ( O=>nx24082z1, I0=>sfi_if_if_read_mem_mcu_data_rsc_lpi_1, 
      I1=>p_1_lpi_3(2), I2=>p_1_lpi_3(1), I3=>p_1_lpi_3(0));
   ix29676z1348 : LUT4
      generic map (INIT => X"0020") 
       port map ( O=>nx29676z3, I0=>asn_sft_3_lpi_1, I1=>
      exit_if_if_if_1_if_for_lpi_1, I2=>exit_Linit_lpi_1, I3=>
      exit_histogram_init_1_sva);
   ix29676z1319 : LUT2
      generic map (INIT => X"4") 
       port map ( O=>nx29676z2, I0=>nx51398z8, I1=>nx29676z3);
   ix26076z34082 : LUT4
      generic map (INIT => X"8000") 
       port map ( O=>nx26076z1, I0=>nx42113z4, I1=>nx42113z9, I2=>nx29676z2, 
      I3=>nx42113z14);
   ix715z1570 : LUT4
      generic map (INIT => X"0100") 
       port map ( O=>nx715z1, I0=>nx42113z2, I1=>nx51398z8, I2=>nx715z2, I3
      =>nx19476z2);
   ix56505z18762 : LUT4
      generic map (INIT => X"4419") 
       port map ( O=>nx56505z15, I0=>nx56505z4, I1=>nx56505z12, I2=>
      nx56505z13, I3=>nx56505z14);
   ix56505z26236 : LUT4
      generic map (INIT => X"6158") 
       port map ( O=>nx56505z3, I0=>nx56505z4, I1=>nx56505z12, I2=>
      nx56505z13, I3=>nx56505z14);
   ix56505z1323 : LUT3
      generic map (INIT => X"08") 
       port map ( O=>nx56505z2, I0=>nx56505z3, I1=>nx56505z15, I2=>
      frame_sva(0));
   ix10935z58662 : LUT4
      generic map (INIT => X"E000") 
       port map ( O=>nx10935z5, I0=>fsm_output(3), I1=>fsm_output(0), I2=>
      exit_if_if_if_1_if_for_lpi_1, I3=>asn_sft_1_lpi_1);
   ix10935z34082 : LUT4
      generic map (INIT => X"8000") 
       port map ( O=>nx10935z1, I0=>nx10935z2, I1=>nx10935z4, I2=>nx10935z5, 
      I3=>sfi_system_input_land_1_lpi_1);
   ix56331z10470 : LUT4
      generic map (INIT => X"23C4") 
       port map ( O=>nx56331z1, I0=>nx56505z4, I1=>nx56505z12, I2=>
      nx56505z13, I3=>nx56505z14);
   ix55334z35783 : LUT4
      generic map (INIT => X"86A5") 
       port map ( O=>nx55334z1, I0=>nx56505z4, I1=>nx56505z12, I2=>
      nx56505z13, I3=>nx56505z14);
   ix56505z39833 : LUT4
      generic map (INIT => X"9669") 
       port map ( O=>nx56505z14, I0=>frame_sva(9), I1=>frame_sva(3), I2=>b_1, 
      I3=>nx56505z7);
   ix56505z28356 : LUT4
      generic map (INIT => X"6996") 
       port map ( O=>nx56505z12, I0=>b_1, I1=>nx56505z5, I2=>nx56505z10, I3
      =>nx56505z11);
   ix56505z1537 : LUT3
      generic map (INIT => X"D4") 
       port map ( O=>nx56505z11, I0=>frame_sva(8), I1=>frame_sva(6), I2=>
      frame_sva(2));
   ix56505z12332 : LUT4
      generic map (INIT => X"2B00") 
       port map ( O=>nx56505z10, I0=>frame_sva(7), I1=>frame_sva(5), I2=>
      frame_sva(1), I3=>frame_sva(4));
   ix56505z7284 : LUT4
      generic map (INIT => X"174D") 
       port map ( O=>nx56505z5, I0=>nx56505z6, I1=>nx56505z8, I2=>nx56505z9, 
      I3=>frame_sva(4));
   ix56505z1473 : LUT3
      generic map (INIT => X"96") 
       port map ( O=>nx56505z9, I0=>frame_sva(8), I1=>frame_sva(6), I2=>
      frame_sva(2));
   ix56505z1534 : LUT3
      generic map (INIT => X"D4") 
       port map ( O=>nx56505z8, I0=>frame_sva(7), I1=>frame_sva(5), I2=>
      frame_sva(1));
   ix56505z1471 : LUT3
      generic map (INIT => X"96") 
       port map ( O=>nx56505z7, I0=>frame_sva(7), I1=>frame_sva(5), I2=>
      frame_sva(1));
   ix56505z1498 : LUT3
      generic map (INIT => X"B2") 
       port map ( O=>nx56505z6, I0=>nx56505z7, I1=>frame_sva(9), I2=>
      frame_sva(3));
   ix56505z39832 : LUT4
      generic map (INIT => X"9669") 
       port map ( O=>nx56505z13, I0=>nx56505z6, I1=>nx56505z8, I2=>nx56505z9, 
      I3=>frame_sva(4));
   ix56505z7574 : LUT4
      generic map (INIT => X"1871") 
       port map ( O=>nx56505z4, I0=>b_1, I1=>nx56505z5, I2=>nx56505z10, I3=>
      nx56505z11);
   ix54337z7460 : LUT4
      generic map (INIT => X"1802") 
       port map ( O=>nx54337z1, I0=>nx56505z4, I1=>nx56505z12, I2=>
      nx56505z13, I3=>nx56505z14);
   ix46688z51736 : LUT4
      generic map (INIT => X"C4F5") 
       port map ( O=>nx46688z2, I0=>exit_if_if_if_1_if_for_lpi_1, I1=>
      sfi_system_input_land_1_lpi_1, I2=>exit_histogram_init_1_sva, I3=>
      nx42113z2);
   ix46688z1322 : LUT2
      generic map (INIT => X"8") 
       port map ( O=>nx46688z1, I0=>nx50292z3, I1=>fsm_output(3));
   ix50292z9507 : LUT4
      generic map (INIT => X"2000") 
       port map ( O=>nx50292z2, I0=>nx50292z3, I1=>nx58700z4, I2=>
      exit_if_if_if_1_if_for_lpi_1, I3=>asn_sft_1_lpi_1);
   ix50292z1498 : LUT3
      generic map (INIT => X"B8") 
       port map ( O=>nx50292z1, I0=>nx50292z2, I1=>fsm_output(3), I2=>
      fsm_output(0));
   ix19476z1323 : LUT3
      generic map (INIT => X"08") 
       port map ( O=>nx19476z2, I0=>asn_sft_3_lpi_1, I1=>exit_Linit_lpi_1, 
      I2=>exit_histogram_init_1_sva);
   ix715z1317 : LUT2
      generic map (INIT => X"2") 
       port map ( O=>nx715z2, I0=>exit_if_if_if_1_if_for_lpi_1, I1=>
      exit_histogram_init_1_sva);
   ix42113z1332 : LUT2
      generic map (INIT => X"4") 
       port map ( O=>nx42113z15, I0=>sfi_if_if_read_mem_mcu_data_rsc_lpi_1, 
      I1=>p_1_lpi_3(0));
   ix42113z1553 : LUT3
      generic map (INIT => X"E2") 
       port map ( O=>nx42113z14, I0=>asn_sft_2_lpi_1, I1=>
      sfi_if_if_read_mem_mcu_data_rsc_lpi_1, I2=>if_if_equal_cse_lpi_1);
   ix42113z1582 : LUT4
      generic map (INIT => X"0100") 
       port map ( O=>nx42113z13, I0=>
      if_if_read_mem_mcu_data_rsc_sft_lpi_1(31), I1=>
      if_if_read_mem_mcu_data_rsc_sft_lpi_1(25), I2=>
      if_if_read_mem_mcu_data_rsc_sft_lpi_1(5), I3=>
      if_if_read_mem_mcu_data_rsc_sft_lpi_1(0));
   ix42113z1326 : LUT4
      generic map (INIT => X"0001") 
       port map ( O=>nx42113z12, I0=>
      if_if_read_mem_mcu_data_rsc_sft_lpi_1(30), I1=>
      if_if_read_mem_mcu_data_rsc_sft_lpi_1(7), I2=>
      if_if_read_mem_mcu_data_rsc_sft_lpi_1(4), I3=>
      if_if_read_mem_mcu_data_rsc_sft_lpi_1(2));
   ix42113z1325 : LUT4
      generic map (INIT => X"0001") 
       port map ( O=>nx42113z11, I0=>
      if_if_read_mem_mcu_data_rsc_sft_lpi_1(27), I1=>
      if_if_read_mem_mcu_data_rsc_sft_lpi_1(24), I2=>
      if_if_read_mem_mcu_data_rsc_sft_lpi_1(23), I3=>
      if_if_read_mem_mcu_data_rsc_sft_lpi_1(20));
   ix42113z1324 : LUT4
      generic map (INIT => X"0001") 
       port map ( O=>nx42113z10, I0=>
      if_if_read_mem_mcu_data_rsc_sft_lpi_1(29), I1=>
      if_if_read_mem_mcu_data_rsc_sft_lpi_1(19), I2=>
      if_if_read_mem_mcu_data_rsc_sft_lpi_1(18), I3=>
      if_if_read_mem_mcu_data_rsc_sft_lpi_1(13));
   ix42113z34090 : LUT4
      generic map (INIT => X"8000") 
       port map ( O=>nx42113z9, I0=>nx42113z10, I1=>nx42113z11, I2=>
      nx42113z12, I3=>nx42113z13);
   ix42113z1322 : LUT4
      generic map (INIT => X"0001") 
       port map ( O=>nx42113z8, I0=>
      if_if_read_mem_mcu_data_rsc_sft_lpi_1(26), I1=>
      if_if_read_mem_mcu_data_rsc_sft_lpi_1(14), I2=>
      if_if_read_mem_mcu_data_rsc_sft_lpi_1(9), I3=>
      if_if_read_mem_mcu_data_rsc_sft_lpi_1(3));
   ix42113z1321 : LUT4
      generic map (INIT => X"0001") 
       port map ( O=>nx42113z7, I0=>
      if_if_read_mem_mcu_data_rsc_sft_lpi_1(15), I1=>
      if_if_read_mem_mcu_data_rsc_sft_lpi_1(11), I2=>
      if_if_read_mem_mcu_data_rsc_sft_lpi_1(6), I3=>
      if_if_read_mem_mcu_data_rsc_sft_lpi_1(1));
   ix42113z1320 : LUT4
      generic map (INIT => X"0001") 
       port map ( O=>nx42113z6, I0=>
      if_if_read_mem_mcu_data_rsc_sft_lpi_1(22), I1=>
      if_if_read_mem_mcu_data_rsc_sft_lpi_1(12), I2=>
      if_if_read_mem_mcu_data_rsc_sft_lpi_1(10), I3=>
      if_if_read_mem_mcu_data_rsc_sft_lpi_1(8));
   ix42113z1319 : LUT4
      generic map (INIT => X"0001") 
       port map ( O=>nx42113z5, I0=>
      if_if_read_mem_mcu_data_rsc_sft_lpi_1(28), I1=>
      if_if_read_mem_mcu_data_rsc_sft_lpi_1(21), I2=>
      if_if_read_mem_mcu_data_rsc_sft_lpi_1(17), I3=>
      if_if_read_mem_mcu_data_rsc_sft_lpi_1(16));
   ix42113z34085 : LUT4
      generic map (INIT => X"8000") 
       port map ( O=>nx42113z4, I0=>nx42113z5, I1=>nx42113z6, I2=>nx42113z7, 
      I3=>nx42113z8);
   ix42113z1444 : LUT3
      generic map (INIT => X"80") 
       port map ( O=>nx42113z3, I0=>nx42113z4, I1=>nx42113z9, I2=>nx42113z14
   );
   ix42113z12237 : LUT4
      generic map (INIT => X"2AAA") 
       port map ( O=>nx42113z2, I0=>nx42113z3, I1=>nx42113z15, I2=>
      p_1_lpi_3(2), I3=>p_1_lpi_3(1));
   ix51398z30889 : LUT4
      generic map (INIT => X"737F") 
       port map ( O=>nx51398z8, I0=>in_data_vld_rsc_z, I1=>fsm_output(3), I2
      =>exit_histogram_init_1_sva, I3=>
      io_read_in_data_vld_rsc_sft_lpi_1_dfm_2);
   ix51398z5714 : LUT4
      generic map (INIT => X"1130") 
       port map ( O=>nx51398z1, I0=>nx51398z2, I1=>nx51398z8, I2=>
      asn_sft_3_lpi_1, I3=>exit_histogram_init_1_sva);
   ix7623z34083 : LUT4
      generic map (INIT => X"8000") 
       port map ( O=>nx7623z2, I0=>nx50292z3, I1=>fsm_output(3), I2=>
      exit_if_if_if_1_if_for_lpi_1, I3=>asn_sft_1_lpi_1);
   ix7623z1514 : LUT3
      generic map (INIT => X"C8") 
       port map ( O=>nx7623z1, I0=>nx60878z2, I1=>nx7623z2, I2=>nx58700z4);
   ix30850z1322 : LUT2
      generic map (INIT => X"8") 
       port map ( O=>nx30850z1, I0=>fsm_output(4), I1=>and_76_itm_1);
   ix60878z36771 : LUT4
      generic map (INIT => X"8A80") 
       port map ( O=>nx60878z2, I0=>nx60878z3, I1=>nx60878z5, I2=>
      fsm_output(3), I3=>histogram_init_idx_sva(2));
   ix29518z1317 : LUT4
      generic map (INIT => X"0001") 
       port map ( O=>nx29518z3, I0=>buffer_buf_vinit_ndx_sva(9), I1=>
      buffer_buf_vinit_ndx_sva(8), I2=>buffer_buf_vinit_ndx_sva(7), I3=>
      buffer_buf_vinit_ndx_sva(6));
   ix29518z1316 : LUT4
      generic map (INIT => X"0001") 
       port map ( O=>nx29518z2, I0=>buffer_buf_vinit_ndx_sva(5), I1=>
      buffer_buf_vinit_ndx_sva(4), I2=>buffer_buf_vinit_ndx_sva(3), I3=>
      buffer_buf_vinit_ndx_sva(2));
   ix29518z1322 : LUT4
      generic map (INIT => X"0008") 
       port map ( O=>nx29518z1, I0=>nx29518z2, I1=>nx29518z3, I2=>
      buffer_buf_vinit_ndx_sva(1), I3=>buffer_buf_vinit_ndx_sva(0));
   ix50292z41400 : LUT4
      generic map (INIT => X"9C93") 
       port map ( O=>nx50292z4, I0=>nx60878z4, I1=>nx50292z5, I2=>
      fsm_output(3), I3=>histogram_init_idx_sva(1));
   ix50292z43037 : LUT4
      generic map (INIT => X"A2F7") 
       port map ( O=>nx50292z5, I0=>fsm_output(3), I1=>
      histogram_init_idx_1_lpi_1(0), I2=>sfi_system_input_land_1_lpi_1, I3=>
      histogram_init_idx_sva(0));
   ix60878z10308 : LUT4
      generic map (INIT => X"2320") 
       port map ( O=>nx60878z3, I0=>nx60878z4, I1=>nx50292z5, I2=>
      fsm_output(3), I3=>histogram_init_idx_sva(1));
   ix51289z27276 : LUT4
      generic map (INIT => X"656A") 
       port map ( O=>nx51289z1, I0=>nx60878z3, I1=>nx60878z5, I2=>
      fsm_output(3), I3=>histogram_init_idx_sva(2));
   ix28896z1530 : LUT3
      generic map (INIT => X"D8") 
       port map ( O=>nx28896z1, I0=>nx26902z2, I1=>nx23875z35, I2=>
      nx23875z40);
   ix27899z1542 : LUT3
      generic map (INIT => X"E4") 
       port map ( O=>nx27899z1, I0=>nx26902z2, I1=>nx23875z7, I2=>nx23875z33
   );
   ix26902z1348 : LUT3
      generic map (INIT => X"20") 
       port map ( O=>nx26902z3, I0=>asn_sft_3_lpi_1_dfm, I1=>
      exit_Linit_lpi_1_dfm_1, I2=>exit_Linit_sva_2);
   ix26902z1331 : LUT3
      generic map (INIT => X"10") 
       port map ( O=>nx26902z2, I0=>nx48581z7, I1=>nx23875z5, I2=>nx26902z3
   );
   ix26902z1542 : LUT3
      generic map (INIT => X"E4") 
       port map ( O=>nx26902z1, I0=>nx26902z2, I1=>nx23875z41, I2=>
      nx23875z42);
   ix9441z49122 : LUT4
      generic map (INIT => X"BABF") 
       port map ( O=>nx9441z2, I0=>fsm_output(4), I1=>
      system_input_window_7_sva(0), I2=>exit_histogram_init_1_sva, I3=>
      shift_window_slc_system_input_window_23_21_ncse_lpi_1_dfm_1(0));
   ix10438z49122 : LUT4
      generic map (INIT => X"BABF") 
       port map ( O=>nx10438z2, I0=>fsm_output(4), I1=>
      system_input_window_7_sva(1), I2=>exit_histogram_init_1_sva, I3=>
      shift_window_slc_system_input_window_23_21_ncse_lpi_1_dfm_1(1));
   ix11435z49122 : LUT4
      generic map (INIT => X"BABF") 
       port map ( O=>nx11435z2, I0=>fsm_output(4), I1=>
      system_input_window_7_sva(2), I2=>exit_histogram_init_1_sva, I3=>
      shift_window_slc_system_input_window_23_21_ncse_lpi_1_dfm_1(2));
   ix49742z1316 : LUT2
      generic map (INIT => X"2") 
       port map ( O=>nx49742z1, I0=>histogram_init_idx_1_lpi_1(0), I1=>
      sfi_system_input_land_1_lpi_1);
   ix60878z1319 : LUT2
      generic map (INIT => X"2") 
       port map ( O=>nx60878z4, I0=>histogram_init_idx_1_lpi_1(1), I1=>
      sfi_system_input_land_1_lpi_1);
   ix60878z1320 : LUT2
      generic map (INIT => X"2") 
       port map ( O=>nx60878z5, I0=>histogram_init_idx_1_lpi_1(2), I1=>
      sfi_system_input_land_1_lpi_1);
   ix61107z1320 : LUT2
      generic map (INIT => X"6") 
       port map ( O=>nx61107z1, I0=>system_input_c_sva(7), I1=>
      system_input_c_sva(6));
   ix9044z1318 : LUT2
      generic map (INIT => X"2") 
       port map ( O=>nx9044z3, I0=>median_i_1_lpi_2(3), I1=>
      exit_histogram_init_1_sva);
   ix10935z1319 : LUT2
      generic map (INIT => X"2") 
       port map ( O=>nx10935z4, I0=>exit_Linit_lpi_1, I1=>
      exit_histogram_init_1_sva);
   ix34709z1322 : LUT2
      generic map (INIT => X"8") 
       port map ( O=>nx34709z1, I0=>in_data_vld_rsc_z, I1=>
      exit_histogram_init_1_sva);
   ix10935z1408 : LUT3
      generic map (INIT => X"5C") 
       port map ( O=>nx10935z3, I0=>nx51398z2, I1=>asn_sft_3_lpi_1, I2=>
      exit_histogram_init_1_sva);
   ix53617z1324 : LUT4
      generic map (INIT => X"0008") 
       port map ( O=>nx53617z4, I0=>exit_if_if_if_1_if_for_lpi_1, I1=>
      asn_sft_1_lpi_1, I2=>if_if_if_acc_5_psp_lpi_1(2), I3=>
      if_if_if_acc_5_psp_lpi_1(1));
   ix53617z1319 : LUT3
      generic map (INIT => X"04") 
       port map ( O=>nx53617z2, I0=>if_if_if_acc_5_psp_lpi_1(0), I1=>
      frame_sva(0), I2=>nx53617z3);
   ix58700z1346 : LUT3
      generic map (INIT => X"1D") 
       port map ( O=>nx58700z4, I0=>asn_sft_lpi_1, I1=>
      sfi_system_input_land_1_lpi_1, I2=>if_if_equal_cse_lpi_1);
   ix50292z3364 : LUT4
      generic map (INIT => X"0800") 
       port map ( O=>nx50292z3, I0=>asn_sft_3_lpi_1, I1=>exit_Linit_lpi_1, 
      I2=>exit_histogram_init_1_sva, I3=>
      io_read_in_data_vld_rsc_sft_lpi_1_dfm_2);
   ix53617z34082 : LUT4
      generic map (INIT => X"8000") 
       port map ( O=>nx53617z1, I0=>nx50292z3, I1=>nx58700z4, I2=>nx53617z2, 
      I3=>nx53617z4);
   ix5327z1455 : LUT3
      generic map (INIT => X"8D") 
       port map ( O=>nx5327z1, I0=>nx6324z2, I1=>nx5327z2, I2=>nx6324z15);
   ix6324z1428 : LUT3
      generic map (INIT => X"72") 
       port map ( O=>nx6324z1, I0=>nx6324z2, I1=>nx6324z4, I2=>nx6324z18);
   ix7321z64802 : LUT4
      generic map (INIT => X"F800") 
       port map ( O=>nx7321z1, I0=>nx49337z2, I1=>nx7321z2, I2=>nx7321z4, I3
      =>nx6324z25);
   ix5327z1543 : LUT3
      generic map (INIT => X"E4") 
       port map ( O=>nx5327z2, I0=>nx6324z5, I1=>nx6324z7, I2=>nx51331z3);
   ix406z1392 : LUT3
      generic map (INIT => X"4E") 
       port map ( O=>nx406z1, I0=>nx6324z2, I1=>nx5327z2, I2=>nx6324z15);
   ix6324z2832 : LUT4
      generic map (INIT => X"05DD") 
       port map ( O=>nx6324z18, I0=>nx170z3, I1=>nx6324z19, I2=>nx6324z23, 
      I3=>nx6324z21);
   ix6324z1505 : LUT3
      generic map (INIT => X"B1") 
       port map ( O=>nx6324z15, I0=>nx170z2, I1=>nx6324z16, I2=>nx6324z17);
   ix6324z1356 : LUT3
      generic map (INIT => X"27") 
       port map ( O=>nx6324z4, I0=>nx6324z5, I1=>nx6324z13, I2=>nx6324z14);
   ix6324z1494 : LUT3
      generic map (INIT => X"B2") 
       port map ( O=>nx6324z3, I0=>nx6324z4, I1=>nx6324z15, I2=>nx6324z18);
   ix6324z45125 : LUT4
      generic map (INIT => X"AB22") 
       port map ( O=>nx6324z2, I0=>nx6324z3, I1=>nx7321z4, I2=>nx6324z24, I3
      =>nx6324z25);
   ix1403z1491 : LUT3
      generic map (INIT => X"B1") 
       port map ( O=>nx1403z1, I0=>nx6324z2, I1=>nx6324z4, I2=>nx6324z18);
   ix6324z64682 : LUT4
      generic map (INIT => X"F770") 
       port map ( O=>nx6324z25, I0=>nx170z23, I1=>nx170z30, I2=>nx49337z2, 
      I3=>nx170z43);
   ix51331z1542 : LUT3
      generic map (INIT => X"E4") 
       port map ( O=>nx51331z1, I0=>nx23875z2, I1=>nx51331z2, I2=>nx51331z10
   );
   ix50334z1530 : LUT3
      generic map (INIT => X"D8") 
       port map ( O=>nx50334z1, I0=>nx23875z2, I1=>nx23875z4, I2=>nx50334z2
   );
   ix51331z63243 : LUT4
      generic map (INIT => X"F1E0") 
       port map ( O=>nx51331z10, I0=>nx48581z7, I1=>nx23875z5, I2=>
      nx51331z11, I3=>nx51331z12);
   ix24872z1530 : LUT3
      generic map (INIT => X"D8") 
       port map ( O=>nx24872z1, I0=>nx23875z2, I1=>nx51331z2, I2=>nx51331z10
   );
   ix50334z1543 : LUT3
      generic map (INIT => X"E4") 
       port map ( O=>nx50334z2, I0=>nx6324z5, I1=>nx6324z13, I2=>nx6324z14);
   ix51331z63236 : LUT4
      generic map (INIT => X"F1E0") 
       port map ( O=>nx51331z3, I0=>nx48581z10, I1=>nx7321z5, I2=>nx51331z4, 
      I3=>nx51331z5);
   ix6324z1366 : LUT3
      generic map (INIT => X"27") 
       port map ( O=>nx6324z14, I0=>nx170z4, I1=>nx170z6, I2=>nx170z50);
   ix6324z63246 : LUT4
      generic map (INIT => X"F1E0") 
       port map ( O=>nx6324z13, I0=>nx48581z10, I1=>nx7321z5, I2=>nx7321z7, 
      I3=>nx7321z8);
   ix6324z1434 : LUT3
      generic map (INIT => X"72") 
       port map ( O=>nx6324z7, I0=>nx170z4, I1=>nx170z46, I2=>nx6324z8);
   ix6324z1396 : LUT3
      generic map (INIT => X"4D") 
       port map ( O=>nx6324z6, I0=>nx6324z7, I1=>nx6324z13, I2=>nx6324z14);
   ix6324z64662 : LUT4
      generic map (INIT => X"F770") 
       port map ( O=>nx6324z5, I0=>nx49337z2, I1=>nx7321z2, I2=>nx7321z4, I3
      =>nx6324z6);
   ix51331z1531 : LUT3
      generic map (INIT => X"D8") 
       port map ( O=>nx51331z2, I0=>nx6324z5, I1=>nx6324z7, I2=>nx51331z3);
   ix23875z63237 : LUT4
      generic map (INIT => X"F1E0") 
       port map ( O=>nx23875z4, I0=>nx48581z7, I1=>nx23875z5, I2=>nx29367z11, 
      I3=>nx29367z12);
   ix23875z1359 : LUT3
      generic map (INIT => X"2B") 
       port map ( O=>nx23875z3, I0=>nx23875z4, I1=>nx51331z2, I2=>nx50334z2
   );
   ix23875z1457 : LUT3
      generic map (INIT => X"8E") 
       port map ( O=>nx23875z2, I0=>nx23875z3, I1=>nx22878z2, I2=>nx23875z47
   );
   ix23875z1542 : LUT3
      generic map (INIT => X"E4") 
       port map ( O=>nx23875z1, I0=>nx23875z2, I1=>nx23875z4, I2=>nx50334z2
   );
   ix6324z1345 : LUT2
      generic map (INIT => X"8") 
       port map ( O=>nx6324z24, I0=>nx49337z2, I1=>nx7321z2);
   ix7321z821 : LUT4
      generic map (INIT => X"FE10") 
       port map ( O=>nx7321z4, I0=>nx48581z10, I1=>nx7321z5, I2=>nx7321z36, 
      I3=>nx7321z37);
   ix23875z1488 : LUT3
      generic map (INIT => X"80") 
       port map ( O=>nx23875z47, I0=>nx49337z2, I1=>nx7321z2, I2=>nx7321z4);
   ix22878z63235 : LUT4
      generic map (INIT => X"F1E0") 
       port map ( O=>nx22878z2, I0=>nx48581z7, I1=>nx23875z5, I2=>nx48581z9, 
      I3=>nx29367z5);
   ix22878z34082 : LUT4
      generic map (INIT => X"8000") 
       port map ( O=>nx22878z1, I0=>nx49337z2, I1=>nx7321z2, I2=>nx22878z2, 
      I3=>nx7321z4);
   ix6324z63241 : LUT4
      generic map (INIT => X"F1E0") 
       port map ( O=>nx6324z8, I0=>nx48581z13, I1=>nx49337z3, I2=>nx6324z9, 
      I3=>nx6324z10);
   ix6324z1506 : LUT3
      generic map (INIT => X"B1") 
       port map ( O=>nx6324z16, I0=>nx170z4, I1=>nx170z46, I2=>nx6324z8);
   ix170z1531 : LUT3
      generic map (INIT => X"D8") 
       port map ( O=>nx170z2, I0=>nx170z3, I1=>nx6324z19, I2=>nx6324z23);
   ix170z1455 : LUT3
      generic map (INIT => X"8D") 
       port map ( O=>nx170z1, I0=>nx170z2, I1=>nx6324z16, I2=>nx6324z17);
   ix6324z652 : LUT4
      generic map (INIT => X"FD54") 
       port map ( O=>nx6324z23, I0=>nx6324z20, I1=>nx6324z21, I2=>nx6324z17, 
      I3=>nx6324z22);
   ix6324z38066 : LUT4
      generic map (INIT => X"8F80") 
       port map ( O=>nx6324z17, I0=>nx49337z9, I1=>nx49337z19, I2=>nx170z22, 
      I3=>nx170z47);
   ix6324z38070 : LUT4
      generic map (INIT => X"8F80") 
       port map ( O=>nx6324z21, I0=>nx170z7, I1=>nx170z11, I2=>nx170z22, I3
      =>nx170z39);
   ix6324z1334 : LUT2
      generic map (INIT => X"1") 
       port map ( O=>nx6324z20, I0=>nx49337z2, I1=>nx7321z2);
   ix6324z55924 : LUT4
      generic map (INIT => X"D540") 
       port map ( O=>nx6324z19, I0=>nx6324z20, I1=>nx6324z21, I2=>nx6324z17, 
      I3=>nx6324z22);
   ix7321z1846 : LUT4
      generic map (INIT => X"0213") 
       port map ( O=>nx7321z2, I0=>nx49337z6, I1=>nx7321z3, I2=>nx170z44, I3
      =>nx170z45);
   ix170z4978 : LUT4
      generic map (INIT => X"0E1F") 
       port map ( O=>nx170z50, I0=>nx48581z13, I1=>nx49337z3, I2=>nx49337z5, 
      I3=>nx170z51);
   ix170z1438 : LUT3
      generic map (INIT => X"4E") 
       port map ( O=>nx170z47, I0=>nx49337z6, I1=>nx170z48, I2=>nx170z49);
   ix170z64855 : LUT4
      generic map (INIT => X"F808") 
       port map ( O=>nx170z46, I0=>nx49337z9, I1=>nx49337z19, I2=>nx170z22, 
      I3=>nx170z47);
   ix170z1395 : LUT3
      generic map (INIT => X"27") 
       port map ( O=>nx170z43, I0=>nx49337z6, I1=>nx170z44, I2=>nx170z45);
   ix170z34123 : LUT4
      generic map (INIT => X"8000") 
       port map ( O=>nx170z42, I0=>nx170z8, I1=>nx170z9, I2=>nx170z10, I3=>
      nx170z11);
   ix170z1430 : LUT3
      generic map (INIT => X"4E") 
       port map ( O=>nx170z39, I0=>nx49337z6, I1=>nx170z40, I2=>nx170z41);
   ix170z37967 : LUT4
      generic map (INIT => X"8F08") 
       port map ( O=>nx170z38, I0=>nx49337z9, I1=>nx49337z19, I2=>nx170z39, 
      I3=>nx170z42);
   ix170z3254 : LUT4
      generic map (INIT => X"077F") 
       port map ( O=>nx170z22, I0=>nx170z23, I1=>nx170z30, I2=>nx170z38, I3
      =>nx170z43);
   ix170z64815 : LUT4
      generic map (INIT => X"F808") 
       port map ( O=>nx170z6, I0=>nx170z7, I1=>nx170z11, I2=>nx170z22, I3=>
      nx170z39);
   ix170z1460 : LUT3
      generic map (INIT => X"8E") 
       port map ( O=>nx170z5, I0=>nx170z6, I1=>nx170z46, I2=>nx170z50);
   ix170z1459 : LUT3
      generic map (INIT => X"8E") 
       port map ( O=>nx170z4, I0=>nx170z5, I1=>nx49337z2, I2=>nx7321z2);
   ix170z1544 : LUT3
      generic map (INIT => X"E4") 
       port map ( O=>nx170z3, I0=>nx170z4, I1=>nx170z6, I2=>nx170z50);
   ix7321z1512 : LUT4
      generic map (INIT => X"00C4") 
       port map ( O=>nx7321z3, I0=>nx49337z32, I1=>nx170z23, I2=>nx170z31, 
      I3=>nx170z36);
   ix6324z51639 : LUT4
      generic map (INIT => X"C480") 
       port map ( O=>nx6324z22, I0=>nx49337z6, I1=>nx7321z3, I2=>nx170z44, 
      I3=>nx170z45);
   ix49337z63235 : LUT4
      generic map (INIT => X"F1E0") 
       port map ( O=>nx49337z2, I0=>nx48581z13, I1=>nx49337z3, I2=>nx7321z38, 
      I3=>nx7321z39);
   ix31361z818 : LUT4
      generic map (INIT => X"FE10") 
       port map ( O=>nx31361z1, I0=>nx48581z4, I1=>nx29367z2, I2=>nx29367z17, 
      I3=>nx29367z18);
   ix30364z818 : LUT4
      generic map (INIT => X"FE10") 
       port map ( O=>nx30364z1, I0=>nx48581z4, I1=>nx29367z2, I2=>nx29367z10, 
      I3=>nx29367z13);
   ix29367z5664 : LUT4
      generic map (INIT => X"10FE") 
       port map ( O=>nx29367z1, I0=>nx48581z4, I1=>nx29367z2, I2=>nx29367z4, 
      I3=>nx29367z15);
   ix16594z63234 : LUT4
      generic map (INIT => X"F1E0") 
       port map ( O=>nx16594z1, I0=>nx48581z4, I1=>nx29367z2, I2=>nx29367z17, 
      I3=>nx29367z18);
   ix17591z63234 : LUT4
      generic map (INIT => X"F1E0") 
       port map ( O=>nx17591z1, I0=>nx48581z4, I1=>nx29367z2, I2=>nx29367z10, 
      I3=>nx29367z13);
   ix18588z58899 : LUT4
      generic map (INIT => X"E0F1") 
       port map ( O=>nx18588z1, I0=>nx48581z4, I1=>nx29367z2, I2=>nx29367z4, 
      I3=>nx29367z15);
   ix64549z1577 : LUT3
      generic map (INIT => X"E4") 
       port map ( O=>nx64549z36, I0=>nx64549z8, I1=>nx29367z21, I2=>
      nx29367z22);
   ix64549z1428 : LUT3
      generic map (INIT => X"72") 
       port map ( O=>nx64549z1, I0=>nx64549z2, I1=>nx64549z35, I2=>
      nx64549z36);
   ix10z1342 : LUT3
      generic map (INIT => X"1B") 
       port map ( O=>nx10z2, I0=>nx64549z8, I1=>nx64549z10, I2=>nx10z3);
   ix64549z1375 : LUT3
      generic map (INIT => X"1B") 
       port map ( O=>nx64549z35, I0=>nx64549z5, I1=>nx29367z19, I2=>
      nx29367z20);
   ix64549z1356 : LUT3
      generic map (INIT => X"27") 
       port map ( O=>nx64549z4, I0=>nx64549z5, I1=>nx64549z7, I2=>nx29367z14
   );
   ix64549z1429 : LUT3
      generic map (INIT => X"71") 
       port map ( O=>nx64549z3, I0=>nx64549z4, I1=>nx64549z35, I2=>nx10z2);
   ix64549z37901 : LUT4
      generic map (INIT => X"8EEA") 
       port map ( O=>nx64549z2, I0=>nx64549z3, I1=>nx29367z6, I2=>nx29367z8, 
      I3=>nx1007z2);
   ix10z1353 : LUT3
      generic map (INIT => X"27") 
       port map ( O=>nx10z1, I0=>nx64549z2, I1=>nx64549z4, I2=>nx10z2);
   ix29367z1449 : LUT3
      generic map (INIT => X"72") 
       port map ( O=>nx29367z22, I0=>nx64549z14, I1=>nx51331z27, I2=>
      nx51331z28);
   ix29367z1549 : LUT3
      generic map (INIT => X"D8") 
       port map ( O=>nx29367z20, I0=>nx64549z8, I1=>nx29367z21, I2=>
      nx29367z22);
   ix29367z1547 : LUT3
      generic map (INIT => X"D8") 
       port map ( O=>nx29367z18, I0=>nx64549z5, I1=>nx29367z19, I2=>
      nx29367z20);
   ix29367z834 : LUT4
      generic map (INIT => X"FE10") 
       port map ( O=>nx29367z17, I0=>nx48581z7, I1=>nx23875z5, I2=>
      nx51331z11, I3=>nx51331z12);
   ix29367z57918 : LUT4
      generic map (INIT => X"DD0D") 
       port map ( O=>nx29367z16, I0=>nx29367z10, I1=>nx29367z13, I2=>
      nx29367z17, I3=>nx29367z18);
   ix29367z831 : LUT4
      generic map (INIT => X"FE10") 
       port map ( O=>nx29367z14, I0=>nx23875z8, I1=>nx51331z13, I2=>
      nx51331z15, I3=>nx51331z16);
   ix29367z836 : LUT4
      generic map (INIT => X"FE10") 
       port map ( O=>nx29367z19, I0=>nx23875z8, I1=>nx51331z13, I2=>
      nx51331z35, I3=>nx51331z36);
   ix10z1430 : LUT3
      generic map (INIT => X"72") 
       port map ( O=>nx10z3, I0=>nx64549z14, I1=>nx51331z23, I2=>nx51331z24
   );
   ix29367z838 : LUT4
      generic map (INIT => X"FE10") 
       port map ( O=>nx29367z21, I0=>nx64549z11, I1=>nx51331z17, I2=>
      nx51331z25, I3=>nx51331z26);
   ix64549z827 : LUT4
      generic map (INIT => X"FE10") 
       port map ( O=>nx64549z10, I0=>nx64549z11, I1=>nx51331z17, I2=>
      nx51331z19, I3=>nx51331z22);
   ix64549z1464 : LUT3
      generic map (INIT => X"8E") 
       port map ( O=>nx64549z9, I0=>nx64549z10, I1=>nx29367z21, I2=>nx10z3);
   ix64549z1463 : LUT3
      generic map (INIT => X"8E") 
       port map ( O=>nx64549z8, I0=>nx64549z9, I1=>nx29367z8, I2=>nx1007z2);
   ix64549z1536 : LUT3
      generic map (INIT => X"D8") 
       port map ( O=>nx64549z7, I0=>nx64549z8, I1=>nx64549z10, I2=>nx10z3);
   ix64549z1531 : LUT3
      generic map (INIT => X"D4") 
       port map ( O=>nx64549z6, I0=>nx64549z7, I1=>nx29367z19, I2=>
      nx29367z14);
   ix64549z1550 : LUT3
      generic map (INIT => X"E8") 
       port map ( O=>nx64549z5, I0=>nx64549z6, I1=>nx29367z6, I2=>nx29367z7
   );
   ix29367z1554 : LUT3
      generic map (INIT => X"E4") 
       port map ( O=>nx29367z13, I0=>nx64549z5, I1=>nx64549z7, I2=>
      nx29367z14);
   ix29367z827 : LUT4
      generic map (INIT => X"FE10") 
       port map ( O=>nx29367z10, I0=>nx48581z7, I1=>nx23875z5, I2=>
      nx29367z11, I3=>nx29367z12);
   ix29367z54452 : LUT4
      generic map (INIT => X"CF8A") 
       port map ( O=>nx29367z9, I0=>nx29367z4, I1=>nx29367z10, I2=>
      nx29367z13, I3=>nx29367z15);
   ix1007z1342 : LUT3
      generic map (INIT => X"1B") 
       port map ( O=>nx1007z2, I0=>nx64549z14, I1=>nx51331z30, I2=>
      nx51331z31);
   ix29367z63241 : LUT4
      generic map (INIT => X"F1E0") 
       port map ( O=>nx29367z8, I0=>nx64549z11, I1=>nx51331z17, I2=>
      nx51331z29, I3=>nx51331z32);
   ix29367z1548 : LUT4
      generic map (INIT => X"00E4") 
       port map ( O=>nx29367z7, I0=>nx64549z14, I1=>nx51331z30, I2=>
      nx51331z31, I3=>nx29367z8);
   ix29367z63239 : LUT4
      generic map (INIT => X"F1E0") 
       port map ( O=>nx29367z6, I0=>nx23875z8, I1=>nx51331z13, I2=>
      nx51331z37, I3=>nx51331z38);
   ix29367z1332 : LUT2
      generic map (INIT => X"4") 
       port map ( O=>nx29367z15, I0=>nx29367z6, I1=>nx29367z7);
   ix29367z821 : LUT4
      generic map (INIT => X"FE10") 
       port map ( O=>nx29367z4, I0=>nx48581z7, I1=>nx23875z5, I2=>nx48581z9, 
      I3=>nx29367z5);
   ix29367z42958 : LUT4
      generic map (INIT => X"A2AA") 
       port map ( O=>nx29367z3, I0=>fsm_output(5), I1=>nx29367z4, I2=>
      nx29367z6, I3=>nx29367z7);
   ix29367z1477 : LUT3
      generic map (INIT => X"A2") 
       port map ( O=>nx29367z2, I0=>nx29367z3, I1=>nx29367z9, I2=>nx29367z16
   );
   ix48581z842 : LUT4
      generic map (INIT => X"FE10") 
       port map ( O=>nx48581z24, I0=>nx48581z7, I1=>nx23875z5, I2=>
      nx23875z35, I3=>nx23875z40);
   ix48581z37352 : LUT4
      generic map (INIT => X"8CAF") 
       port map ( O=>nx48581z23, I0=>nx48581z22, I1=>nx48581z24, I2=>
      thresholding_asn_itm(1), I3=>thresholding_asn_itm(0));
   ix48581z63256 : LUT4
      generic map (INIT => X"F1E0") 
       port map ( O=>nx48581z22, I0=>nx48581z7, I1=>nx23875z5, I2=>nx23875z7, 
      I3=>nx23875z33);
   ix48581z64104 : LUT4
      generic map (INIT => X"F531") 
       port map ( O=>nx48581z21, I0=>nx48581z6, I1=>nx48581z22, I2=>
      thresholding_asn_itm(2), I3=>thresholding_asn_itm(1));
   ix23875z859 : LUT4
      generic map (INIT => X"FE10") 
       port map ( O=>nx23875z42, I0=>nx48581z10, I1=>nx7321z5, I2=>
      nx23875z43, I3=>nx23875z46);
   ix23875z63274 : LUT4
      generic map (INIT => X"F1E0") 
       port map ( O=>nx23875z41, I0=>nx23875z8, I1=>nx51331z13, I2=>
      nx23875z10, I3=>nx23875z21);
   ix23875z63273 : LUT4
      generic map (INIT => X"F1E0") 
       port map ( O=>nx23875z40, I0=>nx23875z8, I1=>nx51331z13, I2=>
      nx23875z28, I3=>nx23875z29);
   ix23875z852 : LUT4
      generic map (INIT => X"FE10") 
       port map ( O=>nx23875z35, I0=>nx48581z10, I1=>nx7321z5, I2=>
      nx23875z36, I3=>nx23875z39);
   ix23875z850 : LUT4
      generic map (INIT => X"FE10") 
       port map ( O=>nx23875z33, I0=>nx48581z10, I1=>nx7321z5, I2=>
      nx48581z12, I3=>nx23875z34);
   ix23875z63240 : LUT4
      generic map (INIT => X"F1E0") 
       port map ( O=>nx23875z7, I0=>nx23875z8, I1=>nx51331z13, I2=>
      nx23875z25, I3=>nx23875z26);
   ix23875z18939 : LUT4
      generic map (INIT => X"44D4") 
       port map ( O=>nx23875z6, I0=>nx23875z7, I1=>nx23875z33, I2=>
      nx23875z35, I3=>nx23875z40);
   ix23875z1496 : LUT4
      generic map (INIT => X"00B2") 
       port map ( O=>nx23875z5, I0=>nx23875z6, I1=>nx23875z41, I2=>
      nx23875z42, I3=>fsm_output(5));
   ix48581z46577 : LUT4
      generic map (INIT => X"B0BB") 
       port map ( O=>nx48581z20, I0=>nx48581z9, I1=>nx29367z5, I2=>
      nx29367z11, I3=>nx29367z12);
   ix51331z63245 : LUT4
      generic map (INIT => X"F1E0") 
       port map ( O=>nx51331z12, I0=>nx23875z8, I1=>nx51331z13, I2=>
      nx51331z35, I3=>nx51331z36);
   ix51331z828 : LUT4
      generic map (INIT => X"FE10") 
       port map ( O=>nx51331z11, I0=>nx48581z10, I1=>nx7321z5, I2=>nx51331z4, 
      I3=>nx51331z5);
   ix29367z63245 : LUT4
      generic map (INIT => X"F1E0") 
       port map ( O=>nx29367z12, I0=>nx23875z8, I1=>nx51331z13, I2=>
      nx51331z15, I3=>nx51331z16);
   ix29367z828 : LUT4
      generic map (INIT => X"FE10") 
       port map ( O=>nx29367z11, I0=>nx48581z10, I1=>nx7321z5, I2=>nx7321z7, 
      I3=>nx7321z8);
   ix48581z57922 : LUT4
      generic map (INIT => X"DD0D") 
       port map ( O=>nx48581z19, I0=>nx29367z11, I1=>nx29367z12, I2=>
      nx51331z11, I3=>nx51331z12);
   ix51331z855 : LUT4
      generic map (INIT => X"FE10") 
       port map ( O=>nx51331z38, I0=>nx23875z11, I1=>nx7321z9, I2=>
      nx51331z39, I3=>nx51331z42);
   ix51331z854 : LUT4
      generic map (INIT => X"FE10") 
       port map ( O=>nx51331z37, I0=>nx64549z11, I1=>nx51331z17, I2=>
      nx51331z29, I3=>nx51331z32);
   ix51331z63269 : LUT4
      generic map (INIT => X"F1E0") 
       port map ( O=>nx51331z36, I0=>nx64549z11, I1=>nx51331z17, I2=>
      nx51331z25, I3=>nx51331z26);
   ix51331z852 : LUT4
      generic map (INIT => X"FE10") 
       port map ( O=>nx51331z35, I0=>nx23875z11, I1=>nx7321z9, I2=>nx51331z6, 
      I3=>nx51331z7);
   ix51331z63249 : LUT4
      generic map (INIT => X"F1E0") 
       port map ( O=>nx51331z16, I0=>nx64549z11, I1=>nx51331z17, I2=>
      nx51331z19, I3=>nx51331z22);
   ix51331z832 : LUT4
      generic map (INIT => X"FE10") 
       port map ( O=>nx51331z15, I0=>nx23875z11, I1=>nx7321z9, I2=>nx7321z11, 
      I3=>nx7321z14);
   ix51331z10209 : LUT4
      generic map (INIT => X"22B2") 
       port map ( O=>nx51331z14, I0=>nx51331z15, I1=>nx51331z16, I2=>
      nx51331z35, I3=>nx51331z36);
   ix51331z46894 : LUT4
      generic map (INIT => X"B200") 
       port map ( O=>nx51331z13, I0=>nx51331z14, I1=>nx51331z37, I2=>
      nx51331z38, I3=>fsm_output(5));
   ix23875z46588 : LUT4
      generic map (INIT => X"B0BB") 
       port map ( O=>nx23875z32, I0=>nx23875z10, I1=>nx23875z21, I2=>
      nx23875z25, I3=>nx23875z26);
   ix23875z63262 : LUT4
      generic map (INIT => X"F1E0") 
       port map ( O=>nx23875z29, I0=>nx64549z11, I1=>nx51331z17, I2=>
      nx23875z30, I3=>nx23875z31);
   ix23875z63261 : LUT4
      generic map (INIT => X"F1E0") 
       port map ( O=>nx23875z28, I0=>nx23875z11, I1=>nx7321z9, I2=>
      nx23875z17, I3=>nx23875z18);
   ix23875z63259 : LUT4
      generic map (INIT => X"F1E0") 
       port map ( O=>nx23875z26, I0=>nx64549z11, I1=>nx51331z17, I2=>
      nx64549z13, I3=>nx23875z27);
   ix23875z63258 : LUT4
      generic map (INIT => X"F1E0") 
       port map ( O=>nx23875z25, I0=>nx23875z11, I1=>nx7321z9, I2=>
      nx23875z15, I3=>nx23875z16);
   ix23875z57926 : LUT4
      generic map (INIT => X"DD0D") 
       port map ( O=>nx23875z24, I0=>nx23875z25, I1=>nx23875z26, I2=>
      nx23875z28, I3=>nx23875z29);
   ix51331z1372 : LUT3
      generic map (INIT => X"1B") 
       port map ( O=>nx51331z32, I0=>nx7321z15, I1=>nx51331z33, I2=>
      nx51331z34);
   ix51331z1381 : LUT3
      generic map (INIT => X"27") 
       port map ( O=>nx51331z29, I0=>nx64549z14, I1=>nx51331z30, I2=>
      nx51331z31);
   ix51331z32045 : LUT4
      generic map (INIT => X"77F0") 
       port map ( O=>nx51331z28, I0=>nx49337z9, I1=>nx49337z19, I2=>
      median_max_1_1_lpi_1(0), I3=>and_18_psp);
   ix51331z36171 : LUT4
      generic map (INIT => X"880F") 
       port map ( O=>nx51331z27, I0=>nx49337z9, I1=>nx49337z19, I2=>
      median_max_0_lpi_1(0), I3=>and_20_psp_1);
   ix51331z1516 : LUT3
      generic map (INIT => X"B1") 
       port map ( O=>nx51331z26, I0=>nx64549z14, I1=>nx51331z27, I2=>
      nx51331z28);
   ix51331z1452 : LUT3
      generic map (INIT => X"72") 
       port map ( O=>nx51331z25, I0=>nx7321z15, I1=>nx51331z8, I2=>nx51331z9
   );
   ix51331z32041 : LUT4
      generic map (INIT => X"77F0") 
       port map ( O=>nx51331z24, I0=>nx170z7, I1=>nx170z11, I2=>
      median_max_1_1_lpi_1(1), I3=>and_18_psp);
   ix51331z36167 : LUT4
      generic map (INIT => X"880F") 
       port map ( O=>nx51331z23, I0=>nx170z7, I1=>nx170z11, I2=>
      median_max_0_lpi_1(1), I3=>and_20_psp_1);
   ix51331z1512 : LUT3
      generic map (INIT => X"B1") 
       port map ( O=>nx51331z22, I0=>nx64549z14, I1=>nx51331z23, I2=>
      nx51331z24);
   ix51331z1446 : LUT3
      generic map (INIT => X"72") 
       port map ( O=>nx51331z19, I0=>nx7321z15, I1=>nx51331z20, I2=>
      nx51331z21);
   ix51331z10213 : LUT4
      generic map (INIT => X"22B2") 
       port map ( O=>nx51331z18, I0=>nx51331z19, I1=>nx51331z22, I2=>
      nx51331z25, I3=>nx51331z26);
   ix51331z46898 : LUT4
      generic map (INIT => X"B200") 
       port map ( O=>nx51331z17, I0=>nx51331z18, I1=>nx51331z29, I2=>
      nx51331z32, I3=>fsm_output(5));
   ix23875z1564 : LUT3
      generic map (INIT => X"E4") 
       port map ( O=>nx23875z23, I0=>nx170z52, I1=>
      median_max2_1_lpi_1_dfm_1(2), I2=>median_max2_2_lpi_1_dfm_1(2));
   ix23875z1551 : LUT3
      generic map (INIT => X"D8") 
       port map ( O=>nx23875z22, I0=>nx64549z14, I1=>
      median_max2_3_lpi_1_dfm_1(2), I2=>median_max_5_2_lpi_1_dfm_1(2));
   ix23875z1572 : LUT3
      generic map (INIT => X"E4") 
       port map ( O=>nx23875z31, I0=>nx170z52, I1=>
      median_max2_1_lpi_1_dfm_1(0), I2=>median_max2_2_lpi_1_dfm_1(0));
   ix23875z1559 : LUT3
      generic map (INIT => X"D8") 
       port map ( O=>nx23875z30, I0=>nx64549z14, I1=>
      median_max2_3_lpi_1_dfm_1(0), I2=>median_max_5_2_lpi_1_dfm_1(0));
   ix23875z1568 : LUT3
      generic map (INIT => X"E4") 
       port map ( O=>nx23875z27, I0=>nx170z52, I1=>
      median_max2_1_lpi_1_dfm_1(1), I2=>median_max2_2_lpi_1_dfm_1(1));
   ix64549z1542 : LUT3
      generic map (INIT => X"D8") 
       port map ( O=>nx64549z13, I0=>nx64549z14, I1=>
      median_max2_3_lpi_1_dfm_1(1), I2=>median_max_5_2_lpi_1_dfm_1(1));
   ix64549z10207 : LUT4
      generic map (INIT => X"22B2") 
       port map ( O=>nx64549z12, I0=>nx64549z13, I1=>nx23875z27, I2=>
      nx23875z30, I3=>nx23875z31);
   ix64549z1466 : LUT4
      generic map (INIT => X"008E") 
       port map ( O=>nx64549z11, I0=>nx64549z12, I1=>nx23875z22, I2=>
      nx23875z23, I3=>fsm_output(5));
   ix23875z63254 : LUT4
      generic map (INIT => X"F1E0") 
       port map ( O=>nx23875z21, I0=>nx64549z11, I1=>nx51331z17, I2=>
      nx23875z22, I3=>nx23875z23);
   ix23875z63243 : LUT4
      generic map (INIT => X"F1E0") 
       port map ( O=>nx23875z10, I0=>nx23875z11, I1=>nx7321z9, I2=>
      nx23875z13, I3=>nx23875z20);
   ix23875z1324 : LUT2
      generic map (INIT => X"2") 
       port map ( O=>nx23875z9, I0=>nx23875z10, I1=>nx23875z21);
   ix23875z1507 : LUT4
      generic map (INIT => X"00BA") 
       port map ( O=>nx23875z8, I0=>nx23875z9, I1=>nx23875z24, I2=>
      nx23875z32, I3=>fsm_output(5));
   ix29367z822 : LUT4
      generic map (INIT => X"FE10") 
       port map ( O=>nx29367z5, I0=>nx23875z8, I1=>nx51331z13, I2=>
      nx51331z37, I3=>nx51331z38);
   ix7321z854 : LUT4
      generic map (INIT => X"FE10") 
       port map ( O=>nx7321z37, I0=>nx48581z13, I1=>nx49337z3, I2=>nx7321z38, 
      I3=>nx7321z39);
   ix7321z63269 : LUT4
      generic map (INIT => X"F1E0") 
       port map ( O=>nx7321z36, I0=>nx23875z11, I1=>nx7321z9, I2=>nx51331z39, 
      I3=>nx51331z42);
   ix51331z63238 : LUT4
      generic map (INIT => X"F1E0") 
       port map ( O=>nx51331z5, I0=>nx23875z11, I1=>nx7321z9, I2=>nx51331z6, 
      I3=>nx51331z7);
   ix51331z821 : LUT4
      generic map (INIT => X"FE10") 
       port map ( O=>nx51331z4, I0=>nx48581z13, I1=>nx49337z3, I2=>nx6324z9, 
      I3=>nx6324z10);
   ix7321z63241 : LUT4
      generic map (INIT => X"F1E0") 
       port map ( O=>nx7321z8, I0=>nx23875z11, I1=>nx7321z9, I2=>nx7321z11, 
      I3=>nx7321z14);
   ix7321z824 : LUT4
      generic map (INIT => X"FE10") 
       port map ( O=>nx7321z7, I0=>nx48581z13, I1=>nx49337z3, I2=>nx49337z5, 
      I3=>nx170z51);
   ix7321z10201 : LUT4
      generic map (INIT => X"22B2") 
       port map ( O=>nx7321z6, I0=>nx7321z7, I1=>nx7321z8, I2=>nx51331z4, I3
      =>nx51331z5);
   ix7321z46886 : LUT4
      generic map (INIT => X"B200") 
       port map ( O=>nx7321z5, I0=>nx7321z6, I1=>nx7321z36, I2=>nx7321z37, 
      I3=>fsm_output(5));
   ix23875z863 : LUT4
      generic map (INIT => X"FE10") 
       port map ( O=>nx23875z46, I0=>nx23875z11, I1=>nx7321z9, I2=>
      nx23875z13, I3=>nx23875z20);
   ix23875z860 : LUT4
      generic map (INIT => X"FE10") 
       port map ( O=>nx23875z43, I0=>nx48581z13, I1=>nx49337z3, I2=>
      nx23875z44, I3=>nx23875z45);
   ix23875z856 : LUT4
      generic map (INIT => X"FE10") 
       port map ( O=>nx23875z39, I0=>nx23875z11, I1=>nx7321z9, I2=>
      nx23875z17, I3=>nx23875z18);
   ix23875z853 : LUT4
      generic map (INIT => X"FE10") 
       port map ( O=>nx23875z36, I0=>nx48581z13, I1=>nx49337z3, I2=>
      nx23875z37, I3=>nx23875z38);
   ix51331z1394 : LUT3
      generic map (INIT => X"27") 
       port map ( O=>nx51331z42, I0=>nx7321z15, I1=>nx51331z33, I2=>
      nx51331z34);
   ix51331z1379 : LUT3
      generic map (INIT => X"1B") 
       port map ( O=>nx51331z39, I0=>nx170z52, I1=>nx51331z40, I2=>
      nx51331z41);
   ix51331z32026 : LUT4
      generic map (INIT => X"77F0") 
       port map ( O=>nx51331z9, I0=>nx49337z9, I1=>nx49337z19, I2=>
      median_max_3_1_lpi_1(0), I3=>and_14_psp);
   ix51331z36152 : LUT4
      generic map (INIT => X"880F") 
       port map ( O=>nx51331z8, I0=>nx49337z9, I1=>nx49337z19, I2=>
      median_max_2_1_lpi_1(0), I3=>and_16_psp);
   ix51331z1497 : LUT3
      generic map (INIT => X"B1") 
       port map ( O=>nx51331z7, I0=>nx7321z15, I1=>nx51331z8, I2=>nx51331z9
   );
   ix51331z1433 : LUT3
      generic map (INIT => X"72") 
       port map ( O=>nx51331z6, I0=>nx170z52, I1=>nx6324z11, I2=>nx6324z12);
   ix51331z32038 : LUT4
      generic map (INIT => X"77F0") 
       port map ( O=>nx51331z21, I0=>nx170z7, I1=>nx170z11, I2=>
      median_max_3_1_lpi_1(1), I3=>and_14_psp);
   ix51331z36164 : LUT4
      generic map (INIT => X"880F") 
       port map ( O=>nx51331z20, I0=>nx170z7, I1=>nx170z11, I2=>
      median_max_2_1_lpi_1(1), I3=>and_16_psp);
   ix7321z1504 : LUT3
      generic map (INIT => X"B1") 
       port map ( O=>nx7321z14, I0=>nx7321z15, I1=>nx51331z20, I2=>
      nx51331z21);
   ix7321z1438 : LUT3
      generic map (INIT => X"72") 
       port map ( O=>nx7321z11, I0=>nx170z52, I1=>nx7321z12, I2=>nx7321z13);
   ix7321z10205 : LUT4
      generic map (INIT => X"22B2") 
       port map ( O=>nx7321z10, I0=>nx7321z11, I1=>nx7321z14, I2=>nx51331z6, 
      I3=>nx51331z7);
   ix7321z1464 : LUT4
      generic map (INIT => X"008E") 
       port map ( O=>nx7321z9, I0=>nx7321z10, I1=>nx51331z39, I2=>nx51331z42, 
      I3=>fsm_output(6));
   ix23875z57921 : LUT4
      generic map (INIT => X"DD0D") 
       port map ( O=>nx23875z19, I0=>nx23875z13, I1=>nx23875z20, I2=>
      nx23875z15, I3=>nx23875z16);
   ix23875z1547 : LUT3
      generic map (INIT => X"D8") 
       port map ( O=>nx23875z18, I0=>nx7321z15, I1=>
      median_max_6_2_lpi_1_dfm_1(0), I2=>median_max_7_3_lpi_1_dfm_1(0));
   ix23875z1558 : LUT3
      generic map (INIT => X"E4") 
       port map ( O=>nx23875z17, I0=>nx64549z14, I1=>
      median_max2_3_lpi_1_dfm_1(0), I2=>median_max_5_2_lpi_1_dfm_1(0));
   ix23875z1545 : LUT3
      generic map (INIT => X"D8") 
       port map ( O=>nx23875z16, I0=>nx7321z15, I1=>
      median_max_6_2_lpi_1_dfm_1(1), I2=>median_max_7_3_lpi_1_dfm_1(1));
   ix23875z1556 : LUT3
      generic map (INIT => X"E4") 
       port map ( O=>nx23875z15, I0=>nx64549z14, I1=>
      median_max2_3_lpi_1_dfm_1(1), I2=>median_max_5_2_lpi_1_dfm_1(1));
   ix23875z46570 : LUT4
      generic map (INIT => X"B0BB") 
       port map ( O=>nx23875z14, I0=>nx23875z15, I1=>nx23875z16, I2=>
      nx23875z17, I3=>nx23875z18);
   ix23875z1549 : LUT3
      generic map (INIT => X"D8") 
       port map ( O=>nx23875z20, I0=>nx7321z15, I1=>
      median_max_6_2_lpi_1_dfm_1(2), I2=>median_max_7_3_lpi_1_dfm_1(2));
   ix64549z1485 : LUT3
      generic map (INIT => X"8A") 
       port map ( O=>nx64549z34, I0=>fsm_output(6), I1=>
      median_max2_3_lpi_1_dfm_1(2), I2=>median_max_5_2_lpi_1_dfm_1(2));
   ix64549z1348 : LUT2
      generic map (INIT => X"2") 
       port map ( O=>nx64549z33, I0=>median_max2_3_lpi_1_dfm_1(2), I1=>
      median_max_5_2_lpi_1_dfm_1(2));
   ix64549z1422 : LUT4
      generic map (INIT => X"004D") 
       port map ( O=>nx64549z32, I0=>nx64549z16, I1=>nx64549z19, I2=>
      nx64549z22, I3=>nx64549z33);
   ix51331z36175 : LUT4
      generic map (INIT => X"880F") 
       port map ( O=>nx51331z31, I0=>nx170z23, I1=>nx170z30, I2=>
      median_max_0_lpi_1(2), I3=>and_20_psp_1);
   ix51331z36174 : LUT4
      generic map (INIT => X"880F") 
       port map ( O=>nx51331z30, I0=>nx170z23, I1=>nx170z30, I2=>
      median_max_1_1_lpi_1(2), I3=>and_18_psp);
   ix64549z1355 : LUT3
      generic map (INIT => X"0B") 
       port map ( O=>nx64549z31, I0=>nx51331z30, I1=>nx51331z31, I2=>
      fsm_output(6));
   ix64549z4419 : LUT4
      generic map (INIT => X"0C04") 
       port map ( O=>nx64549z30, I0=>median_max_1_1_lpi_1(2), I1=>
      median_max_0_lpi_1(2), I2=>and_20_psp_1, I3=>and_18_psp);
   ix64549z1426 : LUT4
      generic map (INIT => X"0054") 
       port map ( O=>nx64549z29, I0=>median_max_1_1_lpi_1(2), I1=>
      median_max_0_lpi_1(2), I2=>and_20_psp_1, I3=>and_18_psp);
   ix64549z64941 : LUT4
      generic map (INIT => X"F870") 
       port map ( O=>nx64549z28, I0=>nx170z23, I1=>nx170z30, I2=>nx64549z29, 
      I3=>nx64549z30);
   ix64549z1390 : LUT3
      generic map (INIT => X"32") 
       port map ( O=>nx64549z27, I0=>median_max_1_1_lpi_1(0), I1=>
      median_max_0_lpi_1(0), I2=>and_18_psp);
   ix64549z23180 : LUT4
      generic map (INIT => X"5551") 
       port map ( O=>nx64549z26, I0=>nx64549z24, I1=>nx64549z27, I2=>
      fsm_output(6), I3=>and_20_psp_1);
   ix64549z1500 : LUT3
      generic map (INIT => X"A2") 
       port map ( O=>nx64549z25, I0=>median_max_1_1_lpi_1(0), I1=>
      median_max_0_lpi_1(0), I2=>and_20_psp_1);
   ix64549z1369 : LUT3
      generic map (INIT => X"20") 
       port map ( O=>nx64549z24, I0=>fsm_output(6), I1=>
      median_max2_3_lpi_1_dfm_1(0), I2=>median_max_5_2_lpi_1_dfm_1(0));
   ix64549z23177 : LUT4
      generic map (INIT => X"5551") 
       port map ( O=>nx64549z23, I0=>nx64549z24, I1=>nx64549z25, I2=>
      fsm_output(6), I3=>and_18_psp);
   ix64549z64695 : LUT4
      generic map (INIT => X"F780") 
       port map ( O=>nx64549z22, I0=>nx49337z9, I1=>nx49337z19, I2=>
      nx64549z23, I3=>nx64549z26);
   ix64549z42522 : LUT4
      generic map (INIT => X"A0E4") 
       port map ( O=>nx64549z21, I0=>fsm_output(6), I1=>
      median_max_0_lpi_1(1), I2=>median_max2_3_lpi_1_dfm_1(1), I3=>
      and_20_psp_1);
   ix64549z64281 : LUT4
      generic map (INIT => X"F5E4") 
       port map ( O=>nx64549z20, I0=>fsm_output(6), I1=>
      median_max_0_lpi_1(1), I2=>median_max2_3_lpi_1_dfm_1(1), I3=>
      and_20_psp_1);
   ix64549z3267 : LUT4
      generic map (INIT => X"078F") 
       port map ( O=>nx64549z19, I0=>nx170z7, I1=>nx170z11, I2=>nx64549z20, 
      I3=>nx64549z21);
   ix64549z25678 : LUT4
      generic map (INIT => X"5F1B") 
       port map ( O=>nx64549z18, I0=>fsm_output(6), I1=>
      median_max_1_1_lpi_1(1), I2=>median_max_5_2_lpi_1_dfm_1(1), I3=>
      and_18_psp);
   ix64549z3917 : LUT4
      generic map (INIT => X"0A1B") 
       port map ( O=>nx64549z17, I0=>fsm_output(6), I1=>
      median_max_1_1_lpi_1(1), I2=>median_max_5_2_lpi_1_dfm_1(1), I3=>
      and_18_psp);
   ix64549z64929 : LUT4
      generic map (INIT => X"F870") 
       port map ( O=>nx64549z16, I0=>nx170z7, I1=>nx170z11, I2=>nx64549z17, 
      I3=>nx64549z18);
   ix64549z1405 : LUT4
      generic map (INIT => X"004D") 
       port map ( O=>nx64549z15, I0=>nx64549z16, I1=>nx64549z19, I2=>
      nx64549z22, I3=>nx64549z28);
   ix64549z46570 : LUT4
      generic map (INIT => X"B0BB") 
       port map ( O=>nx64549z14, I0=>nx64549z15, I1=>nx64549z31, I2=>
      nx64549z32, I3=>nx64549z34);
   ix23875z1554 : LUT3
      generic map (INIT => X"E4") 
       port map ( O=>nx23875z13, I0=>nx64549z14, I1=>
      median_max2_3_lpi_1_dfm_1(2), I2=>median_max_5_2_lpi_1_dfm_1(2));
   ix23875z1497 : LUT4
      generic map (INIT => X"00AC") 
       port map ( O=>nx23875z12, I0=>median_max_6_2_lpi_1_dfm_1(2), I1=>
      median_max_7_3_lpi_1_dfm_1(2), I2=>nx7321z15, I3=>nx23875z13);
   ix23875z48940 : LUT4
      generic map (INIT => X"BA00") 
       port map ( O=>nx23875z11, I0=>nx23875z12, I1=>nx23875z14, I2=>
      nx23875z19, I3=>fsm_output(6));
   ix23875z851 : LUT4
      generic map (INIT => X"FE10") 
       port map ( O=>nx23875z34, I0=>nx23875z11, I1=>nx7321z9, I2=>
      nx23875z15, I3=>nx23875z16);
   ix7321z1432 : LUT3
      generic map (INIT => X"27") 
       port map ( O=>nx7321z39, I0=>nx170z52, I1=>nx51331z40, I2=>nx51331z41
   );
   ix7321z1378 : LUT3
      generic map (INIT => X"1B") 
       port map ( O=>nx7321z38, I0=>nx49337z6, I1=>nx170z44, I2=>nx170z45);
   ix6324z32029 : LUT4
      generic map (INIT => X"77F0") 
       port map ( O=>nx6324z12, I0=>nx49337z9, I1=>nx49337z19, I2=>
      median_max_5_1_lpi_1(0), I3=>and_13_psp);
   ix6324z36155 : LUT4
      generic map (INIT => X"880F") 
       port map ( O=>nx6324z11, I0=>nx49337z9, I1=>nx49337z19, I2=>
      median_max_4_1_lpi_1(0), I3=>and_2_psp);
   ix6324z1500 : LUT3
      generic map (INIT => X"B1") 
       port map ( O=>nx6324z10, I0=>nx170z52, I1=>nx6324z11, I2=>nx6324z12);
   ix170z32066 : LUT4
      generic map (INIT => X"77F0") 
       port map ( O=>nx170z49, I0=>nx49337z9, I1=>nx49337z19, I2=>
      median_max_7_1_lpi_1(0), I3=>and_17_psp);
   ix170z36192 : LUT4
      generic map (INIT => X"880F") 
       port map ( O=>nx170z48, I0=>nx49337z9, I1=>nx49337z19, I2=>
      median_max_6_1_lpi_1(0), I3=>and_15_psp);
   ix6324z1436 : LUT3
      generic map (INIT => X"72") 
       port map ( O=>nx6324z9, I0=>nx49337z6, I1=>nx170z48, I2=>nx170z49);
   ix7321z32030 : LUT4
      generic map (INIT => X"77F0") 
       port map ( O=>nx7321z13, I0=>nx170z7, I1=>nx170z11, I2=>
      median_max_5_1_lpi_1(1), I3=>and_13_psp);
   ix7321z36156 : LUT4
      generic map (INIT => X"880F") 
       port map ( O=>nx7321z12, I0=>nx170z7, I1=>nx170z11, I2=>
      median_max_4_1_lpi_1(1), I3=>and_2_psp);
   ix170z1530 : LUT3
      generic map (INIT => X"45") 
       port map ( O=>nx170z72, I0=>fsm_output(5), I1=>
      median_max2_1_lpi_1_dfm_1(2), I2=>median_max2_2_lpi_1_dfm_1(2));
   ix170z1387 : LUT2
      generic map (INIT => X"2") 
       port map ( O=>nx170z71, I0=>median_max2_1_lpi_1_dfm_1(2), I1=>
      median_max2_2_lpi_1_dfm_1(2));
   ix170z1407 : LUT4
      generic map (INIT => X"0017") 
       port map ( O=>nx170z70, I0=>nx170z54, I1=>nx170z60, I2=>nx170z63, I3
      =>nx170z71);
   ix51331z36185 : LUT4
      generic map (INIT => X"880F") 
       port map ( O=>nx51331z41, I0=>nx170z23, I1=>nx170z30, I2=>
      median_max_4_1_lpi_1(2), I3=>and_2_psp);
   ix51331z36184 : LUT4
      generic map (INIT => X"880F") 
       port map ( O=>nx51331z40, I0=>nx170z23, I1=>nx170z30, I2=>
      median_max_5_1_lpi_1(2), I3=>and_13_psp);
   ix170z1559 : LUT3
      generic map (INIT => X"B0") 
       port map ( O=>nx170z69, I0=>nx51331z40, I1=>nx51331z41, I2=>
      fsm_output(5));
   ix170z3944 : LUT4
      generic map (INIT => X"0A02") 
       port map ( O=>nx170z68, I0=>median_max_4_1_lpi_1(2), I1=>
      median_max_5_1_lpi_1(2), I2=>and_2_psp, I3=>and_13_psp);
   ix170z1499 : LUT4
      generic map (INIT => X"0032") 
       port map ( O=>nx170z67, I0=>median_max_4_1_lpi_1(2), I1=>
      median_max_5_1_lpi_1(2), I2=>and_2_psp, I3=>and_13_psp);
   ix170z64979 : LUT4
      generic map (INIT => X"F870") 
       port map ( O=>nx170z66, I0=>nx170z23, I1=>nx170z30, I2=>nx170z67, I3
      =>nx170z68);
   ix170z22074 : LUT4
      generic map (INIT => X"50D8") 
       port map ( O=>nx170z65, I0=>fsm_output(5), I1=>
      median_max_4_1_lpi_1(1), I2=>median_max2_1_lpi_1_dfm_1(1), I3=>
      and_2_psp);
   ix170z65593 : LUT4
      generic map (INIT => X"FAD8") 
       port map ( O=>nx170z64, I0=>fsm_output(5), I1=>
      median_max_4_1_lpi_1(1), I2=>median_max2_1_lpi_1_dfm_1(1), I3=>
      and_2_psp);
   ix170z64976 : LUT4
      generic map (INIT => X"F870") 
       port map ( O=>nx170z63, I0=>nx170z7, I1=>nx170z11, I2=>nx170z64, I3=>
      nx170z65);
   ix170z46214 : LUT4
      generic map (INIT => X"AF27") 
       port map ( O=>nx170z62, I0=>fsm_output(5), I1=>
      median_max_5_1_lpi_1(1), I2=>median_max2_2_lpi_1_dfm_1(1), I3=>
      and_13_psp);
   ix170z2693 : LUT4
      generic map (INIT => X"0527") 
       port map ( O=>nx170z61, I0=>fsm_output(5), I1=>
      median_max_5_1_lpi_1(1), I2=>median_max2_2_lpi_1_dfm_1(1), I3=>
      and_13_psp);
   ix170z64973 : LUT4
      generic map (INIT => X"F870") 
       port map ( O=>nx170z60, I0=>nx170z7, I1=>nx170z11, I2=>nx170z61, I3=>
      nx170z62);
   ix170z1456 : LUT3
      generic map (INIT => X"54") 
       port map ( O=>nx170z59, I0=>median_max_4_1_lpi_1(0), I1=>
      median_max_5_1_lpi_1(0), I2=>and_13_psp);
   ix170z23152 : LUT4
      generic map (INIT => X"5515") 
       port map ( O=>nx170z58, I0=>nx170z56, I1=>nx170z59, I2=>fsm_output(5), 
      I3=>and_2_psp);
   ix170z1566 : LUT3
      generic map (INIT => X"C4") 
       port map ( O=>nx170z57, I0=>median_max_4_1_lpi_1(0), I1=>
      median_max_5_1_lpi_1(0), I2=>and_2_psp);
   ix170z1385 : LUT3
      generic map (INIT => X"10") 
       port map ( O=>nx170z56, I0=>fsm_output(5), I1=>
      median_max2_1_lpi_1_dfm_1(0), I2=>median_max2_2_lpi_1_dfm_1(0));
   ix170z23149 : LUT4
      generic map (INIT => X"5515") 
       port map ( O=>nx170z55, I0=>nx170z56, I1=>nx170z57, I2=>fsm_output(5), 
      I3=>and_13_psp);
   ix170z64727 : LUT4
      generic map (INIT => X"F780") 
       port map ( O=>nx170z54, I0=>nx49337z9, I1=>nx49337z19, I2=>nx170z55, 
      I3=>nx170z58);
   ix170z1389 : LUT4
      generic map (INIT => X"0017") 
       port map ( O=>nx170z53, I0=>nx170z54, I1=>nx170z60, I2=>nx170z63, I3
      =>nx170z66);
   ix170z46608 : LUT4
      generic map (INIT => X"B0BB") 
       port map ( O=>nx170z52, I0=>nx170z53, I1=>nx170z69, I2=>nx170z70, I3
      =>nx170z72);
   ix170z1541 : LUT3
      generic map (INIT => X"B1") 
       port map ( O=>nx170z51, I0=>nx170z52, I1=>nx7321z12, I2=>nx7321z13);
   ix170z32058 : LUT4
      generic map (INIT => X"77F0") 
       port map ( O=>nx170z41, I0=>nx170z7, I1=>nx170z11, I2=>
      median_max_7_1_lpi_1(1), I3=>and_17_psp);
   ix170z36184 : LUT4
      generic map (INIT => X"880F") 
       port map ( O=>nx170z40, I0=>nx170z7, I1=>nx170z11, I2=>
      median_max_6_1_lpi_1(1), I3=>and_15_psp);
   ix49337z1432 : LUT3
      generic map (INIT => X"72") 
       port map ( O=>nx49337z5, I0=>nx49337z6, I1=>nx170z40, I2=>nx170z41);
   ix49337z10199 : LUT4
      generic map (INIT => X"22B2") 
       port map ( O=>nx49337z4, I0=>nx49337z5, I1=>nx170z51, I2=>nx6324z9, 
      I3=>nx6324z10);
   ix49337z37668 : LUT4
      generic map (INIT => X"8E00") 
       port map ( O=>nx49337z3, I0=>nx49337z4, I1=>nx7321z38, I2=>nx7321z39, 
      I3=>fsm_output(5));
   ix48581z54393 : LUT4
      generic map (INIT => X"CF45") 
       port map ( O=>nx48581z18, I0=>nx48581z16, I1=>nx23875z44, I2=>
      nx23875z45, I3=>nx48581z17);
   ix48581z1559 : LUT3
      generic map (INIT => X"E4") 
       port map ( O=>nx48581z17, I0=>nx49337z6, I1=>
      median_max2_9_lpi_1_dfm_2(1), I2=>median_max_8_lpi_1_dfm_1(1));
   ix23875z1579 : LUT3
      generic map (INIT => X"E4") 
       port map ( O=>nx23875z38, I0=>nx7321z15, I1=>
      median_max_6_2_lpi_1_dfm_1(0), I2=>median_max_7_3_lpi_1_dfm_1(0));
   ix23875z1578 : LUT3
      generic map (INIT => X"E4") 
       port map ( O=>nx23875z37, I0=>nx49337z6, I1=>
      median_max2_9_lpi_1_dfm_2(0), I2=>median_max_8_lpi_1_dfm_1(0));
   ix48581z1558 : LUT3
      generic map (INIT => X"E4") 
       port map ( O=>nx48581z16, I0=>nx7321z15, I1=>
      median_max_6_2_lpi_1_dfm_1(1), I2=>median_max_7_3_lpi_1_dfm_1(1));
   ix48581z43044 : LUT4
      generic map (INIT => X"A2F3") 
       port map ( O=>nx48581z15, I0=>nx48581z16, I1=>nx23875z37, I2=>
      nx23875z38, I3=>nx48581z17);
   ix7321z1486 : LUT3
      generic map (INIT => X"8A") 
       port map ( O=>nx7321z35, I0=>fsm_output(6), I1=>
      median_max_6_2_lpi_1_dfm_1(2), I2=>median_max_7_3_lpi_1_dfm_1(2));
   ix7321z1349 : LUT2
      generic map (INIT => X"2") 
       port map ( O=>nx7321z34, I0=>median_max_6_2_lpi_1_dfm_1(2), I1=>
      median_max_7_3_lpi_1_dfm_1(2));
   ix7321z1423 : LUT4
      generic map (INIT => X"004D") 
       port map ( O=>nx7321z33, I0=>nx7321z17, I1=>nx7321z20, I2=>nx7321z23, 
      I3=>nx7321z34);
   ix51331z36178 : LUT4
      generic map (INIT => X"880F") 
       port map ( O=>nx51331z34, I0=>nx170z23, I1=>nx170z30, I2=>
      median_max_2_1_lpi_1(2), I3=>and_16_psp);
   ix51331z36177 : LUT4
      generic map (INIT => X"880F") 
       port map ( O=>nx51331z33, I0=>nx170z23, I1=>nx170z30, I2=>
      median_max_3_1_lpi_1(2), I3=>and_14_psp);
   ix7321z1356 : LUT3
      generic map (INIT => X"0B") 
       port map ( O=>nx7321z32, I0=>nx51331z33, I1=>nx51331z34, I2=>
      fsm_output(6));
   ix7321z4420 : LUT4
      generic map (INIT => X"0C04") 
       port map ( O=>nx7321z31, I0=>median_max_3_1_lpi_1(2), I1=>
      median_max_2_1_lpi_1(2), I2=>and_16_psp, I3=>and_14_psp);
   ix7321z1427 : LUT4
      generic map (INIT => X"0054") 
       port map ( O=>nx7321z30, I0=>median_max_3_1_lpi_1(2), I1=>
      median_max_2_1_lpi_1(2), I2=>and_16_psp, I3=>and_14_psp);
   ix7321z64942 : LUT4
      generic map (INIT => X"F870") 
       port map ( O=>nx7321z29, I0=>nx170z23, I1=>nx170z30, I2=>nx7321z30, 
      I3=>nx7321z31);
   ix7321z1391 : LUT3
      generic map (INIT => X"32") 
       port map ( O=>nx7321z28, I0=>median_max_3_1_lpi_1(0), I1=>
      median_max_2_1_lpi_1(0), I2=>and_14_psp);
   ix7321z23181 : LUT4
      generic map (INIT => X"5551") 
       port map ( O=>nx7321z27, I0=>nx7321z25, I1=>nx7321z28, I2=>
      fsm_output(6), I3=>and_16_psp);
   ix7321z1501 : LUT3
      generic map (INIT => X"A2") 
       port map ( O=>nx7321z26, I0=>median_max_3_1_lpi_1(0), I1=>
      median_max_2_1_lpi_1(0), I2=>and_16_psp);
   ix7321z1370 : LUT3
      generic map (INIT => X"20") 
       port map ( O=>nx7321z25, I0=>fsm_output(6), I1=>
      median_max_6_2_lpi_1_dfm_1(0), I2=>median_max_7_3_lpi_1_dfm_1(0));
   ix7321z23178 : LUT4
      generic map (INIT => X"5551") 
       port map ( O=>nx7321z24, I0=>nx7321z25, I1=>nx7321z26, I2=>
      fsm_output(6), I3=>and_14_psp);
   ix7321z64696 : LUT4
      generic map (INIT => X"F780") 
       port map ( O=>nx7321z23, I0=>nx49337z9, I1=>nx49337z19, I2=>nx7321z24, 
      I3=>nx7321z27);
   ix7321z42523 : LUT4
      generic map (INIT => X"A0E4") 
       port map ( O=>nx7321z22, I0=>fsm_output(6), I1=>
      median_max_2_1_lpi_1(1), I2=>median_max_6_2_lpi_1_dfm_1(1), I3=>
      and_16_psp);
   ix7321z64282 : LUT4
      generic map (INIT => X"F5E4") 
       port map ( O=>nx7321z21, I0=>fsm_output(6), I1=>
      median_max_2_1_lpi_1(1), I2=>median_max_6_2_lpi_1_dfm_1(1), I3=>
      and_16_psp);
   ix7321z3268 : LUT4
      generic map (INIT => X"078F") 
       port map ( O=>nx7321z20, I0=>nx170z7, I1=>nx170z11, I2=>nx7321z21, I3
      =>nx7321z22);
   ix7321z25679 : LUT4
      generic map (INIT => X"5F1B") 
       port map ( O=>nx7321z19, I0=>fsm_output(6), I1=>
      median_max_3_1_lpi_1(1), I2=>median_max_7_3_lpi_1_dfm_1(1), I3=>
      and_14_psp);
   ix7321z3918 : LUT4
      generic map (INIT => X"0A1B") 
       port map ( O=>nx7321z18, I0=>fsm_output(6), I1=>
      median_max_3_1_lpi_1(1), I2=>median_max_7_3_lpi_1_dfm_1(1), I3=>
      and_14_psp);
   ix7321z64930 : LUT4
      generic map (INIT => X"F870") 
       port map ( O=>nx7321z17, I0=>nx170z7, I1=>nx170z11, I2=>nx7321z18, I3
      =>nx7321z19);
   ix7321z1406 : LUT4
      generic map (INIT => X"004D") 
       port map ( O=>nx7321z16, I0=>nx7321z17, I1=>nx7321z20, I2=>nx7321z23, 
      I3=>nx7321z29);
   ix7321z46571 : LUT4
      generic map (INIT => X"B0BB") 
       port map ( O=>nx7321z15, I0=>nx7321z16, I1=>nx7321z32, I2=>nx7321z33, 
      I3=>nx7321z35);
   ix23875z1586 : LUT3
      generic map (INIT => X"E4") 
       port map ( O=>nx23875z45, I0=>nx7321z15, I1=>
      median_max_6_2_lpi_1_dfm_1(2), I2=>median_max_7_3_lpi_1_dfm_1(2));
   ix49337z1445 : LUT3
      generic map (INIT => X"51") 
       port map ( O=>nx49337z50, I0=>fsm_output(5), I1=>
      median_max2_9_lpi_1_dfm_2(2), I2=>median_max_8_lpi_1_dfm_1(2));
   ix49337z1367 : LUT2
      generic map (INIT => X"4") 
       port map ( O=>nx49337z49, I0=>median_max2_9_lpi_1_dfm_2(2), I1=>
      median_max_8_lpi_1_dfm_1(2));
   ix49337z1385 : LUT4
      generic map (INIT => X"0017") 
       port map ( O=>nx49337z48, I0=>nx49337z8, I1=>nx49337z38, I2=>
      nx49337z41, I3=>nx49337z49);
   ix170z36189 : LUT4
      generic map (INIT => X"880F") 
       port map ( O=>nx170z45, I0=>nx170z23, I1=>nx170z30, I2=>
      median_max_6_1_lpi_1(2), I3=>and_15_psp);
   ix170z36188 : LUT4
      generic map (INIT => X"880F") 
       port map ( O=>nx170z44, I0=>nx170z23, I1=>nx170z30, I2=>
      median_max_7_1_lpi_1(2), I3=>and_17_psp);
   ix49337z1537 : LUT3
      generic map (INIT => X"B0") 
       port map ( O=>nx49337z47, I0=>nx170z44, I1=>nx170z45, I2=>
      fsm_output(5));
   ix49337z3922 : LUT4
      generic map (INIT => X"0A02") 
       port map ( O=>nx49337z46, I0=>median_max_6_1_lpi_1(2), I1=>
      median_max_7_1_lpi_1(2), I2=>and_15_psp, I3=>and_17_psp);
   ix49337z1409 : LUT4
      generic map (INIT => X"0032") 
       port map ( O=>nx49337z45, I0=>median_max_6_1_lpi_1(2), I1=>
      median_max_7_1_lpi_1(2), I2=>and_15_psp, I3=>and_17_psp);
   ix170z14139 : LUT4
      generic map (INIT => X"31F5") 
       port map ( O=>nx170z37, I0=>clip_window_qr_2_lpi_1(2), I1=>
      system_input_window_7_sva(2), I2=>
      sfi_io_read_in_data_vld_rsc_lpi_1_dfm, I3=>and_46_itm_1);
   ix11174z1540 : LUT3
      generic map (INIT => X"E2") 
       port map ( O=>nx11174z1, I0=>buffer_buf_rsci_data_out_d(2), I1=>
      buffer_sel_1_sva_dfm, I2=>buffer_t0_sva_1(2));
   ix170z36687 : LUT4
      generic map (INIT => X"8A0A") 
       port map ( O=>nx170z36, I0=>nx49337z26, I1=>nx11174z1, I2=>nx170z37, 
      I3=>and_45_itm_1);
   ix170z19219 : LUT4
      generic map (INIT => X"45CF") 
       port map ( O=>nx170z35, I0=>system_input_window_6_sva(2), I1=>
      sfi_io_read_in_data_vld_rsc_lpi_1_dfm, I2=>window_6_lpi_1_dfm_2(2), I3
      =>and_34_itm_1);
   ix170z1355 : LUT2
      generic map (INIT => X"8") 
       port map ( O=>nx170z34, I0=>system_input_window_7_sva(2), I1=>
      and_32_itm_1);
   ix170z59202 : LUT4
      generic map (INIT => X"E200") 
       port map ( O=>nx170z33, I0=>buffer_buf_rsci_data_out_d(2), I1=>
      buffer_sel_1_sva_dfm, I2=>buffer_t0_sva_1(2), I3=>and_33_itm_1);
   ix170z48449 : LUT4
      generic map (INIT => X"B800") 
       port map ( O=>nx170z32, I0=>buffer_buf_rsci_data_out_d(2), I1=>
      buffer_sel_1_sva_dfm, I2=>buffer_t0_sva_1(2), I3=>and_35_itm_1);
   ix170z1600 : LUT4
      generic map (INIT => X"0100") 
       port map ( O=>nx170z31, I0=>nx170z32, I1=>nx170z33, I2=>nx170z34, I3
      =>nx170z35);
   ix170z1378 : LUT3
      generic map (INIT => X"23") 
       port map ( O=>nx170z30, I0=>nx170z31, I1=>nx170z36, I2=>nx49337z32);
   ix51334z6274 : LUT4
      generic map (INIT => X"135F") 
       port map ( O=>nx51334z2, I0=>system_input_window_7_sva(2), I1=>
      asn_39_itm(2), I2=>and_36_itm_1, I3=>and_38_itm_1);
   ix170z10477 : LUT4
      generic map (INIT => X"23AF") 
       port map ( O=>nx170z29, I0=>sfi_io_read_in_data_vld_rsc_lpi_1_dfm, I1
      =>buffer_din_sva_1(2), I2=>window_8_lpi_1_dfm_2(2), I3=>and_39_itm_1);
   ix170z59197 : LUT4
      generic map (INIT => X"E200") 
       port map ( O=>nx170z28, I0=>buffer_buf_rsci_data_out_d(2), I1=>
      buffer_sel_1_sva_dfm, I2=>buffer_t0_sva_1(2), I3=>and_37_itm_1);
   ix170z50236 : LUT4
      generic map (INIT => X"BF00") 
       port map ( O=>nx170z27, I0=>nx170z28, I1=>nx170z29, I2=>nx51334z2, I3
      =>median_i_1_lpi_1_dfm_1(3));
   ix170z6298 : LUT4
      generic map (INIT => X"135F") 
       port map ( O=>nx170z26, I0=>nx49337z15, I1=>nx49337z18, I2=>
      window_2_lpi_1_dfm_2(2), I3=>clip_window_qr_1_lpi_1_dfm_2(2));
   ix170z6297 : LUT4
      generic map (INIT => X"135F") 
       port map ( O=>nx170z25, I0=>nx49337z17, I1=>nx49337z12, I2=>
      clip_window_qr_3_lpi_1_dfm_2(2), I3=>mux_36_itm(2));
   ix170z6296 : LUT4
      generic map (INIT => X"135F") 
       port map ( O=>nx170z24, I0=>nx49337z14, I1=>nx49337z11, I2=>
      window_0_lpi_1_dfm_2(2), I3=>clip_window_qr_lpi_1_dfm_2(2));
   ix170z1464 : LUT4
      generic map (INIT => X"0080") 
       port map ( O=>nx170z23, I0=>nx170z24, I1=>nx170z25, I2=>nx170z26, I3
      =>nx170z27);
   ix49337z64958 : LUT4
      generic map (INIT => X"F870") 
       port map ( O=>nx49337z44, I0=>nx170z23, I1=>nx170z30, I2=>nx49337z45, 
      I3=>nx49337z46);
   ix49337z22053 : LUT4
      generic map (INIT => X"50D8") 
       port map ( O=>nx49337z43, I0=>fsm_output(5), I1=>
      median_max_6_1_lpi_1(1), I2=>median_max_8_lpi_1_dfm_1(1), I3=>
      and_15_psp);
   ix49337z65572 : LUT4
      generic map (INIT => X"FAD8") 
       port map ( O=>nx49337z42, I0=>fsm_output(5), I1=>
      median_max_6_1_lpi_1(1), I2=>median_max_8_lpi_1_dfm_1(1), I3=>
      and_15_psp);
   ix49337z64955 : LUT4
      generic map (INIT => X"F870") 
       port map ( O=>nx49337z41, I0=>nx170z7, I1=>nx170z11, I2=>nx49337z42, 
      I3=>nx49337z43);
   ix49337z46193 : LUT4
      generic map (INIT => X"AF27") 
       port map ( O=>nx49337z40, I0=>fsm_output(5), I1=>
      median_max_7_1_lpi_1(1), I2=>median_max2_9_lpi_1_dfm_2(1), I3=>
      and_17_psp);
   ix49337z2672 : LUT4
      generic map (INIT => X"0527") 
       port map ( O=>nx49337z39, I0=>fsm_output(5), I1=>
      median_max_7_1_lpi_1(1), I2=>median_max2_9_lpi_1_dfm_2(1), I3=>
      and_17_psp);
   ix170z19205 : LUT4
      generic map (INIT => X"45CF") 
       port map ( O=>nx170z21, I0=>system_input_window_7_sva(1), I1=>
      sfi_io_read_in_data_vld_rsc_lpi_1_dfm, I2=>window_8_lpi_1_dfm_2(1), I3
      =>and_36_itm_1);
   ix170z6772 : LUT4
      generic map (INIT => X"153F") 
       port map ( O=>nx170z20, I0=>buffer_din_sva_1(1), I1=>asn_39_itm(1), 
      I2=>and_38_itm_1, I3=>and_39_itm_1);
   ix170z59188 : LUT4
      generic map (INIT => X"E200") 
       port map ( O=>nx170z19, I0=>buffer_buf_rsci_data_out_d(1), I1=>
      buffer_sel_1_sva_dfm, I2=>buffer_t0_sva_1(1), I3=>and_37_itm_1);
   ix170z50227 : LUT4
      generic map (INIT => X"BF00") 
       port map ( O=>nx170z18, I0=>nx170z19, I1=>nx170z20, I2=>nx170z21, I3
      =>median_i_1_lpi_1_dfm_1(3));
   ix170z14119 : LUT4
      generic map (INIT => X"31F5") 
       port map ( O=>nx170z17, I0=>clip_window_qr_2_lpi_1(1), I1=>
      system_input_window_7_sva(1), I2=>
      sfi_io_read_in_data_vld_rsc_lpi_1_dfm, I3=>and_46_itm_1);
   ix12171z1540 : LUT3
      generic map (INIT => X"E2") 
       port map ( O=>nx12171z1, I0=>buffer_buf_rsci_data_out_d(1), I1=>
      buffer_sel_1_sva_dfm, I2=>buffer_t0_sva_1(1));
   ix170z37181 : LUT4
      generic map (INIT => X"8C0C") 
       port map ( O=>nx170z16, I0=>nx12171z1, I1=>nx49337z26, I2=>nx170z17, 
      I3=>and_45_itm_1);
   ix50361z6754 : LUT4
      generic map (INIT => X"153F") 
       port map ( O=>nx50361z2, I0=>system_input_window_6_sva(1), I1=>
      system_input_window_7_sva(1), I2=>and_32_itm_1, I3=>and_34_itm_1);
   ix170z1332 : LUT2
      generic map (INIT => X"4") 
       port map ( O=>nx170z15, I0=>sfi_io_read_in_data_vld_rsc_lpi_1_dfm, I1
      =>window_6_lpi_1_dfm_2(1));
   ix170z48431 : LUT4
      generic map (INIT => X"B800") 
       port map ( O=>nx170z14, I0=>buffer_buf_rsci_data_out_d(1), I1=>
      buffer_sel_1_sva_dfm, I2=>buffer_t0_sva_1(1), I3=>and_35_itm_1);
   ix170z59182 : LUT4
      generic map (INIT => X"E200") 
       port map ( O=>nx170z13, I0=>buffer_buf_rsci_data_out_d(1), I1=>
      buffer_sel_1_sva_dfm, I2=>buffer_t0_sva_1(1), I3=>and_33_itm_1);
   ix170z1581 : LUT4
      generic map (INIT => X"0100") 
       port map ( O=>nx170z12, I0=>nx170z13, I1=>nx170z14, I2=>nx170z15, I3
      =>nx50361z2);
   ix170z1839 : LUT4
      generic map (INIT => X"0203") 
       port map ( O=>nx170z11, I0=>nx170z12, I1=>nx170z16, I2=>nx170z18, I3
      =>nx49337z32);
   ix170z6282 : LUT4
      generic map (INIT => X"135F") 
       port map ( O=>nx170z10, I0=>nx49337z18, I1=>nx49337z12, I2=>
      clip_window_qr_1_lpi_1_dfm_2(1), I3=>mux_36_itm(1));
   ix170z6761 : LUT4
      generic map (INIT => X"153F") 
       port map ( O=>nx170z9, I0=>nx49337z11, I1=>nx49337z15, I2=>
      window_2_lpi_1_dfm_2(1), I3=>clip_window_qr_lpi_1_dfm_2(1));
   ix170z6280 : LUT4
      generic map (INIT => X"135F") 
       port map ( O=>nx170z8, I0=>nx49337z14, I1=>nx49337z17, I2=>
      window_0_lpi_1_dfm_2(1), I3=>clip_window_qr_3_lpi_1_dfm_2(1));
   ix170z1448 : LUT3
      generic map (INIT => X"80") 
       port map ( O=>nx170z7, I0=>nx170z8, I1=>nx170z9, I2=>nx170z10);
   ix49337z64952 : LUT4
      generic map (INIT => X"F870") 
       port map ( O=>nx49337z38, I0=>nx170z7, I1=>nx170z11, I2=>nx49337z39, 
      I3=>nx49337z40);
   ix49337z1435 : LUT3
      generic map (INIT => X"54") 
       port map ( O=>nx49337z37, I0=>median_max_6_1_lpi_1(0), I1=>
      median_max_7_1_lpi_1(0), I2=>and_17_psp);
   ix49337z23131 : LUT4
      generic map (INIT => X"5515") 
       port map ( O=>nx49337z36, I0=>nx49337z34, I1=>nx49337z37, I2=>
      fsm_output(5), I3=>and_15_psp);
   ix49337z1545 : LUT3
      generic map (INIT => X"C4") 
       port map ( O=>nx49337z35, I0=>median_max_6_1_lpi_1(0), I1=>
      median_max_7_1_lpi_1(0), I2=>and_15_psp);
   ix49337z1352 : LUT3
      generic map (INIT => X"04") 
       port map ( O=>nx49337z34, I0=>fsm_output(5), I1=>
      median_max2_9_lpi_1_dfm_2(0), I2=>median_max_8_lpi_1_dfm_1(0));
   ix49337z23128 : LUT4
      generic map (INIT => X"5515") 
       port map ( O=>nx49337z33, I0=>nx49337z34, I1=>nx49337z35, I2=>
      fsm_output(5), I3=>and_17_psp);
   ix49337z1410 : LUT4
      generic map (INIT => X"0040") 
       port map ( O=>nx49337z32, I0=>median_i_1_lpi_1_dfm_1(3), I1=>
      median_i_1_lpi_1_dfm_1(2), I2=>median_i_1_lpi_1_dfm_1(1), I3=>
      median_i_1_lpi_1_dfm_1(0));
   ix49337z10480 : LUT4
      generic map (INIT => X"23AF") 
       port map ( O=>nx49337z31, I0=>sfi_io_read_in_data_vld_rsc_lpi_1_dfm, 
      I1=>buffer_din_sva_1(0), I2=>window_8_lpi_1_dfm_2(0), I3=>and_39_itm_1
   );
   ix49337z6303 : LUT4
      generic map (INIT => X"135F") 
       port map ( O=>nx49337z30, I0=>system_input_window_7_sva(0), I1=>
      asn_39_itm(0), I2=>and_36_itm_1, I3=>and_38_itm_1);
   ix49337z59199 : LUT4
      generic map (INIT => X"E200") 
       port map ( O=>nx49337z29, I0=>buffer_buf_rsci_data_out_d(0), I1=>
      buffer_sel_1_sva_dfm, I2=>buffer_t0_sva_1(0), I3=>and_37_itm_1);
   ix49337z50238 : LUT4
      generic map (INIT => X"BF00") 
       port map ( O=>nx49337z28, I0=>nx49337z29, I1=>nx49337z30, I2=>
      nx49337z31, I3=>median_i_1_lpi_1_dfm_1(3));
   ix49337z14130 : LUT4
      generic map (INIT => X"31F5") 
       port map ( O=>nx49337z27, I0=>clip_window_qr_2_lpi_1(0), I1=>
      system_input_window_7_sva(0), I2=>
      sfi_io_read_in_data_vld_rsc_lpi_1_dfm, I3=>and_46_itm_1);
   ix13168z1541 : LUT3
      generic map (INIT => X"E2") 
       port map ( O=>nx13168z2, I0=>buffer_buf_rsci_data_out_d(0), I1=>
      buffer_sel_1_sva_dfm, I2=>buffer_t0_sva_1(0));
   ix49337z17724 : LUT4
      generic map (INIT => X"4000") 
       port map ( O=>nx49337z26, I0=>median_i_1_lpi_1_dfm_1(3), I1=>
      median_i_1_lpi_1_dfm_1(2), I2=>median_i_1_lpi_1_dfm_1(1), I3=>
      median_i_1_lpi_1_dfm_1(0));
   ix49337z36677 : LUT4
      generic map (INIT => X"8A0A") 
       port map ( O=>nx49337z25, I0=>nx49337z26, I1=>nx13168z2, I2=>
      nx49337z27, I3=>and_45_itm_1);
   ix49337z19209 : LUT4
      generic map (INIT => X"45CF") 
       port map ( O=>nx49337z24, I0=>system_input_window_7_sva(0), I1=>
      sfi_io_read_in_data_vld_rsc_lpi_1_dfm, I2=>window_6_lpi_1_dfm_2(0), I3
      =>and_32_itm_1);
   ix49337z1369 : LUT2
      generic map (INIT => X"8") 
       port map ( O=>nx49337z23, I0=>system_input_window_6_sva(0), I1=>
      and_34_itm_1);
   ix49337z48439 : LUT4
      generic map (INIT => X"B800") 
       port map ( O=>nx49337z22, I0=>buffer_buf_rsci_data_out_d(0), I1=>
      buffer_sel_1_sva_dfm, I2=>buffer_t0_sva_1(0), I3=>and_35_itm_1);
   ix49337z59190 : LUT4
      generic map (INIT => X"E200") 
       port map ( O=>nx49337z21, I0=>buffer_buf_rsci_data_out_d(0), I1=>
      buffer_sel_1_sva_dfm, I2=>buffer_t0_sva_1(0), I3=>and_33_itm_1);
   ix49337z1589 : LUT4
      generic map (INIT => X"0100") 
       port map ( O=>nx49337z20, I0=>nx49337z21, I1=>nx49337z22, I2=>
      nx49337z23, I3=>nx49337z24);
   ix49337z1847 : LUT4
      generic map (INIT => X"0203") 
       port map ( O=>nx49337z19, I0=>nx49337z20, I1=>nx49337z25, I2=>
      nx49337z28, I3=>nx49337z32);
   ix49337z2355 : LUT4
      generic map (INIT => X"0400") 
       port map ( O=>nx49337z18, I0=>median_i_1_lpi_1_dfm_1(3), I1=>
      median_i_1_lpi_1_dfm_1(2), I2=>median_i_1_lpi_1_dfm_1(1), I3=>
      median_i_1_lpi_1_dfm_1(0));
   ix49337z5426 : LUT4
      generic map (INIT => X"1000") 
       port map ( O=>nx49337z17, I0=>median_i_1_lpi_1_dfm_1(3), I1=>
      median_i_1_lpi_1_dfm_1(2), I2=>median_i_1_lpi_1_dfm_1(1), I3=>
      median_i_1_lpi_1_dfm_1(0));
   ix49337z6768 : LUT4
      generic map (INIT => X"153F") 
       port map ( O=>nx49337z16, I0=>nx49337z17, I1=>nx49337z18, I2=>
      clip_window_qr_1_lpi_1_dfm_2(0), I3=>clip_window_qr_3_lpi_1_dfm_2(0));
   ix49337z1344 : LUT4
      generic map (INIT => X"0010") 
       port map ( O=>nx49337z15, I0=>median_i_1_lpi_1_dfm_1(3), I1=>
      median_i_1_lpi_1_dfm_1(2), I2=>median_i_1_lpi_1_dfm_1(1), I3=>
      median_i_1_lpi_1_dfm_1(0));
   ix49337z1328 : LUT4
      generic map (INIT => X"0001") 
       port map ( O=>nx49337z14, I0=>median_i_1_lpi_1_dfm_1(3), I1=>
      median_i_1_lpi_1_dfm_1(2), I2=>median_i_1_lpi_1_dfm_1(1), I3=>
      median_i_1_lpi_1_dfm_1(0));
   ix49337z6285 : LUT4
      generic map (INIT => X"135F") 
       port map ( O=>nx49337z13, I0=>nx49337z14, I1=>nx49337z15, I2=>
      window_0_lpi_1_dfm_2(0), I3=>window_2_lpi_1_dfm_2(0));
   ix49337z1329 : LUT4
      generic map (INIT => X"0004") 
       port map ( O=>nx49337z12, I0=>median_i_1_lpi_1_dfm_1(3), I1=>
      median_i_1_lpi_1_dfm_1(2), I2=>median_i_1_lpi_1_dfm_1(1), I3=>
      median_i_1_lpi_1_dfm_1(0));
   ix49337z1580 : LUT4
      generic map (INIT => X"0100") 
       port map ( O=>nx49337z11, I0=>median_i_1_lpi_1_dfm_1(3), I1=>
      median_i_1_lpi_1_dfm_1(2), I2=>median_i_1_lpi_1_dfm_1(1), I3=>
      median_i_1_lpi_1_dfm_1(0));
   ix49337z6282 : LUT4
      generic map (INIT => X"135F") 
       port map ( O=>nx49337z10, I0=>nx49337z11, I1=>nx49337z12, I2=>
      clip_window_qr_lpi_1_dfm_2(0), I3=>mux_36_itm(0));
   ix49337z1450 : LUT3
      generic map (INIT => X"80") 
       port map ( O=>nx49337z9, I0=>nx49337z10, I1=>nx49337z13, I2=>
      nx49337z16);
   ix49337z64681 : LUT4
      generic map (INIT => X"F780") 
       port map ( O=>nx49337z8, I0=>nx49337z9, I1=>nx49337z19, I2=>
      nx49337z33, I3=>nx49337z36);
   ix49337z1343 : LUT4
      generic map (INIT => X"0017") 
       port map ( O=>nx49337z7, I0=>nx49337z8, I1=>nx49337z38, I2=>
      nx49337z41, I3=>nx49337z44);
   ix49337z46562 : LUT4
      generic map (INIT => X"B0BB") 
       port map ( O=>nx49337z6, I0=>nx49337z7, I1=>nx49337z47, I2=>
      nx49337z48, I3=>nx49337z50);
   ix23875z1585 : LUT3
      generic map (INIT => X"E4") 
       port map ( O=>nx23875z44, I0=>nx49337z6, I1=>
      median_max2_9_lpi_1_dfm_2(2), I2=>median_max_8_lpi_1_dfm_1(2));
   ix48581z13696 : LUT4
      generic map (INIT => X"3050") 
       port map ( O=>nx48581z14, I0=>median_max_6_2_lpi_1_dfm_1(2), I1=>
      median_max_7_3_lpi_1_dfm_1(2), I2=>nx23875z44, I3=>nx7321z15);
   ix48581z1513 : LUT4
      generic map (INIT => X"00BA") 
       port map ( O=>nx48581z13, I0=>nx48581z14, I1=>nx48581z15, I2=>
      nx48581z18, I3=>fsm_output(5));
   ix48581z63246 : LUT4
      generic map (INIT => X"F1E0") 
       port map ( O=>nx48581z12, I0=>nx48581z13, I1=>nx49337z3, I2=>
      nx48581z16, I3=>nx48581z17);
   ix48581z10207 : LUT4
      generic map (INIT => X"22B2") 
       port map ( O=>nx48581z11, I0=>nx48581z12, I1=>nx23875z34, I2=>
      nx23875z36, I3=>nx23875z39);
   ix48581z1466 : LUT4
      generic map (INIT => X"008E") 
       port map ( O=>nx48581z10, I0=>nx48581z11, I1=>nx23875z43, I2=>
      nx23875z46, I3=>fsm_output(5));
   ix48581z63243 : LUT4
      generic map (INIT => X"F1E0") 
       port map ( O=>nx48581z9, I0=>nx48581z10, I1=>nx7321z5, I2=>nx7321z36, 
      I3=>nx7321z37);
   ix48581z1333 : LUT2
      generic map (INIT => X"2") 
       port map ( O=>nx48581z8, I0=>nx48581z9, I1=>nx29367z5);
   ix48581z48936 : LUT4
      generic map (INIT => X"BA00") 
       port map ( O=>nx48581z7, I0=>nx48581z8, I1=>nx48581z19, I2=>
      nx48581z20, I3=>fsm_output(5));
   ix48581z63239 : LUT4
      generic map (INIT => X"F1E0") 
       port map ( O=>nx48581z6, I0=>nx48581z7, I1=>nx23875z5, I2=>nx23875z41, 
      I3=>nx23875z42);
   ix48581z1353 : LUT3
      generic map (INIT => X"23") 
       port map ( O=>nx48581z5, I0=>nx48581z6, I1=>fsm_output(5), I2=>
      thresholding_asn_itm(2));
   ix48581z1479 : LUT3
      generic map (INIT => X"A2") 
       port map ( O=>nx48581z4, I0=>nx48581z5, I1=>nx48581z21, I2=>
      nx48581z23);
   ix48581z1323 : LUT2
      generic map (INIT => X"8") 
       port map ( O=>nx48581z2, I0=>io_read_in_data_vld_rsc_sft_lpi_1_dfm, 
      I1=>asn_sft_3_lpi_1_dfm);
   ix48581z3362 : LUT4
      generic map (INIT => X"0800") 
       port map ( O=>nx48581z1, I0=>nx48581z2, I1=>fsm_output(6), I2=>
      exit_Linit_lpi_1_dfm_1, I3=>exit_Linit_sva_2);
   ix59203z34088 : LUT4
      generic map (INIT => X"8000") 
       port map ( O=>nx59203z7, I0=>system_input_r_filter_sva(6), I1=>
      system_input_r_filter_sva(3), I2=>system_input_r_filter_sva(2), I3=>
      system_input_r_filter_sva(0));
   ix59203z1327 : LUT2
      generic map (INIT => X"8") 
       port map ( O=>nx59203z6, I0=>system_input_r_filter_sva(7), I1=>
      system_input_r_filter_sva(5));
   ix59203z3366 : LUT4
      generic map (INIT => X"0800") 
       port map ( O=>nx59203z5, I0=>nx59203z6, I1=>nx59203z7, I2=>
      system_input_r_filter_sva(4), I3=>system_input_r_filter_sva(1));
   ix59203z34085 : LUT4
      generic map (INIT => X"8000") 
       port map ( O=>nx59203z4, I0=>system_input_c_filter_sva(8), I1=>
      system_input_c_filter_sva(2), I2=>system_input_c_filter_sva(1), I3=>
      system_input_c_filter_sva(0));
   ix59203z1380 : LUT3
      generic map (INIT => X"40") 
       port map ( O=>nx59203z3, I0=>system_input_c_filter_sva(7), I1=>
      system_input_c_filter_sva(5), I2=>system_input_c_filter_sva(4));
   ix59203z3363 : LUT4
      generic map (INIT => X"0800") 
       port map ( O=>nx59203z2, I0=>nx59203z3, I1=>nx59203z4, I2=>
      system_input_c_filter_sva(6), I3=>system_input_c_filter_sva(3));
   ix19409z34083 : LUT4
      generic map (INIT => X"8000") 
       port map ( O=>nx19409z2, I0=>in_data_vld_rsc_z, I1=>fsm_output(3), I2
      =>exit_histogram_init_1_sva, I3=>nx59203z2);
   ix51398z1322 : LUT2
      generic map (INIT => X"1") 
       port map ( O=>nx51398z7, I0=>system_input_r_sva(7), I1=>
      system_input_r_sva(5));
   ix51398z1576 : LUT4
      generic map (INIT => X"0100") 
       port map ( O=>nx51398z6, I0=>system_input_r_sva(6), I1=>
      system_input_r_sva(3), I2=>system_input_r_sva(1), I3=>
      system_input_r_sva(0));
   ix51398z1327 : LUT4
      generic map (INIT => X"0008") 
       port map ( O=>nx51398z5, I0=>nx51398z6, I1=>nx51398z7, I2=>
      system_input_r_sva(4), I3=>system_input_r_sva(2));
   ix51398z1324 : LUT4
      generic map (INIT => X"0001") 
       port map ( O=>nx51398z4, I0=>system_input_c_sva(8), I1=>
      system_input_c_sva(5), I2=>system_input_c_sva(4), I3=>
      system_input_c_sva(3));
   ix51398z1318 : LUT3
      generic map (INIT => X"02") 
       port map ( O=>nx51398z3, I0=>nx51398z4, I1=>system_input_c_sva(2), I2
      =>system_input_c_sva(1));
   ix51398z1442 : LUT4
      generic map (INIT => X"007F") 
       port map ( O=>nx51398z2, I0=>nx51398z3, I1=>nx51398z5, I2=>nx9489z4, 
      I3=>system_input_output_vld_sva);
   ix51271z34085 : LUT4
      generic map (INIT => X"8000") 
       port map ( O=>nx51271z4, I0=>system_input_r_sva(6), I1=>
      system_input_r_sva(5), I2=>system_input_r_sva(3), I3=>
      system_input_r_sva(2));
   ix51271z9508 : LUT4
      generic map (INIT => X"2000") 
       port map ( O=>nx51271z3, I0=>system_input_r_sva(7), I1=>
      system_input_r_sva(4), I2=>system_input_r_sva(1), I3=>
      system_input_r_sva(0));
   ix51271z23138 : LUT4
      generic map (INIT => X"5540") 
       port map ( O=>nx51271z1, I0=>nx51271z2, I1=>nx51271z3, I2=>nx51271z4, 
      I3=>rst);
   ix11984z1442 : LUT3
      generic map (INIT => X"80") 
       port map ( O=>nx11984z1, I0=>in_data_vld_rsc_z, I1=>fsm_output(3), I2
      =>exit_histogram_init_1_sva);
   ix9489z1333 : LUT3
      generic map (INIT => X"10") 
       port map ( O=>nx9489z4, I0=>system_input_c_sva(7), I1=>
      system_input_c_sva(6), I2=>system_input_c_sva(0));
   ix9489z34084 : LUT4
      generic map (INIT => X"8000") 
       port map ( O=>nx9489z3, I0=>system_input_c_sva(8), I1=>
      system_input_c_sva(5), I2=>system_input_c_sva(4), I3=>
      system_input_c_sva(3));
   ix9489z34083 : LUT4
      generic map (INIT => X"8000") 
       port map ( O=>nx9489z2, I0=>nx9489z3, I1=>nx9489z4, I2=>
      system_input_c_sva(2), I3=>system_input_c_sva(1));
   ix51271z1322 : LUT3
      generic map (INIT => X"07") 
       port map ( O=>nx51271z2, I0=>nx9489z2, I1=>nx11984z1, I2=>rst);
   ix56505z1322 : LUT4
      generic map (INIT => X"0008") 
       port map ( O=>nx56505z1, I0=>nx51398z1, I1=>median_i_1_lpi_2(3), I2=>
      exit_histogram_init_1_sva, I3=>exit_Linit_lpi_1);
   ix7856z1322 : LUT4
      generic map (INIT => X"0008") 
       port map ( O=>nx7856z1, I0=>nx10935z2, I1=>median_i_1_lpi_2(3), I2=>
      exit_histogram_init_1_sva, I3=>exit_Linit_lpi_1);
   ix33121z61239 : LUT4
      generic map (INIT => X"EA15") 
       port map ( O=>nx33121z1, I0=>nx6197z3, I1=>system_input_c_sva(6), I2
      =>system_input_c_sva(7), I3=>system_input_c_sva(8));
   ix10935z52523 : LUT4
      generic map (INIT => X"C808") 
       port map ( O=>nx10935z2, I0=>io_read_in_data_vld_rsc_sft_lpi_1_dfm_2, 
      I1=>nx10935z3, I2=>exit_histogram_init_1_sva, I3=>in_data_vld_rsc_z);
   ix60014z42437 : LUT4
      generic map (INIT => X"A0A2") 
       port map ( O=>nx60014z2, I0=>nx1352z3, I1=>median_i_1_lpi_2(1), I2=>
      exit_histogram_init_1_sva, I3=>median_i_1_lpi_2(0));
end v35_unfold_1901 ;

library IEEE;
use IEEE.STD_LOGIC_1164.all;
-- Library use clause for technology cells
library unisim ;
use unisim.vcomponents.all;

entity ram_dq_17_0 is 
   port (
      wr_data1 : IN std_logic_vector (16 DOWNTO 0) ;
      rd_data1 : OUT std_logic_vector (16 DOWNTO 0) ;
      addr1 : IN std_logic_vector (2 DOWNTO 0) ;
      wr_clk1 : IN std_logic ;
      rd_clk1 : IN std_logic ;
      wr_ena1 : IN std_logic ;
      rd_ena1 : IN std_logic ;
      ena1 : IN std_logic ;
      rst1 : IN std_logic ;
      regce1 : IN std_logic ;
      wr_data2 : IN std_logic_vector (16 DOWNTO 0) ;
      rd_data2 : OUT std_logic_vector (16 DOWNTO 0) ;
      addr2 : IN std_logic_vector (2 DOWNTO 0) ;
      wr_clk2 : IN std_logic ;
      rd_clk2 : IN std_logic ;
      wr_ena2 : IN std_logic ;
      rd_ena2 : IN std_logic ;
      ena2 : IN std_logic ;
      rst2 : IN std_logic ;
      regce2 : IN std_logic) ;
end ram_dq_17_0 ;

architecture IMPLEMENTATION of ram_dq_17_0 is 
   signal nx51058z1: std_logic ;

begin
   ps_gnd : GND port map ( G=>nx51058z1);
   mem_1 : RAM16X1D port map ( DPO=>rd_data2(0), SPO=>OPEN, A0=>addr1(0), A1
      =>addr1(1), A2=>addr1(2), A3=>nx51058z1, D=>wr_data1(0), DPRA0=>
      addr2(0), DPRA1=>addr2(1), DPRA2=>addr2(2), DPRA3=>nx51058z1, WCLK=>
      wr_clk1, WE=>wr_ena1);
   mem_2 : RAM16X1D port map ( DPO=>rd_data2(1), SPO=>OPEN, A0=>addr1(0), A1
      =>addr1(1), A2=>addr1(2), A3=>nx51058z1, D=>wr_data1(1), DPRA0=>
      addr2(0), DPRA1=>addr2(1), DPRA2=>addr2(2), DPRA3=>nx51058z1, WCLK=>
      wr_clk1, WE=>wr_ena1);
   mem_3 : RAM16X1D port map ( DPO=>rd_data2(2), SPO=>OPEN, A0=>addr1(0), A1
      =>addr1(1), A2=>addr1(2), A3=>nx51058z1, D=>wr_data1(2), DPRA0=>
      addr2(0), DPRA1=>addr2(1), DPRA2=>addr2(2), DPRA3=>nx51058z1, WCLK=>
      wr_clk1, WE=>wr_ena1);
   mem_4 : RAM16X1D port map ( DPO=>rd_data2(3), SPO=>OPEN, A0=>addr1(0), A1
      =>addr1(1), A2=>addr1(2), A3=>nx51058z1, D=>wr_data1(3), DPRA0=>
      addr2(0), DPRA1=>addr2(1), DPRA2=>addr2(2), DPRA3=>nx51058z1, WCLK=>
      wr_clk1, WE=>wr_ena1);
   mem_5 : RAM16X1D port map ( DPO=>rd_data2(4), SPO=>OPEN, A0=>addr1(0), A1
      =>addr1(1), A2=>addr1(2), A3=>nx51058z1, D=>wr_data1(4), DPRA0=>
      addr2(0), DPRA1=>addr2(1), DPRA2=>addr2(2), DPRA3=>nx51058z1, WCLK=>
      wr_clk1, WE=>wr_ena1);
   mem_6 : RAM16X1D port map ( DPO=>rd_data2(5), SPO=>OPEN, A0=>addr1(0), A1
      =>addr1(1), A2=>addr1(2), A3=>nx51058z1, D=>wr_data1(5), DPRA0=>
      addr2(0), DPRA1=>addr2(1), DPRA2=>addr2(2), DPRA3=>nx51058z1, WCLK=>
      wr_clk1, WE=>wr_ena1);
   mem_7 : RAM16X1D port map ( DPO=>rd_data2(6), SPO=>OPEN, A0=>addr1(0), A1
      =>addr1(1), A2=>addr1(2), A3=>nx51058z1, D=>wr_data1(6), DPRA0=>
      addr2(0), DPRA1=>addr2(1), DPRA2=>addr2(2), DPRA3=>nx51058z1, WCLK=>
      wr_clk1, WE=>wr_ena1);
   mem_8 : RAM16X1D port map ( DPO=>rd_data2(7), SPO=>OPEN, A0=>addr1(0), A1
      =>addr1(1), A2=>addr1(2), A3=>nx51058z1, D=>wr_data1(7), DPRA0=>
      addr2(0), DPRA1=>addr2(1), DPRA2=>addr2(2), DPRA3=>nx51058z1, WCLK=>
      wr_clk1, WE=>wr_ena1);
   mem_9 : RAM16X1D port map ( DPO=>rd_data2(8), SPO=>OPEN, A0=>addr1(0), A1
      =>addr1(1), A2=>addr1(2), A3=>nx51058z1, D=>wr_data1(8), DPRA0=>
      addr2(0), DPRA1=>addr2(1), DPRA2=>addr2(2), DPRA3=>nx51058z1, WCLK=>
      wr_clk1, WE=>wr_ena1);
   mem_10 : RAM16X1D port map ( DPO=>rd_data2(9), SPO=>OPEN, A0=>addr1(0), 
      A1=>addr1(1), A2=>addr1(2), A3=>nx51058z1, D=>wr_data1(9), DPRA0=>
      addr2(0), DPRA1=>addr2(1), DPRA2=>addr2(2), DPRA3=>nx51058z1, WCLK=>
      wr_clk1, WE=>wr_ena1);
   mem_11 : RAM16X1D port map ( DPO=>rd_data2(10), SPO=>OPEN, A0=>addr1(0), 
      A1=>addr1(1), A2=>addr1(2), A3=>nx51058z1, D=>wr_data1(10), DPRA0=>
      addr2(0), DPRA1=>addr2(1), DPRA2=>addr2(2), DPRA3=>nx51058z1, WCLK=>
      wr_clk1, WE=>wr_ena1);
   mem_12 : RAM16X1D port map ( DPO=>rd_data2(11), SPO=>OPEN, A0=>addr1(0), 
      A1=>addr1(1), A2=>addr1(2), A3=>nx51058z1, D=>wr_data1(11), DPRA0=>
      addr2(0), DPRA1=>addr2(1), DPRA2=>addr2(2), DPRA3=>nx51058z1, WCLK=>
      wr_clk1, WE=>wr_ena1);
   mem_13 : RAM16X1D port map ( DPO=>rd_data2(12), SPO=>OPEN, A0=>addr1(0), 
      A1=>addr1(1), A2=>addr1(2), A3=>nx51058z1, D=>wr_data1(12), DPRA0=>
      addr2(0), DPRA1=>addr2(1), DPRA2=>addr2(2), DPRA3=>nx51058z1, WCLK=>
      wr_clk1, WE=>wr_ena1);
   mem_14 : RAM16X1D port map ( DPO=>rd_data2(13), SPO=>OPEN, A0=>addr1(0), 
      A1=>addr1(1), A2=>addr1(2), A3=>nx51058z1, D=>wr_data1(13), DPRA0=>
      addr2(0), DPRA1=>addr2(1), DPRA2=>addr2(2), DPRA3=>nx51058z1, WCLK=>
      wr_clk1, WE=>wr_ena1);
   mem_15 : RAM16X1D port map ( DPO=>rd_data2(14), SPO=>OPEN, A0=>addr1(0), 
      A1=>addr1(1), A2=>addr1(2), A3=>nx51058z1, D=>wr_data1(14), DPRA0=>
      addr2(0), DPRA1=>addr2(1), DPRA2=>addr2(2), DPRA3=>nx51058z1, WCLK=>
      wr_clk1, WE=>wr_ena1);
   mem_16 : RAM16X1D port map ( DPO=>rd_data2(15), SPO=>OPEN, A0=>addr1(0), 
      A1=>addr1(1), A2=>addr1(2), A3=>nx51058z1, D=>wr_data1(15), DPRA0=>
      addr2(0), DPRA1=>addr2(1), DPRA2=>addr2(2), DPRA3=>nx51058z1, WCLK=>
      wr_clk1, WE=>wr_ena1);
   mem_17 : RAM16X1D port map ( DPO=>rd_data2(16), SPO=>OPEN, A0=>addr1(0), 
      A1=>addr1(1), A2=>addr1(2), A3=>nx51058z1, D=>wr_data1(16), DPRA0=>
      addr2(0), DPRA1=>addr2(1), DPRA2=>addr2(2), DPRA3=>nx51058z1, WCLK=>
      wr_clk1, WE=>wr_ena1);
end IMPLEMENTATION ;

library IEEE;
use IEEE.STD_LOGIC_1164.all;
-- Library use clause for technology cells
library unisim ;
use unisim.vcomponents.all;

entity filter is 
   port (
      clk : IN std_logic ;
      rst : IN std_logic ;
      in_data_rsc_z : IN std_logic_vector (2 DOWNTO 0) ;
      in_data_rsc_lz : OUT std_logic ;
      in_data_vld_rsc_z : IN std_logic ;
      out_data_rsc_z : OUT std_logic_vector (2 DOWNTO 0) ;
      out_data_rsc_lz : OUT std_logic ;
      mcu_data_rsc_data_in : OUT std_logic_vector (31 DOWNTO 0) ;
      mcu_data_rsc_addr : OUT std_logic_vector (8 DOWNTO 0) ;
      mcu_data_rsc_re : OUT std_logic ;
      mcu_data_rsc_we : OUT std_logic ;
      mcu_data_rsc_data_out : IN std_logic_vector (31 DOWNTO 0)) ;
end filter ;

architecture v35 of filter is 
   component filter_core
      port (
         clk : IN std_logic ;
         rst : IN std_logic ;
         in_data_rsc_z : IN std_logic_vector (2 DOWNTO 0) ;
         in_data_rsc_lz : OUT std_logic ;
         in_data_vld_rsc_z : IN std_logic ;
         out_data_rsc_z : OUT std_logic_vector (2 DOWNTO 0) ;
         out_data_rsc_lz : OUT std_logic ;
         mcu_data_rsci_data_in_d : OUT std_logic_vector (31 DOWNTO 0) ;
         mcu_data_rsci_addr_d : OUT std_logic_vector (8 DOWNTO 0) ;
         mcu_data_rsci_re_d : OUT std_logic ;
         mcu_data_rsci_we_d : OUT std_logic ;
         mcu_data_rsci_data_out_d : IN std_logic_vector (31 DOWNTO 0) ;
         histogram_rsci_data_in_d : OUT std_logic_vector (16 DOWNTO 0) ;
         histogram_rsci_addr_d : OUT std_logic_vector (2 DOWNTO 0) ;
         histogram_rsci_re_d : OUT std_logic ;
         histogram_rsci_we_d : OUT std_logic ;
         histogram_rsci_data_out_d : IN std_logic_vector (16 DOWNTO 0) ;
         buffer_buf_rsci_data_in_d : OUT std_logic_vector (2 DOWNTO 0) ;
         buffer_buf_rsci_addr_d : OUT std_logic_vector (9 DOWNTO 0) ;
         buffer_buf_rsci_re_d : OUT std_logic ;
         buffer_buf_rsci_we_d : OUT std_logic ;
         buffer_buf_rsci_data_out_d : IN std_logic_vector (2 DOWNTO 0) ;
         px2584 : OUT std_logic ;
         px2583 : OUT std_logic ;
         p_fsm_output_4 : OUT std_logic ;
         p_exit_if_if_if_1_if_for_lpi_1_dfm_3 : OUT std_logic) ;
   
   end component ;
   component ram_dq_17_0
      port (
         wr_data1 : IN std_logic_vector (16 DOWNTO 0) ;
         rd_data1 : OUT std_logic_vector (16 DOWNTO 0) ;
         addr1 : IN std_logic_vector (2 DOWNTO 0) ;
         wr_clk1 : IN std_logic ;
         rd_clk1 : IN std_logic ;
         wr_ena1 : IN std_logic ;
         rd_ena1 : IN std_logic ;
         ena1 : IN std_logic ;
         rst1 : IN std_logic ;
         regce1 : IN std_logic ;
         wr_data2 : IN std_logic_vector (16 DOWNTO 0) ;
         rd_data2 : OUT std_logic_vector (16 DOWNTO 0) ;
         addr2 : IN std_logic_vector (2 DOWNTO 0) ;
         wr_clk2 : IN std_logic ;
         rd_clk2 : IN std_logic ;
         wr_ena2 : IN std_logic ;
         rd_ena2 : IN std_logic ;
         ena2 : IN std_logic ;
         rst2 : IN std_logic ;
         regce2 : IN std_logic) ;
   end component ;
   signal out_data_rsc_z_0_EXMPLR233: std_logic ;
   
   signal histogram_rsc_comp_data_in: std_logic_vector (16 DOWNTO 0) ;
   
   signal histogram_rsc_comp_addr: std_logic_vector (2 DOWNTO 0) ;
   
   signal buffer_buf_rsc_comp_data_in: std_logic_vector (2 DOWNTO 0) ;
   
   signal buffer_buf_rsc_comp_addr: std_logic_vector (9 DOWNTO 0) ;
   
   signal buffer_buf_rsc_comp_we_0: std_logic ;
   
   signal filter_core_inst_histogram_rsci_data_out_d: std_logic_vector
    (16 DOWNTO 0) ;
   
   signal filter_core_inst_buffer_buf_rsci_data_out_d: std_logic_vector
    (2 DOWNTO 0) ;
   
   signal histogram_rsc_comp_addr_reg: std_logic_vector (2 DOWNTO 0) ;
   
   signal mcu_data_rsc_addr_4_EXMPLR290, nx15727z1, 
      histogram_rsc_comp_not_en, nx22893z1, nx15727z2, nx15727z3, 
      fsm_output_4_dup_151, exit_if_if_if_1_if_for_lpi_1_dfm_3_dup_152: 
   std_logic ;
   
   signal DANGLING : std_logic_vector (71 downto 0 );

begin
   out_data_rsc_z(2) <= out_data_rsc_z_0_EXMPLR233 ;
   out_data_rsc_z(1) <= out_data_rsc_z_0_EXMPLR233 ;
   out_data_rsc_z(0) <= out_data_rsc_z_0_EXMPLR233 ;
   mcu_data_rsc_data_in(31) <= mcu_data_rsc_addr_4_EXMPLR290 ;
   mcu_data_rsc_data_in(30) <= mcu_data_rsc_addr_4_EXMPLR290 ;
   mcu_data_rsc_data_in(29) <= mcu_data_rsc_addr_4_EXMPLR290 ;
   mcu_data_rsc_data_in(28) <= mcu_data_rsc_addr_4_EXMPLR290 ;
   mcu_data_rsc_data_in(27) <= mcu_data_rsc_addr_4_EXMPLR290 ;
   mcu_data_rsc_data_in(26) <= mcu_data_rsc_addr_4_EXMPLR290 ;
   mcu_data_rsc_data_in(25) <= mcu_data_rsc_addr_4_EXMPLR290 ;
   mcu_data_rsc_data_in(24) <= mcu_data_rsc_addr_4_EXMPLR290 ;
   mcu_data_rsc_data_in(23) <= mcu_data_rsc_addr_4_EXMPLR290 ;
   mcu_data_rsc_data_in(22) <= mcu_data_rsc_addr_4_EXMPLR290 ;
   mcu_data_rsc_data_in(21) <= mcu_data_rsc_addr_4_EXMPLR290 ;
   mcu_data_rsc_data_in(20) <= mcu_data_rsc_addr_4_EXMPLR290 ;
   mcu_data_rsc_data_in(19) <= mcu_data_rsc_addr_4_EXMPLR290 ;
   mcu_data_rsc_data_in(18) <= mcu_data_rsc_addr_4_EXMPLR290 ;
   mcu_data_rsc_data_in(17) <= mcu_data_rsc_addr_4_EXMPLR290 ;
   mcu_data_rsc_addr(8) <= mcu_data_rsc_addr_4_EXMPLR290 ;
   mcu_data_rsc_addr(7) <= mcu_data_rsc_addr_4_EXMPLR290 ;
   mcu_data_rsc_addr(6) <= mcu_data_rsc_addr_4_EXMPLR290 ;
   mcu_data_rsc_addr(5) <= mcu_data_rsc_addr_4_EXMPLR290 ;
   mcu_data_rsc_addr(4) <= mcu_data_rsc_addr_4_EXMPLR290 ;
   filter_core_inst : filter_core port map ( clk=>clk, rst=>rst, 
      in_data_rsc_z(2)=>in_data_rsc_z(2), in_data_rsc_z(1)=>in_data_rsc_z(1), 
      in_data_rsc_z(0)=>in_data_rsc_z(0), in_data_rsc_lz=>in_data_rsc_lz, 
      in_data_vld_rsc_z=>in_data_vld_rsc_z, out_data_rsc_z(2)=>
      out_data_rsc_z_0_EXMPLR233, out_data_rsc_z(1)=>DANGLING(0), 
      out_data_rsc_z(0)=>DANGLING(1), out_data_rsc_lz=>out_data_rsc_lz, 
      mcu_data_rsci_data_in_d(31)=>DANGLING(2), mcu_data_rsci_data_in_d(30)
      =>DANGLING(3), mcu_data_rsci_data_in_d(29)=>DANGLING(4), 
      mcu_data_rsci_data_in_d(28)=>DANGLING(5), mcu_data_rsci_data_in_d(27)
      =>DANGLING(6), mcu_data_rsci_data_in_d(26)=>DANGLING(7), 
      mcu_data_rsci_data_in_d(25)=>DANGLING(8), mcu_data_rsci_data_in_d(24)
      =>DANGLING(9), mcu_data_rsci_data_in_d(23)=>DANGLING(10), 
      mcu_data_rsci_data_in_d(22)=>DANGLING(11), mcu_data_rsci_data_in_d(21)
      =>DANGLING(12), mcu_data_rsci_data_in_d(20)=>DANGLING(13), 
      mcu_data_rsci_data_in_d(19)=>DANGLING(14), mcu_data_rsci_data_in_d(18)
      =>DANGLING(15), mcu_data_rsci_data_in_d(17)=>DANGLING(16), 
      mcu_data_rsci_data_in_d(16)=>mcu_data_rsc_data_in(16), 
      mcu_data_rsci_data_in_d(15)=>mcu_data_rsc_data_in(15), 
      mcu_data_rsci_data_in_d(14)=>mcu_data_rsc_data_in(14), 
      mcu_data_rsci_data_in_d(13)=>mcu_data_rsc_data_in(13), 
      mcu_data_rsci_data_in_d(12)=>mcu_data_rsc_data_in(12), 
      mcu_data_rsci_data_in_d(11)=>mcu_data_rsc_data_in(11), 
      mcu_data_rsci_data_in_d(10)=>mcu_data_rsc_data_in(10), 
      mcu_data_rsci_data_in_d(9)=>mcu_data_rsc_data_in(9), 
      mcu_data_rsci_data_in_d(8)=>mcu_data_rsc_data_in(8), 
      mcu_data_rsci_data_in_d(7)=>mcu_data_rsc_data_in(7), 
      mcu_data_rsci_data_in_d(6)=>mcu_data_rsc_data_in(6), 
      mcu_data_rsci_data_in_d(5)=>mcu_data_rsc_data_in(5), 
      mcu_data_rsci_data_in_d(4)=>mcu_data_rsc_data_in(4), 
      mcu_data_rsci_data_in_d(3)=>mcu_data_rsc_data_in(3), 
      mcu_data_rsci_data_in_d(2)=>mcu_data_rsc_data_in(2), 
      mcu_data_rsci_data_in_d(1)=>mcu_data_rsc_data_in(1), 
      mcu_data_rsci_data_in_d(0)=>mcu_data_rsc_data_in(0), 
      mcu_data_rsci_addr_d(8)=>DANGLING(17), mcu_data_rsci_addr_d(7)=>
      DANGLING(18), mcu_data_rsci_addr_d(6)=>DANGLING(19), 
      mcu_data_rsci_addr_d(5)=>DANGLING(20), mcu_data_rsci_addr_d(4)=>
      DANGLING(21), mcu_data_rsci_addr_d(3)=>mcu_data_rsc_addr(3), 
      mcu_data_rsci_addr_d(2)=>mcu_data_rsc_addr(2), mcu_data_rsci_addr_d(1)
      =>mcu_data_rsc_addr(1), mcu_data_rsci_addr_d(0)=>mcu_data_rsc_addr(0), 
      mcu_data_rsci_re_d=>mcu_data_rsc_re, mcu_data_rsci_we_d=>
      mcu_data_rsc_we, mcu_data_rsci_data_out_d(31)=>
      mcu_data_rsc_data_out(31), mcu_data_rsci_data_out_d(30)=>
      mcu_data_rsc_data_out(30), mcu_data_rsci_data_out_d(29)=>
      mcu_data_rsc_data_out(29), mcu_data_rsci_data_out_d(28)=>
      mcu_data_rsc_data_out(28), mcu_data_rsci_data_out_d(27)=>
      mcu_data_rsc_data_out(27), mcu_data_rsci_data_out_d(26)=>
      mcu_data_rsc_data_out(26), mcu_data_rsci_data_out_d(25)=>
      mcu_data_rsc_data_out(25), mcu_data_rsci_data_out_d(24)=>
      mcu_data_rsc_data_out(24), mcu_data_rsci_data_out_d(23)=>
      mcu_data_rsc_data_out(23), mcu_data_rsci_data_out_d(22)=>
      mcu_data_rsc_data_out(22), mcu_data_rsci_data_out_d(21)=>
      mcu_data_rsc_data_out(21), mcu_data_rsci_data_out_d(20)=>
      mcu_data_rsc_data_out(20), mcu_data_rsci_data_out_d(19)=>
      mcu_data_rsc_data_out(19), mcu_data_rsci_data_out_d(18)=>
      mcu_data_rsc_data_out(18), mcu_data_rsci_data_out_d(17)=>
      mcu_data_rsc_data_out(17), mcu_data_rsci_data_out_d(16)=>
      mcu_data_rsc_data_out(16), mcu_data_rsci_data_out_d(15)=>
      mcu_data_rsc_data_out(15), mcu_data_rsci_data_out_d(14)=>
      mcu_data_rsc_data_out(14), mcu_data_rsci_data_out_d(13)=>
      mcu_data_rsc_data_out(13), mcu_data_rsci_data_out_d(12)=>
      mcu_data_rsc_data_out(12), mcu_data_rsci_data_out_d(11)=>
      mcu_data_rsc_data_out(11), mcu_data_rsci_data_out_d(10)=>
      mcu_data_rsc_data_out(10), mcu_data_rsci_data_out_d(9)=>
      mcu_data_rsc_data_out(9), mcu_data_rsci_data_out_d(8)=>
      mcu_data_rsc_data_out(8), mcu_data_rsci_data_out_d(7)=>
      mcu_data_rsc_data_out(7), mcu_data_rsci_data_out_d(6)=>
      mcu_data_rsc_data_out(6), mcu_data_rsci_data_out_d(5)=>
      mcu_data_rsc_data_out(5), mcu_data_rsci_data_out_d(4)=>
      mcu_data_rsc_data_out(4), mcu_data_rsci_data_out_d(3)=>
      mcu_data_rsc_data_out(3), mcu_data_rsci_data_out_d(2)=>
      mcu_data_rsc_data_out(2), mcu_data_rsci_data_out_d(1)=>
      mcu_data_rsc_data_out(1), mcu_data_rsci_data_out_d(0)=>
      mcu_data_rsc_data_out(0), histogram_rsci_data_in_d(16)=>
      histogram_rsc_comp_data_in(16), histogram_rsci_data_in_d(15)=>
      histogram_rsc_comp_data_in(15), histogram_rsci_data_in_d(14)=>
      histogram_rsc_comp_data_in(14), histogram_rsci_data_in_d(13)=>
      histogram_rsc_comp_data_in(13), histogram_rsci_data_in_d(12)=>
      histogram_rsc_comp_data_in(12), histogram_rsci_data_in_d(11)=>
      histogram_rsc_comp_data_in(11), histogram_rsci_data_in_d(10)=>
      histogram_rsc_comp_data_in(10), histogram_rsci_data_in_d(9)=>
      histogram_rsc_comp_data_in(9), histogram_rsci_data_in_d(8)=>
      histogram_rsc_comp_data_in(8), histogram_rsci_data_in_d(7)=>
      histogram_rsc_comp_data_in(7), histogram_rsci_data_in_d(6)=>
      histogram_rsc_comp_data_in(6), histogram_rsci_data_in_d(5)=>
      histogram_rsc_comp_data_in(5), histogram_rsci_data_in_d(4)=>
      histogram_rsc_comp_data_in(4), histogram_rsci_data_in_d(3)=>
      histogram_rsc_comp_data_in(3), histogram_rsci_data_in_d(2)=>
      histogram_rsc_comp_data_in(2), histogram_rsci_data_in_d(1)=>
      histogram_rsc_comp_data_in(1), histogram_rsci_data_in_d(0)=>
      histogram_rsc_comp_data_in(0), histogram_rsci_addr_d(2)=>
      histogram_rsc_comp_addr(2), histogram_rsci_addr_d(1)=>
      histogram_rsc_comp_addr(1), histogram_rsci_addr_d(0)=>
      histogram_rsc_comp_addr(0), histogram_rsci_re_d=>DANGLING(22), 
      histogram_rsci_we_d=>DANGLING(23), histogram_rsci_data_out_d(16)=>
      filter_core_inst_histogram_rsci_data_out_d(16), 
      histogram_rsci_data_out_d(15)=>
      filter_core_inst_histogram_rsci_data_out_d(15), 
      histogram_rsci_data_out_d(14)=>
      filter_core_inst_histogram_rsci_data_out_d(14), 
      histogram_rsci_data_out_d(13)=>
      filter_core_inst_histogram_rsci_data_out_d(13), 
      histogram_rsci_data_out_d(12)=>
      filter_core_inst_histogram_rsci_data_out_d(12), 
      histogram_rsci_data_out_d(11)=>
      filter_core_inst_histogram_rsci_data_out_d(11), 
      histogram_rsci_data_out_d(10)=>
      filter_core_inst_histogram_rsci_data_out_d(10), 
      histogram_rsci_data_out_d(9)=>
      filter_core_inst_histogram_rsci_data_out_d(9), 
      histogram_rsci_data_out_d(8)=>
      filter_core_inst_histogram_rsci_data_out_d(8), 
      histogram_rsci_data_out_d(7)=>
      filter_core_inst_histogram_rsci_data_out_d(7), 
      histogram_rsci_data_out_d(6)=>
      filter_core_inst_histogram_rsci_data_out_d(6), 
      histogram_rsci_data_out_d(5)=>
      filter_core_inst_histogram_rsci_data_out_d(5), 
      histogram_rsci_data_out_d(4)=>
      filter_core_inst_histogram_rsci_data_out_d(4), 
      histogram_rsci_data_out_d(3)=>
      filter_core_inst_histogram_rsci_data_out_d(3), 
      histogram_rsci_data_out_d(2)=>
      filter_core_inst_histogram_rsci_data_out_d(2), 
      histogram_rsci_data_out_d(1)=>
      filter_core_inst_histogram_rsci_data_out_d(1), 
      histogram_rsci_data_out_d(0)=>
      filter_core_inst_histogram_rsci_data_out_d(0), 
      buffer_buf_rsci_data_in_d(2)=>buffer_buf_rsc_comp_data_in(2), 
      buffer_buf_rsci_data_in_d(1)=>buffer_buf_rsc_comp_data_in(1), 
      buffer_buf_rsci_data_in_d(0)=>buffer_buf_rsc_comp_data_in(0), 
      buffer_buf_rsci_addr_d(9)=>buffer_buf_rsc_comp_addr(9), 
      buffer_buf_rsci_addr_d(8)=>buffer_buf_rsc_comp_addr(8), 
      buffer_buf_rsci_addr_d(7)=>buffer_buf_rsc_comp_addr(7), 
      buffer_buf_rsci_addr_d(6)=>buffer_buf_rsc_comp_addr(6), 
      buffer_buf_rsci_addr_d(5)=>buffer_buf_rsc_comp_addr(5), 
      buffer_buf_rsci_addr_d(4)=>buffer_buf_rsc_comp_addr(4), 
      buffer_buf_rsci_addr_d(3)=>buffer_buf_rsc_comp_addr(3), 
      buffer_buf_rsci_addr_d(2)=>buffer_buf_rsc_comp_addr(2), 
      buffer_buf_rsci_addr_d(1)=>buffer_buf_rsc_comp_addr(1), 
      buffer_buf_rsci_addr_d(0)=>buffer_buf_rsc_comp_addr(0), 
      buffer_buf_rsci_re_d=>DANGLING(24), buffer_buf_rsci_we_d=>
      buffer_buf_rsc_comp_we_0, buffer_buf_rsci_data_out_d(2)=>
      filter_core_inst_buffer_buf_rsci_data_out_d(2), 
      buffer_buf_rsci_data_out_d(1)=>
      filter_core_inst_buffer_buf_rsci_data_out_d(1), 
      buffer_buf_rsci_data_out_d(0)=>
      filter_core_inst_buffer_buf_rsci_data_out_d(0), px2584=>nx15727z2, 
      px2583=>nx15727z3, p_fsm_output_4=>fsm_output_4_dup_151, 
      p_exit_if_if_if_1_if_for_lpi_1_dfm_3=>
      exit_if_if_if_1_if_for_lpi_1_dfm_3_dup_152);
   histogram_rsc_comp_mem : ram_dq_17_0 port map ( wr_data1(16)=>
      histogram_rsc_comp_data_in(16), wr_data1(15)=>
      histogram_rsc_comp_data_in(15), wr_data1(14)=>
      histogram_rsc_comp_data_in(14), wr_data1(13)=>
      histogram_rsc_comp_data_in(13), wr_data1(12)=>
      histogram_rsc_comp_data_in(12), wr_data1(11)=>
      histogram_rsc_comp_data_in(11), wr_data1(10)=>
      histogram_rsc_comp_data_in(10), wr_data1(9)=>
      histogram_rsc_comp_data_in(9), wr_data1(8)=>
      histogram_rsc_comp_data_in(8), wr_data1(7)=>
      histogram_rsc_comp_data_in(7), wr_data1(6)=>
      histogram_rsc_comp_data_in(6), wr_data1(5)=>
      histogram_rsc_comp_data_in(5), wr_data1(4)=>
      histogram_rsc_comp_data_in(4), wr_data1(3)=>
      histogram_rsc_comp_data_in(3), wr_data1(2)=>
      histogram_rsc_comp_data_in(2), wr_data1(1)=>
      histogram_rsc_comp_data_in(1), wr_data1(0)=>
      histogram_rsc_comp_data_in(0), rd_data1(16)=>DANGLING(25), 
      rd_data1(15)=>DANGLING(26), rd_data1(14)=>DANGLING(27), rd_data1(13)=>
      DANGLING(28), rd_data1(12)=>DANGLING(29), rd_data1(11)=>DANGLING(30), 
      rd_data1(10)=>DANGLING(31), rd_data1(9)=>DANGLING(32), rd_data1(8)=>
      DANGLING(33), rd_data1(7)=>DANGLING(34), rd_data1(6)=>DANGLING(35), 
      rd_data1(5)=>DANGLING(36), rd_data1(4)=>DANGLING(37), rd_data1(3)=>
      DANGLING(38), rd_data1(2)=>DANGLING(39), rd_data1(1)=>DANGLING(40), 
      rd_data1(0)=>DANGLING(41), addr1(2)=>histogram_rsc_comp_addr(2), 
      addr1(1)=>histogram_rsc_comp_addr(1), addr1(0)=>
      histogram_rsc_comp_addr(0), wr_clk1=>clk, rd_clk1=>DANGLING(42), 
      wr_ena1=>nx15727z1, rd_ena1=>DANGLING(43), ena1=>DANGLING(44), rst1=>
      DANGLING(45), regce1=>DANGLING(46), wr_data2(16)=>DANGLING(47), 
      wr_data2(15)=>DANGLING(48), wr_data2(14)=>DANGLING(49), wr_data2(13)=>
      DANGLING(50), wr_data2(12)=>DANGLING(51), wr_data2(11)=>DANGLING(52), 
      wr_data2(10)=>DANGLING(53), wr_data2(9)=>DANGLING(54), wr_data2(8)=>
      DANGLING(55), wr_data2(7)=>DANGLING(56), wr_data2(6)=>DANGLING(57), 
      wr_data2(5)=>DANGLING(58), wr_data2(4)=>DANGLING(59), wr_data2(3)=>
      DANGLING(60), wr_data2(2)=>DANGLING(61), wr_data2(1)=>DANGLING(62), 
      wr_data2(0)=>DANGLING(63), rd_data2(16)=>
      filter_core_inst_histogram_rsci_data_out_d(16), rd_data2(15)=>
      filter_core_inst_histogram_rsci_data_out_d(15), rd_data2(14)=>
      filter_core_inst_histogram_rsci_data_out_d(14), rd_data2(13)=>
      filter_core_inst_histogram_rsci_data_out_d(13), rd_data2(12)=>
      filter_core_inst_histogram_rsci_data_out_d(12), rd_data2(11)=>
      filter_core_inst_histogram_rsci_data_out_d(11), rd_data2(10)=>
      filter_core_inst_histogram_rsci_data_out_d(10), rd_data2(9)=>
      filter_core_inst_histogram_rsci_data_out_d(9), rd_data2(8)=>
      filter_core_inst_histogram_rsci_data_out_d(8), rd_data2(7)=>
      filter_core_inst_histogram_rsci_data_out_d(7), rd_data2(6)=>
      filter_core_inst_histogram_rsci_data_out_d(6), rd_data2(5)=>
      filter_core_inst_histogram_rsci_data_out_d(5), rd_data2(4)=>
      filter_core_inst_histogram_rsci_data_out_d(4), rd_data2(3)=>
      filter_core_inst_histogram_rsci_data_out_d(3), rd_data2(2)=>
      filter_core_inst_histogram_rsci_data_out_d(2), rd_data2(1)=>
      filter_core_inst_histogram_rsci_data_out_d(1), rd_data2(0)=>
      filter_core_inst_histogram_rsci_data_out_d(0), addr2(2)=>
      histogram_rsc_comp_addr_reg(2), addr2(1)=>
      histogram_rsc_comp_addr_reg(1), addr2(0)=>
      histogram_rsc_comp_addr_reg(0), wr_clk2=>DANGLING(64), rd_clk2=>
      DANGLING(65), wr_ena2=>DANGLING(66), rd_ena2=>DANGLING(67), ena2=>
      DANGLING(68), rst2=>DANGLING(69), regce2=>DANGLING(70));
   buffer_buf_rsc_comp_mem_1 : RAMB16_S4
      generic map (WRITE_MODE => "WRITE_FIRST") 
       port map ( DO(3)=>DANGLING(71), DO(2)=>
      filter_core_inst_buffer_buf_rsci_data_out_d(2), DO(1)=>
      filter_core_inst_buffer_buf_rsci_data_out_d(1), DO(0)=>
      filter_core_inst_buffer_buf_rsci_data_out_d(0), ADDR(11)=>
      mcu_data_rsc_addr_4_EXMPLR290, ADDR(10)=>mcu_data_rsc_addr_4_EXMPLR290, 
      ADDR(9)=>buffer_buf_rsc_comp_addr(9), ADDR(8)=>
      buffer_buf_rsc_comp_addr(8), ADDR(7)=>buffer_buf_rsc_comp_addr(7), 
      ADDR(6)=>buffer_buf_rsc_comp_addr(6), ADDR(5)=>
      buffer_buf_rsc_comp_addr(5), ADDR(4)=>buffer_buf_rsc_comp_addr(4), 
      ADDR(3)=>buffer_buf_rsc_comp_addr(3), ADDR(2)=>
      buffer_buf_rsc_comp_addr(2), ADDR(1)=>buffer_buf_rsc_comp_addr(1), 
      ADDR(0)=>buffer_buf_rsc_comp_addr(0), CLK=>clk, DI(3)=>
      mcu_data_rsc_addr_4_EXMPLR290, DI(2)=>buffer_buf_rsc_comp_data_in(2), 
      DI(1)=>buffer_buf_rsc_comp_data_in(1), DI(0)=>
      buffer_buf_rsc_comp_data_in(0), EN=>histogram_rsc_comp_not_en, SSR=>
      mcu_data_rsc_addr_4_EXMPLR290, WE=>nx22893z1);
   histogram_rsc_comp_reg_addr_reg_2 : FD port map ( Q=>
      histogram_rsc_comp_addr_reg(2), C=>clk, D=>histogram_rsc_comp_addr(2)
   );
   histogram_rsc_comp_reg_addr_reg_1 : FD port map ( Q=>
      histogram_rsc_comp_addr_reg(1), C=>clk, D=>histogram_rsc_comp_addr(1)
   );
   histogram_rsc_comp_reg_addr_reg_0 : FD port map ( Q=>
      histogram_rsc_comp_addr_reg(0), C=>clk, D=>histogram_rsc_comp_addr(0)
   );
   mcu_data_rsc_data_in_17_EXMPLR294 : GND port map ( G=>
      mcu_data_rsc_addr_4_EXMPLR290);
   histogram_rsc_comp_not_en_EXMPLR295 : VCC port map ( P=>
      histogram_rsc_comp_not_en);
   ix22893z1401 : LUT4
      generic map (INIT => X"0057") 
       port map ( O=>nx22893z1, I0=>buffer_buf_rsc_comp_addr(9), I1=>
      buffer_buf_rsc_comp_addr(8), I2=>buffer_buf_rsc_comp_addr(7), I3=>
      buffer_buf_rsc_comp_we_0);
   ix15727z47189 : LUT4
      generic map (INIT => X"B333") 
       port map ( O=>nx15727z1, I0=>nx15727z2, I1=>nx15727z3, I2=>
      fsm_output_4_dup_151, I3=>exit_if_if_if_1_if_for_lpi_1_dfm_3_dup_152);

end v35 ;

