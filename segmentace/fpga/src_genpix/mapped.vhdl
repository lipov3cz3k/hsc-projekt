
-- 
-- Definition of  genpix
-- 
--      01/06/16 18:01:53
--      
--      Precision RTL Synthesis, 2012a.10
-- 

library IEEE;
use IEEE.STD_LOGIC_1164.all;
-- Library use clause for technology cells
library unisim ;
use unisim.vcomponents.all;

entity genpix_core is 
   port (
      clk : IN std_logic ;
      rst : IN std_logic ;
      pause_rsc_z : IN std_logic ;
      req_rsc_z : IN std_logic ;
      pixel_rsc_z : OUT std_logic_vector (2 DOWNTO 0) ;
      pixel_vld_rsc_z : OUT std_logic) ;
end genpix_core ;

architecture v1 of genpix_core is 
   signal r_sva: std_logic_vector (8 DOWNTO 0) ;
   
   signal c_sva: std_logic_vector (8 DOWNTO 0) ;
   
   signal base_r_sva: std_logic_vector (8 DOWNTO 0) ;
   
   signal base_c_sva: std_logic_vector (8 DOWNTO 0) ;
   
   signal noise_cnt_sva: std_logic_vector (9 DOWNTO 0) ;
   
   signal frame_cnt_sva_5, frame_cnt_sva_4, frame_cnt_sva_3, 
      update_base_pos_inc_r_sg1_sva, update_base_pos_inc_c_sg1_sva, 
      if_1_asn_itm: std_logic ;
   
   signal acc_itm_sg1: std_logic_vector (8 DOWNTO 0) ;
   
   signal acc_8_itm_sg1: std_logic_vector (8 DOWNTO 0) ;
   
   signal nx26657z7, nx14760z1, nx14761z1, nx14762z1, nx14763z1, nx14764z1, 
      nx14765z1, nx14766z1, nx45394z1, nx55496z1, nx55497z1, nx55498z1, 
      nx55499z1, nx4661z1, nx55501z1, nx55502z1, nx4658z1, inc_d_0, nx8474z1, 
      inc_d_1, nx8475z1, inc_d_2, nx8476z1, inc_d_3, nx8477z1, inc_d_4, 
      nx8478z1, inc_d_5, nx8479z1, inc_d_6, nx8480z1, inc_d_7, nx51680z1, 
      inc_d_8, inc_d_0_dup_92, nx18048z1, inc_d_1_dup_95, nx61391z1, 
      inc_d_2_dup_98, nx26338z1, inc_d_3_dup_101, nx17005z1, inc_d_4_dup_104, 
      nx61335z1, inc_d_5_dup_107, nx26394z1, inc_d_6_dup_110, nx48587z1, 
      inc_d_7_dup_113, nx37686z1, inc_d_8_dup_116, inc_d_0_dup_149, 
      nx62978z1, inc_d_1_dup_151, nx64378z1, inc_d_2_dup_153, nx23351z1, 
      inc_d_3_dup_155, nx19992z1, inc_d_4_dup_157, nx63335z1, 
      inc_d_5_dup_159, nx24394z1, inc_d_6_dup_161, nx45600z1, 
      inc_d_7_dup_163, nx8481z1, inc_d_8_dup_166, nx51679z1, inc_d_9, b_2, 
      b_1, b_0, nx32004z1, nx32005z1, nx32006z1, nx32007z1, nx32008z1, 
      nx32009z1, nx32010z1, nx32011z1, NOT_or_dcpl_17, 
      update_base_pos_inc_c_sg1_sva_dfm, update_base_pos_inc_r_sg1_sva_dfm, 
      NOT_if_and_nl, mux_nl_1, GND_EXMPLR30, PWR, NOT_equal_cse_sva_0n0s2, 
      NOT_unequal_tmp_0n0s3, NOT_unequal_tmp_1_0n0s3, not_tmp_14_0n0s2: 
   std_logic ;
   
   signal pixel_rsci_d_4n1s1: std_logic_vector (2 DOWNTO 0) ;
   
   signal not_pause_rsci_d: std_logic ;
   
   signal base_c_sva_9n1s1: std_logic_vector (8 DOWNTO 0) ;
   
   signal base_r_sva_10n1s1: std_logic_vector (8 DOWNTO 0) ;
   
   signal nx51271z1, nx9490z1, nx14759z1, nx14760z2, nx14761z2, nx14762z2, 
      nx14763z2, nx14764z2, nx14765z2, nx14766z2, nx45394z2, nx55495z1, 
      nx55496z2, nx55497z2, nx55498z2, nx55499z2, nx4661z2, nx55501z2, 
      nx55502z2, nx4658z2, nx46882z1, inc_d_1_dup_173, inc_d_2_dup_174, 
      inc_d_3_dup_175, inc_d_4_dup_176, inc_d_5_dup_177, inc_d_0_dup_178, 
      nx46882z2, nx32003z1, nx32004z2, nx32005z2, nx32006z2, nx32007z2, 
      nx32008z2, nx32009z2, nx32010z2, nx32011z2, nx26657z8, nx26657z6, 
      nx26657z10, nx41633z1, nx39639z1, nx37645z1, nx46878z1, nx44884z1, 
      nx42890z1, NOT_a_3, NOT_a_0_dup_257, NOT_a_0_dup_287, nx51271z4, 
      nx19413z1, nx51271z2, nx51271z3, nx51271z5, nx51271z6, nx26657z3, 
      nx26657z4, nx26657z1, nx26657z2, nx26657z5, nx43627z1, nx43627z2, 
      nx48872z2, nx48872z3, nx26657z9, nx26657z11, nx25660z1, nx48872z1, 
      nx24663z1, nx61438z1: std_logic ;

begin
   acc_itm_sg1_sub9_0_xorcy_0 : XORCY port map ( O=>acc_itm_sg1(0), CI=>PWR, 
      LI=>nx14759z1);
   acc_itm_sg1_sub9_0_muxcy_0 : MUXCY_L port map ( LO=>nx14760z1, CI=>PWR, 
      DI=>c_sva(0), S=>nx14759z1);
   acc_itm_sg1_sub9_0_xorcy_1 : XORCY port map ( O=>acc_itm_sg1(1), CI=>
      nx14760z1, LI=>nx14760z2);
   acc_itm_sg1_sub9_0_muxcy_1 : MUXCY_L port map ( LO=>nx14761z1, CI=>
      nx14760z1, DI=>c_sva(1), S=>nx14760z2);
   acc_itm_sg1_sub9_0_xorcy_2 : XORCY port map ( O=>acc_itm_sg1(2), CI=>
      nx14761z1, LI=>nx14761z2);
   acc_itm_sg1_sub9_0_muxcy_2 : MUXCY_L port map ( LO=>nx14762z1, CI=>
      nx14761z1, DI=>c_sva(2), S=>nx14761z2);
   acc_itm_sg1_sub9_0_xorcy_3 : XORCY port map ( O=>acc_itm_sg1(3), CI=>
      nx14762z1, LI=>nx14762z2);
   acc_itm_sg1_sub9_0_muxcy_3 : MUXCY_L port map ( LO=>nx14763z1, CI=>
      nx14762z1, DI=>c_sva(3), S=>nx14762z2);
   acc_itm_sg1_sub9_0_xorcy_4 : XORCY port map ( O=>acc_itm_sg1(4), CI=>
      nx14763z1, LI=>nx14763z2);
   acc_itm_sg1_sub9_0_muxcy_4 : MUXCY_L port map ( LO=>nx14764z1, CI=>
      nx14763z1, DI=>c_sva(4), S=>nx14763z2);
   acc_itm_sg1_sub9_0_xorcy_5 : XORCY port map ( O=>acc_itm_sg1(5), CI=>
      nx14764z1, LI=>nx14764z2);
   acc_itm_sg1_sub9_0_muxcy_5 : MUXCY_L port map ( LO=>nx14765z1, CI=>
      nx14764z1, DI=>c_sva(5), S=>nx14764z2);
   acc_itm_sg1_sub9_0_xorcy_6 : XORCY port map ( O=>acc_itm_sg1(6), CI=>
      nx14765z1, LI=>nx14765z2);
   acc_itm_sg1_sub9_0_muxcy_6 : MUXCY_L port map ( LO=>nx14766z1, CI=>
      nx14765z1, DI=>c_sva(6), S=>nx14765z2);
   acc_itm_sg1_sub9_0_xorcy_7 : XORCY port map ( O=>acc_itm_sg1(7), CI=>
      nx14766z1, LI=>nx14766z2);
   acc_itm_sg1_sub9_0_muxcy_7 : MUXCY_L port map ( LO=>nx45394z1, CI=>
      nx14766z1, DI=>c_sva(7), S=>nx14766z2);
   acc_itm_sg1_sub9_0_xorcy_8 : XORCY port map ( O=>acc_itm_sg1(8), CI=>
      nx45394z1, LI=>nx45394z2);
   acc_8_itm_sg1_sub9_1_xorcy_0 : XORCY port map ( O=>acc_8_itm_sg1(0), CI=>
      PWR, LI=>nx55495z1);
   acc_8_itm_sg1_sub9_1_muxcy_0 : MUXCY_L port map ( LO=>nx55496z1, CI=>PWR, 
      DI=>r_sva(0), S=>nx55495z1);
   acc_8_itm_sg1_sub9_1_xorcy_1 : XORCY port map ( O=>acc_8_itm_sg1(1), CI=>
      nx55496z1, LI=>nx55496z2);
   acc_8_itm_sg1_sub9_1_muxcy_1 : MUXCY_L port map ( LO=>nx55497z1, CI=>
      nx55496z1, DI=>r_sva(1), S=>nx55496z2);
   acc_8_itm_sg1_sub9_1_xorcy_2 : XORCY port map ( O=>acc_8_itm_sg1(2), CI=>
      nx55497z1, LI=>nx55497z2);
   acc_8_itm_sg1_sub9_1_muxcy_2 : MUXCY_L port map ( LO=>nx55498z1, CI=>
      nx55497z1, DI=>r_sva(2), S=>nx55497z2);
   acc_8_itm_sg1_sub9_1_xorcy_3 : XORCY port map ( O=>acc_8_itm_sg1(3), CI=>
      nx55498z1, LI=>nx55498z2);
   acc_8_itm_sg1_sub9_1_muxcy_3 : MUXCY_L port map ( LO=>nx55499z1, CI=>
      nx55498z1, DI=>r_sva(3), S=>nx55498z2);
   acc_8_itm_sg1_sub9_1_xorcy_4 : XORCY port map ( O=>acc_8_itm_sg1(4), CI=>
      nx55499z1, LI=>nx55499z2);
   acc_8_itm_sg1_sub9_1_muxcy_4 : MUXCY_L port map ( LO=>nx4661z1, CI=>
      nx55499z1, DI=>r_sva(4), S=>nx55499z2);
   acc_8_itm_sg1_sub9_1_xorcy_5 : XORCY port map ( O=>acc_8_itm_sg1(5), CI=>
      nx4661z1, LI=>nx4661z2);
   acc_8_itm_sg1_sub9_1_muxcy_5 : MUXCY_L port map ( LO=>nx55501z1, CI=>
      nx4661z1, DI=>r_sva(5), S=>nx4661z2);
   acc_8_itm_sg1_sub9_1_xorcy_6 : XORCY port map ( O=>acc_8_itm_sg1(6), CI=>
      nx55501z1, LI=>nx55501z2);
   acc_8_itm_sg1_sub9_1_muxcy_6 : MUXCY_L port map ( LO=>nx55502z1, CI=>
      nx55501z1, DI=>r_sva(6), S=>nx55501z2);
   acc_8_itm_sg1_sub9_1_xorcy_7 : XORCY port map ( O=>acc_8_itm_sg1(7), CI=>
      nx55502z1, LI=>nx55502z2);
   acc_8_itm_sg1_sub9_1_muxcy_7 : MUXCY_L port map ( LO=>nx4658z1, CI=>
      nx55502z1, DI=>r_sva(7), S=>nx55502z2);
   acc_8_itm_sg1_sub9_1_xorcy_8 : XORCY port map ( O=>acc_8_itm_sg1(8), CI=>
      nx4658z1, LI=>nx4658z2);
   reg_q_8 : FDRE port map ( Q=>r_sva(8), C=>clk, CE=>nx51271z1, D=>inc_d_8, 
      R=>nx51271z4);
   reg_q_7 : FDRE port map ( Q=>r_sva(7), C=>clk, CE=>nx51271z1, D=>inc_d_7, 
      R=>nx51271z4);
   reg_q_6 : FDRE port map ( Q=>r_sva(6), C=>clk, CE=>nx51271z1, D=>inc_d_6, 
      R=>nx51271z4);
   reg_q_5 : FDRE port map ( Q=>r_sva(5), C=>clk, CE=>nx51271z1, D=>inc_d_5, 
      R=>nx51271z4);
   reg_q_4 : FDRE port map ( Q=>r_sva(4), C=>clk, CE=>nx51271z1, D=>inc_d_4, 
      R=>nx51271z4);
   reg_q_3 : FDRE port map ( Q=>r_sva(3), C=>clk, CE=>nx51271z1, D=>inc_d_3, 
      R=>nx51271z4);
   reg_q_2 : FDRE port map ( Q=>r_sva(2), C=>clk, CE=>nx51271z1, D=>inc_d_2, 
      R=>nx51271z4);
   reg_q_1 : FDRE port map ( Q=>r_sva(1), C=>clk, CE=>nx51271z1, D=>inc_d_1, 
      R=>nx51271z4);
   reg_q_0 : FDRE port map ( Q=>r_sva(0), C=>clk, CE=>nx51271z1, D=>inc_d_0, 
      R=>nx51271z4);
   xorcy_0 : XORCY port map ( O=>inc_d_0, CI=>PWR, LI=>r_sva(0));
   muxcy_0 : MUXCY_L port map ( LO=>nx8474z1, CI=>PWR, DI=>GND_EXMPLR30, S=>
      r_sva(0));
   xorcy_1 : XORCY port map ( O=>inc_d_1, CI=>nx8474z1, LI=>r_sva(1));
   muxcy_1 : MUXCY_L port map ( LO=>nx8475z1, CI=>nx8474z1, DI=>GND_EXMPLR30, 
      S=>r_sva(1));
   xorcy_2 : XORCY port map ( O=>inc_d_2, CI=>nx8475z1, LI=>r_sva(2));
   muxcy_2 : MUXCY_L port map ( LO=>nx8476z1, CI=>nx8475z1, DI=>GND_EXMPLR30, 
      S=>r_sva(2));
   xorcy_3 : XORCY port map ( O=>inc_d_3, CI=>nx8476z1, LI=>r_sva(3));
   muxcy_3 : MUXCY_L port map ( LO=>nx8477z1, CI=>nx8476z1, DI=>GND_EXMPLR30, 
      S=>r_sva(3));
   xorcy_4 : XORCY port map ( O=>inc_d_4, CI=>nx8477z1, LI=>r_sva(4));
   muxcy_4 : MUXCY_L port map ( LO=>nx8478z1, CI=>nx8477z1, DI=>GND_EXMPLR30, 
      S=>r_sva(4));
   xorcy_5 : XORCY port map ( O=>inc_d_5, CI=>nx8478z1, LI=>r_sva(5));
   muxcy_5 : MUXCY_L port map ( LO=>nx8479z1, CI=>nx8478z1, DI=>GND_EXMPLR30, 
      S=>r_sva(5));
   xorcy_6 : XORCY port map ( O=>inc_d_6, CI=>nx8479z1, LI=>r_sva(6));
   muxcy_6 : MUXCY_L port map ( LO=>nx8480z1, CI=>nx8479z1, DI=>GND_EXMPLR30, 
      S=>r_sva(6));
   xorcy_7 : XORCY port map ( O=>inc_d_7, CI=>nx8480z1, LI=>r_sva(7));
   muxcy_7 : MUXCY_L port map ( LO=>nx51680z1, CI=>nx8480z1, DI=>
      GND_EXMPLR30, S=>r_sva(7));
   xorcy_8 : XORCY port map ( O=>inc_d_8, CI=>nx51680z1, LI=>r_sva(8));
   reg_q_8_dup_0 : FDRE port map ( Q=>c_sva(8), C=>clk, CE=>nx9490z1, D=>
      inc_d_8_dup_116, R=>nx51271z1);
   reg_q_7_dup_1 : FDRE port map ( Q=>c_sva(7), C=>clk, CE=>nx9490z1, D=>
      inc_d_7_dup_113, R=>nx51271z1);
   reg_q_6_dup_2 : FDRE port map ( Q=>c_sva(6), C=>clk, CE=>nx9490z1, D=>
      inc_d_6_dup_110, R=>nx51271z1);
   reg_q_5_dup_3 : FDRE port map ( Q=>c_sva(5), C=>clk, CE=>nx9490z1, D=>
      inc_d_5_dup_107, R=>nx51271z1);
   reg_q_4_dup_4 : FDRE port map ( Q=>c_sva(4), C=>clk, CE=>nx9490z1, D=>
      inc_d_4_dup_104, R=>nx51271z1);
   reg_q_3_dup_5 : FDRE port map ( Q=>c_sva(3), C=>clk, CE=>nx9490z1, D=>
      inc_d_3_dup_101, R=>nx51271z1);
   reg_q_2_dup_6 : FDRE port map ( Q=>c_sva(2), C=>clk, CE=>nx9490z1, D=>
      inc_d_2_dup_98, R=>nx51271z1);
   reg_q_1_dup_7 : FDRE port map ( Q=>c_sva(1), C=>clk, CE=>nx9490z1, D=>
      inc_d_1_dup_95, R=>nx51271z1);
   reg_q_0_dup_8 : FDRE port map ( Q=>c_sva(0), C=>clk, CE=>nx9490z1, D=>
      inc_d_0_dup_92, R=>nx51271z1);
   xorcy_0_dup_9 : XORCY port map ( O=>inc_d_0_dup_92, CI=>PWR, LI=>c_sva(0)
   );
   muxcy_0_dup_10 : MUXCY_L port map ( LO=>nx18048z1, CI=>PWR, DI=>
      GND_EXMPLR30, S=>c_sva(0));
   xorcy_1_dup_11 : XORCY port map ( O=>inc_d_1_dup_95, CI=>nx18048z1, LI=>
      c_sva(1));
   muxcy_1_dup_12 : MUXCY_L port map ( LO=>nx61391z1, CI=>nx18048z1, DI=>
      GND_EXMPLR30, S=>c_sva(1));
   xorcy_2_dup_13 : XORCY port map ( O=>inc_d_2_dup_98, CI=>nx61391z1, LI=>
      c_sva(2));
   muxcy_2_dup_14 : MUXCY_L port map ( LO=>nx26338z1, CI=>nx61391z1, DI=>
      GND_EXMPLR30, S=>c_sva(2));
   xorcy_3_dup_15 : XORCY port map ( O=>inc_d_3_dup_101, CI=>nx26338z1, LI=>
      c_sva(3));
   muxcy_3_dup_16 : MUXCY_L port map ( LO=>nx17005z1, CI=>nx26338z1, DI=>
      GND_EXMPLR30, S=>c_sva(3));
   xorcy_4_dup_17 : XORCY port map ( O=>inc_d_4_dup_104, CI=>nx17005z1, LI=>
      c_sva(4));
   muxcy_4_dup_18 : MUXCY_L port map ( LO=>nx61335z1, CI=>nx17005z1, DI=>
      GND_EXMPLR30, S=>c_sva(4));
   xorcy_5_dup_19 : XORCY port map ( O=>inc_d_5_dup_107, CI=>nx61335z1, LI=>
      c_sva(5));
   muxcy_5_dup_20 : MUXCY_L port map ( LO=>nx26394z1, CI=>nx61335z1, DI=>
      GND_EXMPLR30, S=>c_sva(5));
   xorcy_6_dup_21 : XORCY port map ( O=>inc_d_6_dup_110, CI=>nx26394z1, LI=>
      c_sva(6));
   muxcy_6_dup_22 : MUXCY_L port map ( LO=>nx48587z1, CI=>nx26394z1, DI=>
      GND_EXMPLR30, S=>c_sva(6));
   xorcy_7_dup_23 : XORCY port map ( O=>inc_d_7_dup_113, CI=>nx48587z1, LI=>
      c_sva(7));
   muxcy_7_dup_24 : MUXCY_L port map ( LO=>nx37686z1, CI=>nx48587z1, DI=>
      GND_EXMPLR30, S=>c_sva(7));
   xorcy_8_dup_25 : XORCY port map ( O=>inc_d_8_dup_116, CI=>nx37686z1, LI=>
      c_sva(8));
   reg_q_9 : FDRE port map ( Q=>noise_cnt_sva(9), C=>clk, CE=>nx9490z1, D=>
      inc_d_9, R=>nx19413z1);
   reg_q_8_dup_26 : FDRE port map ( Q=>noise_cnt_sva(8), C=>clk, CE=>
      nx9490z1, D=>inc_d_8_dup_166, R=>nx19413z1);
   reg_q_7_dup_27 : FDRE port map ( Q=>noise_cnt_sva(7), C=>clk, CE=>
      nx9490z1, D=>inc_d_7_dup_163, R=>nx19413z1);
   reg_q_6_dup_28 : FDRE port map ( Q=>noise_cnt_sva(6), C=>clk, CE=>
      nx9490z1, D=>inc_d_6_dup_161, R=>nx19413z1);
   reg_q_5_dup_29 : FDRE port map ( Q=>noise_cnt_sva(5), C=>clk, CE=>
      nx9490z1, D=>inc_d_5_dup_159, R=>nx19413z1);
   reg_q_4_dup_30 : FDRE port map ( Q=>noise_cnt_sva(4), C=>clk, CE=>
      nx9490z1, D=>inc_d_4_dup_157, R=>nx19413z1);
   reg_q_3_dup_31 : FDRE port map ( Q=>noise_cnt_sva(3), C=>clk, CE=>
      nx9490z1, D=>inc_d_3_dup_155, R=>nx19413z1);
   reg_q_2_dup_32 : FDRE port map ( Q=>noise_cnt_sva(2), C=>clk, CE=>
      nx9490z1, D=>inc_d_2_dup_153, R=>nx19413z1);
   reg_q_1_dup_33 : FDRE port map ( Q=>noise_cnt_sva(1), C=>clk, CE=>
      nx9490z1, D=>inc_d_1_dup_151, R=>nx19413z1);
   reg_q_0_dup_34 : FDRE port map ( Q=>noise_cnt_sva(0), C=>clk, CE=>
      nx9490z1, D=>inc_d_0_dup_149, R=>nx19413z1);
   xorcy_0_dup_35 : XORCY port map ( O=>inc_d_0_dup_149, CI=>PWR, LI=>
      noise_cnt_sva(0));
   muxcy_0_dup_36 : MUXCY_L port map ( LO=>nx62978z1, CI=>PWR, DI=>
      GND_EXMPLR30, S=>noise_cnt_sva(0));
   xorcy_1_dup_37 : XORCY port map ( O=>inc_d_1_dup_151, CI=>nx62978z1, LI=>
      noise_cnt_sva(1));
   muxcy_1_dup_38 : MUXCY_L port map ( LO=>nx64378z1, CI=>nx62978z1, DI=>
      GND_EXMPLR30, S=>noise_cnt_sva(1));
   xorcy_2_dup_39 : XORCY port map ( O=>inc_d_2_dup_153, CI=>nx64378z1, LI=>
      noise_cnt_sva(2));
   muxcy_2_dup_40 : MUXCY_L port map ( LO=>nx23351z1, CI=>nx64378z1, DI=>
      GND_EXMPLR30, S=>noise_cnt_sva(2));
   xorcy_3_dup_41 : XORCY port map ( O=>inc_d_3_dup_155, CI=>nx23351z1, LI=>
      noise_cnt_sva(3));
   muxcy_3_dup_42 : MUXCY_L port map ( LO=>nx19992z1, CI=>nx23351z1, DI=>
      GND_EXMPLR30, S=>noise_cnt_sva(3));
   xorcy_4_dup_43 : XORCY port map ( O=>inc_d_4_dup_157, CI=>nx19992z1, LI=>
      noise_cnt_sva(4));
   muxcy_4_dup_44 : MUXCY_L port map ( LO=>nx63335z1, CI=>nx19992z1, DI=>
      GND_EXMPLR30, S=>noise_cnt_sva(4));
   xorcy_5_dup_45 : XORCY port map ( O=>inc_d_5_dup_159, CI=>nx63335z1, LI=>
      noise_cnt_sva(5));
   muxcy_5_dup_46 : MUXCY_L port map ( LO=>nx24394z1, CI=>nx63335z1, DI=>
      GND_EXMPLR30, S=>noise_cnt_sva(5));
   xorcy_6_dup_47 : XORCY port map ( O=>inc_d_6_dup_161, CI=>nx24394z1, LI=>
      noise_cnt_sva(6));
   muxcy_6_dup_48 : MUXCY_L port map ( LO=>nx45600z1, CI=>nx24394z1, DI=>
      GND_EXMPLR30, S=>noise_cnt_sva(6));
   xorcy_7_dup_49 : XORCY port map ( O=>inc_d_7_dup_163, CI=>nx45600z1, LI=>
      noise_cnt_sva(7));
   muxcy_7_dup_50 : MUXCY_L port map ( LO=>nx8481z1, CI=>nx45600z1, DI=>
      GND_EXMPLR30, S=>noise_cnt_sva(7));
   xorcy_8_dup_51 : XORCY port map ( O=>inc_d_8_dup_166, CI=>nx8481z1, LI=>
      noise_cnt_sva(8));
   muxcy_8 : MUXCY_L port map ( LO=>nx51679z1, CI=>nx8481z1, DI=>
      GND_EXMPLR30, S=>noise_cnt_sva(8));
   xorcy_9 : XORCY port map ( O=>inc_d_9, CI=>nx51679z1, LI=>
      noise_cnt_sva(9));
   reg_q_5_dup_52 : FDRE port map ( Q=>frame_cnt_sva_5, C=>clk, CE=>
      nx46882z1, D=>inc_d_5_dup_177, R=>rst);
   reg_q_4_dup_53 : FDRE port map ( Q=>frame_cnt_sva_4, C=>clk, CE=>
      nx46882z1, D=>inc_d_4_dup_176, R=>rst);
   reg_q_3_dup_54 : FDRE port map ( Q=>frame_cnt_sva_3, C=>clk, CE=>
      nx46882z1, D=>inc_d_3_dup_175, R=>rst);
   reg_q_2_dup_55 : FDRE port map ( Q=>b_2, C=>clk, CE=>nx46882z1, D=>
      inc_d_2_dup_174, R=>rst);
   reg_q_1_dup_56 : FDRE port map ( Q=>b_1, C=>clk, CE=>nx46882z1, D=>
      inc_d_1_dup_173, R=>rst);
   reg_q_0_dup_57 : FDRE port map ( Q=>b_0, C=>clk, CE=>nx46882z1, D=>
      inc_d_0_dup_178, R=>rst);
   acc_9_nl_sub9_2_muxcy_0 : MUXCY_L port map ( LO=>nx32004z1, CI=>PWR, DI=>
      acc_itm_sg1(0), S=>nx32003z1);
   acc_9_nl_sub9_2_muxcy_1 : MUXCY_L port map ( LO=>nx32005z1, CI=>nx32004z1, 
      DI=>acc_itm_sg1(1), S=>nx32004z2);
   acc_9_nl_sub9_2_muxcy_2 : MUXCY_L port map ( LO=>nx32006z1, CI=>nx32005z1, 
      DI=>acc_itm_sg1(2), S=>nx32005z2);
   acc_9_nl_sub9_2_muxcy_3 : MUXCY_L port map ( LO=>nx32007z1, CI=>nx32006z1, 
      DI=>acc_itm_sg1(3), S=>nx32006z2);
   acc_9_nl_sub9_2_muxcy_4 : MUXCY_L port map ( LO=>nx32008z1, CI=>nx32007z1, 
      DI=>acc_itm_sg1(4), S=>nx32007z2);
   acc_9_nl_sub9_2_muxcy_5 : MUXCY_L port map ( LO=>nx32009z1, CI=>nx32008z1, 
      DI=>acc_itm_sg1(5), S=>nx32008z2);
   acc_9_nl_sub9_2_muxcy_6 : MUXCY_L port map ( LO=>nx32010z1, CI=>nx32009z1, 
      DI=>acc_itm_sg1(6), S=>nx32009z2);
   acc_9_nl_sub9_2_muxcy_7 : MUXCY_L port map ( LO=>nx32011z1, CI=>nx32010z1, 
      DI=>acc_itm_sg1(7), S=>nx32010z2);
   acc_9_nl_sub9_2_muxcy_8 : MUXCY port map ( O=>nx26657z7, CI=>nx32011z1, 
      DI=>acc_itm_sg1(8), S=>nx32011z2);
   reg_pixel_rsci_d_2 : FDRE port map ( Q=>pixel_rsc_z(2), C=>clk, CE=>
      not_tmp_14_0n0s2, D=>pixel_rsci_d_4n1s1(2), R=>rst);
   reg_pixel_rsci_d_1 : FDRE port map ( Q=>pixel_rsc_z(1), C=>clk, CE=>
      not_tmp_14_0n0s2, D=>pixel_rsci_d_4n1s1(1), R=>rst);
   reg_pixel_rsci_d_0 : FDRE port map ( Q=>pixel_rsc_z(0), C=>clk, CE=>
      not_tmp_14_0n0s2, D=>pixel_rsci_d_4n1s1(0), R=>rst);
   reg_pixel_vld_rsci_d : FDR port map ( Q=>pixel_vld_rsc_z, C=>clk, D=>
      not_pause_rsci_d, R=>rst);
   reg_base_c_sva_8 : FDRE port map ( Q=>base_c_sva(8), C=>clk, CE=>
      NOT_or_dcpl_17, D=>base_c_sva_9n1s1(8), R=>rst);
   reg_base_c_sva_7 : FDRE port map ( Q=>base_c_sva(7), C=>clk, CE=>
      NOT_or_dcpl_17, D=>base_c_sva_9n1s1(7), R=>rst);
   reg_base_c_sva_6 : FDSE port map ( Q=>base_c_sva(6), C=>clk, CE=>
      NOT_or_dcpl_17, D=>base_c_sva_9n1s1(6), S=>rst);
   reg_base_c_sva_5 : FDSE port map ( Q=>base_c_sva(5), C=>clk, CE=>
      NOT_or_dcpl_17, D=>base_c_sva_9n1s1(5), S=>rst);
   reg_base_c_sva_4 : FDRE port map ( Q=>base_c_sva(4), C=>clk, CE=>
      NOT_or_dcpl_17, D=>base_c_sva_9n1s1(4), R=>rst);
   reg_base_c_sva_3 : FDRE port map ( Q=>base_c_sva(3), C=>clk, CE=>
      NOT_or_dcpl_17, D=>base_c_sva_9n1s1(3), R=>rst);
   reg_base_c_sva_2 : FDSE port map ( Q=>base_c_sva(2), C=>clk, CE=>
      NOT_or_dcpl_17, D=>base_c_sva_9n1s1(2), S=>rst);
   reg_base_c_sva_1 : FDRE port map ( Q=>base_c_sva(1), C=>clk, CE=>
      NOT_or_dcpl_17, D=>base_c_sva_9n1s1(1), R=>rst);
   reg_base_c_sva_0 : FDRE port map ( Q=>base_c_sva(0), C=>clk, CE=>
      NOT_or_dcpl_17, D=>base_c_sva_9n1s1(0), R=>rst);
   reg_base_r_sva_8 : FDRE port map ( Q=>base_r_sva(8), C=>clk, CE=>
      NOT_or_dcpl_17, D=>base_r_sva_10n1s1(8), R=>rst);
   reg_base_r_sva_7 : FDRE port map ( Q=>base_r_sva(7), C=>clk, CE=>
      NOT_or_dcpl_17, D=>base_r_sva_10n1s1(7), R=>rst);
   reg_base_r_sva_6 : FDSE port map ( Q=>base_r_sva(6), C=>clk, CE=>
      NOT_or_dcpl_17, D=>base_r_sva_10n1s1(6), S=>rst);
   reg_base_r_sva_5 : FDSE port map ( Q=>base_r_sva(5), C=>clk, CE=>
      NOT_or_dcpl_17, D=>base_r_sva_10n1s1(5), S=>rst);
   reg_base_r_sva_4 : FDRE port map ( Q=>base_r_sva(4), C=>clk, CE=>
      NOT_or_dcpl_17, D=>base_r_sva_10n1s1(4), R=>rst);
   reg_base_r_sva_3 : FDRE port map ( Q=>base_r_sva(3), C=>clk, CE=>
      NOT_or_dcpl_17, D=>base_r_sva_10n1s1(3), R=>rst);
   reg_base_r_sva_2 : FDSE port map ( Q=>base_r_sva(2), C=>clk, CE=>
      NOT_or_dcpl_17, D=>base_r_sva_10n1s1(2), S=>rst);
   reg_base_r_sva_1 : FDRE port map ( Q=>base_r_sva(1), C=>clk, CE=>
      NOT_or_dcpl_17, D=>base_r_sva_10n1s1(1), R=>rst);
   reg_base_r_sva_0 : FDRE port map ( Q=>base_r_sva(0), C=>clk, CE=>
      NOT_or_dcpl_17, D=>base_r_sva_10n1s1(0), R=>rst);
   reg_update_base_pos_inc_r_sg1_sva : FDRE port map ( Q=>
      update_base_pos_inc_r_sg1_sva, C=>clk, CE=>NOT_or_dcpl_17, D=>
      update_base_pos_inc_r_sg1_sva_dfm, R=>rst);
   reg_update_base_pos_inc_c_sg1_sva : FDRE port map ( Q=>
      update_base_pos_inc_c_sg1_sva, C=>clk, CE=>NOT_or_dcpl_17, D=>
      update_base_pos_inc_c_sg1_sva_dfm, R=>rst);
   reg_if_1_asn_itm : FDS port map ( Q=>if_1_asn_itm, C=>clk, D=>
      GND_EXMPLR30, S=>rst);
   ix49869z1328 : LUT4
      generic map (INIT => X"000E") 
       port map ( O=>NOT_or_dcpl_17, I0=>req_rsc_z, I1=>if_1_asn_itm, I2=>
      NOT_unequal_tmp_0n0s3, I3=>NOT_unequal_tmp_1_0n0s3);
   ix43627z45227 : LUT4
      generic map (INIT => X"AB88") 
       port map ( O=>update_base_pos_inc_c_sg1_sva_dfm, I0=>
      update_base_pos_inc_c_sg1_sva, I1=>NOT_a_0_dup_287, I2=>nx43627z1, I3
      =>nx43627z2);
   ix48872z45227 : LUT4
      generic map (INIT => X"AB88") 
       port map ( O=>update_base_pos_inc_r_sg1_sva_dfm, I0=>
      update_base_pos_inc_r_sg1_sva, I1=>NOT_a_0_dup_257, I2=>nx48872z2, I3
      =>nx48872z3);
   ix26657z1314 : LUT4
      generic map (INIT => X"FFFE") 
       port map ( O=>NOT_if_and_nl, I0=>acc_8_itm_sg1(5), I1=>
      acc_8_itm_sg1(4), I2=>nx26657z1, I3=>nx26657z2);
   ix26657z1541 : LUT3
      generic map (INIT => X"D8") 
       port map ( O=>mux_nl_1, I0=>nx26657z7, I1=>acc_itm_sg1(2), I2=>
      acc_8_itm_sg1(2));
   GND_EXMPLR32 : GND port map ( G=>GND_EXMPLR30);
   PWR_EXMPLR33 : VCC port map ( P=>PWR);
   ix26657z1324 : LUT4
      generic map (INIT => X"FFFD") 
       port map ( O=>NOT_equal_cse_sva_0n0s2, I0=>noise_cnt_sva(1), I1=>
      noise_cnt_sva(0), I2=>nx26657z3, I3=>nx26657z4);
   ix51271z1572 : LUT3
      generic map (INIT => X"FD") 
       port map ( O=>NOT_unequal_tmp_0n0s3, I0=>r_sva(0), I1=>nx51271z5, I2
      =>nx51271z6);
   ix51271z1568 : LUT3
      generic map (INIT => X"FD") 
       port map ( O=>NOT_unequal_tmp_1_0n0s3, I0=>c_sva(0), I1=>nx51271z2, 
      I2=>nx51271z3);
   ix26657z1328 : LUT2
      generic map (INIT => X"E") 
       port map ( O=>not_tmp_14_0n0s2, I0=>req_rsc_z, I1=>if_1_asn_itm);
   ix26657z33890 : LUT4
      generic map (INIT => X"7F3F") 
       port map ( O=>pixel_rsci_d_4n1s1(2), I0=>NOT_if_and_nl, I1=>
      NOT_equal_cse_sva_0n0s2, I2=>nx26657z5, I3=>nx26657z11);
   ix25660z33889 : LUT4
      generic map (INIT => X"7F3F") 
       port map ( O=>pixel_rsci_d_4n1s1(1), I0=>NOT_if_and_nl, I1=>
      NOT_equal_cse_sva_0n0s2, I2=>nx26657z5, I3=>nx25660z1);
   ix24663z17569 : LUT4
      generic map (INIT => X"3F7F") 
       port map ( O=>pixel_rsci_d_4n1s1(0), I0=>NOT_if_and_nl, I1=>
      NOT_equal_cse_sva_0n0s2, I2=>nx26657z5, I3=>nx24663z1);
   ix64531z1315 : LUT1
      generic map (INIT => X"1") 
       port map ( O=>not_pause_rsci_d, I0=>pause_rsc_z);
   ix36648z43964 : LUT4
      generic map (INIT => X"A69A") 
       port map ( O=>base_c_sva_9n1s1(8), I0=>base_c_sva(8), I1=>
      base_c_sva(7), I2=>update_base_pos_inc_c_sg1_sva_dfm, I3=>nx37645z1);
   ix37645z1464 : LUT3
      generic map (INIT => X"96") 
       port map ( O=>base_c_sva_9n1s1(7), I0=>nx37645z1, I1=>base_c_sva(7), 
      I2=>update_base_pos_inc_c_sg1_sva_dfm);
   ix38642z43964 : LUT4
      generic map (INIT => X"A69A") 
       port map ( O=>base_c_sva_9n1s1(6), I0=>base_c_sva(6), I1=>
      base_c_sva(5), I2=>update_base_pos_inc_c_sg1_sva_dfm, I3=>nx39639z1);
   ix39639z1464 : LUT3
      generic map (INIT => X"96") 
       port map ( O=>base_c_sva_9n1s1(5), I0=>nx39639z1, I1=>base_c_sva(5), 
      I2=>update_base_pos_inc_c_sg1_sva_dfm);
   ix40636z43964 : LUT4
      generic map (INIT => X"A69A") 
       port map ( O=>base_c_sva_9n1s1(4), I0=>base_c_sva(4), I1=>
      base_c_sva(3), I2=>update_base_pos_inc_c_sg1_sva_dfm, I3=>nx41633z1);
   ix41633z1464 : LUT3
      generic map (INIT => X"96") 
       port map ( O=>base_c_sva_9n1s1(3), I0=>nx41633z1, I1=>base_c_sva(3), 
      I2=>update_base_pos_inc_c_sg1_sva_dfm);
   ix42630z44684 : LUT4
      generic map (INIT => X"A96A") 
       port map ( O=>base_c_sva_9n1s1(2), I0=>base_c_sva(2), I1=>
      base_c_sva(1), I2=>base_c_sva(0), I3=>
      update_base_pos_inc_c_sg1_sva_dfm);
   ix43627z1464 : LUT3
      generic map (INIT => X"96") 
       port map ( O=>base_c_sva_9n1s1(1), I0=>base_c_sva(0), I1=>
      base_c_sva(1), I2=>update_base_pos_inc_c_sg1_sva_dfm);
   ix44624z1315 : LUT1
      generic map (INIT => X"1") 
       port map ( O=>base_c_sva_9n1s1(0), I0=>base_c_sva(0));
   ix41893z43964 : LUT4
      generic map (INIT => X"A69A") 
       port map ( O=>base_r_sva_10n1s1(8), I0=>base_r_sva(8), I1=>
      base_r_sva(7), I2=>update_base_pos_inc_r_sg1_sva_dfm, I3=>nx42890z1);
   ix42890z1464 : LUT3
      generic map (INIT => X"96") 
       port map ( O=>base_r_sva_10n1s1(7), I0=>nx42890z1, I1=>base_r_sva(7), 
      I2=>update_base_pos_inc_r_sg1_sva_dfm);
   ix43887z43964 : LUT4
      generic map (INIT => X"A69A") 
       port map ( O=>base_r_sva_10n1s1(6), I0=>base_r_sva(6), I1=>
      base_r_sva(5), I2=>update_base_pos_inc_r_sg1_sva_dfm, I3=>nx44884z1);
   ix44884z1464 : LUT3
      generic map (INIT => X"96") 
       port map ( O=>base_r_sva_10n1s1(5), I0=>nx44884z1, I1=>base_r_sva(5), 
      I2=>update_base_pos_inc_r_sg1_sva_dfm);
   ix45881z43964 : LUT4
      generic map (INIT => X"A69A") 
       port map ( O=>base_r_sva_10n1s1(4), I0=>base_r_sva(4), I1=>
      base_r_sva(3), I2=>update_base_pos_inc_r_sg1_sva_dfm, I3=>nx46878z1);
   ix46878z1464 : LUT3
      generic map (INIT => X"96") 
       port map ( O=>base_r_sva_10n1s1(3), I0=>nx46878z1, I1=>base_r_sva(3), 
      I2=>update_base_pos_inc_r_sg1_sva_dfm);
   ix47875z44684 : LUT4
      generic map (INIT => X"A96A") 
       port map ( O=>base_r_sva_10n1s1(2), I0=>base_r_sva(2), I1=>
      base_r_sva(1), I2=>base_r_sva(0), I3=>
      update_base_pos_inc_r_sg1_sva_dfm);
   ix48872z1464 : LUT3
      generic map (INIT => X"96") 
       port map ( O=>base_r_sva_10n1s1(1), I0=>base_r_sva(0), I1=>
      base_r_sva(1), I2=>update_base_pos_inc_r_sg1_sva_dfm);
   ix49869z1316 : LUT1
      generic map (INIT => X"1") 
       port map ( O=>base_r_sva_10n1s1(0), I0=>base_r_sva(0));
   ix51271z45088 : LUT4
      generic map (INIT => X"AAFE") 
       port map ( O=>nx51271z1, I0=>rst, I1=>req_rsc_z, I2=>if_1_asn_itm, I3
      =>NOT_unequal_tmp_1_0n0s3);
   ix9490z1568 : LUT3
      generic map (INIT => X"FE") 
       port map ( O=>nx9490z1, I0=>rst, I1=>req_rsc_z, I2=>if_1_asn_itm);
   ix14759z1323 : LUT2
      generic map (INIT => X"9") 
       port map ( O=>nx14759z1, I0=>c_sva(0), I1=>base_c_sva(0));
   ix14760z1323 : LUT2
      generic map (INIT => X"9") 
       port map ( O=>nx14760z2, I0=>c_sva(1), I1=>base_c_sva(1));
   ix14761z1323 : LUT2
      generic map (INIT => X"9") 
       port map ( O=>nx14761z2, I0=>c_sva(2), I1=>base_c_sva(2));
   ix14762z1323 : LUT2
      generic map (INIT => X"9") 
       port map ( O=>nx14762z2, I0=>c_sva(3), I1=>base_c_sva(3));
   ix14763z1323 : LUT2
      generic map (INIT => X"9") 
       port map ( O=>nx14763z2, I0=>c_sva(4), I1=>base_c_sva(4));
   ix14764z1323 : LUT2
      generic map (INIT => X"9") 
       port map ( O=>nx14764z2, I0=>c_sva(5), I1=>base_c_sva(5));
   ix14765z1323 : LUT2
      generic map (INIT => X"9") 
       port map ( O=>nx14765z2, I0=>c_sva(6), I1=>base_c_sva(6));
   ix14766z1323 : LUT2
      generic map (INIT => X"9") 
       port map ( O=>nx14766z2, I0=>c_sva(7), I1=>base_c_sva(7));
   ix45394z1323 : LUT2
      generic map (INIT => X"9") 
       port map ( O=>nx45394z2, I0=>c_sva(8), I1=>base_c_sva(8));
   ix55495z1323 : LUT2
      generic map (INIT => X"9") 
       port map ( O=>nx55495z1, I0=>r_sva(0), I1=>base_r_sva(0));
   ix55496z1323 : LUT2
      generic map (INIT => X"9") 
       port map ( O=>nx55496z2, I0=>r_sva(1), I1=>base_r_sva(1));
   ix55497z1323 : LUT2
      generic map (INIT => X"9") 
       port map ( O=>nx55497z2, I0=>r_sva(2), I1=>base_r_sva(2));
   ix55498z1323 : LUT2
      generic map (INIT => X"9") 
       port map ( O=>nx55498z2, I0=>r_sva(3), I1=>base_r_sva(3));
   ix55499z1323 : LUT2
      generic map (INIT => X"9") 
       port map ( O=>nx55499z2, I0=>r_sva(4), I1=>base_r_sva(4));
   ix4661z1323 : LUT2
      generic map (INIT => X"9") 
       port map ( O=>nx4661z2, I0=>r_sva(5), I1=>base_r_sva(5));
   ix55501z1323 : LUT2
      generic map (INIT => X"9") 
       port map ( O=>nx55501z2, I0=>r_sva(6), I1=>base_r_sva(6));
   ix55502z1323 : LUT2
      generic map (INIT => X"9") 
       port map ( O=>nx55502z2, I0=>r_sva(7), I1=>base_r_sva(7));
   ix4658z1323 : LUT2
      generic map (INIT => X"9") 
       port map ( O=>nx4658z2, I0=>r_sva(8), I1=>base_r_sva(8));
   ix46882z1328 : LUT2
      generic map (INIT => X"E") 
       port map ( O=>nx46882z1, I0=>rst, I1=>NOT_or_dcpl_17);
   ix21374z1320 : LUT2
      generic map (INIT => X"6") 
       port map ( O=>inc_d_1_dup_173, I0=>b_1, I1=>b_0);
   ix1378z1420 : LUT3
      generic map (INIT => X"6A") 
       port map ( O=>inc_d_2_dup_174, I0=>b_2, I1=>b_1, I2=>b_0);
   ix41406z28620 : LUT4
      generic map (INIT => X"6AAA") 
       port map ( O=>inc_d_3_dup_175, I0=>frame_cnt_sva_3, I1=>b_2, I2=>b_1, 
      I3=>b_0);
   ix46882z44941 : LUT4
      generic map (INIT => X"AA6A") 
       port map ( O=>inc_d_4_dup_176, I0=>frame_cnt_sva_4, I1=>
      frame_cnt_sva_3, I2=>b_2, I3=>nx46882z2);
   ix61438z44940 : LUT4
      generic map (INIT => X"AA6A") 
       port map ( O=>inc_d_5_dup_177, I0=>frame_cnt_sva_5, I1=>
      frame_cnt_sva_4, I2=>frame_cnt_sva_3, I3=>nx61438z1);
   ix21410z1315 : LUT1
      generic map (INIT => X"1") 
       port map ( O=>inc_d_0_dup_178, I0=>b_0);
   ix46882z1323 : LUT2
      generic map (INIT => X"7") 
       port map ( O=>nx46882z2, I0=>b_1, I1=>b_0);
   ix32003z1323 : LUT2
      generic map (INIT => X"9") 
       port map ( O=>nx32003z1, I0=>acc_itm_sg1(0), I1=>acc_8_itm_sg1(0));
   ix32004z1323 : LUT2
      generic map (INIT => X"9") 
       port map ( O=>nx32004z2, I0=>acc_itm_sg1(1), I1=>acc_8_itm_sg1(1));
   ix32005z1323 : LUT2
      generic map (INIT => X"9") 
       port map ( O=>nx32005z2, I0=>acc_itm_sg1(2), I1=>acc_8_itm_sg1(2));
   ix32006z1323 : LUT2
      generic map (INIT => X"9") 
       port map ( O=>nx32006z2, I0=>acc_itm_sg1(3), I1=>acc_8_itm_sg1(3));
   ix32007z1323 : LUT2
      generic map (INIT => X"9") 
       port map ( O=>nx32007z2, I0=>acc_itm_sg1(4), I1=>acc_8_itm_sg1(4));
   ix32008z1323 : LUT2
      generic map (INIT => X"9") 
       port map ( O=>nx32008z2, I0=>acc_itm_sg1(5), I1=>acc_8_itm_sg1(5));
   ix32009z1323 : LUT2
      generic map (INIT => X"9") 
       port map ( O=>nx32009z2, I0=>acc_itm_sg1(6), I1=>acc_8_itm_sg1(6));
   ix32010z1323 : LUT2
      generic map (INIT => X"9") 
       port map ( O=>nx32010z2, I0=>acc_itm_sg1(7), I1=>acc_8_itm_sg1(7));
   ix32011z1323 : LUT2
      generic map (INIT => X"9") 
       port map ( O=>nx32011z2, I0=>acc_itm_sg1(8), I1=>acc_8_itm_sg1(8));
   ix26657z58147 : LUT4
      generic map (INIT => X"DDF5") 
       port map ( O=>nx26657z8, I0=>frame_cnt_sva_3, I1=>acc_itm_sg1(1), I2
      =>acc_8_itm_sg1(1), I3=>nx26657z7);
   ix26657z1367 : LUT3
      generic map (INIT => X"2B") 
       port map ( O=>nx26657z6, I0=>frame_cnt_sva_4, I1=>mux_nl_1, I2=>
      nx26657z8);
   ix26657z49375 : LUT4
      generic map (INIT => X"BBAF") 
       port map ( O=>nx26657z10, I0=>frame_cnt_sva_5, I1=>acc_itm_sg1(3), I2
      =>acc_8_itm_sg1(3), I3=>nx26657z7);
   ix41633z931 : LUT4
      generic map (INIT => X"FE80") 
       port map ( O=>nx41633z1, I0=>base_c_sva(2), I1=>base_c_sva(1), I2=>
      base_c_sva(0), I3=>update_base_pos_inc_c_sg1_sva_dfm);
   ix39639z65027 : LUT4
      generic map (INIT => X"F8E0") 
       port map ( O=>nx39639z1, I0=>base_c_sva(4), I1=>base_c_sva(3), I2=>
      update_base_pos_inc_c_sg1_sva_dfm, I3=>nx41633z1);
   ix37645z65027 : LUT4
      generic map (INIT => X"F8E0") 
       port map ( O=>nx37645z1, I0=>base_c_sva(6), I1=>base_c_sva(5), I2=>
      update_base_pos_inc_c_sg1_sva_dfm, I3=>nx39639z1);
   ix46878z931 : LUT4
      generic map (INIT => X"FE80") 
       port map ( O=>nx46878z1, I0=>base_r_sva(2), I1=>base_r_sva(1), I2=>
      base_r_sva(0), I3=>update_base_pos_inc_r_sg1_sva);
   ix44884z65027 : LUT4
      generic map (INIT => X"F8E0") 
       port map ( O=>nx44884z1, I0=>base_r_sva(4), I1=>base_r_sva(3), I2=>
      update_base_pos_inc_r_sg1_sva_dfm, I3=>nx46878z1);
   ix42890z65027 : LUT4
      generic map (INIT => X"F8E0") 
       port map ( O=>nx42890z1, I0=>base_r_sva(6), I1=>base_r_sva(5), I2=>
      update_base_pos_inc_r_sg1_sva_dfm, I3=>nx44884z1);
   ix43627z1332 : LUT2
      generic map (INIT => X"E") 
       port map ( O=>NOT_a_3, I0=>base_c_sva(7), I1=>base_c_sva(0));
   ix48872z1314 : LUT4
      generic map (INIT => X"FFFE") 
       port map ( O=>NOT_a_0_dup_257, I0=>base_r_sva(2), I1=>base_r_sva(1), 
      I2=>base_r_sva(0), I3=>nx48872z1);
   ix43627z1314 : LUT4
      generic map (INIT => X"FFFE") 
       port map ( O=>NOT_a_0_dup_287, I0=>base_c_sva(6), I1=>base_c_sva(3), 
      I2=>base_c_sva(2), I3=>base_c_sva(1));
   ix51271z1494 : LUT3
      generic map (INIT => X"B0") 
       port map ( O=>nx51271z4, I0=>rst, I1=>NOT_unequal_tmp_0n0s3, I2=>
      nx51271z1);
   ix51271z803 : LUT4
      generic map (INIT => X"FDFF") 
       port map ( O=>nx51271z2, I0=>c_sva(8), I1=>c_sva(7), I2=>c_sva(6), I3
      =>c_sva(5));
   ix51271z34084 : LUT4
      generic map (INIT => X"7FFF") 
       port map ( O=>nx51271z3, I0=>c_sva(4), I1=>c_sva(3), I2=>c_sva(2), I3
      =>c_sva(1));
   ix51271z50471 : LUT4
      generic map (INIT => X"BFFF") 
       port map ( O=>nx51271z5, I0=>r_sva(8), I1=>r_sva(7), I2=>r_sva(6), I3
      =>r_sva(5));
   ix51271z50472 : LUT4
      generic map (INIT => X"BFFF") 
       port map ( O=>nx51271z6, I0=>r_sva(4), I1=>r_sva(3), I2=>r_sva(2), I3
      =>r_sva(1));
   ix26657z34088 : LUT4
      generic map (INIT => X"7FFF") 
       port map ( O=>nx26657z3, I0=>noise_cnt_sva(9), I1=>noise_cnt_sva(8), 
      I2=>noise_cnt_sva(7), I3=>noise_cnt_sva(6));
   ix26657z1305 : LUT4
      generic map (INIT => X"FFEF") 
       port map ( O=>nx26657z4, I0=>noise_cnt_sva(5), I1=>noise_cnt_sva(4), 
      I2=>noise_cnt_sva(3), I3=>noise_cnt_sva(2));
   ix26657z1315 : LUT4
      generic map (INIT => X"FFFE") 
       port map ( O=>nx26657z1, I0=>acc_itm_sg1(8), I1=>acc_itm_sg1(7), I2=>
      acc_itm_sg1(6), I3=>acc_itm_sg1(5));
   ix26657z1316 : LUT4
      generic map (INIT => X"FFFE") 
       port map ( O=>nx26657z2, I0=>acc_itm_sg1(4), I1=>acc_8_itm_sg1(8), I2
      =>acc_8_itm_sg1(7), I3=>acc_8_itm_sg1(6));
   ix26657z49189 : LUT4
      generic map (INIT => X"BAFA") 
       port map ( O=>nx26657z5, I0=>NOT_if_and_nl, I1=>nx26657z6, I2=>
      nx26657z9, I3=>nx26657z10);
   ix43627z1188 : LUT4
      generic map (INIT => X"FF7F") 
       port map ( O=>nx43627z1, I0=>base_c_sva(8), I1=>base_c_sva(5), I2=>
      base_c_sva(4), I3=>NOT_a_3);
   ix43627z1317 : LUT4
      generic map (INIT => X"FFFE") 
       port map ( O=>nx43627z2, I0=>base_c_sva(8), I1=>base_c_sva(5), I2=>
      base_c_sva(4), I3=>NOT_a_3);
   ix48872z50469 : LUT4
      generic map (INIT => X"BFFF") 
       port map ( O=>nx48872z2, I0=>base_r_sva(8), I1=>base_r_sva(7), I2=>
      base_r_sva(6), I3=>base_r_sva(5));
   ix48872z1317 : LUT4
      generic map (INIT => X"FFFE") 
       port map ( O=>nx48872z3, I0=>base_r_sva(8), I1=>base_r_sva(7), I2=>
      base_r_sva(6), I3=>base_r_sva(5));
   ix26657z58148 : LUT4
      generic map (INIT => X"DDF5") 
       port map ( O=>nx26657z9, I0=>frame_cnt_sva_5, I1=>acc_itm_sg1(3), I2
      =>acc_8_itm_sg1(3), I3=>nx26657z7);
   ix26657z55644 : LUT4
      generic map (INIT => X"D42B") 
       port map ( O=>nx26657z11, I0=>frame_cnt_sva_4, I1=>mux_nl_1, I2=>
      nx26657z8, I3=>nx26657z10);
   ix25660z1465 : LUT3
      generic map (INIT => X"96") 
       port map ( O=>nx25660z1, I0=>nx26657z8, I1=>frame_cnt_sva_4, I2=>
      mux_nl_1);
   ix48872z1331 : LUT2
      generic map (INIT => X"E") 
       port map ( O=>nx48872z1, I0=>base_r_sva(4), I1=>base_r_sva(3));
   ix24663z27517 : LUT4
      generic map (INIT => X"665A") 
       port map ( O=>nx24663z1, I0=>frame_cnt_sva_3, I1=>acc_itm_sg1(1), I2
      =>acc_8_itm_sg1(1), I3=>nx26657z7);
   ix61438z1442 : LUT3
      generic map (INIT => X"7F") 
       port map ( O=>nx61438z1, I0=>b_2, I1=>b_1, I2=>b_0);
   ix19413z1142 : LUT4
      generic map (INIT => X"FF54") 
       port map ( O=>nx19413z1, I0=>NOT_equal_cse_sva_0n0s2, I1=>
      if_1_asn_itm, I2=>req_rsc_z, I3=>rst);
end v1 ;

library IEEE;
use IEEE.STD_LOGIC_1164.all;

entity genpix is 
   port (
      clk : IN std_logic ;
      rst : IN std_logic ;
      pause_rsc_z : IN std_logic ;
      req_rsc_z : IN std_logic ;
      pixel_rsc_z : OUT std_logic_vector (2 DOWNTO 0) ;
      pixel_vld_rsc_z : OUT std_logic) ;
end genpix ;

architecture v1 of genpix is 
   component genpix_core
      port (
         clk : IN std_logic ;
         rst : IN std_logic ;
         pause_rsc_z : IN std_logic ;
         req_rsc_z : IN std_logic ;
         pixel_rsc_z : OUT std_logic_vector (2 DOWNTO 0) ;
         pixel_vld_rsc_z : OUT std_logic) ;
   end component ;
begin
   genpix_core_inst : genpix_core port map ( clk=>clk, rst=>rst, pause_rsc_z
      =>pause_rsc_z, req_rsc_z=>req_rsc_z, pixel_rsc_z(2)=>pixel_rsc_z(2), 
      pixel_rsc_z(1)=>pixel_rsc_z(1), pixel_rsc_z(0)=>pixel_rsc_z(0), 
      pixel_vld_rsc_z=>pixel_vld_rsc_z);
end v1 ;

